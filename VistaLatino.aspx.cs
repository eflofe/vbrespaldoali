﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VistaLatino : System.Web.UI.Page
{
    Log logC = new Log();
    DatosJson datosJs = new DatosJson();
    string usuario = "";
    string passp = "";
    string movimiento = "COTIZACION";

    protected void Page_Load(object sender, EventArgs e)
    {
        FillDrop();
        FillEdad();
    }
    /// <summary>
    /// Método que crea el json a partir de los datos agregados
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void BtnCrearJson(object sender, EventArgs e)
    {
        //Vehiculo
        Vehiculo vr = new Vehiculo();
        Direccion dR = new Direccion();
        string clave_Marca = vr.Marca = DropMarca.SelectedValue;
        string año = vr.Año = DropModelo.SelectedValue;
        string clave_SubMarca = vr.SubMarca = DropSubMarca.SelectedValue;
        vr.Servicio = "PARTICULAR";
        vr.Uso = "PARTICULAR";
        vr.Modelo = DropModelo.SelectedValue;
        string desc = vr.Descripcion = DropDescripcion.SelectedValue;
        vr.Modelo = DropModelo.SelectedValue;
        string descripcionCompleta = desc;
        string claveDesc = logC.GetClabeDescripcionMysqlLatino(clave_Marca, clave_SubMarca, año, descripcionCompleta);
        List<Clave> list = JsonConvert.DeserializeObject<List<Clave>>(claveDesc);
        foreach (var item in list)
        {
            datosJs.Clave = item.Clave_descripcion;
        }
        //cliente
        Cliente cl = new Cliente();
        cl.FechaNacimiento = "01/01/1980";
        cl.Genero = DropGenero.SelectedValue;
        ////cl.direccion = dR;
        Direccion dr = new Direccion();
        dr.CodPostal = TxtCp.Text;
        cl.direccion = dr;
        cl.Telefono = TxtTelefono.Text;
        cl.Edad = DropEdad.SelectedValue;
        //Cotizacion
        Cotizacion cT = new Cotizacion();
        Emision eM = new Emision();
        Pago pG = new Pago();
        pG.Carrier = "0";
        ////LLenado de datos
        //Datos afuera 
        datosJs.Paquete = "AMPLIA";
        datosJs.Aseguradora = "LATINO";
        datosJs.Cp = TxtCp.Text;
        datosJs.Descripcion = DropDescripcion.SelectedValue;
        datosJs.Descuento = DropDownDescuento.SelectedValue;
        datosJs.Edad = DropEdad.SelectedValue;
        datosJs.FechaNacimiento = "01/01/1980";
        datosJs.Genero = DropGenero.SelectedValue;
        datosJs.Marca = DropMarca.SelectedValue;
        datosJs.Modelo = DropModelo.SelectedValue;
        datosJs.Movimiento = "cotizacion";
        datosJs.Servicio = "PARTICULAR";

        string response = JsonConvert.SerializeObject(datosJs);
        lblJsno.Text = response;
    }

    /// <summary>
    /// Método que rellena las marcas de la base de datos de la latino
    /// </summary>
    public void FillDrop()
    {
        string jsonMarcas = logC.GetMarcasLatiMysql();
        List<Marcas> list = JsonConvert.DeserializeObject<List<Marcas>>(jsonMarcas);

        foreach (var item in list)
        {
            DropMarca.Items.Add(item.Clave_marca);
        }
    }

    /// <summary>
    /// Método que regresa la aseguradora
    /// </summary>
    public void FillAseguradoras()
    {

    }

    /// <summary>
    /// Método que rellena la el drop submarcas
    /// </summary>
    public void FillSubMarcas()
    {
        string jsonSubMarcas = logC.GetSubMarcasLatinoMysql(lblMarcaSelected.Text);
        List<SubMarcas> listSub = JsonConvert.DeserializeObject<List<SubMarcas>>(jsonSubMarcas);
        foreach (var item in listSub)
        {
            DropSubMarca.Items.Add(item.Clave_SubMarca);
        }

    }

    /// <summary>
    /// Método que rellena los modelos
    /// </summary>
    public void FillModelos()
    {
        string jsonModelos = logC.GetModelosLatiMysql(lblMarcaSelected.Text, lblSubMarca.Text);
        List<Modelos> listMo = JsonConvert.DeserializeObject<List<Modelos>>(jsonModelos);
        foreach (var item in listMo)
        {
            DropModelo.Items.Add(item.Año.ToString());
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public void FillDescripcion()
    {
        string jsDescripcion = logC.GetDescripcionesMysqlatino(lblMarcaSelected.Text, lblSubMarca.Text, lblModelo.Text);
        List<DescripcionAuto> listDesc = JsonConvert.DeserializeObject<List<DescripcionAuto>>(jsDescripcion);
        foreach (var item in listDesc)
        {
            DropDescripcion.Items.Add(item.Descripcion.ToString());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void FillEdad()
    {
        int edad = 0;
        for (edad = 18; edad < 100; edad++)
        {
            DropEdad.Items.Add(edad.ToString());
        }
    }


    /// <summary>
    /// Método que busca las submarcas dependiedo de la marca que seleccionas
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="event"></param>
    protected void DropMarca_SelectedIndexChanged(object sender, EventArgs e)
    {
        string marca = DropMarca.SelectedItem.ToString();
        lblMarcaSelected.Text = marca;
        DropSubMarca.Items.Clear();
        DropSubMarca.Items.Add("Seleccione una SubMarca");
        FillSubMarcas();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DropSubMarca_SelectedIndexChanged(object sender, EventArgs e)
    {
        string subMarca = DropSubMarca.SelectedItem.ToString();
        lblSubMarca.Text = subMarca;
        DropModelo.Items.Clear();
        DropModelo.Items.Add("Seleccione una Modelo");
        FillModelos();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DropModelo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string modelo = DropModelo.SelectedItem.ToString();
        lblModelo.Text = modelo;
        DropDescripcion.Items.Clear();
        DropDescripcion.Items.Add("Seleccione una descripcion");
        FillDescripcion();

    }
    protected void DropDescripcion_SelectedIndexChanged(object sender, EventArgs e)
    {
        string descripcion = DropDescripcion.SelectedItem.ToString();
        Lbldescripcion.Text = descripcion;
    }

    /// <summary>
    /// Metodo drop aseguradora
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DropAseguradora_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void DropEdad_SelectedIndexChanged(object sender, EventArgs e)
    {
        string edad = DropEdad.SelectedItem.ToString();
    }

    protected void Dropdescuento_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void DropGenero_SelectedIndexChanged(object sender, EventArgs e)
    {

    }


    /// <summary>
    /// Métodos get and setter para el ambiente de pruebas
    /// </summary>
    public partial class Marcas
    {

        public string Clave_marca { get; set; }
    }


    public partial class SubMarcas
    {
        public string Clave_SubMarca { get; set; }
    }
    public partial class Modelos
    {
        public long Año { get; set; }
    }
    public partial class DescripcionAuto
    {
        public string Descripcion { get; set; }
    }

    public partial class Clave
    {
        public string Clave_descripcion { get; set; }
    }
    /// <summary>
    /// Método donde se almacenan los datos de el coche
    /// </summary>
    public partial class Aseguradoras
    {
        [JsonProperty("Aseguradora")]
        public string Aseguradora { get; set; }
    }


    public partial class DatosJson
    {
        public string Clave { get; set; }
        public string Cp { get; set; }
        public string Descripcion { get; set; }

        public string Descuento { get; set; }
        public string Edad { get; set; }

        public string FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Movimiento { get; set; }

        public string Servicio { get; set; }

        //public Cliente cliente { get; set; }
        //public Cotizacion cotizacion { get; set; }
        //public Emision emision { get; set; }
        //public Vehiculo vehiculo { get; set; }
        //public Pago pago { get; set; }
        public string Paquete { get; set; }
        public string Aseguradora { get; set; }
    }

    public partial class Cliente
    {
        public string FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public Direccion direccion { get; set; }
        public string Telefono { get; set; }
        public string Edad { get; set; }

    }

    public partial class Direccion
    {
        public string CodPostal { get; set; }

    }

    public partial class Cotizacion
    {

    }
    public partial class Emision
    {

    }
    public partial class Vehiculo
    {
        public string Descripcion { get; set; }
        public string Uso { get; set; }
        public string Marca { get; set; }
        public string Clave { get; set; }
        public string Servicio { get; set; }
        public string Año { get; set; }
        public string SubMarca { get; set; }
        public string Modelo { get; set; }

    }

    public partial class Pago
    {
        public string Carrier { get; set; }

    }
}