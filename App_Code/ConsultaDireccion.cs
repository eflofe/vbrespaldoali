﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Descripción breve de ConsultaDireccion
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class ConsultaDireccion : System.Web.Services.WebService
{
    AbaHogWs.CatalogosClient client = new AbaHogWs.CatalogosClient();
    AbaHogWs.Token token = new AbaHogWs.Token();
    Datos datos = new Datos();
    string user = "WSTRIGARANTE";
    string pass = "VIRTUAL1$";

    [WebMethod]
    public string HelloWorld()
    {
        return "Hola a todos";
    }

    [WebMethod]
    public string getMunicipio(int idEdo)
    {
        try
        {

            AbaHogWs.RequestGetMunicipios requestGetMunicipios = new AbaHogWs.RequestGetMunicipios();
            AbaHogWs.InputGetMunicipios getMunicipios = new AbaHogWs.InputGetMunicipios();
            token.Usuario = this.user;
            token.Password = this.pass;
            getMunicipios.PaisId = 1;
            getMunicipios.EstadoId = idEdo;
            requestGetMunicipios.InputGetMunicipios = getMunicipios;
            requestGetMunicipios.Token = token;
            AbaHogWs.Generico[] genericos = client.GetMunicipios(requestGetMunicipios.Token, requestGetMunicipios.InputGetMunicipios).Municipios;
            List<Datos> list = new List<Datos>();

            foreach (AbaHogWs.Generico muni in genericos)
            {
                //datos.Id = muni.Id;
                //datos.Desc = muni.Desc;
                //list.Add(datos);
                Datos item = new Datos();
                item.Id = muni.Id;
                item.Desc = muni.Desc;
                list.Add(item);
            }
            string JsonMunicipios = JsonConvert.SerializeObject(list);
            return JsonMunicipios;
        }
        catch (Exception ex)
        {
            return "ex" + ex;
        }
    }

    [WebMethod]
    public string getPoblaciones(int idMunicipio)
    {

        AbaHogWs.RequestGetPoblaciones getPoblaciones = new AbaHogWs.RequestGetPoblaciones();
        AbaHogWs.InputGetPoblaciones inputPoblaciones = new AbaHogWs.InputGetPoblaciones();
        token.Usuario = this.user;
        token.Password = this.pass;
        inputPoblaciones.MunicipioId = idMunicipio;
        getPoblaciones.InputGetPoblaciones = inputPoblaciones;
        getPoblaciones.Token = token;
        AbaHogWs.Generico[] genericos = client.GetPoblaciones(getPoblaciones.Token, getPoblaciones.InputGetPoblaciones).Poblaciones;
        List<Datos> list = new List<Datos>();
        foreach (AbaHogWs.Generico poblaciones in genericos)
        {
            Datos item = new Datos();
            item.Id = poblaciones.Id;
            item.Desc = poblaciones.Desc;
            list.Add(item);
        }
        string JsonPoblaciones = JsonConvert.SerializeObject(list);
        return JsonPoblaciones;
    }
    [WebMethod]
    public string getColonias(int EstadoId, int municipioId, int poblacionId)
    {
        try
        {
            AbaHogWs.RequestGetColonias requestColonias = new AbaHogWs.RequestGetColonias();
            AbaHogWs.InputGetColonias inputColonias = new AbaHogWs.InputGetColonias();
            token.Usuario = this.user;
            token.Password = this.pass;
            inputColonias.PaisId = 1;
            inputColonias.EstadoId = EstadoId;
            inputColonias.MunicipioId = municipioId;
            inputColonias.PoblacionId = poblacionId;
            inputColonias.ColoniaId = 0;
            inputColonias.PalabraClave = "";
            requestColonias.InputGetColonias = inputColonias;
            requestColonias.Token = token;
            AbaHogWs.Colonia[] colonia = client.GetColonias(requestColonias.Token, requestColonias.InputGetColonias).Colonias;

            List<Colonias> list = new List<Colonias>();
            foreach (AbaHogWs.Colonia col in colonia)
            {
                Colonias colonias = new Colonias();
                colonias.ColoniaId = col.ColoniaId;
                colonias.ColoniaDsc = col.ColoniaDsc;
                colonias.IvaPct = col.IvaPct;
                list.Add(colonias);
            }
            string JsonColonias = JsonConvert.SerializeObject(list);
            return JsonColonias;
        }
        catch (Exception ex)
        {
            return "error: " + ex;
        }
    }


    public string GetCP(int idEstado, int idMunicipio, int idPoblacion)
    {
        try
        {
            AbaHogWs.RequestGetColonias requestCP = new AbaHogWs.RequestGetColonias();
            AbaHogWs.InputGetColonias inputCol = new AbaHogWs.InputGetColonias();
            token.Usuario = this.user;
            token.Password = this.pass;
            inputCol.PaisId = 1;
            inputCol.EstadoId = idEstado;
            inputCol.MunicipioId = idMunicipio;
            inputCol.PoblacionId = idPoblacion;
            inputCol.ColoniaId = 0;
            inputCol.PalabraClave = "";
            requestCP.InputGetColonias = inputCol;
            requestCP.Token = token;
            AbaHogWs.Colonia[] colonia = client.GetColonias(requestCP.Token, requestCP.InputGetColonias).Colonias;
            var codpos = "";
            // List<CP> list = new List<CP>();
            foreach (AbaHogWs.Colonia col in colonia)
            {
                CP codP = new CP();
                codP.cp = col.CodigoPostal;
                codpos = col.CodigoPostal;
                //   list.Add(codP);
            }


            return codpos;
        }
        catch (Exception ex)
        {
            return "error: " + ex;
        }
    }
    public class Datos
    {
        public long Id { get; set; }
        public string Desc { get; set; }

    }
    public class Colonias
    {
        public long ColoniaId { get; set; }
        public string ColoniaDsc { get; set; }
        public double IvaPct { get; set; }
    }
    public class CP
    {
        public string cp { get; set; }
    }
}


