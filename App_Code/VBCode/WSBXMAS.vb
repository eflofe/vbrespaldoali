﻿Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Net
Imports System.IO
Imports System.Xml.Serialization
Imports Newtonsoft.Json
Imports MySql.Data.MySqlClient
Imports System.Data

Public Class WSBXMAS
    Public Shared Function BXMASCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)

        Dim ObjBXMAS As New BXMASService.transaccionalAutosWs
        Dim ObjBXMASCatalogo As New BXMASServiceCatalogo.catalogosAutosWs
        Dim serializer As New JavaScriptSerializer
        Dim respuesta = New BXMASService.cotizacionInformacionDto
        Dim respuestaImportes = New BXMASService.datosImportesDto
        Dim respuestaCoberturas = New BXMASService.coberturaCotizacionDto
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        Dim lg As Log = New Log()
        Try
            Dim versionCarga = 0
            Dim idSubMarca = 0
            Dim DesSubmarca = ""
            Dim LineaNegocio As Int16 = 0
            Dim IdMarca
            Try
                IdMarca = GetIdMarca(ObjData.Vehiculo.Marca)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta marca del vehiculo")
            End Try
            Dim tipoAuto
            Try
                tipoAuto = GetSubMarca(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta submarca del vehiculo")
            End Try
            Dim datosVehi
            Try
                datosVehi = GetDatosVehiculo(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al intentar consultar los datos del vehiculo")
            End Try
            Dim DatDireccion
            Dim idEdo
            Try
                DatDireccion = ConsultaMunicipiosANA(ObjData.Cliente.Direccion.CodPostal)
                idEdo = GetEstado(DatDireccion(0))
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta estado/municipio (CP)")
            End Try
            Dim IdPaquete = 882
            Dim idFormaPago = "1"
            versionCarga = CInt(datosVehi(2))
            idSubMarca = CInt(datosVehi(1))
            DesSubmarca = datosVehi(0)
            If ObjData.Paquete = "AMPLIA" Then
                IdPaquete = 876
            ElseIf ObjData.Paquete = "LIMITADA" Then
                IdPaquete = 878
            ElseIf ObjData.Paquete = "BASICA" Then
                IdPaquete = 877
            End If

            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                idFormaPago = "1"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                idFormaPago = "4"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                idFormaPago = "2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                idFormaPago = "3"
            End If

            If tipoAuto = "AUT" Then
                tipoAuto = "AUTOS"
                LineaNegocio = 1
            Else
                tipoAuto = "PICK"
                LineaNegocio = 1235
            End If
            Dim specified = 1
            Dim IdEmpresa = 8
            Dim IdMoneda = 484
            Dim IdProducto = 1445
            Dim IdVersionProducto = 20
            Dim claveUsuario = "AGTESYFT"

            respuesta = ObjBXMAS.guardarCotizacion(8, specified, "AGTESYFT", 222, specified, IdProducto, specified, IdVersionProducto, specified, 484, specified, LineaNegocio, specified, IdMarca, specified, DesSubmarca, CInt(ObjData.Vehiculo.Modelo), specified, ObjData.Vehiculo.Clave, "PB", idEdo, "PR", IdPaquete, specified, 1, specified, versionCarga, specified, tipoAuto, "ARMANDO BARRIENTOS", "0", "0", idSubMarca, specified)

            If Not (ObjData.Descuento Is Nothing) Then
                ObjBXMAS.guardarCondicionEspecialDescuentoRecargoGlobal(respuesta.idCotizacion, specified, ObjData.Descuento, specified, "descuento", 8, specified, "AGTESYFT")
            End If
            ObjData.Cotizacion.IDCotizacion = respuesta.idCotizacion
            ObjData.Cotizacion.CotID = respuesta.numCotizacion
            respuestaCoberturas = ObjBXMAS.recuperarCoberturasCotizacion(IdEmpresa, specified, IdProducto, specified, IdMoneda, specified, LineaNegocio, specified, respuesta.idCotizacion, specified, 0, specified, respuesta.numCotizacion, specified, claveUsuario)
            ObjBXMAS.actualizarCobertura(ObjData.Cotizacion.IDCotizacion, specified, LineaNegocio, specified, 1626, specified, "APMUL", 0, specified, 0, specified, 0, specified, 0, specified, 0, specified)

            'muerte accidental
            ObjBXMAS.actualizarCobertura(ObjData.Cotizacion.IDCotizacion, specified, LineaNegocio, specified, 9, specified, "AP", False, specified, 0, specified, 0, specified, 0, specified, 0, specified)
            'Extensions responsabilidad civil
            ObjBXMAS.actualizarCobertura(ObjData.Cotizacion.IDCotizacion, specified, LineaNegocio, specified, 1592, specified, "DMPT", False, specified, 0, specified, 0, specified, 0, specified, 0, specified)
            'Daños materiales por perdida total
            ObjBXMAS.actualizarCobertura(ObjData.Cotizacion.IDCotizacion, specified, LineaNegocio, specified, 10, specified, "RCET", False, specified, 0, specified, 0, specified, 0, specified, 0, specified)

            ObjBXMAS.cotizarCoberturas(ObjData.Cotizacion.IDCotizacion, specified, IdEmpresa, specified, "AGTESYFT", IdPaquete, specified, IdProducto, specified, IdVersionProducto, specified, IdMoneda, specified)

            ObjBXMAS.actualizarFormaPagoCotizacion(respuesta.idCotizacion, specified, idFormaPago, specified, IdPaquete, specified)

            respuestaImportes = ObjBXMAS.recuperarImportesCotizacion(respuesta.idCotizacion, 1)
            ObjData.Cotizacion.PrimaTotal = respuestaImportes.impPrimaTotal
            ObjData.Cotizacion.Derechos = respuestaImportes.impDerechos
            ObjData.Cotizacion.PrimaNeta = respuestaImportes.impPrimaNeta
            ObjData.Cotizacion.Recargos = respuestaImportes.impRecargos
            ObjData.Cotizacion.PrimerPago = respuestaImportes.impPrimerRecibo
            ObjData.Cotizacion.PagosSubsecuentes = respuestaImportes.impReciboSubsecuente
            ObjData.Cotizacion.Impuesto = respuestaImportes.impIva

            respuestaCoberturas = ObjBXMAS.recuperarCoberturasCotizacion(IdEmpresa, specified, IdProducto, specified, 484, specified, LineaNegocio, specified, respuesta.idCotizacion, specified, 0, specified, respuesta.numCotizacion, specified, claveUsuario)
            Try
                Dim x2 As New XmlSerializer(respuestaCoberturas.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, respuestaCoberturas)
                Dim errorws4 As String = xml2.ToString
                ' lg.UpdateLogResponse(errorws4, idLogWSCot)
                ' Funciones.updateLogWSCot(errorws4, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception

            End Try
            Dim Coberturas As New Coberturas
            For k = 0 To respuestaCoberturas.coberturas.Length - 1
                Dim cobertura As String = respuestaCoberturas.coberturas(k).cveTipoCobertura
                Dim descripcionCobertura As String = ""
                Dim nombreCobertura = ""
                Dim SA = respuestaCoberturas.coberturas(k).sumaAsegurada
                Dim deducible = CInt(respuestaCoberturas.coberturas(k).deducibleDft)
                Select Case cobertura
                    Case "DM"
                        nombreCobertura = "DAÑOS MATERIALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & deducible
                        Coberturas.DanosMateriales = descripcionCobertura
                    Case "RT"
                        nombreCobertura = "ROBO TOTAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & deducible
                        Coberturas.RoboTotal = descripcionCobertura
                    Case "RCBI"
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                        Coberturas.RCBienes = descripcionCobertura
                    Case "GMO"
                        nombreCobertura = "GASTOS MÉDICOS EVENTO"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                        Coberturas.GastosMedicosEvento = descripcionCobertura
                    Case "PROC"
                        nombreCobertura = "ASISTENCIA LEGAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                        Coberturas.DefensaJuridica = descripcionCobertura
                    Case "PROV"
                        nombreCobertura = "ASISTENCIA VIAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                        Coberturas.AsitenciaCompleta = descripcionCobertura
                    Case "DVIN"
                        nombreCobertura = "CRISTALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D" & deducible
                        Coberturas.Cristales = descripcionCobertura

                End Select
            Next
            ObjData.Coberturas.Add(Coberturas)
            ObjData.Cotizacion.Resultado = "True"
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Dim JsonRespuesta As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(JsonRespuesta, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function BXMASEmision(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim ObjBXMASTrans As New BXMASService.transaccionalAutosWs
        Dim ObjBXMASCatalogo As New BXMASServiceCatalogo.catalogosAutosWs
        Dim respuesta = New BXMASService.cotizacionInformacionDto
        Dim serializer As New JavaScriptSerializer
        Dim cveUsuario As String = "AGTESYFT"
        Dim idEmpresa As Integer = 8
        Dim idAgente As Integer = 1911
        Dim IdPaquete As Integer = 882
        Dim idSubMarca = 0
        Dim desSubmarca = ""
        Try
            Dim IdMarca
            Try
                IdMarca = GetIdMarca(ObjData.Vehiculo.Marca)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta marca del vehiculo")
            End Try
            Dim idTIpoVehiculo
            Try
                idTIpoVehiculo = GetSubMarca(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta submarca del vehiculo")
            End Try
            Dim datosVehi
            Try
                datosVehi = GetDatosVehiculo(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al intentar consultar los datos del vehiculo")
            End Try
            Dim idModelo = CInt(ObjData.Vehiculo.Modelo)
            Dim idCotizacion = ObjData.Cotizacion.IDCotizacion
            Dim idVersionCarga As Integer = 0
            desSubmarca = datosVehi(0)
            idSubMarca = CInt(datosVehi(1))
            idVersionCarga = CInt(datosVehi(2))
            If ObjData.Paquete = "AMPLIA" Then
                IdPaquete = 876
            ElseIf ObjData.Paquete = "LIMITADA" Then
                IdPaquete = 878
            ElseIf ObjData.Paquete = "BASICA" Then
                IdPaquete = 877
            End If
            Dim cveTipoBien As String
            Dim idLineaNegocio As Int16 = 0
            If idTIpoVehiculo = "AUT" Then
                cveTipoBien = "AUTOS"
                idLineaNegocio = 1
            Else
                cveTipoBien = "PICK"
                idLineaNegocio = 1235
            End If

            Dim sexo As String
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = "M"
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = "F"
            End If

            Dim formaPago = ""
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                formaPago = "1"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                formaPago = "2"
            Else
                formaPago = ObjData.PeriodicidadDePago
            End If

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            Dim idProducto As Integer = 1445
            Dim idMoneda As Integer = 484
            Dim idCentroEmisor As Integer = 222
            Dim idVersionProducto As Integer = 20
            Dim idVersionPoliza As Integer = 1
            Dim recuperarClientesPersonaFisicaResp() As BXMASService.clienteFisicoDto
            Dim clienteFisicoDTO As BXMASService.clienteFisicoDto
            Dim rfcBusqueda = ObjData.Cliente.RFC.Substring(0, 4) & "-" & ObjData.Cliente.RFC.Substring(4, 6) & "-" & ObjData.Cliente.RFC.Substring(10, 3)
            Try
                recuperarClientesPersonaFisicaResp = ObjBXMASTrans.recuperarClientesPersonaFisica(ObjData.Cliente.Nombre, ObjData.Cliente.ApellidoPat, ObjData.Cliente.ApellidoMat)
                If recuperarClientesPersonaFisicaResp IsNot Nothing Then
                    For Each clienteFisicoIter In recuperarClientesPersonaFisicaResp
                        If rfcBusqueda.Equals(clienteFisicoIter.rfc) Then
                            clienteFisicoDTO = clienteFisicoIter
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
            End Try
            'Paso 32
            Dim idClienteOut As Integer
            Dim direccion As String = "Calle:" & ObjData.Cliente.Direccion.Calle
            direccion += ", C.P.:" & ObjData.Cliente.Direccion.CodPostal
            direccion += "No.Ext:" & ObjData.Cliente.Direccion.NoExt
            Dim homoclaveRfC As String = ObjData.Cliente.RFC.Substring(10, 3)
            Dim recuperarDomicilioResp As BXMASService.domicilioVo
            recuperarDomicilioResp = ObjBXMASTrans.recuperarDomicilio(ObjData.Cliente.Direccion.CodPostal)
            If clienteFisicoDTO Is Nothing Then
                Dim siglasRfc As String = ObjData.Cliente.RFC.Substring(0, 4)
                Dim fechaRfc As String = ObjData.Cliente.RFC.Substring(4, 6)
                Dim fechaNacimiento As String = ObjData.Cliente.FechaNacimiento
                Dim cveEstadoCivil As String = "S"
                Dim cvePaisNac As String = "PAMEXI"
                Dim cveNacionalidad As String = "PAMEXI"
                Dim curp As String = ObjData.Cliente.RFC
                Dim numSerieFirmaElec As String = "N/A"
                Dim idGiroOcup As Integer = 2731
                Dim cveActividad As String = "9191016"
                Dim idPep As Boolean
                Dim descCargoPep As String = "N/A"
                Dim numIdentificacion As String = "123456789"
                Dim cveTipoIdentificacion As String = "IFE"
                Dim nombreImagenIdentificacion As String = "N/A"
                Dim imagenIdentificacion As String = "cid:1384896043647"
                Dim calle As String = ObjData.Cliente.Direccion.Calle
                Dim numExt As String = ObjData.Cliente.Direccion.NoExt
                Dim numInt As String = ObjData.Cliente.Direccion.NoInt
                Dim idMunicipio As Integer = recuperarDomicilioResp.idMunicipio
                Dim idEstado As Integer = recuperarDomicilioResp.idEstado
                Dim telOficina As Integer
                Dim telFax As Integer
                Dim telCasa As Integer = ObjData.Cliente.Telefono
                Dim correoElectronico As String = ObjData.Cliente.Email
                Dim lugarNacimiento As String = "MEXICO"
                ObjBXMASTrans.guardarClientePersonaFisica(ObjData.Cliente.Nombre, ObjData.Cliente.ApellidoPat, ObjData.Cliente.ApellidoMat, siglasRfc, fechaRfc,
                                                          homoclaveRfC, sexo, fechaNacimiento, cveEstadoCivil, cvePaisNac, cveNacionalidad, curp,
                                                          numSerieFirmaElec, idGiroOcup, cveActividad, idPep, 1, descCargoPep, numIdentificacion,
                                                          cveTipoIdentificacion, nombreImagenIdentificacion, System.Text.Encoding.Unicode.GetBytes(imagenIdentificacion),
                                                          ObjData.Cliente.Direccion.CodPostal, calle, numExt, numInt, ObjData.Cliente.Direccion.Colonia,
                                                          idMunicipio, recuperarDomicilioResp.descMunicipio, idEstado, recuperarDomicilioResp.descEstado,
                                                          telOficina, telFax, telCasa, correoElectronico, cveUsuario, idCotizacion, 1, lugarNacimiento,
                                                          idClienteOut, 1)
                recuperarClientesPersonaFisicaResp = ObjBXMASTrans.recuperarClientesPersonaFisica(ObjData.Cliente.Nombre, ObjData.Cliente.ApellidoPat, ObjData.Cliente.ApellidoMat)
                If recuperarClientesPersonaFisicaResp IsNot Nothing Then
                    For Each clienteFisicoIter In recuperarClientesPersonaFisicaResp
                        If rfcBusqueda.Equals(clienteFisicoIter.rfc) Then
                            clienteFisicoDTO = clienteFisicoIter
                            Exit For
                        End If
                    Next
                End If
            End If
            'Paso 25
            ObjBXMASTrans.establecerClientePoliza(clienteFisicoDTO.idCliente, 1, clienteFisicoDTO.idDomicilio, 1, direccion, ObjData.Cliente.Direccion.Colonia, ObjData.Cliente.Direccion.CodPostal,
                                                      recuperarDomicilioResp.descEstado, recuperarDomicilioResp.descMunicipio,
                                                      ObjData.Cliente.Nombre, ObjData.Cliente.ApellidoPat, ObjData.Cliente.ApellidoMat, homoclaveRfC, idCotizacion, 1)
            'Paso 34
            Dim niv As String = ObjData.Vehiculo.NoSerie
            Dim numMotor As String = ObjData.Vehiculo.NoMotor
            Dim placa As String = ObjData.Vehiculo.NoPlacas
            Dim rfv As String = ""
            Dim numEconomico As String = ""
            Dim idInciso As Integer = 1
            ObjBXMASTrans.actualizarComplementoVehiculo(idCotizacion, 1, idLineaNegocio, 1, niv, numMotor, placa, rfv, numEconomico, idAgente,
                                                        1, idModelo, 1, idVersionPoliza, 1, idInciso, 1)
            ObjBXMASTrans.actualizarFormaPagoCotizacion(idCotizacion, 1, formaPago, 1, IdPaquete, 1)
            'Paso 35
            Dim numPolizaOut As Integer
            ObjBXMASTrans.emitirPoliza(idCotizacion, 1, idEmpresa, 1, cveUsuario, numPolizaOut, 1)
            ObjData.Emision.Poliza = numPolizaOut
            If ObjData.Emision.Poliza <> Nothing Or ObjData.Emision.Poliza <> "" Then
                'Paso 36
                Dim polizaPDFResp = ObjBXMASTrans.generarReportePolizaPDF(idCotizacion, 1)
                Dim ruta As String = "W:\Documentos\Proyecto\ws-ali-ahorraseguros-prod\Archivo\"
                File.WriteAllBytes(ruta + ObjData.Emision.Poliza + ".pdf", polizaPDFResp)
                ObjData.Emision.Documento = "https://ws-se.com/Archivo/" + ObjData.Emision.Poliza + ".pdf"
                ObjData.Emision.PrimaTotal = ObjData.Cotizacion.PrimaTotal
                ObjData.Emision.Derechos = ObjData.Cotizacion.Derechos
                ObjData.Emision.PrimaNeta = ObjData.Cotizacion.PrimaNeta
                ObjData.Emision.Recargos = ObjData.Cotizacion.Recargos
                ObjData.Emision.PrimerPago = ObjData.Cotizacion.PrimerPago
                ObjData.Emision.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes
                ObjData.Emision.Impuesto = ObjData.Cotizacion.Impuesto
                ObjData.Emision.Resultado = "True"
            End If
        Catch ex As Exception
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Return serializer.Serialize(ObjData)
    End Function

    Private Shared Function GetIdMarca(ByVal Marca As String) As Int16
        Dim strSQLQuery As String
        Dim IdMarca As Int16
        Dim connectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;"
        strSQLQuery = "Select IdMarca from Ali_R_CatMarcasBXMAS where Marca = '" + Marca + "'"

        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQuery, Connection)
            Try
                Connection.Open()
                Dim rs As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                rs.Read()
                'rsCPostal.Close()
                IdMarca = rs("IdMarca")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return IdMarca
    End Function

    Private Shared Function GetSubMarca(ByVal clave As String) As String
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "BXMAS", clave)
        Dim mapaVehiculo = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim tipo = mapaVehiculo.Item("vehicle_type")
        Return tipo
    End Function
    Private Shared Function GetDatosVehiculo(ByVal clave As String) As String()
        Dim strSQLQuery As String
        Dim datos(2) As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQuery = "Select * from Ali_R_CatSubMarcasBXMAS where provider_code = '" + clave + "'"

        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQuery, Connection)
            Try
                Connection.Open()
                Dim rs As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                rs.Read()
                'rsCPostal.Close()
                datos(0) = rs("desSubmarca")
                datos(1) = rs("IdSubmarca")
                datos(2) = rs("TipoCarga")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return datos
    End Function

    Private Shared Function ConsultaMunicipiosANA(ByVal CPostal As String) As String()
        Dim Lista(1) As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        Dim strSQLQuery = "Select Estado, Municipio from `Ali_R_CatEdosANA` where CPostal = '" + CPostal + "'"

        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQuery, Connection)
            Try
                Connection.Open()
                Dim rsCPostal As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                rsCPostal.Read()
                Lista(0) = rsCPostal("Estado")
                Lista(1) = rsCPostal("Municipio")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using

        If UCase(Lista(0)) = "DISTRITO FEDERAL" Then
            Lista(0) = "CIUDAD DE MÉXICO"
        ElseIf UCase(Lista(0)) = "COAHUILA DE ZARAGOZA" Then
            Lista(0) = "COAHUILA"
        ElseIf UCase(Lista(0)) = "ESTADO DE MEXICO" Then
            Lista(0) = "MEXICO"
        ElseIf UCase(Lista(0)) = "MICHOACAN DE OCAMPO" Then
            Lista(0) = "MICHOACAN"
        End If
        Return Lista
    End Function
    Private Shared Function GetEstado(ByVal estado As String) As String
        Dim strSQLQuery As String
        Dim datos As String = ""
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        Dim Query = "Select * from `Ali_R_catEdosBxmas` where Estado ='" + estado + "'"

        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(Query, Connection)
            Try
                Connection.Open()
                Dim rs As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                rs.Read()
                datos = rs("IdPoblacion")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using

        Return datos
    End Function
End Class
