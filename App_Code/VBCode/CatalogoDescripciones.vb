﻿Imports Microsoft.VisualBasic

Public Class CatalogoDescripciones
    Private _ListadoDescripciones As New List(Of ListadoDescripciones)
    Private _Marca As String
    Private _Modelo As String
    Private Sub Constructor()
        Me._Marca = ""
        Me._Modelo = ""
    End Sub
    Public Property Marca As String
        Get
            Return Me._Marca
        End Get
        Set(ByVal value As String)
            Me._Marca = value
        End Set
    End Property
    Public Property Modelo As String
        Get
            Return Me._Modelo
        End Get
        Set(ByVal value As String)
            Me._Modelo = value
        End Set
    End Property
    Public Property ListadoDescripciones As List(Of ListadoDescripciones)
        Get
            Return Me._ListadoDescripciones
        End Get
        Set(ByVal value As List(Of ListadoDescripciones))
            Me._ListadoDescripciones = value
        End Set
    End Property
End Class
Public Class ListadoDescripciones
    Private _Descripcion As String
    Private Sub Constructor()
        Me._Descripcion = ""
    End Sub
    Public Property Descripcion As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property
End Class