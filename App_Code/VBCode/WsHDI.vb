﻿Imports Microsoft.VisualBasic

Imports SuperObjeto
Imports System.IO
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports System.Web.Script.Serialization
Imports System.ServiceModel.Security
Imports UNO.Public
Imports Newtonsoft.Json
Imports MySql.Data.MySqlClient
Imports System.Data

Public Class WsHDI
    Private Shared user As String = "066081"
    Private Shared psw As String = "TRIGARANTPR*2"
    Private Shared url As String = "http://enterpriseservices.implementation.hdi.com.mx/b2b/Partners/WCF/Autos/PublicServicesAutos.svc"

    Public Shared Function HDICotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)

        Try
            Dim objCotizacion As New HDIService.PublicServicesAutosContractClient()
            objCotizacion.ClientCredentials.UserName.UserName = user
            objCotizacion.ClientCredentials.UserName.Password = psw
            objCotizacion.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.PeerTrust
            objCotizacion.ClientCredentials.CreateSecurityTokenManager()
            objCotizacion.ClientCredentials.Clone()
            Dim ObjParam As New HDIService.ObtenerPaquetesRequest
            Dim ObjVehiculo As New HDIService.CaracteristicasVehiculo
            Dim ObjDatosAdi As New HDIService.DatosAdicionalesGLM
            Dim ObjDatosConductor As New HDIService.DatosConductor
            Dim ObjMarcas As New HDIService.ObtenerMarcasRequest
            Dim Ajuste As New HDIService.Ajuste
            Dim ObjSubmarcas As New HDIService.ObtenerTiposRequest
            Dim ObjTransmision As New HDIService.ObtenerTransmisionesRequest
            Dim ObjPaquetesCam(0) As HDIService.PaqueteCoberturas
            Dim ObjPaquetesCam1 As HDIService.PaqueteCoberturas = New HDIService.PaqueteCoberturas
            Dim ListarPaquetes As New HDIService.StringArrayContract
            Dim Objversiones As New HDIService.ObtenerVersionesRequest
            Dim ObjUso As New HDIService.ObtenerUsoRequest
            Dim ObjNombres As New HDIService.NombresConducor
            Dim ObjPaqueteCoberturas As New HDIService.PaqueteCoberturas

            Dim IdTipoVehiculo As Integer = 0
            Dim IdMarca As Integer
            Dim FormaPago As String = ""
            Dim IdDescripcion As String = ""
            Dim IdVersion As String = ""
            Dim IdTransmision As String = ""
            Dim IdUso As String = ""
            Dim paquete As String = ObjData.Paquete
            Dim DatosVehiculo
            Try
                DatosVehiculo = GetDatosVehiculo(ObjData.Vehiculo.Clave, ObjData.Vehiculo.Modelo)
            Catch ex As Exception
                Throw New System.Exception("Error al reaizar consulta datos vehiculo (clave de vehiculo)")
            End Try

            Dim TipoVehiculo = DatosVehiculo(1)
            IdMarca = DatosVehiculo(3)
            IdDescripcion = DatosVehiculo(4)
            IdVersion = DatosVehiculo(5)
            IdTransmision = DatosVehiculo(6)

            Dim IdFormaPago As Integer = 0
            If ObjData.PeriodicidadDePago = 0 Then
                FormaPago = "Contado"
            ElseIf ObjData.PeriodicidadDePago = 4 Then
                FormaPago = "Mensual"
            End If
            Dim tipvhc
            Try
                tipvhc = GetTipoVehiculo(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al reaizar consulta tipo vehiculo (clave de vehiculo)")
            End Try

            Dim plan(0) As String
            If UCase(paquete) = "AMPLIA" And UCase(tipvhc(0)) = "AUT" Then
                plan(0) = "19"
            ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc(0)) = "AUT" Then
                plan(0) = "21"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc(0)) = "AUT" Then
                plan(0) = "22"
            ElseIf UCase(paquete) = "AMPLIA" And UCase(tipvhc(0)) = "PKP" Then
                plan(0) = "23"
            ElseIf UCase(paquete) = "LIMITADA" And IdTransmision = "PKP" Then
                plan(0) = "24"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc(0)) = "PKP" Then
                plan(0) = "25"
            End If


            Dim FormasPago = objCotizacion.ObtenerFormasPago()
            For i = 0 To FormasPago.Length - 1
                If FormasPago(i).Descripcion = UCase(FormaPago) Then
                    IdFormaPago = FormasPago(i).Clave
                    Exit For
                End If
            Next

            'Dim respuestaUso = objCotizacion.ObtenerUsos(ObjUso)
            IdUso = 4581

            Dim EdoCivil = objCotizacion.ObtenerEstadoCivil()
            ' ------------------------------------- 
            '       Inicio de Objeto Cotizacion 
            '-------------------------------------
            Dim Nombres = Split(ObjData.Cliente.Nombre)
            Dim numeroNom = Nombres.Length
            Dim fechaNac As String = ObjData.Cliente.FechaNacimiento
            Dim sexo As Integer = 0

            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = 1
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = 2
            End If

            ObjParam.IDTipoSumaAsegurada = 4452
            ObjParam.IDTipoSumaAseguradaSpecified = True
            ObjParam.IvaPorcentaje = 0
            ObjParam.IvaPorcentajeSpecified = True
            ObjParam.SumaAsegurada = 0
            ObjParam.SumaAseguradaSpecified = True


            ObjVehiculo.idVehiculo = ObjData.Vehiculo.Clave
            ObjVehiculo.idMarca = IdMarca
            ObjVehiculo.idMarcaSpecified = True
            ObjVehiculo.idModelo = CInt(ObjData.Vehiculo.Modelo)
            ObjVehiculo.idModeloSpecified = True
            ObjVehiculo.idTipo = IdDescripcion
            ObjVehiculo.idVersion = IdVersion
            ObjVehiculo.idTransmision = IdTransmision
            ObjVehiculo.idTransmisionSpecified = True
            ObjVehiculo.idUso = IdUso
            ObjVehiculo.idUsoSpecified = True
            ObjVehiculo.numeroSerie = 0
            ObjVehiculo.idZonaCirculacion = 0
            ObjVehiculo.idZonaCirculacionSpecified = True
            ObjVehiculo.idTonelaje = 0
            ObjVehiculo.idTonelajeSpecified = True
            ObjVehiculo.idServicio = 4601
            ObjVehiculo.idServicioSpecified = True
            ObjDatosAdi.CPCirculacion = ObjData.Cliente.Direccion.CodPostal
            ObjDatosAdi.Renovaciones = 0
            ObjDatosAdi.idRemolque = 4603
            ObjVehiculo.DatosAdicionales = ObjDatosAdi
            ObjParam.datosVehiculo = ObjVehiculo
            ObjDatosConductor.ApellidoPaterno = "PEREZ"
            ObjDatosConductor.ApellidoMaterno = "GOMEZ"
            ObjNombres.PrimerNombre = "LUIS"
            If numeroNom = 2 Then
                ObjNombres.SegundoNombre = Nombres(1)
            End If
            ObjDatosConductor.Nombres = ObjNombres
            ObjDatosConductor.FechaDeNacimiento = Convert.ToDateTime(fechaNac, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            ObjDatosConductor.FechaDeNacimientoSpecified = True
            ObjDatosConductor.RFC = "PEGL881119"
            ObjDatosConductor.Sexo = sexo
            ObjDatosConductor.SexoSpecified = True
            ObjDatosConductor.EstadoCivil = "C"
            ObjDatosConductor.Ocupacion = "96"
            ObjDatosConductor.OcupacionSpecified = True
            ObjDatosConductor.CuentaConCochera = True
            ObjDatosConductor.CuentaConCocheraSpecified = True
            ObjVehiculo.Conductor = ObjDatosConductor





            '-------------------------------------------
            '                 Formas pago
            '-------------------------------------------

            ObjParam.idFormaPago = IdFormaPago
            ObjParam.idFormaPagoSpecified = True
            ListarPaquetes.StringArray = plan
            ObjParam.listaPaquetesACalcular = ListarPaquetes
            ObjParam.obtenerTodosPaquetes = False
            ObjParam.obtenerTodosPaquetesSpecified = False

            ObjPaqueteCoberturas.ListaDatosFormasPagos = New HDIService.DatosFormasPagos(1) {}
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0) = New HDIService.DatosFormasPagos()
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPago = IdFormaPago
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPagoSpecified = True
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaquete = plan(0)
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaqueteSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias = New HDIService.Cobertura(3) {}
            ObjPaqueteCoberturas.CoberturasObligatorias(0) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Regla = 287
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Clave = 233
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Descripcion = "Daños Materiales"
            ObjPaqueteCoberturas.CoberturasObligatorias(0).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(0).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Deducible = 5
            ObjPaqueteCoberturas.CoberturasObligatorias(0).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).PrimaNeta = 12158.1190263578
            ObjPaqueteCoberturas.CoberturasObligatorias(0).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias(1) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Regla = 339
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Clave = 236
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Descripcion = "Robo Total"
            ObjPaqueteCoberturas.CoberturasObligatorias(1).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(1).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Deducible = 10
            ObjPaqueteCoberturas.CoberturasObligatorias(1).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasObligatorias(1).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias(2) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Regla = 655
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Clave = 253
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Descripcion = "Responsabilidad Civil (Límite Único y Co"
            ObjPaqueteCoberturas.CoberturasObligatorias(2).SumaAsegurada = 750000
            ObjPaqueteCoberturas.CoberturasObligatorias(2).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(2).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ProveedorAsistencia = 6923
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasObligatorias(2).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias(3) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(3).Regla = 295
            ObjPaqueteCoberturas.CoberturasObligatorias(3).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).Clave = 242
            ObjPaqueteCoberturas.CoberturasObligatorias(3).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).Descripcion = "Asistencia Jurídica"
            ObjPaqueteCoberturas.CoberturasObligatorias(3).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(3).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(3).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).ProveedorAsistencia = 6923
            ObjPaqueteCoberturas.CoberturasObligatorias(3).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasObligatorias(3).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(3).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales = New HDIService.Cobertura(6) {}
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Regla = 292
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Clave = 239
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Descripcion = "Gastos Médicos Ocupantes (Límite Único C"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).SumaAsegurada = 50000
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Regla = 653
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Clave = 366
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Descripcion = "Responsabilidad Civil Exceso por Muerte"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).SumaAsegurada = 2000000
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).PrimaNeta = 251.72414
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Regla = 2806
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Clave = 249
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Descripcion = "Asistencia en viajes"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).SumaAsegurada = 2000000
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ProveedorAsistencia = 6923
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Regla = 317
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Clave = 264
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Descripcion = "Extensión de Responsabilidad Civil para Automóvil Particular"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).PrimaNeta = 0.00
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Calculada = False
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).Regla = 398
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).Clave = 267
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).Descripcion = "Responsabilidad Civil Familiar"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).PrimaNeta = 0.00
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).Calculada = False
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(4).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).Regla = 322
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).Clave = 269
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).Descripcion = "Asistencia Médica"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).PrimaNeta = 0.00
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).Calculada = False
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(5).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).Regla = 513
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).Clave = 365
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).Descripcion = "Asistencia Funeraria"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).PrimaNeta = 0.00
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).Calculada = False
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(6).CalculadaSpecified = True


            ObjPaqueteCoberturas.CoberturasOpcionales = New HDIService.Cobertura(2) {}
            ObjPaqueteCoberturas.CoberturasOpcionales(0) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasOpcionales(0).Regla = 689
            ObjPaqueteCoberturas.CoberturasOpcionales(0).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).Clave = 355
            ObjPaqueteCoberturas.CoberturasOpcionales(0).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).Descripcion = "Responsabilidad Civil por daños a los Ocupantes"
            ObjPaqueteCoberturas.CoberturasOpcionales(0).SumaAsegurada = 350000
            ObjPaqueteCoberturas.CoberturasOpcionales(0).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).Deducible = 0
            ObjPaqueteCoberturas.CoberturasOpcionales(0).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasOpcionales(0).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasOpcionales(0).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).Calculada = True
            ObjPaqueteCoberturas.CoberturasOpcionales(0).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasOpcionales(1) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasOpcionales(1).Regla = 361
            ObjPaqueteCoberturas.CoberturasOpcionales(1).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).Clave = 235
            ObjPaqueteCoberturas.CoberturasOpcionales(1).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).Descripcion = "Accidentes Automovilísticos al Conductor"
            ObjPaqueteCoberturas.CoberturasOpcionales(1).SumaAsegurada = 100000
            ObjPaqueteCoberturas.CoberturasOpcionales(1).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).Deducible = 0
            ObjPaqueteCoberturas.CoberturasOpcionales(1).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasOpcionales(1).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasOpcionales(1).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).Calculada = True
            ObjPaqueteCoberturas.CoberturasOpcionales(1).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasOpcionales(2) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasOpcionales(2).Regla = 2931
            ObjPaqueteCoberturas.CoberturasOpcionales(2).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).Clave = 266
            ObjPaqueteCoberturas.CoberturasOpcionales(2).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).Descripcion = "Responsabilidad USA y Canadá"
            ObjPaqueteCoberturas.CoberturasOpcionales(2).SumaAsegurada = 150000
            ObjPaqueteCoberturas.CoberturasOpcionales(2).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).Deducible = 0
            ObjPaqueteCoberturas.CoberturasOpcionales(2).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasOpcionales(2).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).PrimaNeta = 0.0
            ObjPaqueteCoberturas.CoberturasOpcionales(2).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).Calculada = True
            ObjPaqueteCoberturas.CoberturasOpcionales(2).CalculadaSpecified = True

            Ajuste.PorcentajeAjuste = CInt(ObjData.Descuento)
            Ajuste.PorcentajeAjusteSpecified = True
            Ajuste.TipoAjuste = 4612
            Ajuste.TipoAjusteSpecified = True
            ObjPaqueteCoberturas.Clave = plan(0)
            ObjPaqueteCoberturas.ClaveSpecified = True
            ObjPaqueteCoberturas.Ajuste = Ajuste
            ObjPaquetesCam(0) = ObjPaqueteCoberturas
            ObjParam.paquetesConCambios = ObjPaquetesCam
            ObjParam.usuario = user
            Try
                Dim x As New XmlSerializer(ObjParam.GetType)
                Dim xml As New StringWriter
                x.Serialize(xml, ObjParam)
                Dim errorws2 As String = xml.ToString
                Funciones.updateLogWSCot(errorws2, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception

            End Try

            Dim respuestaFinal = objCotizacion.ObtenerPaquetes(ObjParam)

            ObjData.Cotizacion.Resultado = "True"

            Try
                Dim x2 As New XmlSerializer(respuestaFinal.ListaPaquetes(0).GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, respuestaFinal.ListaPaquetes(0))
                Dim errorws4 As String = xml2.ToString
                Funciones.updateLogWSCot(errorws4, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception

            End Try

            ObjData.Cotizacion.PrimaNeta = respuestaFinal.ListaPaquetes(0).Totales.PrimaNeta
            ObjData.Cotizacion.Derechos = respuestaFinal.ListaPaquetes(0).Totales.DerechoPoliza
            ObjData.Cotizacion.Impuesto = respuestaFinal.ListaPaquetes(0).Totales.IVA
            ObjData.Cotizacion.PrimaTotal = respuestaFinal.ListaPaquetes(0).Totales.PrimaTotal
            ObjData.Cotizacion.PrimerPago = respuestaFinal.ListaPaquetes(0).Recibos.PrimeraExhibicion
            ObjData.Cotizacion.PagosSubsecuentes = respuestaFinal.ListaPaquetes(0).Recibos.RecibosSubsecuentes


            Dim Coberturas As New Coberturas

            Coberturas.DanosMateriales = "-NDAÑOS MATERIALES-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(0).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(0).Deducible
            Coberturas.RoboTotal = "-NROBO TOTAL-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(1).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(1).Deducible
            Coberturas.RC = "-NRESPONSABILIDAD CIVIL-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(2).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(2).Deducible
            Coberturas.DefensaJuridica = "-NDEF. JUD. Y ASIS. LEGAL-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(3).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(3).Deducible
            Coberturas.GastosMedicosOcupantes = "-NGASTOS MEDICOS-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(0).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(0).Deducible
            Coberturas.RCExtension = "-NEXTENSION RC-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(1).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(1).Deducible
            Coberturas.MuerteAccidental = "-NMUERTE DEL CONDUCTOR POR ACCIDENTE AUTOMOVILISTICO-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(2).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(2).Deducible
            Coberturas.RCFamiliar = "-NRESPONSABILIDAD CIVIL FAMILIAR-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(3).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(3).Deducible
            ObjData.Coberturas.Add(Coberturas)
            ObjData.Cotizacion.IDCotizacion = GuardarCotizacion(ObjData)
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function GetTipoVehiculo(ByVal Clave As String) As String()
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "HDI", Clave)
        Dim mapaVehiculo = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim Tipo(1) As String
        Tipo(0) = mapaVehiculo.Item("vehicle_type")
        Tipo(1) = mapaVehiculo.Item("providers_trim")
        Return Tipo
    End Function

    Public Shared Function HDIEmision(ByVal ObjData As Seguro)
        Try
            Dim objCotizacion As New HDIService.PublicServicesAutosContractClient()
            objCotizacion.ClientCredentials.UserName.UserName = user
            objCotizacion.ClientCredentials.UserName.Password = psw
            objCotizacion.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.PeerTrust
            objCotizacion.ClientCredentials.CreateSecurityTokenManager()
            objCotizacion.ClientCredentials.Clone()
            Dim ObjParam As New HDIService.DatosCotizacion
            Dim ObjPaqueteCoberturas As New HDIService.PaqueteCoberturas
            Dim ObjVehiculo As New HDIService.CaracteristicasVehiculo
            Dim ObjDatosAdi As New HDIService.DatosAdicionalesGLM
            Dim ObjDatosConductor As New HDIService.DatosConductor
            Dim ObjMarcas As New HDIService.ObtenerMarcasRequest
            Dim Ajuste As New HDIService.Ajuste
            Dim ObjSubmarcas As New HDIService.ObtenerTiposRequest
            Dim ObjTransmision As New HDIService.ObtenerTransmisionesRequest
            Dim ObjPaquetesCam(0) As HDIService.PaqueteCoberturas
            Dim ObjPaquetesCam1 As HDIService.PaqueteCoberturas = New HDIService.PaqueteCoberturas
            Dim ListarPaquetes As New HDIService.StringArrayContract
            Dim Objversiones As New HDIService.ObtenerVersionesRequest
            Dim ObjUso As New HDIService.ObtenerUsoRequest
            Dim ObjNombres As New HDIService.NombresConducor
            Dim objCliente As New HDIService.Cliente
            Dim ObjDireccion As New HDIService.Direccion
            Dim ObjInfoPago As New HDIService.InformacionPago
            Dim ObjBenef As New HDIService.BeneficiarioPreferente
            Dim ObjTarifExpres As New HDIService.PaqueteTarificarExpress
            Dim ObjInfCorredor As New HDIService.InformacionAdicionalCorredor
            Dim vigencia As New HDIService.Vigencia

            Dim fechaNacCond As String = ObjData.Cliente.FechaNacimiento
            Dim Nombres = Split(ObjData.Cliente.Nombre)
            Dim numeroNom = Nombres.Length
            Dim fechaNac As String = ObjData.Cliente.FechaNacimiento
            Dim sexo As Integer = 0

            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = 1
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = 2
            End If

            If UCase(ObjData.Cliente.Direccion.Ciudad) = "DISTRITO FEDERAL" Then
                ObjData.Cliente.Direccion.Ciudad = "CIUDAD DE MÉXICO"
            End If

            Dim FormaPago As String = ""
            Dim IdTipoVehiculo As Integer = 0
            Dim IdMarca As Integer
            Dim IdDescripcion As String = ""
            Dim IdVersion As String = ""
            Dim IdTransmision As String = ""
            Dim IdUso As String = ""
            IdUso = 4581
            Dim paquete As String = ObjData.Paquete
            Dim tipvhc As String = "AUT"

            Dim IdFormaPago As Integer = 0
            If ObjData.PeriodicidadDePago = 0 Then
                FormaPago = "Contado"
            ElseIf ObjData.PeriodicidadDePago = 4 Then
                FormaPago = "Mensual"
            End If
            Dim plan As String
            If UCase(paquete) = "AMPLIA" And UCase(tipvhc) = "AUT" Then
                plan = "19"
            ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc) = "AUT" Then
                plan = "21"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc) = "AUT" Then
                plan = "22"
            ElseIf UCase(paquete) = "AMPLIA" And UCase(tipvhc) = "PKP" Then
                plan = "23"
            ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc) = "PKP" Then
                plan = "24"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc) = "PKP" Then
                plan = "25"
            End If
            Dim FormasPago = objCotizacion.ObtenerFormasPago()
            For i = 0 To FormasPago.Length - 1
                If FormasPago(i).Descripcion = UCase(FormaPago) Then
                    IdFormaPago = FormasPago(i).Clave
                    Exit For
                End If
            Next
            Dim IdBanco = obtenerCveBanco(ObjData.Pago.Banco)

            Dim TipoVehiculo = GetTipoVehiculo(ObjData.Vehiculo.Clave)
            Dim TipoVehiculo2 = objCotizacion.ObtenerTiposVehiculo(user)
            If TipoVehiculo(0) = "PKP" Then
                IdTipoVehiculo = 3829
            ElseIf TipoVehiculo(0) = "AUT" Then
                IdTipoVehiculo = 4579
            End If
            Dim Estados = objCotizacion.ObtenerEstados("00007")
            Dim IdEstado = ""
            For g = 0 To Estados.Length - 1
                If Estados(g).Descripcion = UCase(ObjData.Cliente.Direccion.Ciudad) Then
                    IdEstado = Estados(g).Clave
                    Exit For
                End If
            Next

            Dim ciudades = objCotizacion.ObtenerCiudades(IdEstado)
            Dim IdCiudad = ""

            For g = 0 To ciudades.Length - 1
                If ciudades(g).Descripcion = UCase(ObjData.Cliente.Direccion.Poblacion) Then
                    IdCiudad = ciudades(g).Clave
                    Exit For
                End If
            Next

            Dim ben = objCotizacion.ObtenerTiposBeneficiarioPreferente()

            Dim ocupacion = ""

            If UCase(ObjData.Cliente.Ocupacion) = "COMERCIANTE" Then
                ocupacion = "710A"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "ESTUDIANTE" Then
                ocupacion = "995A"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "AMA DE CASA" Then
                ocupacion = "996A"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "EMPLEADO" Then
                ocupacion = "9BJG"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "EMPRESARIO" Then
                ocupacion = "9BJK"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "JUBILADO" Then
                ocupacion = "9BOL"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "PROFESIONISTA" Then
                ocupacion = "9D01"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = UCase("Actividades Agrícolas y Ganaderas") Then
                ocupacion = "9D02"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = UCase("Fuerzas de seguridad y Militares") Then
                ocupacion = "9D03"
            Else
                ocupacion = "9BJG"
            End If

            Dim CCobro = 0
            If ObjData.Pago.MedioPago = "CREDITO" Then
                CCobro = 4
            ElseIf ObjData.Pago.MedioPago = "DEBITO" Then
                CCobro = 5
            End If

            ObjMarcas.IdModelo = ObjData.Vehiculo.Modelo
            ObjMarcas.IdModeloSpecified = True
            ObjMarcas.IdTipoVehiculo = IdTipoVehiculo
            ObjMarcas.IdTipoVehiculoSpecified = True
            ObjMarcas.usuario = user


            Dim respuesta = objCotizacion.ObtenerTiposVehiculo(user)
            Dim respuestaMarca = objCotizacion.ObtenerMarcas(ObjMarcas)
            For i = 0 To respuestaMarca.Length - 1
                If respuestaMarca(i).Descripcion = UCase(ObjData.Vehiculo.Marca) Then
                    IdMarca = respuestaMarca(i).Clave
                    Exit For
                End If
            Next

            ObjSubmarcas.IdMarca = IdMarca
            ObjSubmarcas.IdMarcaSpecified = True
            ObjSubmarcas.IdModelo = ObjData.Vehiculo.Modelo
            ObjSubmarcas.IdModeloSpecified = True
            ObjSubmarcas.IdTipoVehiculo = IdTipoVehiculo
            ObjSubmarcas.IdTipoVehiculoSpecified = True
            Dim respuestaSubmarca = objCotizacion.ObtenerTipos(ObjSubmarcas)
            For i = 0 To respuestaSubmarca.Length - 1
                If TipoVehiculo(1).Contains(UCase(respuestaSubmarca(i).Descripcion)) Then
                    IdDescripcion = respuestaSubmarca(i).Clave
                    Exit For
                End If
            Next

            Objversiones.IdMarca = IdMarca
            Objversiones.IdMarcaSpecified = True
            Objversiones.IdModelo = ObjData.Vehiculo.Modelo
            Objversiones.IdModeloSpecified = True
            Objversiones.IdTipo = IdDescripcion
            Objversiones.IdTipoVehiculo = IdTipoVehiculo
            Objversiones.IdTipoVehiculoSpecified = True
            Objversiones.usuario = user
            Dim respuestaVersion = objCotizacion.ObtenerVersiones(Objversiones)

            For i = 0 To respuestaVersion.Length - 1
                If TipoVehiculo(1).Contains(UCase(respuestaVersion(i).Descripcion)) Then
                    IdVersion = respuestaVersion(i).Clave
                    Exit For
                End If
            Next
            Ajuste.PorcentajeAjuste = 0
            Ajuste.PorcentajeAjusteSpecified = True
            Ajuste.TipoAjuste = 4612
            Ajuste.TipoAjusteSpecified = True

            vigencia.Inicial = Date.Now
            vigencia.Inicial = Date.Now.AddYears(1)


            ObjTransmision.IdMarca = IdMarca
            ObjTransmision.IdMarcaSpecified = True
            ObjTransmision.IdModelo = CInt(ObjData.Vehiculo.Modelo)
            ObjTransmision.IdModeloSpecified = True
            ObjTransmision.IdTipo = IdDescripcion
            ObjTransmision.IdTipoVehiculo = IdTipoVehiculo
            ObjTransmision.IdTipoVehiculoSpecified = True
            ObjTransmision.IdVersion = IdVersion

            Dim respuestaTransmision = objCotizacion.ObtenerTransmisiones(ObjTransmision)
            IdTransmision = respuestaTransmision(0).Clave

            ObjPaqueteCoberturas.Clave = plan
            ObjPaqueteCoberturas.ClaveSpecified = True
            ObjPaqueteCoberturas.Vigencia = vigencia
            ObjPaqueteCoberturas.ListaDatosFormasPagos = New HDIService.DatosFormasPagos(1) {}
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0) = New HDIService.DatosFormasPagos()
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPago = IdFormaPago
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPagoSpecified = True
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaquete = plan
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaqueteSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias = New HDIService.Cobertura(2) {}
            ObjPaqueteCoberturas.CoberturasObligatorias(0) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Regla = 287
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Clave = 233
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Descripcion = "Daños Materiales"
            ObjPaqueteCoberturas.CoberturasObligatorias(0).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(0).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Deducible = 5
            ObjPaqueteCoberturas.CoberturasObligatorias(0).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(0).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).PrimaNeta = 12158.1190263578
            ObjPaqueteCoberturas.CoberturasObligatorias(0).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(0).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias(1) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Regla = 339
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Clave = 236
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Descripcion = "Robo Total"
            ObjPaqueteCoberturas.CoberturasObligatorias(1).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(1).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Deducible = 10
            ObjPaqueteCoberturas.CoberturasObligatorias(1).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(1).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).PrimaNeta = 3972.6375140569
            ObjPaqueteCoberturas.CoberturasObligatorias(1).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(1).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatorias(2) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Regla = 295
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Clave = 242
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Descripcion = "Responsabilidad Civil (Límite Único y Co"
            ObjPaqueteCoberturas.CoberturasObligatorias(2).SumaAsegurada = 1500000
            ObjPaqueteCoberturas.CoberturasObligatorias(2).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatorias(2).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ProveedorAsistencia = 6923
            ObjPaqueteCoberturas.CoberturasObligatorias(2).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).PrimaNeta = 312
            ObjPaqueteCoberturas.CoberturasObligatorias(2).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatorias(2).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales = New HDIService.Cobertura(3) {}
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Regla = 292
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Clave = 239
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Descripcion = "Gastos Médicos Ocupantes (Límite Único C"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).SumaAsegurada = 20000
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).PrimaNeta = 401.901562730822
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Regla = 317
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Clave = 264
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Descripcion = "Extensión de Responsabilidad Civil para Automóvil Particular"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).SumaAsegurada = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).PrimaNeta = 0.00
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Regla = 653
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Clave = 366
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Descripcion = "Responsabilidad Civil Exceso por Muerte"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).SumaAsegurada = 2000000
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).PrimaNeta = 251.72414
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).CalculadaSpecified = True

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3) = New HDIService.Cobertura()
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Regla = 398
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ReglaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Clave = 267
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ClaveSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Descripcion = "Responsabilidad Civil Familiar"
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).SumaAsegurada = 100000
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).SumaAseguradaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Deducible = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).DeducibleSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ProveedorAsistencia = 0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ProveedorAsistenciaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).PrimaNeta = 41.0
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).PrimaNetaSpecified = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Calculada = True
            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).CalculadaSpecified = True

            ObjPaqueteCoberturas.condiciones = New HDIService.CondicionesTarificar
            ObjPaqueteCoberturas.condiciones.DeducibleDM = 5
            ObjPaqueteCoberturas.condiciones.DeducibleDMSpecified = True
            ObjPaqueteCoberturas.condiciones.DeducibleRT = 10
            ObjPaqueteCoberturas.condiciones.DeducibleRTSpecified = True

            Ajuste.PorcentajeAjuste = CInt(ObjData.Descuento)
            Ajuste.PorcentajeAjusteSpecified = True
            Ajuste.TipoAjuste = 4612
            Ajuste.TipoAjusteSpecified = True

            ObjPaqueteCoberturas.Ajuste = Ajuste

            ObjDatosAdi.CPCirculacion = ObjData.Cliente.Direccion.CodPostal
            ObjDatosAdi.Renovaciones = 0
            ObjDatosAdi.idRemolque = 4603

            ObjVehiculo.idVehiculo = ObjData.Vehiculo.Clave
            ObjVehiculo.idMarca = IdMarca
            ObjVehiculo.idMarcaSpecified = True
            ObjVehiculo.idModelo = CInt(ObjData.Vehiculo.Modelo)
            ObjVehiculo.idModeloSpecified = True
            ObjVehiculo.idTipo = IdDescripcion
            ObjVehiculo.idVersion = IdVersion
            ObjVehiculo.idTransmision = IdTransmision
            ObjVehiculo.idTransmisionSpecified = True
            ObjVehiculo.idUso = IdUso
            ObjVehiculo.idUsoSpecified = True
            ObjVehiculo.tipoVehiculo = IdTipoVehiculo
            ObjVehiculo.tipoVehiculoSpecified = True
            ObjVehiculo.numeroMotor = ObjData.Vehiculo.NoMotor
            ObjVehiculo.placas = ObjData.Vehiculo.NoPlacas
            ObjVehiculo.numeroSerie = ObjData.Vehiculo.NoSerie
            ObjVehiculo.pasajeros = 0
            ObjVehiculo.pasajerosSpecified = True
            ObjVehiculo.idZonaCirculacion = 0
            ObjVehiculo.idZonaCirculacionSpecified = True
            ObjVehiculo.idTonelaje = 0
            ObjVehiculo.idTonelajeSpecified = True
            ObjVehiculo.idServicio = 4601
            ObjVehiculo.idServicioSpecified = True
            ObjVehiculo.idRiesgoCarga = 0
            ObjVehiculo.idRiesgoCargaSpecified = True
            ObjVehiculo.DatosAdicionales = ObjDatosAdi

            ObjDireccion.calle = ObjData.Cliente.Direccion.Calle
            ObjDireccion.ciudad = IdCiudad
            ObjDireccion.codigoPostal = ObjData.Cliente.Direccion.CodPostal
            ObjDireccion.colonia = ObjData.Cliente.Direccion.Colonia
            ObjDireccion.correoElectronico = ObjData.Cliente.Email
            ObjDireccion.nacionalidad = "002"
            ObjDireccion.estado = IdEstado
            ObjDireccion.pais = "00007"
            ObjDireccion.telefonoCasa = ObjData.Cliente.Telefono

            objCliente.actividadPolitica = False
            objCliente.actividadPoliticaSpecified = True
            objCliente.apellidoMaterno = ObjData.Cliente.ApellidoMat
            objCliente.apellidoPaterno = ObjData.Cliente.ApellidoPat
            objCliente.entidadGubernamental = 0
            objCliente.entidadGubernamentalSpecified = True
            objCliente.estadoCivil = "C"
            objCliente.fechaNacimiento = Convert.ToDateTime(fechaNac, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            objCliente.fechaNacimientoSpecified = True
            objCliente.giroActividad = "EMPLEADO"
            objCliente.ocupacion = UCase(ocupacion)
            objCliente.primerNombre = Nombres(0)
            If numeroNom = 2 Then
                objCliente.segundoNombre = Nombres(1)
            End If
            objCliente.rfc = ObjData.Cliente.RFC
            objCliente.sexo = sexo
            objCliente.sexoSpecified = True
            objCliente.Direccion = ObjDireccion

            ObjBenef.tipoBeneficiario = ben(0).Clave
            ObjBenef.nombreCompleto = ObjData.Cliente.Nombre & ObjData.Cliente.ApellidoPat & ObjData.Cliente.ApellidoMat


            ObjTarifExpres.IdPaquete = plan
            ObjTarifExpres.IdPaqueteSpecified = True
            ObjTarifExpres.ListaCondiciones = New HDIService.CondicionesTarificar(0) {}
            ObjTarifExpres.ListaCondiciones(0) = New HDIService.CondicionesTarificar()
            ObjTarifExpres.ListaCondiciones(0).DeducibleDM = 5
            ObjTarifExpres.ListaCondiciones(0).DeducibleDMSpecified = True
            ObjTarifExpres.ListaCondiciones(0).DeducibleRT = 10
            ObjTarifExpres.ListaCondiciones(0).DeducibleRTSpecified = True
            ObjTarifExpres.IdPromocion = 0
            ObjTarifExpres.ListaConfiguraciones = New HDIService.ConfiguracionPaquete(0) {}
            ObjTarifExpres.ListaConfiguraciones(0) = New HDIService.ConfiguracionPaquete()
            ObjTarifExpres.ListaConfiguraciones(0).UsuarioCotiza = user
            ObjTarifExpres.ListaConfiguraciones(0).InicioVigencia = Date.Now
            ObjTarifExpres.ListaConfiguraciones(0).InicioVigenciaSpecified = True
            ObjTarifExpres.ListaConfiguraciones(0).FinVigencia = Date.Now.AddYears(1)
            ObjTarifExpres.ListaConfiguraciones(0).FinVigenciaSpecified = True
            ObjTarifExpres.ListaConfiguraciones(0).EsCotizacionDiasPorMes = False
            ObjTarifExpres.ListaConfiguraciones(0).EsCotizacionDiasPorMesSpecified = True
            ObjTarifExpres.ListaConfiguraciones(0).Descuento = CInt(ObjData.Descuento)
            ObjTarifExpres.ListaConfiguraciones(0).DescuentoSpecified = True
            ObjTarifExpres.ListaConfiguraciones(0).AgenteCotiza = user

            ObjInfCorredor.FechaEmisionFactura = Date.Now


            ObjParam.PaqueteCoberturas = ObjPaqueteCoberturas
            ObjParam.CaracteristicasVehiculo = ObjVehiculo
            ObjParam.Cliente = objCliente
            ObjParam.fechaInicioVigencia = Date.Now
            ObjParam.fechaInicioVigenciaSpecified = True
            ObjParam.idFormaPago = IdFormaPago
            ObjParam.idFormaPagoSpecified = True
            ObjParam.idConductoCobro = CCobro
            ObjParam.idConductoCobroSpecified = True
            ObjParam.idEstado = IdEstado
            ObjParam.idCiudad = IdCiudad
            ObjParam.BeneficiarioPreferente = ObjBenef
            ObjParam.PaqueteEmitir = ObjTarifExpres

            ObjParam.IDTipoSumaAsegurada = 4452
            ObjParam.IDTipoSumaAseguradaSpecified = True
            ObjParam.SumaAsegurada = 0
            ObjParam.SumaAseguradaSpecified = True
            ObjParam.InformacionAdicionalCorredor = ObjInfCorredor
            ObjParam.porcentajeIva = 0
            ObjParam.porcentajeIvaSpecified = True

            ObjData.Pago.AnioExp = ObjData.Pago.AnioExp.Substring(2, 2)

            ObjInfoPago.idBanco = IdBanco
            ObjInfoPago.idBancoSpecified = True
            ObjInfoPago.idTipoTarjeta = Pago.CatCarrier.VISA
            ObjInfoPago.idTipoTarjetaSpecified = True
            ObjInfoPago.nombreTitularTarjeta = ObjData.Pago.NombreTarjeta
            ObjInfoPago.numCuentaHabiente = ObjData.Pago.NoTarjeta
            ObjInfoPago.numCodigoSeguridad = ObjData.Pago.CodigoSeguridad
            ObjInfoPago.numVencimientoTarjeta = ObjData.Pago.MesExp & ObjData.Pago.AnioExp

            ObjParam.InformacionPago = ObjInfoPago



            Try
                Dim x2 As New XmlSerializer(ObjParam.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, ObjParam)
                Dim errorws3 As String = xml2.ToString
                FuncionesGeneral.xmlWS(errorws3, ObjData, "Emision", "EmisionEnvio")
            Catch ex As Exception

            End Try

            Dim res = objCotizacion.EmitirCotizacion(ObjParam, user)

            Try
                Dim x3 As New XmlSerializer(res.GetType)
                Dim xml3 As New StringWriter
                x3.Serialize(xml3, res)
                Dim errorws4 As String = xml3.ToString
                FuncionesGeneral.xmlWS(errorws4, ObjData, "Emision", "EmisionRespuesta")
            Catch ex As Exception

            End Try
            ObjData.Emision.Poliza = res.idPoliza
            Dim oficina As String = res.idOficina
            Dim cotizacion = res.idCotizacion

            Dim ObjImprimir As New HDIService.PolizaAImprimir

            ObjImprimir.Cotizacion = cotizacion
            ObjImprimir.NumeroPoliza = ObjData.Emision.Poliza
            ObjImprimir.usuario = user
            ObjImprimir.Agencia = oficina
            If ObjData.Emision.Poliza <> Nothing Or ObjData.Emision.Poliza <> "" Then
                Dim URL = objCotizacion.ObtenerPolizaPDF(ObjImprimir)
                Dim ruta As String = "W:\Documentos\Proyecto\ws-ali-ahorraseguros-prod\Archivo\"
                File.WriteAllBytes(ruta + ObjData.Emision.Poliza + ".pdf", URL.ByteArray)
                ObjData.Emision.Documento = "https://ws-se.com/Archivo/" + ObjData.Emision.Poliza + ".pdf"
                ObjData.Emision.Resultado = "True"
                ObjData.Emision.PrimaTotal = ObjData.Cotizacion.PrimaTotal
                ObjData.Emision.PrimaNeta = ObjData.Cotizacion.PrimaNeta
                ObjData.Emision.Derechos = ObjData.Cotizacion.Derechos
                ObjData.Emision.Impuesto = ObjData.Cotizacion.Impuesto
                ObjData.Emision.PrimerPago = ObjData.Cotizacion.PrimerPago
                ObjData.Emision.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes

            Else
                For i = 0 To res.errores.Length - 1
                    ObjData.CodigoError = ObjData.CodigoError + " " + res.errores(i).descripcion
                    Exit For
                Next
                ObjData.Emision.Resultado = "False"
            End If

        Catch ex As Exception
            ObjData.Emision.Resultado = "False"

            ObjData.CodigoError = ex.Message.ToString
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)

    End Function

    Private Shared Function obtenerCveBanco(ByVal nBanco As String) As Integer
        Select Case UCase(nBanco)
            Case "BANAMEX"
                Return 2
            Case "HSBC"
                Return 6
            Case "CITIBANK"
                Return 6
            Case "NAFIN"
                Return 8
            Case "SANTANDER"
                Return 11
            Case "BBVA BANCOMER"
                Return 12
            Case "BNCI"
                Return 14
            Case "ATLANTICO"
                Return 15
            Case "JP MORGAN"
                Return 20
            Case "BANCO INDUSTRIAL"
                Return 21
            Case "PROMEX"
                Return 25
            Case "BANJERCITO"
                Return 26
            Case "BANORTE"
                Return 28
            Case "BAJIO"
                Return 30
            Case "BANCEN"
                Return 31
            Case "INBURSA"
                Return 33
            Case "INTERACCIONES"
                Return 34
            Case "BANOBRAS"
                Return 38
            Case "INVEX"
                Return 43
            Case "AFIRME"
                Return 44
            Case "IXE"
                Return 52
            Case "GE MONEY"
                Return 87
            Case "BANSI"
                Return 88
            Case "BANK BOSTON"
                Return 95
            Case "SCOTIABANK"
                Return 96
            Case "BANK ONE"
                Return 98
            Case "DEUTSCHE BANK"
                Return 108
            Case "BMULTIVA"
                Return 111
            Case "CITIBANK"
                Return 116
            Case "BANREGIO"
                Return 117
            Case "ALBERTA"
                Return 120
            Case "ING BANK"
                Return 127
            Case "VE POR MAS"
                Return 129
            Case "WAL-MART"
                Return 135
            Case "BANCOPPEL"
                Return 137
        End Select
        Return "ERROR EN EL BANCO"
    End Function
    Private Shared Function UnicodeBytesToString(ByVal bytes() As Byte) As String
        Return System.Text.ASCIIEncoding.ASCII.GetString(bytes)
    End Function

    Private Shared Function GetDatosVehiculo(ByVal Clave As String, ByVal modelo As String) As String()
        Dim strSQLQueryCPostal As String
        Dim Tipo(7) As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQueryCPostal = "Select * from `Ali_R_catalogoHDI` where amis = '" & Clave & "' and modelo='" & modelo & "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Tipo(0) = reader("amis")
                    Tipo(1) = reader("tipovehiculo")
                    Tipo(2) = reader("modelo")
                    Tipo(3) = reader("IdMarca")
                    Tipo(4) = reader("tipo")
                    Tipo(5) = reader("version")
                    Tipo(6) = reader("transmision")
                End While
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Tipo
    End Function
    Private Shared Function GuardarCotizacion(ByVal ObjData As Seguro) As String
        Dim objCotizacion As New HDIService.PublicServicesAutosContractClient()
        objCotizacion.ClientCredentials.UserName.UserName = user
        objCotizacion.ClientCredentials.UserName.Password = psw
        objCotizacion.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.PeerTrust
        objCotizacion.ClientCredentials.CreateSecurityTokenManager()
        objCotizacion.ClientCredentials.Clone()
        Dim ObjParam As New HDIService.DatosCotizacion
        Dim ObjPaqueteCoberturas As New HDIService.PaqueteCoberturas
        Dim Ajuste As New HDIService.Ajuste
        Dim ObjVehiculo As New HDIService.CaracteristicasVehiculo
        Dim ObjDatosAdi As New HDIService.DatosAdicionalesGLM
        Dim objCliente As New HDIService.Cliente
        Dim ObjDireccion As New HDIService.Direccion
        Dim ObjTarifExpres As New HDIService.PaqueteTarificarExpress
        Dim idCotizacion = ""
        Dim paquete As String = ObjData.Paquete
        Dim plan As String
        Dim tipvhc As String = "AUT"
        If UCase(paquete) = "AMPLIA" And UCase(tipvhc) = "AUT" Then
            plan = "19"
        ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc) = "AUT" Then
            plan = "21"
        ElseIf UCase(paquete) = "RC" And UCase(tipvhc) = "AUT" Then
            plan = "22"
        ElseIf UCase(paquete) = "AMPLIA" And UCase(tipvhc) = "PKP" Then
            plan = "23"
        ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc) = "PKP" Then
            plan = "24"
        ElseIf UCase(paquete) = "RC" And UCase(tipvhc) = "PKP" Then
            plan = "25"
        End If
        Dim vigencia As New HDIService.Vigencia
        vigencia.Inicial = Date.Now
        vigencia.Inicial = Date.Now.AddYears(1)

        Dim FormasPago = objCotizacion.ObtenerFormasPago()
        Dim IdFormaPago As Integer = 0
        Dim FormaPago As String = ""
        For i = 0 To FormasPago.Length - 1
            If FormasPago(i).Descripcion = UCase(FormaPago) Then
                IdFormaPago = FormasPago(i).Clave
                Exit For
            End If
        Next
        Dim IdMarca As Integer
        Dim IdDescripcion As String = ""
        Dim IdVersion As String = ""
        Dim IdTransmision As String = ""
        Dim IdUso As String = ""
        IdUso = 4581
        Dim DatosVehiculo = GetDatosVehiculo(ObjData.Vehiculo.Clave, ObjData.Vehiculo.Modelo)
        Dim TipoVehiculo = DatosVehiculo(1)
        IdMarca = DatosVehiculo(3)
        IdDescripcion = DatosVehiculo(4)
        IdVersion = DatosVehiculo(5)
        IdTransmision = DatosVehiculo(6)
        Dim IdTipoVehiculo As Integer = 0
        If TipoVehiculo(0) = "PKP" Then
            IdTipoVehiculo = 3829
        ElseIf TipoVehiculo(0) = "AUT" Then
            IdTipoVehiculo = 4579
        End If
        Dim fechaNac As String = "19/11/1988"
        Dim sexo As Integer = 0
        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            sexo = 1
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            sexo = 2
        End If

        Dim Estados = objCotizacion.ObtenerEstados("00007")
        Dim IdEstado = ""
        For g = 0 To Estados.Length - 1
            If Estados(g).Descripcion = UCase("CIUDAD DE MÉXICO") Then
                IdEstado = Estados(g).Clave
                Exit For
            End If
        Next

        Dim ciudades = objCotizacion.ObtenerCiudades(IdEstado)
        Dim IdCiudad = ""

        For g = 0 To ciudades.Length - 1
            If ciudades(g).Descripcion = UCase("COYOACAN") Then
                IdCiudad = ciudades(g).Clave
                Exit For
            End If
        Next

        ObjPaqueteCoberturas.Clave = plan
        ObjPaqueteCoberturas.ClaveSpecified = True
        ObjPaqueteCoberturas.Vigencia = vigencia
        ObjPaqueteCoberturas.ListaDatosFormasPagos = New HDIService.DatosFormasPagos(1) {}
        ObjPaqueteCoberturas.ListaDatosFormasPagos(0) = New HDIService.DatosFormasPagos()
        ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPago = IdFormaPago
        ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPagoSpecified = True
        ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaquete = plan
        ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaqueteSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatorias = New HDIService.Cobertura(2) {}
        ObjPaqueteCoberturas.CoberturasObligatorias(0) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatorias(0).Regla = 287
        ObjPaqueteCoberturas.CoberturasObligatorias(0).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).Clave = 233
        ObjPaqueteCoberturas.CoberturasObligatorias(0).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).Descripcion = "Daños Materiales"
        ObjPaqueteCoberturas.CoberturasObligatorias(0).SumaAsegurada = 0
        ObjPaqueteCoberturas.CoberturasObligatorias(0).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).Deducible = 5
        ObjPaqueteCoberturas.CoberturasObligatorias(0).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).ProveedorAsistencia = 0
        ObjPaqueteCoberturas.CoberturasObligatorias(0).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).PrimaNeta = 12158.1190263578
        ObjPaqueteCoberturas.CoberturasObligatorias(0).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatorias(0).CalculadaSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatorias(1) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatorias(1).Regla = 339
        ObjPaqueteCoberturas.CoberturasObligatorias(1).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).Clave = 236
        ObjPaqueteCoberturas.CoberturasObligatorias(1).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).Descripcion = "Robo Total"
        ObjPaqueteCoberturas.CoberturasObligatorias(1).SumaAsegurada = 0
        ObjPaqueteCoberturas.CoberturasObligatorias(1).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).Deducible = 10
        ObjPaqueteCoberturas.CoberturasObligatorias(1).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).ProveedorAsistencia = 0
        ObjPaqueteCoberturas.CoberturasObligatorias(1).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).PrimaNeta = 3972.6375140569
        ObjPaqueteCoberturas.CoberturasObligatorias(1).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatorias(1).CalculadaSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatorias(2) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatorias(2).Regla = 295
        ObjPaqueteCoberturas.CoberturasObligatorias(2).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).Clave = 242
        ObjPaqueteCoberturas.CoberturasObligatorias(2).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).Descripcion = "Responsabilidad Civil (Límite Único y Co"
        ObjPaqueteCoberturas.CoberturasObligatorias(2).SumaAsegurada = 1500000
        ObjPaqueteCoberturas.CoberturasObligatorias(2).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).Deducible = 0
        ObjPaqueteCoberturas.CoberturasObligatorias(2).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).ProveedorAsistencia = 6923
        ObjPaqueteCoberturas.CoberturasObligatorias(2).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).PrimaNeta = 312
        ObjPaqueteCoberturas.CoberturasObligatorias(2).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatorias(2).CalculadaSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales = New HDIService.Cobertura(3) {}
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Regla = 292
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Clave = 239
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Descripcion = "Gastos Médicos Ocupantes (Límite Único C"
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).SumaAsegurada = 20000
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Deducible = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ProveedorAsistencia = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).PrimaNeta = 401.901562730822
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(0).CalculadaSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Regla = 317
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Clave = 264
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Descripcion = "Extensión de Responsabilidad Civil para Automóvil Particular"
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).SumaAsegurada = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Deducible = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ProveedorAsistencia = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).PrimaNeta = 0.00
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(1).CalculadaSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Regla = 653
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Clave = 366
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Descripcion = "Responsabilidad Civil Exceso por Muerte"
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).SumaAsegurada = 2000000
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Deducible = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ProveedorAsistencia = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).PrimaNeta = 251.72414
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(2).CalculadaSpecified = True

        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3) = New HDIService.Cobertura()
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Regla = 398
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ReglaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Clave = 267
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ClaveSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Descripcion = "Responsabilidad Civil Familiar"
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).SumaAsegurada = 100000
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).SumaAseguradaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Deducible = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).DeducibleSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ProveedorAsistencia = 0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).ProveedorAsistenciaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).PrimaNeta = 41.0
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).PrimaNetaSpecified = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).Calculada = True
        ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales(3).CalculadaSpecified = True

        ObjPaqueteCoberturas.condiciones = New HDIService.CondicionesTarificar
        ObjPaqueteCoberturas.condiciones.DeducibleDM = 5
        ObjPaqueteCoberturas.condiciones.DeducibleDMSpecified = True
        ObjPaqueteCoberturas.condiciones.DeducibleRT = 10
        ObjPaqueteCoberturas.condiciones.DeducibleRTSpecified = True

        Ajuste.PorcentajeAjuste = CInt(ObjData.Descuento)
        Ajuste.PorcentajeAjusteSpecified = True
        Ajuste.TipoAjuste = 4612
        Ajuste.TipoAjusteSpecified = True

        ObjPaqueteCoberturas.Ajuste = Ajuste

        ObjDatosAdi.CPCirculacion = ObjData.Cliente.Direccion.CodPostal
        ObjDatosAdi.Renovaciones = 0
        ObjDatosAdi.idRemolque = 4603
        Dim numero As String = numeroAleatorio()

        ObjVehiculo.idVehiculo = ObjData.Vehiculo.Clave
        ObjVehiculo.idMarca = IdMarca
        ObjVehiculo.idMarcaSpecified = True
        ObjVehiculo.idModelo = CInt(ObjData.Vehiculo.Modelo)
        ObjVehiculo.idModeloSpecified = True
        ObjVehiculo.idTipo = IdDescripcion
        ObjVehiculo.idVersion = IdVersion
        ObjVehiculo.idTransmision = IdTransmision
        ObjVehiculo.idTransmisionSpecified = True
        ObjVehiculo.idUso = IdUso
        ObjVehiculo.idUsoSpecified = True
        ObjVehiculo.tipoVehiculo = IdTipoVehiculo
        ObjVehiculo.tipoVehiculoSpecified = True
        ObjVehiculo.numeroMotor = ObjData.Vehiculo.NoMotor
        ObjVehiculo.placas = ObjData.Vehiculo.NoPlacas
        ObjVehiculo.numeroSerie = "114422" & numero
        ObjVehiculo.pasajeros = 0
        ObjVehiculo.pasajerosSpecified = True
        ObjVehiculo.idZonaCirculacion = 0
        ObjVehiculo.idZonaCirculacionSpecified = True
        ObjVehiculo.idTonelaje = 0
        ObjVehiculo.idTonelajeSpecified = True
        ObjVehiculo.idServicio = 4601
        ObjVehiculo.idServicioSpecified = True
        ObjVehiculo.idRiesgoCarga = 0
        ObjVehiculo.idRiesgoCargaSpecified = True
        ObjVehiculo.DatosAdicionales = ObjDatosAdi

        objCliente.idTipoPersona = "00"
        objCliente.actividadPolitica = False
        objCliente.actividadPoliticaSpecified = True
        objCliente.apellidoMaterno = "PEREZ"
        objCliente.apellidoPaterno = "GOMEZ"
        objCliente.entidadGubernamental = 0
        objCliente.entidadGubernamentalSpecified = True
        objCliente.estadoCivil = "C"
        objCliente.fechaNacimiento = Convert.ToDateTime(fechaNac, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
        objCliente.fechaNacimientoSpecified = True
        objCliente.giroActividad = "EMPLEADO"
        objCliente.ocupacion = "710A"
        objCliente.primerNombre = "LUIS"
        objCliente.rfc = "PEGL881119"
        objCliente.sexo = sexo
        objCliente.sexoSpecified = True
        objCliente.Direccion = ObjDireccion

        ObjDireccion.calle = "CIPRESES"
        ObjDireccion.ciudad = IdCiudad
        ObjDireccion.codigoPostal = ObjData.Cliente.Direccion.CodPostal
        ObjDireccion.colonia = "JORGE RUIZ"
        ObjDireccion.nacionalidad = "002"
        ObjDireccion.estado = IdEstado
        ObjDireccion.pais = "00007"
        ObjDireccion.telefonoCasa = "5555555555"

        ObjTarifExpres.IdPaquete = plan
        ObjTarifExpres.IdPaqueteSpecified = True
        ObjTarifExpres.ListaCondiciones = New HDIService.CondicionesTarificar(0) {}
        ObjTarifExpres.ListaCondiciones(0) = New HDIService.CondicionesTarificar()
        ObjTarifExpres.ListaCondiciones(0).DeducibleDM = 5
        ObjTarifExpres.ListaCondiciones(0).DeducibleDMSpecified = True
        ObjTarifExpres.ListaCondiciones(0).DeducibleRT = 10
        ObjTarifExpres.ListaCondiciones(0).DeducibleRTSpecified = True
        ObjTarifExpres.IdPromocion = 0
        ObjTarifExpres.ListaConfiguraciones = New HDIService.ConfiguracionPaquete(0) {}
        ObjTarifExpres.ListaConfiguraciones(0) = New HDIService.ConfiguracionPaquete()
        ObjTarifExpres.ListaConfiguraciones(0).UsuarioCotiza = user
        ObjTarifExpres.ListaConfiguraciones(0).InicioVigencia = Date.Now
        ObjTarifExpres.ListaConfiguraciones(0).InicioVigenciaSpecified = True
        ObjTarifExpres.ListaConfiguraciones(0).FinVigencia = Date.Now.AddYears(1)
        ObjTarifExpres.ListaConfiguraciones(0).FinVigenciaSpecified = True
        ObjTarifExpres.ListaConfiguraciones(0).EsCotizacionDiasPorMes = False
        ObjTarifExpres.ListaConfiguraciones(0).EsCotizacionDiasPorMesSpecified = True
        ObjTarifExpres.ListaConfiguraciones(0).Descuento = CInt(ObjData.Descuento)
        ObjTarifExpres.ListaConfiguraciones(0).DescuentoSpecified = True
        ObjTarifExpres.ListaConfiguraciones(0).AgenteCotiza = user

        ObjParam.PaqueteCoberturas = ObjPaqueteCoberturas
        ObjParam.CaracteristicasVehiculo = ObjVehiculo
        ObjParam.Cliente = objCliente
        ObjParam.fechaInicioVigencia = Date.Now
        ObjParam.fechaInicioVigenciaSpecified = True
        ObjParam.idFormaPago = IdFormaPago
        ObjParam.idFormaPagoSpecified = True
        ObjParam.idConductoCobro = 1
        ObjParam.idConductoCobroSpecified = True
        ObjParam.idEstado = IdEstado
        ObjParam.idCiudad = IdCiudad
        ObjParam.PaqueteEmitir = ObjTarifExpres

        ObjParam.IDTipoSumaAsegurada = 4452
        ObjParam.IDTipoSumaAseguradaSpecified = True
        ObjParam.SumaAsegurada = 0
        ObjParam.SumaAseguradaSpecified = True
        ObjParam.porcentajeIva = 0
        ObjParam.porcentajeIvaSpecified = True

        Dim res = objCotizacion.GuardarCotizacion(ObjParam, user)
        idCotizacion = res.idCotizacion
        Return idCotizacion
    End Function

    Private Shared Function numeroAleatorio() As String
        Dim numero As Random = New Random()
        Dim respuesta = numero.Next(1000, 1999).ToString()
        Return respuesta
    End Function

End Class
