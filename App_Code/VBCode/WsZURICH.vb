﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Xml.Serialization
Imports SuperObjeto
Imports System.Net

Public Class WsZURICH
    Public Shared Function ZURICHCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)

        Try
            CallWebService(ObjData, idLogWSCot)
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Dim serializer As New JavaScriptSerializer
        Dim lg As Log = New Log()
        Dim jsonResponse As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(jsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Sub CallWebService(ObjData As Seguro, ByVal idLogWSCot As String)
        Dim _url = "http://pruebas.autolinea.ezurich.com.mx:80/ZurichWS_QA/autos/solCotV2/publicService"

        Dim soapEnvelopeXml As XmlDocument = CreateSoapEnvelope(ObjData)
        Dim webRequest As HttpWebRequest = CreateWebRequest(_url, "")
        InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest)
        Dim FormaPago As String = ""
        Dim PaqueteZurich = ""
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            FormaPago = "CONTADO"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            FormaPago = "MENSUAL"
        End If
        If ObjData.Paquete = "AMPLIA" Then
            PaqueteZurich = "AMPLIO"
        ElseIf ObjData.Paquete = "LIMITADA" Then
            PaqueteZurich = "LIMITADO"
        ElseIf ObjData.Paquete = "RC" Then
            PaqueteZurich = "R.C."
        End If
        ' begin async call to web request.
        Dim asyncResult As IAsyncResult = webRequest.BeginGetResponse(Nothing, Nothing)

        ' suspend this thread until call is complete. You might want to
        ' do something usefull here like update your UI.
        asyncResult.AsyncWaitHandle.WaitOne()

        ' get the response from the completed web request.
        Dim soapResult As String
        Dim nombreCobertura As String = ""
        Dim doc As XmlDocument = New XmlDocument()
        Dim Coberturas As New Coberturas
        Dim lg As Log = New Log()

        Try
            Dim x2 As New XmlSerializer(soapEnvelopeXml.GetType)
            Dim xml2 As New StringWriter
            x2.Serialize(xml2, soapEnvelopeXml)
            Dim xmlRespuesta As String = xml2.ToString
            lg.UpdateLogRequest(xmlRespuesta, idLogWSCot)

            ' Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "RequestWS", "FechaInicio")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Using webResponse As WebResponse = webRequest.EndGetResponse(asyncResult)
            Using rd As New StreamReader(webResponse.GetResponseStream())
                soapResult = rd.ReadToEnd()
            End Using
            doc.LoadXml(soapResult)
        End Using

        Try
            lg.UpdateLogResponse(soapResult, idLogWSCot)
            '  Funciones.updateLogWSCot(soapResult, idLogWSCot, "ResponseWS", "FechaFin")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Dim colElementos As XmlNodeList = doc.GetElementsByTagName("tns:PAQUETE")
        For Each objNodoPaquete In colElementos
            Dim paquete = objNodoPaquete("tns:descripcion_paquete").InnerText
            If (paquete.ToUpper).Contains(PaqueteZurich) Then
                Dim xmlnode2 As XmlNodeList = objNodoPaquete.GetElementsByTagName("tns:FORMA_PAGO")
                Dim xmlnode3 As XmlNodeList = objNodoPaquete.GetElementsByTagName("tns:COBERTURA")
                For Each objnodo4 In xmlnode2
                    Dim fpago = (objnodo4("tns:descripcion_forma_pago").InnerText).ToUpper
                    If FormaPago = fpago Then
                        ObjData.Cotizacion.PrimaNeta = objnodo4("tns:prima_neta").InnerText
                        ObjData.Cotizacion.Derechos = objnodo4("tns:derechos").InnerText
                        ObjData.Cotizacion.Impuesto = objnodo4("tns:iva").InnerText
                        ObjData.Cotizacion.PrimaTotal = objnodo4("tns:prima_total").InnerText
                        ObjData.Cotizacion.Recargos = objnodo4("tns:recargos").InnerText
                        ObjData.Cotizacion.PrimerPago = objnodo4("tns:pago_inicial").InnerText
                        ObjData.Cotizacion.PagosSubsecuentes = objnodo4("tns:pago_subsecuente").InnerText
                        ObjData.Cotizacion.Resultado = "True"
                        Exit For
                    End If
                Next
                For Each objnodo4 In xmlnode3
                    Dim cobertura As String = objnodo4("tns:id_cobertura").InnerText
                    Dim descripcionCobertura As String = ""
                    Dim suma = objnodo4("tns:monto_asegurado").InnerText
                    If suma = "1" Then
                        suma = "AMPARADA"
                    End If
                    Select Case cobertura

                        Case "341"
                            nombreCobertura = "DAÑOS MATERIALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.DanosMateriales = descripcionCobertura
                        Case "331"
                            nombreCobertura = "ROBO TOTAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.RoboTotal = descripcionCobertura
                        Case "312"
                            nombreCobertura = "RESPONSABILIDAD CIVIL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.RC = descripcionCobertura
                        Case "352"
                            nombreCobertura = "GASTOS MÉDICOS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.GastosMedicosOcupantes = descripcionCobertura
                        Case "326"
                            nombreCobertura = "GASTOS LEGALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.DefensaJuridica = descripcionCobertura
                        Case "681"
                            nombreCobertura = "ASISTENCIA VIAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.AsitenciaCompleta = descripcionCobertura
                        Case "826"
                            nombreCobertura = "RC EN EL EXTRANJERO"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.RCExtranjero = descripcionCobertura
                        Case "737"
                            nombreCobertura = "RC FAMILIAR"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("tns:porcentaje_deducible").InnerText
                            Coberturas.RCFamiliar = descripcionCobertura
                    End Select
                Next
            End If
        Next
        ObjData.Coberturas.Add(Coberturas)

    End Sub


    Private Shared Function CreateWebRequest(url As String, action As String) As HttpWebRequest
        Dim webRequest__1 As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
        webRequest__1.Accept = "text/xml"
        webRequest__1.Method = "POST"
        Return webRequest__1
    End Function

    Private Shared Function CreateSoapEnvelope(ObjData As Seguro) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        Dim Finicio = Date.Now.ToString("yyyyMMdd")
        Dim FFin = Date.Now.AddYears(1).ToString("yyyyMMdd")
        Dim Genero = ""
        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = "H"
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = "M"
        End If
        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:web=""http://webservices.zurich.com/""><soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd""><UsernameToken xmlns=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd""><Username>tri74066ws</Username><Password>tri74066ws</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:SOLICITUD_COT_AUTOS_REQ><web:num_req>8</web:num_req><web:usuario>tri74066ws</web:usuario><web:idOficina>74</web:idOficina><web:programaComercial>65305022</web:programaComercial><web:tipoVehiculo>1</web:tipoVehiculo><web:cve_zurich>" & ObjData.Vehiculo.Clave & "</web:cve_zurich><web:modelo>" & ObjData.Vehiculo.Modelo & "</web:modelo><web:id_estado>0</web:id_estado><web:id_ciudad>0</web:id_ciudad><web:id_tipoValor>7</web:id_tipoValor><web:id_tipoUso>1</web:id_tipoUso><web:cve_agente>74066</web:cve_agente><web:tipo_producto>0</web:tipo_producto><web:tipo_carga>0</web:tipo_carga><web:tipo_persona>F</web:tipo_persona><web:edad>" & ObjData.Cliente.Edad & "</web:edad><web:genero>" & Genero & "</web:genero><web:estadoCivil>7</web:estadoCivil><web:ocupacion>1</web:ocupacion><web:giro>1</web:giro><web:nacionalidad>46</web:nacionalidad><web:id_moneda>0</web:id_moneda><web:fecha_inicio>" & Finicio & "</web:fecha_inicio><web:fecha_fin>" & FFin & "</web:fecha_fin><web:monto_asegurado>0</web:monto_asegurado><web:codigoPostal>" & ObjData.Cliente.Direccion.CodPostal & "</web:codigoPostal><web:situacionVehiculo></web:situacionVehiculo><web:mesesVigencia>12</web:mesesVigencia><web:tipoMovimiento>1</web:tipoMovimiento><web:polizaAnterior>0</web:polizaAnterior></web:SOLICITUD_COT_AUTOS_REQ></soapenv:Body></soapenv:Envelope>")
        Return soapEnvelop
    End Function

    Private Shared Sub InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml As XmlDocument, webRequest As HttpWebRequest)
        Using stream As Stream = webRequest.GetRequestStream()
            soapEnvelopeXml.Save(stream)
        End Using
    End Sub

    Public Shared Sub CallWebServiceEmision(ObjData As Seguro)
        Dim _url = "http://pruebas.autolinea.ezurich.com.mx:80/ZurichWS_QA/autos/solCotV2/publicService"

        Dim soapEnvelopeXml As XmlDocument = CreateSoapEnvelopeEmision(ObjData)
        Dim webRequest As HttpWebRequest = CreateWebRequest(_url, "")
        InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest)
        Dim FormaPago As String = ""
        Dim PaqueteZurich = ""

        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            FormaPago = "CONTADO"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            FormaPago = "MENSUAL"
        End If
        If ObjData.Paquete = "AMPLIA" Then
            PaqueteZurich = "AMPLIO"
        ElseIf ObjData.Paquete = "LIMITADA" Then
            PaqueteZurich = "LIMITADO"
        ElseIf ObjData.Paquete = "RC" Then
            PaqueteZurich = "R.C."
        End If
        ' begin async call to web request.
        Dim asyncResult As IAsyncResult = webRequest.BeginGetResponse(Nothing, Nothing)

        ' suspend this thread until call is complete. You might want to
        ' do something usefull here like update your UI.
        asyncResult.AsyncWaitHandle.WaitOne()

        ' get the response from the completed web request.
        Dim soapResult As String
        Dim doc As XmlDocument = New XmlDocument()

        Using webResponse As WebResponse = webRequest.EndGetResponse(asyncResult)
            Using rd As New StreamReader(webResponse.GetResponseStream())
                soapResult = rd.ReadToEnd()
            End Using
            doc.LoadXml(soapResult)
        End Using
        Dim colElementos As XmlNodeList = doc.GetElementsByTagName("tns:PAQUETE")
        For Each objNodoPaquete In colElementos
            Dim paquete = objNodoPaquete("tns:descripcion_paquete").InnerText
            If (paquete.ToUpper).Contains(PaqueteZurich) Then
                Dim xmlnode2 As XmlNodeList = objNodoPaquete.GetElementsByTagName("tns:FORMA_PAGO")
                For Each objnodo4 In xmlnode2
                    Dim fpago = (objnodo4("tns:descripcion_forma_pago").InnerText).ToUpper
                    If FormaPago = fpago Then
                        ObjData.Cotizacion.PrimaNeta = objnodo4("tns:prima_neta").InnerText
                        ObjData.Cotizacion.Derechos = objnodo4("tns:derechos").InnerText
                        ObjData.Cotizacion.Impuesto = objnodo4("tns:iva").InnerText
                        ObjData.Cotizacion.PrimaTotal = objnodo4("tns:prima_total").InnerText
                        ObjData.Cotizacion.Recargos = objnodo4("tns:recargos").InnerText
                        ObjData.Cotizacion.PrimerPago = objnodo4("tns:pago_inicial").InnerText
                        ObjData.Cotizacion.PagosSubsecuentes = objnodo4("tns:pago_subsecuente").InnerText
                        ObjData.Cotizacion.Resultado = "True"
                    End If
                Next
            End If
        Next
    End Sub
    Private Shared Function CreateSoapEnvelopeEmision(ObjData As Seguro) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        Dim Finicio = Date.Now.ToString("yyyyMMdd")
        Dim FFin = Date.Now.AddYears(1).ToString("yyyyMMdd")
        Dim Genero = ""
        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = "H"
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = "M"
        End If
        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:web=""http://webservices.zurich.com/""><soapenv:Header><wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd""><UsernameToken xmlns=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd""><Username>tri74066ws</Username><Password>tri74066ws</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:SOLICITUD_COT_AUTOS_REQ><web:num_req>8</web:num_req><web:usuario>tri74066ws</web:usuario><web:idOficina>74</web:idOficina><web:programaComercial>65305022</web:programaComercial><web:tipoVehiculo>1</web:tipoVehiculo><web:cve_zurich>" & ObjData.Vehiculo.Clave & "</web:cve_zurich><web:modelo>" & ObjData.Vehiculo.Modelo & "</web:modelo><web:id_estado>0</web:id_estado><web:id_ciudad>0</web:id_ciudad><web:id_tipoValor>7</web:id_tipoValor><web:id_tipoUso>1</web:id_tipoUso><web:cve_agente>74066</web:cve_agente><web:tipo_producto>0</web:tipo_producto><web:tipo_carga>0</web:tipo_carga><web:tipo_persona>F</web:tipo_persona><web:edad>" & ObjData.Cliente.Edad & "</web:edad><web:genero>" & Genero & "</web:genero><web:estadoCivil>7</web:estadoCivil><web:ocupacion>1</web:ocupacion><web:giro>1</web:giro><web:nacionalidad>46</web:nacionalidad><web:id_moneda>0</web:id_moneda><web:fecha_inicio>" & Finicio & "</web:fecha_inicio><web:fecha_fin>" & FFin & "</web:fecha_fin><web:monto_asegurado>0</web:monto_asegurado><web:codigoPostal>" & ObjData.Cliente.Direccion.CodPostal & "</web:codigoPostal><web:situacionVehiculo></web:situacionVehiculo><web:mesesVigencia>12</web:mesesVigencia><web:tipoMovimiento>1</web:tipoMovimiento><web:polizaAnterior>0</web:polizaAnterior></web:SOLICITUD_COT_AUTOS_REQ></soapenv:Body></soapenv:Envelope>")
        Return soapEnvelop
    End Function

End Class
