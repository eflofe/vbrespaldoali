﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports SuperObjeto
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.Xml
Imports System.IO
Imports System.ServiceModel.Security
Imports System.Xml.Serialization
Imports MySql.Data.MySqlClient

Public Class WsBANORTE
    Private Shared sUsuario As String = "WS3B502"          'Usuario de Produccion
    Private Shared sOficina As String = "3B5"              'Oficina de Produccion
    Private Shared sContrasena As String = "11220"
    Private Shared cSelec As String = String.Empty
    Private Shared fpSelec As String = String.Empty
    Dim lg As Log = New Log()


    Public Shared Function BANORTECotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim respuestaWS As String = ""
        Dim serializer As New JavaScriptSerializer
        Dim Aseguradora As String = "30"
        Dim paquete As String = ObjData.Paquete
        Dim FPid As String = ""
        Dim Submarca As String = ObjData.Vehiculo.Descripcion
        Dim Modelo As String = ObjData.Vehiculo.Modelo
        Dim Marca As String = ObjData.Vehiculo.Marca
        Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
        For i = lengthCP To 4
            ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
        Next
        Dim CP As String = ObjData.Cliente.Direccion.CodPostal
        Dim cevic As String = ObjData.Vehiculo.Clave
        Dim edad As String = ObjData.Cliente.Edad
        Dim genero As String = ObjData.Cliente.Genero
        Dim porcentaje As Decimal = 0.3
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
        End If

        Try
            Dim tipoVehi As String
            Try
                tipoVehi = getTipoVehi(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al reaizar consulta tipo vehiculo (clave de vehiculo)")
            End Try

            Dim tipoVehiDig As String = IIf(tipoVehi = "AUT", "01", "02")
            Dim edoMun
            Try
                edoMun = getDatosEdo(CP)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de estados y municipios (CP)")
            End Try

            'Inicializamos el Proxy del Servicio Web
            Dim ObjWS As New BANORTEService.WSCotizacionBGClient()
            ObjWS.ClientCredentials.UserName.UserName = sUsuario & "|" & sOficina
            ObjWS.ClientCredentials.UserName.Password = sContrasena
            ObjWS.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = ServiceModel.Security.X509CertificateValidationMode.None

            Dim ObjParam As New BANORTEService.Parametro()
            ObjParam.AmparaBote = 0 '---
            ObjParam.AmparaRemolque = 0 '---
            ObjParam.ArrastraRemolque = 0 'Arrastre Remolque
            ObjParam.CodigoEstado = edoMun(0) 'Estado
            ObjParam.CodigoMunicipio = edoMun(1) 'Municipio
            ObjParam.CodigoColonia = CP 'Colonia
            ObjParam.DescripcionAdicional = ""
            ObjParam.DescripcionAuto = ""
            ObjParam.DiasCobertura = 0 'Vigencia
            ObjParam.EdadConductor = edad 'Edad 1
            ObjParam.EdadConductorAdicional = -1 'Edad 2 
            ObjParam.Frontera = 0
            ObjParam.Modelo = Modelo 'Modelo
            ObjParam.Paquete = paquetesBanorte(tipoVehiDig, paquete) 'Paquete
            ObjParam.Pasajeros = 0 'Pasajeros
            ObjParam.PeriodoVigencia = "0" 'Mes o Dias
            If ObjData.Descuento <> "" Or ObjData.Descuento <> Nothing Then
                ObjParam.PorcentajeDescuento = (CInt(ObjData.Descuento) / 100).ToString  'Descuento Adicional
            Else
                ObjParam.PorcentajeDescuento = "0"  'Descuento Adicional
            End If
            ObjParam.PuertoEntrada = "" 'Puerto de Entrada
            ObjParam.SBG = cevic 'SBG
            ObjParam.Subramo = tipoVehiDig 'Subramo
            ObjParam.Tarifa = 0 'Tarifa
            ObjParam.TipoVehiculo = 0 'Tipo de Vehículo (remolque)
            ObjParam.TipoVigencia = 3 'Tipo de Vigencia
            ObjParam.Uso = "01" 'Uso
            ObjParam.ValorAmparaBote = 0 '---
            ObjParam.ValorAmparaRemolque = 0 '---
            ObjParam.ValorDeclarado = 0 'Valor factura

            Dim Deserializer As New JavaScriptSerializer
            Dim xser As New Serialization.XmlSerializer(ObjParam.GetType)
            Dim x As New StringWriter
            xser.Serialize(x, ObjParam)

            Dim sXML As String = String.Empty
            Dim numRecibos = 0
            Dim lgh As Log = New Log()

            Try
                Dim x2 As New XmlSerializer(ObjParam.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, ObjParam)
                Dim xmlRespuesta As String = xml2.ToString
                lgh.UpdateLogRequest(xmlRespuesta, idLogWSCot)
            Catch ex As Exception
                Dim errr = ex
            End Try
            Dim s As BANORTEService.TokenResult = ObjWS.CotizarAuto(ObjParam, BANORTEService.DatoFlotilla.NO, sXML)
            Try
                lgh.UpdateLogJsonResponse(sXML, idLogWSCot)
            Catch ex As Exception
                Dim errr = ex
            End Try
            If s.Ok Then
                Dim xmldoc1 As New XmlDocument
                xmldoc1.LoadXml(sXML)
                Dim xmlnode As XmlNodeList
                Dim objNodo As XmlNode
                cSelec = ""
                fpSelec = ""
                cSelec = coberturasSelec(sXML)
                'Primas
                xmlnode = xmldoc1.GetElementsByTagName("FP")
                For Each objNodo In xmlnode
                    If objNodo("FPid").InnerText = formaPago(ObjData.PeriodicidadDePago) Then
                        fpSelec = "<DatosXML><FP><FPid>" & objNodo("FPid").InnerText & "</FPid><FPdsc>" & objNodo("FPdsc").InnerText & "</FPdsc><PP>" & objNodo("PP").InnerText & "</PP><PS>" & objNodo("PS").InnerText & "</PS><Iva>" & objNodo("Iva").InnerText & "</Iva><IvaOfi>" & objNodo("IvaOfi").InnerText & "</IvaOfi><R>" & objNodo("R").InnerText & "</R><D>" & objNodo("D").InnerText & "</D><NR>" & objNodo("NR").InnerText & "</NR><NM>" & objNodo("NM").InnerText & "</NM><DR>" & objNodo("DR").InnerText & "</DR></FP></DatosXML>"
                        Exit For
                    End If
                Next
                Dim xmldoc2 As New XmlDocument
                xmldoc2.LoadXml(cSelec)
                xmlnode = xmldoc2.GetElementsByTagName("C")
                Dim Coberturas As New Coberturas
                Dim nombreCobertura = ""
                Dim sumatoriaPrimas As Double = 0.0
                For Each objNodo In xmlnode
                    If objNodo("Cid").InnerText = "DM" Then
                        nombreCobertura = "DAÑOS MATERIALES"
                        Coberturas.DanosMateriales = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "RT" Then
                        nombreCobertura = "ROBO TOTAL"
                        Coberturas.RoboTotal = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "RC" Then
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        Coberturas.RC = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "RCEX" Then
                        nombreCobertura = "EXTENSION RC"
                        Coberturas.RCExtension = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "SA" Then
                        nombreCobertura = "GASTOS LEGALES"
                        Coberturas.DefensaJuridica = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                        nombreCobertura = "ASISTENCIA VIAL"
                        Coberturas.AsitenciaCompleta = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "GM" Then
                        nombreCobertura = "GASTOS MÉDICOS EVENTO"
                        Coberturas.GastosMedicosEvento = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    End If
                    sumatoriaPrimas += Double.Parse(objNodo("Pr").InnerText)
                Next
                ObjData.Coberturas.Add(Coberturas)

                'se reaiza cambio en las coberturas
                Dim noCotizacion = String.Empty
                Dim sexo As Integer = 0
                If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                    sexo = 1
                ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                    sexo = 2
                End If
                Dim oInteresado As New BANORTEService.DatoExtra()

                oInteresado.Nombres = "PRUEBA"
                oInteresado.ApellidoPaterno = "EMISION INDIVIDUAL"
                oInteresado.ApellidoMaterno = "ADRISA"
                oInteresado.Telefono = "123456789"
                oInteresado.Correo = "PRUEBA@ADRISA.COM"
                oInteresado.FechaNacimiento = "01/01/1985"
                oInteresado.Sexo = BANORTEService.Genero.Masculino
                oInteresado.Estado = 19
                oInteresado.Edad = ""
                oInteresado.Peso = ""
                oInteresado.Estatura = ""
                oInteresado.NombresMenor = ""
                oInteresado.ApellidoPaternoMenor = ""
                oInteresado.ApellidoMaternoMenor = ""
                oInteresado.EdadMenor = ""
                oInteresado.SexoMenor = BANORTEService.Genero.Masculino
                oInteresado.FechaNacimientoMenor = ""
                oInteresado.CodigoAgente = "12674"

                Dim x3 As New XmlSerializer(ObjParam.GetType)
                Dim xml3 As New StringWriter
                x3.Serialize(xml3, ObjParam)
                Dim xmlObjParam As String = xml3.ToString


                Dim x4 As New XmlSerializer(oInteresado.GetType)
                Dim xml4 As New StringWriter
                x4.Serialize(xml4, oInteresado)
                Dim xmloInte As String = xml4.ToString

                Dim saveCotizacion As BANORTEService.TokenResult
                saveCotizacion = ObjWS.InsertarCotizacionIndividual(ObjParam, oInteresado, cSelec, fpSelec, Convert.ToDecimal(".0"), BANORTEService.DatoFlotilla.NO, noCotizacion)

                Dim x5 As New XmlSerializer(saveCotizacion.GetType)
                Dim xml5 As New StringWriter
                x5.Serialize(xml5, saveCotizacion)
                Dim xmlRespuesta As String = xml5.ToString
                If saveCotizacion.Ok Then
                    ObjData.Cotizacion.IDCotizacion = noCotizacion
                    Dim sXML2 As String = String.Empty
                    Dim cotiFPgao = ObjWS.CalcularFormasPago(ObjParam, fpSelec, sumatoriaPrimas, Convert.ToDecimal(".0"), sXML2)
                    If cotiFPgao.Ok Then
                        Dim xmldoc3 As New XmlDocument
                        xmldoc3.LoadXml(sXML2)
                        Dim xmlnode3 As XmlNodeList
                        Dim objNodo3 As XmlNode
                        xmlnode3 = xmldoc3.GetElementsByTagName("FP")
                        For Each objNodo3 In xmlnode3
                            If objNodo3("FPid").InnerText = formaPago(ObjData.PeriodicidadDePago) Then
                                ObjData.Cotizacion.Derechos = objNodo3("DR").InnerText
                                ObjData.Cotizacion.Impuesto = objNodo3("Iva").InnerText
                                ObjData.Cotizacion.PrimerPago = objNodo3("PP").InnerText.Replace("$", "")
                                ObjData.Cotizacion.PrimerPago = ObjData.Cotizacion.PrimerPago.Replace(",", "")
                                ObjData.Cotizacion.PagosSubsecuentes = objNodo3("PS").InnerText
                                ObjData.Cotizacion.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes.Replace("1 Pago de $", "")
                                ObjData.Cotizacion.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes.Replace("3 Pagos de $", "")
                                ObjData.Cotizacion.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes.Replace("11 Pagos de $", "")
                                ObjData.Cotizacion.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes.Replace(",", "")
                                ObjData.Cotizacion.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes.Replace("No Aplica", "0")
                                If ObjData.PeriodicidadDePago = 0 Then
                                    ObjData.Cotizacion.PrimaTotal = ObjData.Cotizacion.PrimerPago
                                Else
                                    Select Case (ObjData.PeriodicidadDePago)
                                        Case Seguro.PeriodicidadPago.MENSUAL
                                            numRecibos = 11
                                        Case Seguro.PeriodicidadPago.TRIMESTRAL
                                            numRecibos = 3
                                        Case Seguro.PeriodicidadPago.SEMESTRAL
                                            numRecibos = 1
                                    End Select
                                    ObjData.Cotizacion.PrimaTotal = ((CInt(ObjData.Cotizacion.PagosSubsecuentes) * numRecibos) + CInt(ObjData.Cotizacion.PrimerPago)).ToString
                                End If
                                ObjData.Cotizacion.PrimaNeta = objNodo3("PNT").InnerText
                                ObjData.Cotizacion.Recargos = objNodo3("R").InnerText
                                Exit For
                            End If
                        Next
                    End If
                End If
            Else
                respuestaWS = s.Mensaje + " - " + s.Detalle
                Throw New System.Exception("Error al realizar proceso de cotizacion")
            End If
            If ObjData.Cotizacion.PrimaTotal = "522" Then
                ObjData.Cotizacion.Derechos = String.Empty
                ObjData.Cotizacion.Impuesto = String.Empty
                ObjData.Cotizacion.PrimaTotal = String.Empty
                ObjData.Cotizacion.PrimerPago = String.Empty
                ObjData.Cotizacion.PrimaNeta = String.Empty
                ObjData.Cotizacion.Recargos = String.Empty
                ObjData.Cotizacion.PagosSubsecuentes = String.Empty
                ObjData.Cotizacion.Resultado = "False"
                ObjData.CodigoError = "Vehículo no encontrado"
            Else
                ObjData.Cotizacion.Resultado = "True"
            End If
        Catch ex As ServiceModel.FaultException
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        Catch err As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + err.Message + "], StackTrace: [" + err.StackTrace.ToString() + "]"
        End Try
        Dim JsonResponse As String = serializer.Serialize(ObjData)
        Dim lg As Log = New Log()
        lg.UpdateLogJsonResponse(JsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function BANORTEemision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        'Variables de la emisión

        Dim respuestaWS As String = ""
        Dim ocupacion As String = "EMPLEADO"
        Dim edoCivil As String = "SOLTERO"
        Dim CP As String = ObjData.Cliente.Direccion.CodPostal
        Dim porcentaje As Decimal = 0.3
        Dim diaN As Integer = Day(Now)
        Dim mesN As Integer = Month(Now)
        Dim anioN As Integer = Year(Now)

        Dim sexo As Integer = 0

        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            sexo = 1
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            sexo = 2
        End If

        Dim fecVig As Date = Date.Now
        Dim IniVigTxt As String = fecVig.ToString("dd/MM/yyyy")
        Dim FinvigTxt As String = fecVig.AddYears(1).ToString("dd/MM/yyyy")
        Dim fnac As Date = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))

        Dim cevic As String = ObjData.Vehiculo.Clave
        Dim edad As String = ObjData.Cliente.Edad
        Dim Modelo As String = ObjData.Vehiculo.Modelo

        Dim respuesta(9) As String
        Dim num_poliza As String = ""
        Dim paquete As String = ObjData.Paquete
        Dim fPago = ObjData.PeriodicidadDePago
        Dim ObjWS As New BANORTEService.WSCotizacionBGClient()
        ObjWS.ClientCredentials.UserName.UserName = sUsuario & "|" & sOficina
        ObjWS.ClientCredentials.UserName.Password = sContrasena
        ObjWS.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None

        Try
            Dim tipoVehi As String
            Try
                tipoVehi = getTipoVehi(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al reaizar consulta tipo vehiculo (clave de vehiculo)")
            End Try
            Dim tipoVehiDig As String = IIf(tipoVehi = "AUT", "01", "02")
            Dim edoMun
            Try
                edoMun = getDatosEdo(CP)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de estados y municipios (CP)")
            End Try
            Dim oParam As New BANORTEService.Parametro()
            oParam.AmparaBote = 0 '---
            oParam.AmparaRemolque = 0 '---
            oParam.ArrastraRemolque = 0 'Arrastre Remolque
            oParam.CodigoEstado = edoMun(0) 'Estado
            oParam.CodigoMunicipio = edoMun(1) 'Municipio
            oParam.CodigoColonia = CP 'Colonia
            oParam.DescripcionAdicional = ""
            oParam.DescripcionAuto = ""
            oParam.DiasCobertura = 0 'Vigencia
            oParam.EdadConductor = edad 'Edad 1
            oParam.EdadConductorAdicional = -1 'Edad 2 
            oParam.Frontera = 0
            oParam.Modelo = Modelo 'Modelo
            oParam.Paquete = paquetesBanorte(tipoVehiDig, paquete) 'Paquete
            oParam.Pasajeros = 0 'Pasajeros
            oParam.PeriodoVigencia = "0" 'Mes o Dias
            If ObjData.Descuento <> "" Or ObjData.Descuento <> Nothing Then
                oParam.PorcentajeDescuento = (CInt(ObjData.Descuento) / 100).ToString  'Descuento Adicional
            Else
                oParam.PorcentajeDescuento = "0"  'Descuento Adicional
            End If
            oParam.PuertoEntrada = "" 'Puerto de Entrada
            oParam.SBG = cevic 'SBG
            oParam.Subramo = tipoVehiDig 'Subramo
            oParam.Tarifa = 0 'Tarifa
            oParam.TipoVehiculo = 0 'Tipo de Vehículo (remolque)
            oParam.TipoVigencia = 3 'Tipo de Vigencia
            oParam.Uso = "01" 'Uso
            oParam.ValorAmparaBote = 0 '---
            oParam.ValorAmparaRemolque = 0 '---
            oParam.ValorDeclarado = 0 'Valor factura

            Dim oInteresado As New BANORTEService.DatoExtra()
            oInteresado.Nombres = ObjData.Cliente.Nombre
            oInteresado.ApellidoPaterno = ObjData.Cliente.ApellidoPat
            oInteresado.ApellidoMaterno = ObjData.Cliente.ApellidoMat
            oInteresado.Telefono = ObjData.Cliente.Telefono
            oInteresado.Correo = ObjData.Cliente.Email
            oInteresado.FechaNacimiento = ObjData.Cliente.FechaNacimiento + " 00:00:00 p. m."
            oInteresado.Sexo = IIf(sexo = "0", BANORTEService.Genero.Masculino, BANORTEService.Genero.Femenino)
            oInteresado.Estado = edoMun(0)
            oInteresado.Edad = ""
            oInteresado.Peso = ""
            oInteresado.Estatura = ""
            oInteresado.CodigoAgente = "12674"
            oInteresado.NombresMenor = ""
            oInteresado.ApellidoPaternoMenor = ""
            oInteresado.ApellidoMaternoMenor = ""
            oInteresado.EdadMenor = ""
            oInteresado.SexoMenor = IIf(sexo = "0", BANORTEService.Genero.Masculino, BANORTEService.Genero.Femenino)
            oInteresado.FechaNacimientoMenor = ""


            Dim sNumCotizacion = String.Empty
            Dim saveCotizacion As BANORTEService.TokenResult

            Dim sXML As String = String.Empty
            Dim s As BANORTEService.TokenResult = ObjWS.CotizarAuto(oParam, BANORTEService.DatoFlotilla.NO, sXML)
            If s.Ok Then
                Dim xmldoc1 As New XmlDocument
                xmldoc1.LoadXml(sXML)
                Dim xmlnode As XmlNodeList
                Dim objNodo As XmlNode
                cSelec = ""
                cSelec = coberturasSelec(sXML)
                'Primas
                xmlnode = xmldoc1.GetElementsByTagName("FP")
                For Each objNodo In xmlnode
                    If objNodo("FPid").InnerText = formaPago(fPago) Then
                        fpSelec = "<DatosXML><FP><FPid>" & objNodo("FPid").InnerText & "</FPid><FPdsc>" & objNodo("FPdsc").InnerText & "</FPdsc><PP>" & objNodo("PP").InnerText & "</PP><PS>" & objNodo("PS").InnerText & "</PS><Iva>" & objNodo("Iva").InnerText & "</Iva><IvaOfi>" & objNodo("IvaOfi").InnerText & "</IvaOfi><R>" & objNodo("R").InnerText & "</R><D>" & objNodo("D").InnerText & "</D><NR>" & objNodo("NR").InnerText & "</NR><NM>" & objNodo("NM").InnerText & "</NM><DR>" & objNodo("DR").InnerText & "</DR></FP></DatosXML>"
                        Exit For
                    End If
                Next
                Dim sumatoriaPrimas As Double = 0.0
                Dim numRecibos = 0
                Dim xmldoc2 As New XmlDocument
                xmldoc2.LoadXml(cSelec)
                xmlnode = xmldoc2.GetElementsByTagName("C")
                Dim Coberturas As New Coberturas
                Dim nombreCobertura = ""
                For Each objNodo In xmlnode
                    If objNodo("Cid").InnerText = "DM" Then
                        nombreCobertura = "DAÑOS MATERIALES"
                        Coberturas.DanosMateriales = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "RT" Then
                        nombreCobertura = "ROBO TOTAL"
                        Coberturas.RoboTotal = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "RC" Then
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        Coberturas.RC = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "RCEX" Then
                        nombreCobertura = "EXTENSION RC"
                        Coberturas.RCExtension = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "SA" Then
                        nombreCobertura = "GASTOS LEGALES"
                        Coberturas.DefensaJuridica = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                        nombreCobertura = "ASISTENCIA VIAL"
                        Coberturas.AsitenciaCompleta = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    ElseIf objNodo("Cid").InnerText = "GM" Then
                        nombreCobertura = "GASTOS MÉDICOS EVENTO"
                        Coberturas.DefensaJuridica = "-N" & nombreCobertura & "-S" & objNodo("SA").InnerText & "-D" & objNodo("Ded").InnerText
                    End If
                    sumatoriaPrimas += Double.Parse(objNodo("Pr").InnerText)
                Next
                saveCotizacion = ObjWS.InsertarCotizacionIndividual(oParam, oInteresado, cSelec, fpSelec, Convert.ToDecimal(".0"), BANORTEService.DatoFlotilla.NO, sNumCotizacion)
                If saveCotizacion.Ok Then
                    Dim sXML2 As String = String.Empty
                    Dim cotiFPgao = ObjWS.CalcularFormasPago(oParam, fpSelec, sumatoriaPrimas, Convert.ToDecimal(".0"), sXML2)
                    If cotiFPgao.Ok Then
                        Dim xmldoc3 As New XmlDocument
                        xmldoc3.LoadXml(sXML2)
                        Dim xmlnode3 As XmlNodeList
                        Dim objNodo3 As XmlNode
                        xmlnode3 = xmldoc3.GetElementsByTagName("FP")
                        For Each objNodo3 In xmlnode3
                            If objNodo3("FPid").InnerText = formaPago(ObjData.PeriodicidadDePago) Then
                                ObjData.Emision.Derechos = objNodo3("DR").InnerText
                                ObjData.Emision.Impuesto = objNodo3("Iva").InnerText
                                ObjData.Emision.PrimerPago = objNodo3("PP").InnerText.Replace("$", "")
                                ObjData.Emision.PrimerPago = ObjData.Emision.PrimerPago.Replace(",", "")
                                ObjData.Emision.PagosSubsecuentes = objNodo3("PS").InnerText
                                ObjData.Emision.PagosSubsecuentes = ObjData.Emision.PagosSubsecuentes.Replace("1 Pago de $", "")
                                ObjData.Emision.PagosSubsecuentes = ObjData.Emision.PagosSubsecuentes.Replace("3 Pagos de $", "")
                                ObjData.Emision.PagosSubsecuentes = ObjData.Emision.PagosSubsecuentes.Replace("11 Pagos de $", "")
                                ObjData.Emision.PagosSubsecuentes = ObjData.Emision.PagosSubsecuentes.Replace(",", "")
                                ObjData.Emision.PagosSubsecuentes = ObjData.Emision.PagosSubsecuentes.Replace("No Aplica", "0")
                                If ObjData.PeriodicidadDePago = 0 Then
                                    ObjData.Emision.PrimaTotal = ObjData.Emision.PrimerPago
                                Else
                                    Select Case (ObjData.PeriodicidadDePago)
                                        Case Seguro.PeriodicidadPago.MENSUAL
                                            numRecibos = 11
                                        Case Seguro.PeriodicidadPago.TRIMESTRAL
                                            numRecibos = 3
                                        Case Seguro.PeriodicidadPago.SEMESTRAL
                                            numRecibos = 1
                                    End Select
                                    ObjData.Emision.PrimaTotal = ((CInt(ObjData.Emision.PagosSubsecuentes) * numRecibos) + CInt(ObjData.Emision.PrimerPago)).ToString
                                End If
                                ObjData.Emision.PrimaNeta = objNodo3("PNT").InnerText
                                ObjData.Emision.Recargos = objNodo3("R").InnerText
                                Exit For
                            End If
                        Next
                    End If
                End If
            Else
                respuestaWS = s.Mensaje + " - " + s.Detalle
                Throw New System.Exception("Error al realizar proceso de emision")
            End If

            Dim oDatosComplementarios = New BANORTEEmision.DatosComplementarios
            'DATOS DEL AUTO
            Dim oAuto = New BANORTEEmision.Vehiculo()
            oAuto.ClaveActivacion = ""
            oAuto.ClaveMarca = ObjData.Vehiculo.Clave
            oAuto.ClaveSBG = ObjData.Vehiculo.Clave
            oAuto.ClaveServicio = "01"
            oAuto.Placas = ObjData.Vehiculo.NoPlacas
            oAuto.NumeroSerie = ObjData.Vehiculo.NoSerie
            oAuto.NumeroMotor = ObjData.Vehiculo.NoMotor
            oAuto.Renave = ""
            oAuto.EquipoEspecial = ""
            oAuto.Modelo = Convert.ToInt32(ObjData.Vehiculo.Modelo)
            oAuto.Descripcion = ObjData.Vehiculo.Descripcion
            oAuto.Marca = ObjData.Vehiculo.Marca
            oAuto.ClaveSubramo = tipoVehiDig
            oAuto.CodigoEstado = edoMun(0)
            oAuto.TieneDanio = 0
            oDatosComplementarios.Auto = oAuto

            'ESTE PODRIA SER EL CONDUCTOR HABITUAL
            Dim oConductor = New BANORTEEmision.Conductor()
            oConductor.ApellidoPaterno = ObjData.Cliente.ApellidoPat
            oConductor.ApellidoMaterno = ObjData.Cliente.ApellidoMat
            oConductor.Nombre = ObjData.Cliente.Nombre
            oConductor.FechaNacimiento = fnac
            oConductor.NumeroLicencia = ""
            oConductor.Sexo = IIf(sexo = "0", BANORTEService.Genero.Masculino, BANORTEService.Genero.Femenino) 'IIf(UCase(sexo) = "MASCULINO", 1, 0)
            oDatosComplementarios.ConductorPrincipal = oConductor

            'DATOS DEL BENEFICIARIO
            Dim oBeneficiario = New BANORTEEmision.Beneficiario()
            oBeneficiario.Nombre = ""
            oBeneficiario.ApellidoParteno = ""
            oBeneficiario.ApellidoMaterno = ""
            oBeneficiario.Porcentaje = "100%"
            oBeneficiario.Parentesco = "FAMILIAR" '"DATO_HARDCODE_EN_IU"
            oDatosComplementarios.Beneficiario = oBeneficiario

            'DATOS DEL CONTRATANTE
            Dim oContratante = New BANORTEEmision.Contratante
            oContratante.ApellidoPaterno = ObjData.Cliente.ApellidoPat
            oContratante.ApellidoMaterno = ObjData.Cliente.ApellidoMat
            oContratante.Nombre = ObjData.Cliente.Nombre
            oContratante.Calle = ObjData.Cliente.Direccion.Calle
            oContratante.Celular = ObjData.Cliente.Telefono
            oContratante.CodigoPostal = ObjData.Cliente.Direccion.CodPostal
            oContratante.Colonia = ObjData.Cliente.Direccion.Colonia
            oContratante.ContratanteId = 0
            oContratante.CURP = ""
            oContratante.Email = ObjData.Cliente.Email
            oContratante.Estado = edoMun(0) 'Clave de estado
            oContratante.EstadoCivil = 1
            oContratante.FechaNacimiento = ObjData.Cliente.FechaNacimiento
            oContratante.LugarNacimiento = ""
            oContratante.Municipio = edoMun(1)
            oContratante.Nacionalidad = ""
            oContratante.Numero = ObjData.Cliente.Direccion.NoExt
            oContratante.RFC = ObjData.Cliente.RFC
            oContratante.Sexo = IIf(sexo = "0", BANORTEService.Genero.Masculino, BANORTEService.Genero.Femenino) 'IIf(UCase(sexo) = "MASCULINO", 1, 0)
            oContratante.Telefono = ObjData.Cliente.Telefono
            oContratante.Telefono2 = ""
            oContratante.TipoPersona = IIf(ObjData.Cliente.TipoPersona = "F", "1", "2")
            oDatosComplementarios.Contratante = oContratante

            'DATOS DEL ASEGURADO
            Dim oAsegurado = New BANORTEEmision.Asegurado
            oAsegurado.AseguradoId = 0
            oAsegurado.ApellidoPaterno = ObjData.Cliente.ApellidoPat
            oAsegurado.ApellidoMaterno = ObjData.Cliente.ApellidoMat
            oAsegurado.Nombre = ObjData.Cliente.Nombre
            oAsegurado.FechaNacimiento = ObjData.Cliente.FechaNacimiento
            oAsegurado.LugarNacimiento = ""
            oAsegurado.Sexo = IIf(sexo = "0", BANORTEService.Genero.Masculino, BANORTEService.Genero.Femenino) 'IIf(UCase(sexo) = "MASCULINO", 1, 0)
            oAsegurado.EstadoCivil = 1
            oAsegurado.Nacionalidad = ""
            oAsegurado.RFC = ObjData.Cliente.RFC
            oAsegurado.CURP = ""
            oAsegurado.TipoPersona = IIf(ObjData.Cliente.TipoPersona = "F", "1", "2")
            oAsegurado.Telefono = ObjData.Cliente.Telefono
            oAsegurado.Telefono2 = ""
            oAsegurado.Celular = ""
            oAsegurado.Email = ObjData.Cliente.Email
            oAsegurado.Calle = ObjData.Cliente.Direccion.Calle
            oAsegurado.Numero = ObjData.Cliente.Direccion.NoExt
            oAsegurado.Colonia = ObjData.Cliente.Direccion.Colonia
            oAsegurado.Municipio = edoMun(1)
            oAsegurado.CodigoPostal = ObjData.Cliente.Direccion.CodPostal
            oAsegurado.Estado = edoMun(0) 'Clave de estado
            oAsegurado.ConductorApellidoPaterno = ObjData.Cliente.ApellidoPat
            oAsegurado.ConductorApellidoMaterno = ObjData.Cliente.ApellidoMat
            oAsegurado.ConductorNombre = ObjData.Cliente.Nombre
            oDatosComplementarios.Asegurado = oAsegurado

            'DATOS DE LA VIGENCIA
            Dim oVigenciaPoliza = New BANORTEEmision.VigenciaPoliza()
            oVigenciaPoliza.FechaEmision = Convert.ToDateTime(IniVigTxt, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            oVigenciaPoliza.FechaInicio = Convert.ToDateTime(IniVigTxt, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            oVigenciaPoliza.FechaFin = Convert.ToDateTime(FinvigTxt, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            oVigenciaPoliza.HoraInicioVigencia = ""
            oVigenciaPoliza.HoraFinVigencia = ""
            oVigenciaPoliza.NumeroPoliza = ""
            oVigenciaPoliza.NumeroSolicitud = ""
            oDatosComplementarios.VigenciaPoliza = oVigenciaPoliza

            'DATOS DEL AGENTE
            Dim oAgente = New BANORTEEmision.Agente
            oAgente.AgenteId = 12674
            oAgente.Nombre = "TRIGARANTE AGTE DE SEGUROS Y DE FIANZAS"
            oDatosComplementarios.Agente = oAgente

            Dim oFormaPago = New BANORTEEmision.FormaPago()
            oFormaPago.ClaveBanco = bancoBanorte(ObjData.Pago.Banco)
            oFormaPago.ClaveConductoPago = tipoCobro(ObjData.Pago.MedioPago)
            oFormaPago.ClaveConductoPagoCon = ""
            oFormaPago.ClaveIdentificacion = IIf(ObjData.Cliente.TipoPersona = "F", 4, 3)
            oFormaPago.NombreBanco = ObjData.Pago.Banco
            oFormaPago.NumeroCuenta = ObjData.Pago.NoTarjeta
            oFormaPago.NumeroIdentificacion = "" 'IIf(tipopersona = "0", "0000000000000", "0000000000000")
            oFormaPago.CobroNLinea = 1

            'DATOS DE TARJETA
            Dim oTarjeta = New BANORTEEmision.Tarjeta()
            oTarjeta.TipoTarjeta = IIf(UCase(ObjData.Pago.Carrier) = "0", "01", "02") '01 VISA - 02 MASTERCARD
            oTarjeta.MesVencimiento = ObjData.Pago.MesExp
            oTarjeta.AnioVencimiento = ObjData.Pago.AnioExp
            oTarjeta.CodigoSeguridad = ObjData.Pago.CodigoSeguridad
            oFormaPago.Tarjeta = oTarjeta
            oDatosComplementarios.FormaPago = oFormaPago

            'DATOS GENERALES PARA LA EMISION
            oDatosComplementarios.NumeroEmpleado = 0
            oDatosComplementarios.NumeroInventario = ""
            oDatosComplementarios.Referencia = ""
            oDatosComplementarios.AreaDepartamento = ""

            Dim client As New BANORTEEmision.WSEmisionBGClient()
            client.ClientCredentials.UserName.UserName = sUsuario + "|" + sOficina
            client.ClientCredentials.UserName.Password = sContrasena
            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None

            Dim shortDescripcion As String
            Try
                shortDescripcion = Replace(Mid(ObjData.Vehiculo.Descripcion, 1, InStr(ObjData.Vehiculo.Descripcion, " ")), "/", "")
            Catch ex As Exception
                shortDescripcion = Replace(ObjData.Vehiculo.Descripcion, "/", "")
            End Try

            Try
                Dim y3 As New XmlSerializer(oDatosComplementarios.GetType)
                Dim xml2 As New StringWriter
                y3.Serialize(xml2, oDatosComplementarios)
                Dim errorws4 As String = xml2.ToString
                Funciones.updateLogWS(errorws4, idLogWS, "RequestWS")
            Catch ex As Exception
            End Try

            Dim result As BANORTEEmision.TokenResult = client.GenerarPolizaIndividual(sNumCotizacion, oDatosComplementarios, num_poliza)

            Try
                Dim y3 As New XmlSerializer(result.GetType)
                Dim xml2 As New StringWriter
                y3.Serialize(xml2, result)
                Dim errorws4 As String = xml2.ToString
                Funciones.updateLogWS(errorws4, idLogWS, "ResponseWS")
            Catch ex As Exception
            End Try

            Dim binaryData As Byte()
            Dim PDFstr64bits As String = ""
            If result.Ok Then
                respuesta(0) = sNumCotizacion
                ObjData.Emision.Poliza = num_poliza
                ObjData.Emision.Resultado = "True"
                respuesta(7) = "El número de póliza generado es: " + num_poliza + " y el código de pago de tarjeta es: " + result.Mensaje

                Try
                    'Inicializamos el Proxy del Servicio Web
                    Dim clientImpresion As New BANORTEImpresion.WSImpresionBGClient()
                    clientImpresion.ClientCredentials.UserName.UserName = sUsuario & "|" & sOficina
                    clientImpresion.ClientCredentials.UserName.Password = sContrasena
                    clientImpresion.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None
                    Dim URL As BANORTEImpresion.TokenResult = clientImpresion.ImprimePoliza(ObjData.Emision.Poliza, BANORTEImpresion.TipoDocumentoReporte.PolizaAutoAsegurado, PDFstr64bits)
                    binaryData = Convert.FromBase64String(PDFstr64bits)
                    If binaryData IsNot Nothing Then
                        Dim ruta As String = "W:\Documentos\Proyecto\ws-ali-ahorraseguros-prod\Archivo\"
                        File.WriteAllBytes(ruta + ObjData.Emision.Poliza + ".pdf", binaryData)
                        ObjData.Emision.Documento = "https://ws-se.com/Archivo/" + ObjData.Emision.Poliza + ".pdf"
                    End If
                Catch ex As Exception
                    ObjData.Emision.Resultado = "False"
                    ObjData.CodigoError = "WSRequest: []" + ", Message: [" + ex.Message + "], StackTrace: []"
                End Try
            Else
                ObjData.Emision.Resultado = "False"
                If num_poliza <> "" Then
                    respuesta(1) = num_poliza
                    respuestaWS = result.Mensaje
                Else
                    respuestaWS = result.Mensaje + " - " + result.Detalle
                End If
                ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [Error al realizar invocacion del metodo GenerarPolizaIndividual], StackTrace: []"
            End If


        Catch ex As Exception
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function paquetesBanorte(subramo As String, paquete As String) As String
        If UCase(paquete) = "AMPLIA" And subramo = "01" Then
            Return "1"
        ElseIf UCase(paquete) = "AMPLIA" And subramo = "02" Then
            Return "4"
        ElseIf UCase(paquete) = "LIMITADA" And subramo = "01" Then
            Return "2"
        ElseIf UCase(paquete) = "LIMITADA" And subramo = "02" Then
            Return "5"
        ElseIf UCase(paquete) = "RC" And subramo = "01" Then
            Return "3"
        ElseIf UCase(paquete) = "RC" And subramo = "02" Then
            Return "6"
        End If
        Return "No existe paquete solicitado"
    End Function

    Private Shared Function getTipoVehi(ByVal clave As String) As String
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "BANORTE", clave)
        Dim serializer As New JavaScriptSerializer
        Dim mapaVehiculo = serializer.Deserialize(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim tipoVehi = mapaVehiculo.Item("vehicle_type")
        Return tipoVehi
    End Function
    Private Shared Function getDatosEdo(ByVal cp As String) As String()
        Dim objConnection As SqlConnection
        Dim objCommand As SqlCommand
        Dim strSQLQuery As String
        Dim Respuesta(1) As String
        Dim ConnectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;"
        Dim sql = "Select IdEdo,IdMun from `Ali_R_CatCpostalBanorte` where CP ='" + cp + "'"
        Using Connection As New MySqlConnection(ConnectionString)
            Dim command As New MySqlCommand(sql, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                reader.Read()
                Respuesta(0) = reader("IdEdo")
                Respuesta(1) = reader("IdMun")
                Connection.Close()

            Catch ex As Exception
                Dim errr = ex

            End Try
        End Using
        Return Respuesta
    End Function
    Private Shared Function coberturasSelec(xml As String) As String
        Dim xmldoc1 As New XmlDocument
        xmldoc1.LoadXml(xml)
        Dim colElementos1 As XmlNodeList
        Dim colElementos2 As XmlNodeList
        Dim objNodo1 As XmlNode
        Dim objNodo2 As XmlNode
        Dim Cobertura As String = ""
        Dim nodo As String = ""
        Dim xmlCont As String = "<DatosXML>"
        colElementos1 = xmldoc1.GetElementsByTagName("C")
        colElementos2 = xmldoc1.GetElementsByTagName("C")
        Try
            Dim colElementos1Cont = 0
            For Each objNodo1 In colElementos1
                'colElementos1Cont = Convert.ToInt32(objNodo1.NodeType)
                Cobertura = objNodo1("Cid").InnerXml()
                If Cobertura <> "EE" And Cobertura <> "ASDM" And Cobertura <> "EDDM" And Cobertura <> "EDRT" And Cobertura <> "AS" And Cobertura <> "MC" Then
                    If Cobertura = "DM" Or Cobertura = "RT" Then
                        objNodo1("SDED").RemoveAll()
                    ElseIf Cobertura = "RC" Or Cobertura = "GM" Then
                        Dim nodes As XmlNodeList
                        nodes = objNodo1.SelectNodes("SsAs")
                        For Each node As XmlNode In nodes
                            If node IsNot Nothing Then
                                Try
                                    node.RemoveAll()
                                Catch ex As Exception
                                End Try
                            End If
                        Next
                    End If
                    xmlCont &= xmldoc1.GetElementsByTagName("C").Item(colElementos1Cont).OuterXml()
                End If
                colElementos1Cont += 1
            Next
            xmlCont &= "</DatosXML>"
        Catch ex As Exception
            Dim x = ex.Message.ToString
        End Try
        Return xmlCont
    End Function
    Private Shared Function formaPago(FPid As String) As String
        Select Case (UCase(FPid))
            Case Seguro.PeriodicidadPago.ANUAL
                Return "01"
            Case Seguro.PeriodicidadPago.MENSUAL
                Return "04"
            Case Seguro.PeriodicidadPago.TRIMESTRAL
                Return "03"
            Case Seguro.PeriodicidadPago.SEMESTRAL
                Return "02"
        End Select
        Return "No se dispone de la forma de pago solicitada"
    End Function
    Private Shared Function bancoBanorte(ByVal nBanco As String) As String
        Select Case UCase(nBanco)
            Case "BANAMEX"
                Return "002"
            Case "BBVA BANCOMER"
                Return "012"
            Case "SANTANDER"
                Return "014"
            Case "BANJERCITO"
                Return "019"
            Case "HSBC"
                Return "021"
            Case "BAJIO"
                Return "030"
            Case "IXE"
                Return "032"
            Case "INBURSA"
                Return "036"
            Case "INTERACCIONES"
                Return "037"
            Case "MIFEL"
                Return "042"
            Case "SCOTIABANK"
                Return "044"
            Case "BANREGIO"
                Return "058"
            Case "INVEX"
                Return "059"
            Case "AFIRME"
                Return "062"
            Case "BANORTE"
                Return "072"
            Case "AZTECA"
                Return "127"
            Case "FAMSA"
                Return "131"
            Case "WAL-MART"
                Return "134"
            Case "BANCOPPEL"
                Return "137"
            Case "FACIL"
                Return "940"
        End Select
        Return "ERROR EN EL BANCO"
    End Function
    Private Shared Function tipoCobro(medio As String) As String
        Select Case (UCase(medio))
            Case "CONTADO"
                Return "00"
            Case "CREDITO"
                Return "04"
            Case "DEBITO" 'CLABE"
                Return "04"
        End Select
        Return "No existe forma de pago"
    End Function

End Class
