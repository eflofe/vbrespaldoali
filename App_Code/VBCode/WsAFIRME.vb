﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Script.Serialization
Imports System.Xml
Imports SuperObjeto
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports MySql.Data.MySqlClient
Imports System.Data

Public Class WsAFIRME
    Public Shared Function AFIRMEEmision(ByVal ObjData As Seguro, ByVal idLogWS As String, ByVal idLogWSCot As String)
        Dim ObjAfirme As New AFIRMEService.CotizacionAutoIndividualService
        Dim serializer As New JavaScriptSerializer
        Dim token = "a6131309-d28e-4004-b804-73797279e408"
        Dim jsonFormat = New JsonSerializerSettings
        jsonFormat.NullValueHandling = NullValueHandling.Ignore
        Dim respuesta = "", idCliente = ""
        Dim FormaPago = "", tipoTarjeta = "VS"
        ObjData.Emision.Resultado = "false"
        ObjData.CodigoError = ""

        If ObjData.Pago.Carrier = Pago.CatCarrier.MASTERCARD Then
            tipoTarjeta = "MC"
        End If

        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            FormaPago = "1"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            FormaPago = "2"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            FormaPago = "3"
        End If
        Try
            cotizarPoliza(ObjData, idLogWSCot)

            Dim direccion
            Try
                direccion = ConsultaCP(ObjData.Cliente.Direccion.CodPostal, "")
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta codigo postal (CP)")
            End Try

            If direccion(0) = "DISTRITO FEDERAL" Then
                direccion(0) = "CIUDAD DE MEXICO"
            End If
            Dim descuento As Double = Convert.ToDecimal(ObjData.Descuento)
            Dim Edo_Mun
            Try
                Edo_Mun = ConsultaDireccionAfirme(direccion(0), direccion(1))
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta direccion afirme")
            End Try

            Dim afirmeEmitePoliza = New AFIRMEEmitePolizaEnvio
            afirmeEmitePoliza.idCotizacion = ObjData.Cotizacion.IDCotizacion
            afirmeEmitePoliza.observaciones = "comentario de prueba"
            afirmeEmitePoliza.documentacionCompleta = "true"

            Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")

            Dim conductor = New Dictionary(Of String, String)
            conductor.Add("nombreConductor", ObjData.Cliente.Nombre)
            conductor.Add("apellidoPaternoConductor", ObjData.Cliente.ApellidoPat)
            conductor.Add("apellidoMaternoConductor", ObjData.Cliente.ApellidoMat)
            conductor.Add("fechaNacimiento", fechaNacimiento + " 12:23:00")
            conductor.Add("numeroLicencia", "123456789")
            conductor.Add("ocupacion", "Empleado")

            afirmeEmitePoliza.autoInciso = New Dictionary(Of String, Object)
            afirmeEmitePoliza.autoInciso.Add("conductor", conductor)
            afirmeEmitePoliza.autoInciso.Add("numeroMotor", ObjData.Vehiculo.NoMotor)
            afirmeEmitePoliza.autoInciso.Add("numeroSerie", ObjData.Vehiculo.NoSerie)
            afirmeEmitePoliza.autoInciso.Add("placa", ObjData.Vehiculo.NoPlacas)
            afirmeEmitePoliza.autoInciso.Add("emailContacto", ObjData.Cliente.Email)
            afirmeEmitePoliza.autoInciso.Add("rutaCirculacion", "19039")
            afirmeEmitePoliza.autoInciso.Add("observacionesInciso", "N/A")
            afirmeEmitePoliza.autoInciso.Add("nombreAsegurado", ObjData.Cliente.Nombre + " " + ObjData.Cliente.ApellidoPat + " " + ObjData.Cliente.ApellidoMat)

            afirmeEmitePoliza.paquete = New Dictionary(Of String, String)
            afirmeEmitePoliza.paquete.Add("idFormaPago", FormaPago)
            'Datos de cobro
            afirmeEmitePoliza.conductoCobro = New Dictionary(Of String, Object)
            Dim datosTarjeta As Dictionary(Of String, String)
            Dim datosTitular As Dictionary(Of String, String)
            afirmeEmitePoliza.conductoCobro.Add("idMedioPago", "4")
            datosTarjeta = New Dictionary(Of String, String)
            datosTitular = New Dictionary(Of String, String)
            datosTarjeta.Add("idBanco", BancosAFIRME(ObjData.Pago.Banco))
            datosTarjeta.Add("numeroTarjeta", ObjData.Pago.NoTarjeta)
            Dim anioExp = ObjData.Pago.AnioExp
            If anioExp.Length = 4 Then
                anioExp = anioExp.Substring(2, 2)
            End If
            datosTarjeta.Add("fechaVencimientoTarjeta", ObjData.Pago.MesExp + anioExp)
            datosTarjeta.Add("codigoSeguridadTarjeta", ObjData.Pago.CodigoSeguridad)
            datosTarjeta.Add("idTipoTarjeta", tipoTarjeta)
            afirmeEmitePoliza.conductoCobro.Add("datosTarjeta", datosTarjeta)
            datosTitular.Add("tarjetaHabiente", ObjData.Cliente.Nombre + " " + ObjData.Cliente.ApellidoPat + " " + ObjData.Cliente.ApellidoMat)
            datosTitular.Add("correo", ObjData.Cliente.Email)
            datosTitular.Add("telefono", ObjData.Cliente.Telefono)
            datosTitular.Add("clavePais", "PAMEXI")
            datosTitular.Add("idEstado", Edo_Mun(0))
            datosTitular.Add("idMunicipio", Edo_Mun(1))
            datosTitular.Add("nombreColonia", ObjData.Cliente.Direccion.Colonia)
            datosTitular.Add("calleNumero", ObjData.Cliente.Direccion.NoExt)
            datosTitular.Add("codigoPostal", ObjData.Cliente.Direccion.CodPostal)
            datosTitular.Add("rfc", ObjData.Cliente.RFC)
            afirmeEmitePoliza.conductoCobro.Add("datosTitular", datosTitular)

            Dim request = JsonConvert.SerializeObject(afirmeEmitePoliza, jsonFormat)

            Try
                Funciones.updateLogWS(request, idLogWS, "RequestWS")
            Catch ex As Exception
            End Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            respuesta = ObjAfirme.emitirPoliza(request, token)

            Try
                Funciones.updateLogWS(respuesta, idLogWS, "ResponseWS")
            Catch ex As Exception
            End Try
            Try
                Dim jsonRespuesta As JObject
                If respuesta.Contains("numeroPoliza") Then
                    jsonRespuesta = JObject.Parse(respuesta)
                    ObjData.Emision.IDCotizacion = jsonRespuesta.GetValue("idPoliza")
                    ObjData.Emision.Poliza = jsonRespuesta.GetValue("numeroPoliza")
                    ObjData.Emision.PrimaNeta = ObjData.Cotizacion.PrimaNeta
                    ObjData.Emision.PrimaTotal = ObjData.Cotizacion.PrimaTotal
                    ObjData.Emision.Impuesto = ObjData.Cotizacion.Impuesto
                    ObjData.Emision.Derechos = ObjData.Cotizacion.Derechos
                    ObjData.Emision.Recargos = ObjData.Cotizacion.Recargos
                    Try
                        Dim binaryData As Byte()
                        binaryData = ObjAfirme.imprimirPoliza(ObjData.Emision.IDCotizacion, True, token)
                        File.WriteAllBytes("W:\Documentos\Proyecto\ws-ali-ahorraseguros-prod\Archivo\" + ObjData.Emision.Poliza + ".pdf", binaryData)
                        ObjData.Emision.Documento = "https://ws-se.com/Archivo/" + ObjData.Emision.Poliza + ".pdf"
                        ObjData.Emision.Resultado = "true"
                    Catch ex As Exception
                        ObjData.CodigoError = "Message: [Error al imprimir la poliza]"
                        ObjData.Emision.Resultado = "false"
                    End Try
                Else
                    ObjData.CodigoError = respuesta
                End If
            Catch ex As Exception
                Throw New System.Exception("Error al parsear respuesta de servicio web")
            End Try
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest:  [" + respuesta + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Emision.Resultado = "false"
        End Try
        Return serializer.Serialize(ObjData)
    End Function
    Public Shared Function AFIRMECotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim ObjAfirme As New AFIRMEService.CotizacionAutoIndividualService
        Dim ObjBXMASCatalogo As New BXMASServiceCatalogo.catalogosAutosWs
        Dim serializer As New JavaScriptSerializer
        Dim respuesta As String = ""
        Dim coberturasAfirm = New AFIRMEService.coberturaView() {}
        Try
            Dim IdPaquete = 1
            If ObjData.Paquete = "AMPLIA" Then
                IdPaquete = 1
            ElseIf ObjData.Paquete = "LIMITADA" Then
                IdPaquete = 3
            ElseIf ObjData.Paquete = "BASICA" Then
                IdPaquete = 776
            End If
            Dim FormaPago = 1
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                FormaPago = 1
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                FormaPago = 2
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                FormaPago = 3
            End If
            Dim token = "a6131309-d28e-4004-b804-73797279e408"
            Dim idMarca
            Try
                idMarca = GetIdMarca(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta idMarca (clave vehiculo)")
            End Try
            Dim direccion
            Try
                direccion = ConsultaCP(ObjData.Cliente.Direccion.CodPostal, "")
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta codigo postal (CP)")
            End Try

            If direccion(0) = "DISTRITO FEDERAL" Then
                direccion(0) = "CIUDAD DE MEXICO"
            End If
            Dim descuento As Double = Convert.ToDecimal(ObjData.Descuento)
            Dim Edo_Mun
            Try
                Edo_Mun = ConsultaDireccionAfirme(direccion(0), direccion(1))
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta direccion afirme")
            End Try

            'Nueva implementacion 

            Dim afirmeCotizarPolizaEnvio = New AFIRMECotizarPolizaEnvio
            afirmeCotizarPolizaEnvio.idAgente = "92901"     'OK
            afirmeCotizarPolizaEnvio.datosPoliza = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.datosPoliza.Add("idNegocio", 461)    'OK
            afirmeCotizarPolizaEnvio.datosPoliza.Add("idProducto", 658)    'OK
            afirmeCotizarPolizaEnvio.datosPoliza.Add("idTipoPoliza", 83)
            afirmeCotizarPolizaEnvio.zonaCirculacion = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.zonaCirculacion.Add("idEstadoCirculacion", "08000")    'OK
            afirmeCotizarPolizaEnvio.zonaCirculacion.Add("idMunicipioCirculacion", "08037")   'OK   
            afirmeCotizarPolizaEnvio.vehiculo = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.vehiculo.Add("idLineaNegocio", 755)
            afirmeCotizarPolizaEnvio.vehiculo.Add("idMarca", "309")  'OK
            afirmeCotizarPolizaEnvio.vehiculo.Add("modelo", CInt(ObjData.Vehiculo.Modelo))  'OK
            afirmeCotizarPolizaEnvio.vehiculo.Add("idEstilo", CInt(ObjData.Vehiculo.Clave))  'OK
            afirmeCotizarPolizaEnvio.paquete = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.paquete.Add("idPaquete", IdPaquete)  'OK
            afirmeCotizarPolizaEnvio.paquete.Add("idFormaPago", FormaPago)  'OK
            afirmeCotizarPolizaEnvio.paquete.Add("pctDescuentoEstado", descuento)  'OK

            afirmeCotizarPolizaEnvio.coberturas = New List(Of AFIRMECoberturas)

            Dim afirmeCoberturas1 = New AFIRMECoberturas
            afirmeCoberturas1.idCobertura = 2510
            afirmeCoberturas1.obligatoriedad = 0
            afirmeCoberturas1.contratada = "true"
            afirmeCoberturas1.descripcion = "DAÑOS MATERIALES"
            afirmeCoberturas1.sumaAsegurada = "Valor Comercial"
            afirmeCoberturas1.deducible = 5.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas1)

            Dim afirmeCoberturas2 = New AFIRMECoberturas
            afirmeCoberturas2.idCobertura = 2520
            afirmeCoberturas2.obligatoriedad = 0
            afirmeCoberturas2.contratada = "true"
            afirmeCoberturas2.descripcion = "ROBO TOTAL"
            afirmeCoberturas2.sumaAsegurada = "Valor Comercial"
            afirmeCoberturas2.deducible = 10.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas2)

            Dim afirmeCoberturas3 = New AFIRMECoberturas
            afirmeCoberturas3.idCobertura = 2530
            afirmeCoberturas3.obligatoriedad = 0
            afirmeCoberturas3.contratada = "true"
            afirmeCoberturas3.descripcion = "RESPONSABILIDAD CIVIL"
            afirmeCoberturas3.sumaAsegurada = "2500000.0"
            afirmeCoberturas3.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas3)

            Dim afirmeCoberturas4 = New AFIRMECoberturas
            afirmeCoberturas4.idCobertura = 2540
            afirmeCoberturas4.obligatoriedad = 1
            afirmeCoberturas4.contratada = "true"
            afirmeCoberturas4.descripcion = "EXENCIÓN DE DEDUCIBLES DM"
            afirmeCoberturas4.sumaAsegurada = "Amparada"
            afirmeCoberturas4.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas4)

            Dim afirmeCoberturas5 = New AFIRMECoberturas
            afirmeCoberturas5.idCobertura = 2550
            afirmeCoberturas5.obligatoriedad = 0
            afirmeCoberturas5.contratada = "true"
            afirmeCoberturas5.descripcion = "GASTOS MÉDICOS"
            afirmeCoberturas5.sumaAsegurada = "40000.0"
            afirmeCoberturas5.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas5)

            Dim afirmeCoberturas6 = New AFIRMECoberturas
            afirmeCoberturas6.idCobertura = 2580
            afirmeCoberturas6.obligatoriedad = 1
            afirmeCoberturas6.contratada = "true"
            afirmeCoberturas6.descripcion = "EXTENSIÓN DE RESPONSABILIDAD CIVIL"
            afirmeCoberturas6.sumaAsegurada = "Amparada"
            afirmeCoberturas6.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas6)

            Dim afirmeCoberturas7 = New AFIRMECoberturas
            afirmeCoberturas7.idCobertura = 2610
            afirmeCoberturas7.obligatoriedad = 0
            afirmeCoberturas7.contratada = "true"
            afirmeCoberturas7.descripcion = "ASISTENCIA JURÍDICA"
            afirmeCoberturas7.sumaAsegurada = "Amparada"
            afirmeCoberturas7.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas7)

            Dim afirmeCoberturas8 = New AFIRMECoberturas
            afirmeCoberturas8.idCobertura = 2620
            afirmeCoberturas8.obligatoriedad = 0
            afirmeCoberturas8.contratada = "true"
            afirmeCoberturas8.descripcion = "ASISTENCIA EN VIAJES Y VIAL KM"
            afirmeCoberturas8.sumaAsegurada = "Amparada"
            afirmeCoberturas8.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas8)

            Dim afirmeCoberturas9 = New AFIRMECoberturas
            afirmeCoberturas9.idCobertura = 2630
            afirmeCoberturas9.obligatoriedad = 1
            afirmeCoberturas9.contratada = "true"
            afirmeCoberturas9.descripcion = "EXENCIÓN DE DEDUCIBLES RT"
            afirmeCoberturas9.sumaAsegurada = "Amparada"
            afirmeCoberturas9.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas9)

            Dim afirmeCoberturas10 = New AFIRMECoberturas
            afirmeCoberturas10.idCobertura = 4814
            afirmeCoberturas10.obligatoriedad = 0
            afirmeCoberturas10.contratada = "true"
            afirmeCoberturas10.descripcion = "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE"
            afirmeCoberturas10.sumaAsegurada = "3000000.0"
            afirmeCoberturas10.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas10)

            Dim afirmeCoberturas11 = New AFIRMECoberturas
            afirmeCoberturas11.idCobertura = 4868
            afirmeCoberturas11.obligatoriedad = 0
            afirmeCoberturas11.contratada = "true"
            afirmeCoberturas11.descripcion = "RC EN USA Y CANADA LUC OTORGADA POR CHUBB SEGUROS"
            afirmeCoberturas11.sumaAsegurada = "Amparada"
            afirmeCoberturas11.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas11)

            Dim jsonFormat = New JsonSerializerSettings
            jsonFormat.NullValueHandling = NullValueHandling.Ignore
            Dim request = JsonConvert.SerializeObject(afirmeCotizarPolizaEnvio, jsonFormat)
            Dim lg As Log = New Log()
            Try
                lg.UpdateLogRequest(request, idLogWSCot)

                ' Funciones.updateLogWSCot(request, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            respuesta = ObjAfirme.cotizarPoliza(request, token)

            Try
                lg.UpdateLogResponse(respuesta, idLogWSCot)
                'Funciones.updateLogWSCot(respuesta, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try
            'Nueva implementacion 

            Dim jsonRespuesta As JObject
            Try
                jsonRespuesta = JObject.Parse(respuesta)
            Catch ex As Exception
                Throw New System.Exception("Error al parsear respuesta de servicio web")
            End Try
            Dim Coberturas As New Coberturas
            For Each Row In jsonRespuesta
                If Row.Key = "esquemaPago" Then
                    Dim jsonEsquema As JObject = JObject.Parse(Row.Value.ToString)
                    For Each RowEsquema In jsonEsquema
                        If FormaPago.ToString = RowEsquema.Key Then
                            Dim jsonEsquema2 As JObject = JObject.Parse(RowEsquema.Value.ToString)
                            For Each RowEsquema2 In jsonEsquema2
                                If RowEsquema2.Key = "pagoInicial" Then
                                    ObjData.Cotizacion.PrimerPago = RowEsquema2.Value.ToString
                                ElseIf RowEsquema2.Key = "pagoSubsecuente" Then
                                    ObjData.Cotizacion.PagosSubsecuentes = RowEsquema2.Value.ToString
                                End If
                            Next
                        End If
                    Next
                ElseIf Row.Key = "coberturas" Then
                    Dim jsonCoberturas As JObject = JObject.Parse(Row.Value.ToString)
                    For Each RowCobertura In jsonCoberturas
                        Dim jsonCoberturas2 As JObject = JObject.Parse(RowCobertura.Value.ToString)
                        Dim descripcionCobertura As String = ""
                        Dim nombreCobertura = ""
                        Dim deducible = ""
                        Dim cobertura = ""
                        Dim SA = ""
                        For Each RowCobertura2 In jsonCoberturas2
                            If RowCobertura2.Key = "deducible" Then
                                deducible = RowCobertura2.Value.ToString
                            ElseIf RowCobertura2.Key = "descripcion" Then
                                cobertura = RowCobertura2.Value.ToString
                            ElseIf RowCobertura2.Key = "sumaAsegurada" Then
                                SA = RowCobertura2.Value.ToString
                            End If
                        Next
                        Select Case cobertura
                            Case "DAÑOS MATERIALES"
                                nombreCobertura = "DAÑOS MATERIALES"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & deducible
                                Coberturas.DanosMateriales = descripcionCobertura
                            Case "ROBO TOTAL"
                                nombreCobertura = "ROBO TOTAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & deducible
                                Coberturas.RoboTotal = descripcionCobertura
                            Case "RESPONSABILIDAD CIVIL"
                                nombreCobertura = "RESPONSABILIDAD CIVIL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.RC = descripcionCobertura
                            Case "GASTOS MÉDICOS"
                                nombreCobertura = "GASTOS MÉDICOS"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.GastosMedicosOcupantes = descripcionCobertura
                            Case "ASISTENCIA JURÍDICA"
                                nombreCobertura = "ASISTENCIA LEGAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.DefensaJuridica = descripcionCobertura
                            Case "ASISTENCIA EN VIAJES Y VIAL KM"
                                nombreCobertura = "ASISTENCIA VIAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.AsitenciaCompleta = descripcionCobertura
                            Case "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE"
                                nombreCobertura = "MUERTE ACCIDENTAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.MuerteAccidental = descripcionCobertura
                            Case "RC EN USA Y CANADA LUC OTORGADA POR CHUBB SEGUROS"
                                nombreCobertura = "RC EN EL EXTRANJERO"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.RCExtranjero = descripcionCobertura
                        End Select
                    Next

                ElseIf Row.Key = "idToCotizacion" Then
                    ObjData.Cotizacion.IDCotizacion = Row.Value.ToString
                ElseIf Row.Key = "iva" Then
                    ObjData.Cotizacion.Impuesto = Row.Value.ToString
                ElseIf Row.Key = "primaTotal" Then
                    ObjData.Cotizacion.PrimaTotal = Row.Value.ToString
                ElseIf Row.Key = "recargo" Then
                    ObjData.Cotizacion.Recargos = Row.Value.ToString
                ElseIf Row.Key = "primaNeta" Then
                    ObjData.Cotizacion.PrimaNeta = Row.Value.ToString
                ElseIf Row.Key = "derechoPago" Then
                    ObjData.Cotizacion.Derechos = Row.Value.ToString
                End If
            Next
            ObjData.Coberturas.Add(Coberturas)
            ObjData.Cotizacion.Resultado = "true"
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [" + respuesta + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "false"
        End Try
        Dim jsonResponse As String = serializer.Serialize(ObjData)
        Dim log As Log = New Log()
        log.UpdateLogJsonResponse(jsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Private Shared Function cotizarPoliza(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
        Dim ObjAfirme As New AFIRMEService.CotizacionAutoIndividualService
        Dim ObjBXMASCatalogo As New BXMASServiceCatalogo.catalogosAutosWs
        Dim serializer As New JavaScriptSerializer
        Dim respuesta As String = ""
        Dim coberturasAfirm = New AFIRMEService.coberturaView() {}
        Dim primaTotal = ObjData.Cotizacion.PrimaTotal
        ObjData.Cotizacion.Resultado = "false"
        Try
            Dim IdPaquete = 1
            If ObjData.Paquete = "AMPLIA" Then
                IdPaquete = 1
            ElseIf ObjData.Paquete = "LIMITADA" Then
                IdPaquete = 3
            ElseIf ObjData.Paquete = "BASICA" Then
                IdPaquete = 776
            End If
            Dim FormaPago = 1
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                FormaPago = 1
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                FormaPago = 2
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                FormaPago = 3
            End If
            Dim token = "a6131309-d28e-4004-b804-73797279e408"
            Dim idMarca
            Try
                idMarca = GetIdMarca(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta idMarca (clave vehiculo)")
            End Try
            Dim direccion
            Try
                direccion = ConsultaCP(ObjData.Cliente.Direccion.CodPostal, "")
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta codigo postal (CP)")
            End Try

            If direccion(0) = "DISTRITO FEDERAL" Then
                direccion(0) = "CIUDAD DE MEXICO"
            End If
            Dim descuento As Double = Convert.ToDecimal(ObjData.Descuento)
            Dim Edo_Mun
            Try
                Edo_Mun = ConsultaDireccionAfirme(direccion(0), direccion(1))
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta direccion afirme")
            End Try

            Dim idColonia, nombreColonia, nombreColoniaTemp
            Try
                Dim listaColonias = ObjAfirme.getListColonias(ObjData.Cliente.Direccion.CodPostal, token)
                Dim jsonListaColonias As Dictionary(Of String, String) = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(listaColonias)
                For Each iKey As String In jsonListaColonias.Keys
                    nombreColoniaTemp = jsonListaColonias(iKey).ToUpper
                    If ObjData.Cliente.Direccion.Colonia.ToUpper = nombreColoniaTemp Then
                        nombreColonia = jsonListaColonias(iKey)
                        idColonia = iKey
                        Exit For
                    End If
                Next
            Catch ex As Exception
            End Try

            If idColonia = Nothing Then
                nombreColonia = ObjData.Cliente.Direccion.Colonia
            End If

            'Nueva implementacion 

            Dim afirmeCotizarPolizaEnvio = New AFIRMECotizarPolizaEnvio
            afirmeCotizarPolizaEnvio.idAgente = "92901"     'OK
            afirmeCotizarPolizaEnvio.datosPoliza = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.datosPoliza.Add("idNegocio", 461)    'OK
            afirmeCotizarPolizaEnvio.datosPoliza.Add("idProducto", 658)    'OK
            afirmeCotizarPolizaEnvio.datosPoliza.Add("idTipoPoliza", 83)

            'Borrar bloque de codigo

            'Dim inicioPoliza = Convert.ToDateTime(Date.Now, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
            'Dim finPoliza = Convert.ToDateTime(Date.Now.AddDays(2), System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
            'afirmeCotizarPolizaEnvio.datosPoliza.Add("inicioVigenciaPoliza", inicioPoliza + " 00:00:00-05:00")
            'afirmeCotizarPolizaEnvio.datosPoliza.Add("finVigenciaPoliza", finPoliza + " 00:00:00-05:00")

            'Borrar bloque de codigo

            afirmeCotizarPolizaEnvio.zonaCirculacion = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.zonaCirculacion.Add("idEstadoCirculacion", Edo_Mun(0))    'OK
            afirmeCotizarPolizaEnvio.zonaCirculacion.Add("idMunicipioCirculacion", Edo_Mun(1))   'OK   
            afirmeCotizarPolizaEnvio.vehiculo = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.vehiculo.Add("idLineaNegocio", 755)   'pendiente
            afirmeCotizarPolizaEnvio.vehiculo.Add("idMarca", idMarca)  'OK
            afirmeCotizarPolizaEnvio.vehiculo.Add("modelo", CInt(ObjData.Vehiculo.Modelo))  'OK
            afirmeCotizarPolizaEnvio.vehiculo.Add("idEstilo", CInt(ObjData.Vehiculo.Clave))  'OK
            afirmeCotizarPolizaEnvio.paquete = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.paquete.Add("idPaquete", IdPaquete)  'OK
            afirmeCotizarPolizaEnvio.paquete.Add("idFormaPago", FormaPago)  'OK
            afirmeCotizarPolizaEnvio.paquete.Add("pctDescuentoEstado", descuento)  'OK

            afirmeCotizarPolizaEnvio.contratante = New Dictionary(Of String, String)
            afirmeCotizarPolizaEnvio.contratante.Add("nombreContratante", ObjData.Cliente.Nombre)
            afirmeCotizarPolizaEnvio.contratante.Add("apellidoPaterno", ObjData.Cliente.ApellidoPat)
            afirmeCotizarPolizaEnvio.contratante.Add("apellidoMaterno", ObjData.Cliente.ApellidoMat)
            afirmeCotizarPolizaEnvio.contratante.Add("rfc", ObjData.Cliente.RFC)
            afirmeCotizarPolizaEnvio.contratante.Add("claveEstadoCivil", "C")
            afirmeCotizarPolizaEnvio.contratante.Add("idEstadoNacimiento", Edo_Mun(0))
            afirmeCotizarPolizaEnvio.contratante.Add("idMunicipioNacimiento", Edo_Mun(1))
            Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
            afirmeCotizarPolizaEnvio.contratante.Add("fechaNacimiento", fechaNacimiento + " 12:00:00")
            afirmeCotizarPolizaEnvio.contratante.Add("codigoPostal", ObjData.Cliente.Direccion.CodPostal)
            afirmeCotizarPolizaEnvio.contratante.Add("idCiudad", Edo_Mun(1))
            afirmeCotizarPolizaEnvio.contratante.Add("idEstado", Edo_Mun(0))
            afirmeCotizarPolizaEnvio.contratante.Add("claveSexo", ObjData.Cliente.TipoPersona.ToString)
            afirmeCotizarPolizaEnvio.contratante.Add("claveColonia", idColonia)
            afirmeCotizarPolizaEnvio.contratante.Add("nombreColonia", nombreColonia)
            afirmeCotizarPolizaEnvio.contratante.Add("calle", ObjData.Cliente.Direccion.Calle)
            afirmeCotizarPolizaEnvio.contratante.Add("numero", ObjData.Cliente.Direccion.NoExt)
            afirmeCotizarPolizaEnvio.contratante.Add("telefonoCasa", ObjData.Cliente.Telefono)
            afirmeCotizarPolizaEnvio.contratante.Add("telefonoOficina", ObjData.Cliente.Telefono)
            afirmeCotizarPolizaEnvio.contratante.Add("email", ObjData.Cliente.Email)

            afirmeCotizarPolizaEnvio.coberturas = New List(Of AFIRMECoberturas)

            Dim afirmeCoberturas1 = New AFIRMECoberturas
            afirmeCoberturas1.idCobertura = 2510
            afirmeCoberturas1.obligatoriedad = 0
            afirmeCoberturas1.contratada = "true"
            afirmeCoberturas1.descripcion = "DAÑOS MATERIALES"
            afirmeCoberturas1.sumaAsegurada = "Valor Comercial"
            afirmeCoberturas1.deducible = 5.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas1)

            Dim afirmeCoberturas2 = New AFIRMECoberturas
            afirmeCoberturas2.idCobertura = 2520
            afirmeCoberturas2.obligatoriedad = 0
            afirmeCoberturas2.contratada = "true"
            afirmeCoberturas2.descripcion = "ROBO TOTAL"
            afirmeCoberturas2.sumaAsegurada = "Valor Comercial"
            afirmeCoberturas2.deducible = 10.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas2)

            Dim afirmeCoberturas3 = New AFIRMECoberturas
            afirmeCoberturas3.idCobertura = 2530
            afirmeCoberturas3.obligatoriedad = 0
            afirmeCoberturas3.contratada = "true"
            afirmeCoberturas3.descripcion = "RESPONSABILIDAD CIVIL"
            afirmeCoberturas3.sumaAsegurada = "2500000.0"
            afirmeCoberturas3.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas3)

            Dim afirmeCoberturas4 = New AFIRMECoberturas
            afirmeCoberturas4.idCobertura = 2540
            afirmeCoberturas4.obligatoriedad = 1
            afirmeCoberturas4.contratada = "true"
            afirmeCoberturas4.descripcion = "EXENCIÓN DE DEDUCIBLES DM"
            afirmeCoberturas4.sumaAsegurada = "Amparada"
            afirmeCoberturas4.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas4)

            Dim afirmeCoberturas5 = New AFIRMECoberturas
            afirmeCoberturas5.idCobertura = 2550
            afirmeCoberturas5.obligatoriedad = 0
            afirmeCoberturas5.contratada = "true"
            afirmeCoberturas5.descripcion = "GASTOS MÉDICOS"
            afirmeCoberturas5.sumaAsegurada = "40000.0"
            afirmeCoberturas5.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas5)

            Dim afirmeCoberturas6 = New AFIRMECoberturas
            afirmeCoberturas6.idCobertura = 2580
            afirmeCoberturas6.obligatoriedad = 1
            afirmeCoberturas6.contratada = "true"
            afirmeCoberturas6.descripcion = "EXTENSIÓN DE RESPONSABILIDAD CIVIL"
            afirmeCoberturas6.sumaAsegurada = "Amparada"
            afirmeCoberturas6.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas6)

            Dim afirmeCoberturas7 = New AFIRMECoberturas
            afirmeCoberturas7.idCobertura = 2610
            afirmeCoberturas7.obligatoriedad = 0
            afirmeCoberturas7.contratada = "true"
            afirmeCoberturas7.descripcion = "ASISTENCIA JURÍDICA"
            afirmeCoberturas7.sumaAsegurada = "Amparada"
            afirmeCoberturas7.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas7)

            Dim afirmeCoberturas8 = New AFIRMECoberturas
            afirmeCoberturas8.idCobertura = 2620
            afirmeCoberturas8.obligatoriedad = 0
            afirmeCoberturas8.contratada = "true"
            afirmeCoberturas8.descripcion = "ASISTENCIA EN VIAJES Y VIAL KM"
            afirmeCoberturas8.sumaAsegurada = "Amparada"
            afirmeCoberturas8.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas8)

            Dim afirmeCoberturas9 = New AFIRMECoberturas
            afirmeCoberturas9.idCobertura = 2630
            afirmeCoberturas9.obligatoriedad = 1
            afirmeCoberturas9.contratada = "true"
            afirmeCoberturas9.descripcion = "EXENCIÓN DE DEDUCIBLES RT"
            afirmeCoberturas9.sumaAsegurada = "Amparada"
            afirmeCoberturas9.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas9)

            Dim afirmeCoberturas10 = New AFIRMECoberturas
            afirmeCoberturas10.idCobertura = 4814
            afirmeCoberturas10.obligatoriedad = 0
            afirmeCoberturas10.contratada = "true"
            afirmeCoberturas10.descripcion = "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE"
            afirmeCoberturas10.sumaAsegurada = "3000000.0"
            afirmeCoberturas10.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas10)

            Dim afirmeCoberturas11 = New AFIRMECoberturas
            afirmeCoberturas11.idCobertura = 4868
            afirmeCoberturas11.obligatoriedad = 0
            afirmeCoberturas11.contratada = "true"
            afirmeCoberturas11.descripcion = "RC EN USA Y CANADA LUC OTORGADA POR CHUBB SEGUROS"
            afirmeCoberturas11.sumaAsegurada = "Amparada"
            afirmeCoberturas11.deducible = 0.0
            afirmeCotizarPolizaEnvio.coberturas.Add(afirmeCoberturas11)

            Dim jsonFormat = New JsonSerializerSettings
            jsonFormat.NullValueHandling = NullValueHandling.Ignore
            Dim request = JsonConvert.SerializeObject(afirmeCotizarPolizaEnvio, jsonFormat)

            Try
                Funciones.updateLogWSCot(request, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try

            respuesta = ObjAfirme.cotizarPoliza(request, token)

            Try
                Funciones.updateLogWSCot(respuesta, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try

            Dim jsonRespuesta As JObject
            Try
                jsonRespuesta = JObject.Parse(respuesta)
            Catch ex As Exception
                Throw New System.Exception("Error al parsear respuesta de servicio web")
            End Try
            Dim Coberturas As New Coberturas
            For Each Row In jsonRespuesta
                If Row.Key = "esquemaPago" Then
                    Dim jsonEsquema As JObject = JObject.Parse(Row.Value.ToString)
                    For Each RowEsquema In jsonEsquema
                        If FormaPago.ToString = RowEsquema.Key Then
                            Dim jsonEsquema2 As JObject = JObject.Parse(RowEsquema.Value.ToString)
                            For Each RowEsquema2 In jsonEsquema2
                                If RowEsquema2.Key = "pagoInicial" Then
                                    ObjData.Cotizacion.PrimerPago = RowEsquema2.Value.ToString
                                ElseIf RowEsquema2.Key = "pagoSubsecuente" Then
                                    ObjData.Cotizacion.PagosSubsecuentes = RowEsquema2.Value.ToString
                                End If
                            Next
                        End If
                    Next
                ElseIf Row.Key = "coberturas" Then
                    Dim jsonCoberturas As JObject = JObject.Parse(Row.Value.ToString)
                    For Each RowCobertura In jsonCoberturas
                        Dim jsonCoberturas2 As JObject = JObject.Parse(RowCobertura.Value.ToString)
                        Dim descripcionCobertura As String = ""
                        Dim nombreCobertura = ""
                        Dim deducible = ""
                        Dim cobertura = ""
                        Dim SA = ""
                        For Each RowCobertura2 In jsonCoberturas2
                            If RowCobertura2.Key = "deducible" Then
                                deducible = RowCobertura2.Value.ToString
                            ElseIf RowCobertura2.Key = "descripcion" Then
                                cobertura = RowCobertura2.Value.ToString
                            ElseIf RowCobertura2.Key = "sumaAsegurada" Then
                                SA = RowCobertura2.Value.ToString
                            End If
                        Next
                        Select Case cobertura
                            Case "DAÑOS MATERIALES"
                                nombreCobertura = "DAÑOS MATERIALES"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & deducible
                                Coberturas.DanosMateriales = descripcionCobertura
                            Case "ROBO TOTAL"
                                nombreCobertura = "ROBO TOTAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & deducible
                                Coberturas.RoboTotal = descripcionCobertura
                            Case "RESPONSABILIDAD CIVIL"
                                nombreCobertura = "RESPONSABILIDAD CIVIL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.RC = descripcionCobertura
                            Case "GASTOS MÉDICOS"
                                nombreCobertura = "GASTOS MÉDICOS"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.GastosMedicosOcupantes = descripcionCobertura
                            Case "ASISTENCIA JURÍDICA"
                                nombreCobertura = "ASISTENCIA LEGAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.DefensaJuridica = descripcionCobertura
                            Case "ASISTENCIA EN VIAJES Y VIAL KM"
                                nombreCobertura = "ASISTENCIA VIAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.AsitenciaCompleta = descripcionCobertura
                            Case "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE"
                                nombreCobertura = "MUERTE ACCIDENTAL"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.MuerteAccidental = descripcionCobertura
                            Case "RC EN USA Y CANADA LUC OTORGADA POR CHUBB SEGUROS"
                                nombreCobertura = "RC EN EL EXTRANJERO"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & SA & "-D0"
                                Coberturas.RCExtranjero = descripcionCobertura
                        End Select
                    Next

                ElseIf Row.Key = "idToCotizacion" Then
                    ObjData.Cotizacion.IDCotizacion = Row.Value.ToString
                ElseIf Row.Key = "iva" Then
                    ObjData.Cotizacion.Impuesto = Row.Value.ToString
                ElseIf Row.Key = "primaTotal" Then
                    ObjData.Cotizacion.PrimaTotal = Row.Value.ToString
                ElseIf Row.Key = "recargo" Then
                    ObjData.Cotizacion.Recargos = Row.Value.ToString
                ElseIf Row.Key = "primaNeta" Then
                    ObjData.Cotizacion.PrimaNeta = Row.Value.ToString
                ElseIf Row.Key = "derechoPago" Then
                    ObjData.Cotizacion.Derechos = Row.Value.ToString
                End If
            Next
            If primaTotal = ObjData.Cotizacion.PrimaTotal Then
                ObjData.Coberturas.Add(Coberturas)
                ObjData.Cotizacion.Resultado = "true"
            End If
        Catch ex As Exception
            ObjData.CodigoError = "Error al realizar intento de re-cotizacion WSRequest: [" + respuesta + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "false"
        End Try
    End Function

    Private Shared Function GetIdMarca(ByVal Clave As String) As Int16
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "AFIRME", Clave)
        Dim mapaVehiculo = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim marcaAseguradora = mapaVehiculo.Item("providers_maker")


        Dim IdMarca As Int16

        If marcaAseguradora = "CHEVROLET" Then
            marcaAseguradora = "GENERAL MOTORS"
        ElseIf marcaAseguradora = "DODGE" Then
            marcaAseguradora = "CHRYSLER"
        End If

        Dim ConnectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;"
        Dim sql = "SELECT IdMarca FROM `Ali_R_CatMarcasAfirme` where Marca = '" + marcaAseguradora + "'"
        Using Connection As New MySqlConnection(ConnectionString)
            Dim command As New MySqlCommand(sql, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)

                reader.Read()
                IdMarca = reader("IdMarca")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return IdMarca
    End Function
    Private Shared Function ConsultaCP(ByVal CPostal As String, ByVal Colonia As String) As String()
        Dim Lista(1) As String
        Dim BusquedaColonia As String = ""
        If Colonia <> "" And Colonia <> Nothing Then
            BusquedaColonia = " And Colonia='" & Colonia & "'"
        End If
        Dim ConnectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;"
        Dim strMYSQLQueryCPostal = "Select * from `Ali_R_CatEdosQUA` where CPostal = '" & CPostal & "'" & BusquedaColonia
        Using Connection As New MySqlConnection(ConnectionString)
            Dim command As New MySqlCommand(strMYSQLQueryCPostal, Connection)
            Try
                Connection.Open()

                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                reader.Read()
                Lista(0) = reader("Estado")
                Lista(1) = reader("Municipio")
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Lista
    End Function
    Private Shared Function ConsultaDireccionAfirme(ByVal estado As String, ByVal municipio As String) As String()
        Dim strSQLQueryCPostal As String
        Dim Lista(1) As String
        Dim BusquedaColonia As String = ""


        If estado = "VERACRUZ DE IGNACIO DE LA LLAVE" Then
            estado = "VERACRUZ"
        ElseIf estado = "COAHUILA DE ZARAGOZA" Then
            estado = "COAHUILA"
        ElseIf estado = "BAJA CALIFORNIA" Then
            estado = "BAJA CALIFORNIA NORTE"
        ElseIf estado = "MICHOACAN DE OCAMPO" Then
            estado = "MICHOACAN"
        End If

        If municipio = "SAN PEDRO TLAQUEPAQUE" Then
            municipio = "TLAQUEPAQUE"
        End If
        Dim connectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db"
        strSQLQueryCPostal = "Select * from `ws_db`.`Ali_R_CatEdosAfirme`  where estado = '" & estado & "' and municipio='" & municipio & "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim rsCPostal As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (rsCPostal.Read())
                    Lista(0) = rsCPostal("IdEdo")
                    Lista(1) = rsCPostal("IdMunicipio")
                End While
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Lista
    End Function

    Private Shared Function BancosAFIRME(ByVal banco As String) As String
        Select Case UCase(banco)
            Case "AFIRME"
                Return "51"
            Case "BANAMEX"
                Return "50"
            Case "BANCA MIFEL SA"
                Return "72"
            Case "BANCO DEL BAJIO"
                Return "65"
            Case "BANCO FAMSA"
                Return "80"
            Case "BANCO INVEX"
                Return "68"
            Case "BANJERCITO"
                Return "67"
            Case "BANORTE"
                Return "53"
            Case "BANREGIO"
                Return "62"
            Case "BBVA BANCOMER"
                Return "56"
            Case "HSBC"
                Return "57"
            Case "INBURSA"
                Return "54"
            Case "ITAUCARD"
                Return "73"
            Case "IXE BANCO"
                Return "58"
            Case "SANTANDER"
                Return "55"
            Case "SCOTIABANK"
                Return "59"
        End Select
        Return "Error EN EL BANCO"
    End Function

End Class

Class AFIRMECotizarPolizaEnvio
    Public Property idAgente As String
    Public Property datosPoliza As Dictionary(Of String, String)
    Public Property zonaCirculacion As Dictionary(Of String, String)
    Public Property vehiculo As Dictionary(Of String, String)
    Public Property paquete As Dictionary(Of String, String)
    Public Property contratante As Dictionary(Of String, String)
    Public Property coberturas As List(Of AFIRMECoberturas)
End Class

Class AFIRMEEmitePolizaEnvio
    Public Property idCotizacion As String
    Public Property observaciones As String
    Public Property documentacionCompleta As String
    Public Property autoInciso As Dictionary(Of String, Object)
    Public Property paquete As Dictionary(Of String, String)
    Public Property conductoCobro As Dictionary(Of String, Object)
    Public Property contratante As Dictionary(Of String, Integer)
End Class

Class AFIRMECoberturas
    Public Property idCobertura As Integer
    Public Property obligatoriedad As Integer
    Public Property contratada As Boolean
    Public Property descripcion As String
    Public Property sumaAsegurada As String
    Public Property deducible As Double
End Class

Class AFIRMECliente
    Public Property codigoRFC As String
    Public Property nombre As String
    Public Property idCliente As String
End Class