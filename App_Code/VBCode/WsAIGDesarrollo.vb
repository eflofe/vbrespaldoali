﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Xml.Serialization
Imports SuperObjeto
Imports System.Net
Imports System.Globalization

Public Class WsAIGDesarrollo
    Public Shared Function AIGCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)

        Try
            CallWebService(ObjData, idLogWSCot)
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Dim serializer As New JavaScriptSerializer
        Dim lg As Log = New Log()
        Dim jsonResponse As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(jsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function AIGEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Try
            Dim generalesAIG = New GeneralesAIG()
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                generalesAIG.genero = "2"
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                generalesAIG.genero = "1"
            End If
            If ObjData.Pago.Carrier = Pago.CatCarrier.VISA Then
                generalesAIG.conducto = "2"
            ElseIf ObjData.Pago.Carrier = Pago.CatCarrier.MASTERCARD Then
                generalesAIG.conducto = "3"
            ElseIf ObjData.Pago.Carrier = Pago.CatCarrier.AMEX Then
                generalesAIG.conducto = "4"
            End If
            If UCase(ObjData.Pago.MedioPago) = "DEBITO" Then
                generalesAIG.conducto = "7"
            End If
            If UCase(ObjData.Pago.MedioPago) = "EFECTIVO" Then
                generalesAIG.conducto = "1"
            End If
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                generalesAIG.formaPago = "1"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                generalesAIG.formaPago = "6"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                generalesAIG.formaPago = "2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                generalesAIG.formaPago = "4"
            End If
            If ObjData.Paquete = "AMPLIA" Then
                generalesAIG.paquete = "1"
            ElseIf ObjData.Paquete = "LIMITADA" Then
                generalesAIG.paquete = "2"
            ElseIf ObjData.Paquete = "RC" Then
                generalesAIG.paquete = "3"
            ElseIf ObjData.Paquete = "GOLD" Then
                generalesAIG.paquete = "4"
            End If
            ObjData.Emision.Resultado = ""
            ObjData.CodigoError = ""
            generalesAIG.operacion = GeneralesAIG.EnumOperacion.consultaCP
            CallWebServiceEmision(ObjData, idLogWS, generalesAIG)
            generalesAIG.operacion = GeneralesAIG.EnumOperacion.obtenerModeloSISA
            CallWebServiceEmision(ObjData, idLogWS, generalesAIG)
            BancosAIG(ObjData.Pago.Banco, generalesAIG)
            generalesAIG.operacion = GeneralesAIG.EnumOperacion.generaEmision
            CallWebServiceEmision(ObjData, idLogWS, generalesAIG)
            If ObjData.CodigoError Is String.Empty Then
                generalesAIG.operacion = GeneralesAIG.EnumOperacion.procedePago
                CallWebServiceEmision(ObjData, idLogWS, generalesAIG)
            End If
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function CallWebServiceEmision(ByVal ObjData As Seguro, ByVal idLogWSCot As String, ByVal generalesAIG As GeneralesAIG)
        Dim respuestaWS As String = ""
        Try
            Dim soapEnvelopeXml As XmlDocument
            Dim _url As String = "https://www-404uat.aig.com/EOL.AutoIndividual.WebServices/WSEmision.svc"
            Dim action As String = ""
            Select Case generalesAIG.operacion
                Case GeneralesAIG.EnumOperacion.generaEmision
                    ObjData.Emision.Resultado = True
                    action = "http://tempuri.org/IWsEmision/GenerarEmision140"
                    soapEnvelopeXml = CreateSoapEnvelopeGeneraEmision(ObjData, generalesAIG)
                Case GeneralesAIG.EnumOperacion.consultaCP
                    _url = "https://www-404uat.aig.com/EOL.AutoIndividual.WebServices/WSCatalogo.svc"
                    action = "http://tempuri.org/IWsCatalogo/ObtenerColoniaCp"
                    soapEnvelopeXml = CreateSoapEnvelopeConsultaCP(ObjData)
                Case GeneralesAIG.EnumOperacion.obtenerModeloSISA
                    _url = "https://www-404uat.aig.com/EOL.AutoIndividual.WebServices/WSCatalogo.svc"
                    action = "http://tempuri.org/IWsCatalogo/ObtenerModeloSisa"
                    soapEnvelopeXml = CreateSoapEnvelopeObtenerModeloSISA(ObjData)
                Case GeneralesAIG.EnumOperacion.procedePago
                    ObjData.Emision.Resultado = True
                    _url = "https://www-404.aig.com.mx/AIG.Payment/Payment.svc"
                    action = "http://tempuri.org/IPayment/SubmitPayment"
                    soapEnvelopeXml = CreateSoapEnvelopeProcePago(ObjData, generalesAIG)
            End Select

            Dim webRequest = CreateWebRequest(_url, action)
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest)
            ' begin async call to web request.
            Dim asyncResult As IAsyncResult = webRequest.BeginGetResponse(Nothing, Nothing)
            ' suspend this thread until call is complete. You might want to
            ' do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne()
            ' get the response from the completed web request.
            Dim soapResult As String
            Dim doc As XmlDocument = New XmlDocument()
            If generalesAIG.operacion = GeneralesAIG.EnumOperacion.generaEmision Or generalesAIG.operacion = GeneralesAIG.EnumOperacion.procedePago Then
                Try
                    Dim x2 As New XmlSerializer(soapEnvelopeXml.GetType)
                    Dim xml2 As New StringWriter
                    x2.Serialize(xml2, soapEnvelopeXml)
                    Dim xmlRespuesta As String = xml2.ToString
                    generalesAIG.xmlSolicitud = generalesAIG.xmlSolicitud + " | " + xmlRespuesta
                    Funciones.updateLogWS(generalesAIG.xmlSolicitud, idLogWSCot, "RequestWS")
                Catch ex As Exception
                    Dim errr = ex
                End Try
            End If
            Using webResponse As WebResponse = webRequest.EndGetResponse(asyncResult)
                Using rd As New StreamReader(webResponse.GetResponseStream())
                    soapResult = rd.ReadToEnd()
                End Using
                If generalesAIG.operacion = GeneralesAIG.EnumOperacion.procedePago Then
                    soapResult = soapResult.Replace("&gt;", " ").Replace("&lt;", " ").Replace("&#xD;", "").Replace("&amp;", "").Replace("acute", "")
                End If
                soapResult = soapResult.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&#xD;", "").Replace("&amp;", "").Replace("acute", "").Replace("<?xml version=""1.0"" encoding=""utf-8""?>", "")
                If generalesAIG.operacion = GeneralesAIG.EnumOperacion.generaEmision Or generalesAIG.operacion = GeneralesAIG.EnumOperacion.procedePago Then
                    Try
                        generalesAIG.xmlRespuesta = generalesAIG.xmlRespuesta + " | " + soapResult
                        Funciones.updateLogWS(generalesAIG.xmlRespuesta, idLogWSCot, "ResponseWS")
                    Catch ex As Exception
                        Dim errr = ex
                    End Try
                End If
                doc.LoadXml(soapResult)
            End Using
            Dim colElementos As XmlNodeList
            Select Case generalesAIG.operacion
                Case generalesAIG.EnumOperacion.validaEmision
                    colElementos = doc.GetElementsByTagName("wsEmitirValidaDatosResult")
                    For Each iterElementos In colElementos
                        Dim txtImporte = iterElementos("a:_isValid").InnerText
                        If txtImporte = "false" Then
                            ObjData.Emision.Resultado = "false"
                            Dim errores As XmlNodeList = doc.GetElementsByTagName("a:clsErroresValidaEmisionResponse")
                            Dim mensajeError = "["
                            For Each iterErrores In errores
                                mensajeError += iterErrores("a:code").InnerText & ","
                                mensajeError += iterErrores("a:desc").InnerText & " "
                            Next
                            mensajeError += "]"
                            respuestaWS = mensajeError
                            Throw New System.Exception("Error al invocar metodo valida emision")
                        Else
                            ObjData.Emision.Resultado = "true"
                        End If
                    Next
                Case generalesAIG.EnumOperacion.guardaEmision
                    colElementos = doc.GetElementsByTagName("GuardaEmision140Response")
                    For Each iterElementos In colElementos
                        ObjData.Emision.Poliza = iterElementos("GuardaEmision140Result").InnerText
                    Next
                Case generalesAIG.EnumOperacion.generaEmision
                    colElementos = doc.GetElementsByTagName("GenerarEmision140Response")
                    For Each iterElementos In colElementos
                        ObjData.Emision.Documento = iterElementos("GenerarEmision140Result").InnerText
                        ObjData.Emision.PrimaTotal = ObjData.Cotizacion.PrimaTotal
                        ObjData.Emision.PrimaNeta = ObjData.Cotizacion.PrimaNeta
                        ObjData.Emision.Derechos = ObjData.Cotizacion.Derechos
                        ObjData.Emision.Impuesto = ObjData.Cotizacion.Impuesto
                        ObjData.Emision.Recargos = ObjData.Cotizacion.Recargos
                        ObjData.Emision.PrimerPago = ObjData.Cotizacion.PrimerPago
                        Try
                            For Index As Integer = ObjData.Emision.Documento.Count - 5 To 0 Step -1
                                If ObjData.Emision.Documento.Substring(Index, 1).Equals("_") Then
                                    ObjData.Emision.Poliza = ObjData.Emision.Documento.Substring(Index + 1, (ObjData.Emision.Documento.Count - 5) - Index)
                                    Exit For
                                End If
                            Next
                        Catch ex As Exception
                            ObjData.CodigoError = "WSRequest: [Operacion: " + generalesAIG.operacion.ToString + " - " + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
                            ObjData.Emision.Resultado = "false"
                        End Try
                    Next
                Case generalesAIG.EnumOperacion.consultaCP
                    colElementos = doc.GetElementsByTagName("Table")
                    For Each iterElementos In colElementos
                        generalesAIG.codEstado = iterElementos("cod_estado").InnerText
                        generalesAIG.codMunicipio = iterElementos("cod_municipio").InnerText
                        generalesAIG.codColonia = iterElementos("cod_colonia").InnerText
                        generalesAIG.codCiudad = iterElementos("cod_ciudad").InnerText
                        Exit For
                    Next
                Case generalesAIG.EnumOperacion.obtenerModeloSISA
                    colElementos = doc.GetElementsByTagName("Table")
                    For Each iterElementos In colElementos
                        generalesAIG.codMarca = iterElementos("cod_marca").InnerText
                        generalesAIG.codModelo = iterElementos("cod_modelo").InnerText
                        Exit For
                    Next
                Case GeneralesAIG.EnumOperacion.procedePago
                    colElementos = doc.GetElementsByTagName("a:result")
                    For Each iterElementos In colElementos
                        generalesAIG.idCodigoPago = iterElementos("a:ID").InnerText
                        generalesAIG.desRespuestaPago = iterElementos("a:Description").InnerText
                        If generalesAIG.idCodigoPago = "00" Then
                            ObjData.Emision.Resultado = "true"
                        Else
                            ObjData.Emision.Resultado = "false"
                            ObjData.CodigoError = generalesAIG.desRespuestaPago
                        End If
                        Exit For
                    Next
            End Select
        Catch exweb As WebException
            Try
                Using stream = exweb.Response.GetResponseStream()
                    Dim reader = New StreamReader(stream)
                    respuestaWS = reader.ReadToEnd()
                    If generalesAIG.operacion = GeneralesAIG.EnumOperacion.generaEmision Or generalesAIG.operacion = GeneralesAIG.EnumOperacion.procedePago Then
                        Try
                            generalesAIG.xmlRespuesta = generalesAIG.xmlRespuesta + " | " + respuestaWS
                            Funciones.updateLogWS(generalesAIG.xmlRespuesta, idLogWSCot, "ResponseWS")
                        Catch ex As Exception
                            Dim errr = ex
                        End Try
                    End If
                    ObjData.CodigoError = "WSRequest: [Operacion: " + generalesAIG.operacion.ToString + " - " + respuestaWS + "]" + ", Message: [" + exweb.Message + "], StackTrace: [" + exweb.StackTrace.ToString() + "]"
                    ObjData.Emision.Resultado = "false"
                End Using
            Catch ex As Exception
                ObjData.CodigoError = "Error al recuperar codigo de respuesta"
                ObjData.Emision.Resultado = "false"
            End Try
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [Operacion: " + generalesAIG.operacion.ToString + " - " + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Emision.Resultado = "false"
        End Try
    End Function

    Public Shared Sub CallWebService(ObjData As Seguro, ByVal idLogWSCot As String)
        Dim _url = "https://www-404uat.aig.com/EOL.AutoIndividual.WebServices/WsCotizacion.svc"
        Dim action = "http://tempuri.org/IWsCotizacion/GenerarCotizacion"

        Dim soapEnvelopeXml As XmlDocument = CreateSoapEnvelope(ObjData)
        Dim webRequest As HttpWebRequest = CreateWebRequest(_url, action)
        InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest)


        ' begin async call to web request.
        Dim asyncResult As IAsyncResult = webRequest.BeginGetResponse(Nothing, Nothing)

        ' suspend this thread until call is complete. You might want to
        ' do something usefull here like update your UI.
        asyncResult.AsyncWaitHandle.WaitOne()

        ' get the response from the completed web request.
        Dim soapResult As String
        Dim nombreCobertura As String = ""
        Dim doc As XmlDocument = New XmlDocument()
        Dim Coberturas As New Coberturas
        Dim lg As Log = New Log()
        Try
            Dim x2 As New XmlSerializer(soapEnvelopeXml.GetType)
            Dim xml2 As New StringWriter
            x2.Serialize(xml2, soapEnvelopeXml)
            Dim xmlRespuesta As String = xml2.ToString
            lg.UpdateLogRequest(xmlRespuesta, idLogWSCot)
            ' Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "RequestWS", "FechaInicio")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Using webResponse As WebResponse = webRequest.EndGetResponse(asyncResult)
            Using rd As New StreamReader(webResponse.GetResponseStream())
                soapResult = rd.ReadToEnd()
            End Using
            soapResult = soapResult.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&#xD;", "").Replace("<?xml version=""1.0"" encoding=""utf-8""?>", "")
            doc.LoadXml(soapResult)
        End Using

        Try
            lg.UpdateLogResponse(soapResult, idLogWSCot)
            'Funciones.updateLogWSCot(soapResult, idLogWSCot, "ResponseWS", "FechaFin")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Dim colElementos As XmlNodeList = doc.GetElementsByTagName("NewDataSet")
        For Each objNodoPaquete In colElementos
            Dim colElementos2 As XmlNodeList = objNodoPaquete.GetElementsByTagName("Importes")
            For Each objnodo2 In colElementos2
                Dim txtImporte = objnodo2("txt_Importe").InnerText
                If txtImporte = "Prima Neta:" Then
                    ObjData.Cotizacion.PrimaNeta = objnodo2("imp_Pq_Amplio").InnerText
                ElseIf txtImporte = "Recargo por Pago Fraccionado: " Then
                    ObjData.Cotizacion.Recargos = objnodo2("imp_Pq_Amplio").InnerText
                ElseIf txtImporte = "Iva: " Then
                    ObjData.Cotizacion.Impuesto = objnodo2("imp_Pq_Amplio").InnerText
                ElseIf txtImporte = "Prima Total: " Then
                    ObjData.Cotizacion.PrimaTotal = objnodo2("imp_Pq_Amplio").InnerText
                    ObjData.Cotizacion.Resultado = "True"
                ElseIf txtImporte = "Primer Pago:" Then
                    ObjData.Cotizacion.PrimerPago = objnodo2("imp_Pq_Amplio").InnerText
                ElseIf txtImporte = "Gastos de Expedición" Then
                    ObjData.Cotizacion.Derechos = objnodo2("imp_Pq_Amplio").InnerText
                ElseIf txtImporte = "Pagos Subsecuentes:" Then
                    ObjData.Cotizacion.PagosSubsecuentes = objnodo2("imp_Pq_Amplio").InnerText
                End If


            Next
            Dim colElementos3 As XmlNodeList = objNodoPaquete.GetElementsByTagName("Coberturas")
            For Each objnodo3 In colElementos3
                nombreCobertura = objnodo3("txt_desc_cobertura").InnerText
                Dim deducible = (objnodo3("deducibles").InnerText).replace(".00 %", "")
                Dim suma = objnodo3("suma").InnerText
                Dim descripcionCobertura As String = ""

                If suma = "VALOR GUIA EBC" Then
                    suma = "AMPARADA"
                End If
                Select Case nombreCobertura

                    Case "DAÑO MATERIAL"
                        nombreCobertura = "DAÑOS MATERIALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & deducible
                        Coberturas.DanosMateriales = descripcionCobertura
                    Case "ROBO TOTAL"
                        nombreCobertura = "ROBO TOTAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & deducible
                        Coberturas.RoboTotal = descripcionCobertura
                    Case "RESPONSABILIDAD CIVIL (L.U.C.)"
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & deducible
                        Coberturas.RC = descripcionCobertura
                    Case "GASTOS MEDICOS"
                        nombreCobertura = "GASTOS MÉDICOS"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & deducible
                        Coberturas.GastosMedicosOcupantes = descripcionCobertura
                    Case "S.O.S. DEFENSA LEGAL Y FIANZA"
                        nombreCobertura = "GASTOS LEGALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & deducible
                        Coberturas.DefensaJuridica = descripcionCobertura
                    Case "ASISTENCIA VIAL Y EN VIAJES"
                        nombreCobertura = "ASISTENCIA VIAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & deducible
                        Coberturas.AsitenciaCompleta = descripcionCobertura
                End Select
            Next
            Dim colElementos4 As XmlNodeList = objNodoPaquete.GetElementsByTagName("Cotizacion")
            For Each objnodo4 In colElementos4
                ObjData.Cotizacion.IDCotizacion = objnodo4("cod_Cotizacion").InnerText
            Next

            Try
                Dim colElementos5 As XmlNodeList = objNodoPaquete.GetElementsByTagName("ErrorDesc")
                ObjData.CodigoError = colElementos5.Item(0).InnerText
                ObjData.Cotizacion.Resultado = "FALSE"
            Catch ex As Exception
            End Try
        Next
        ObjData.Coberturas.Add(Coberturas)
    End Sub

    Private Shared Function CreateSoapEnvelope(ObjData As Seguro) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        Dim Finicio = Date.Now.ToString("yyyyMMdd")
        Dim FFin = Date.Now.AddYears(1).ToString("yyyyMMdd")
        Dim FormaPago As String = ""
        Dim Genero = ""
        Dim paquete = ""
        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = "2"
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = "1"
        End If
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            FormaPago = "1"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            FormaPago = "6"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            FormaPago = "2"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            FormaPago = "4"
        End If
        If ObjData.Paquete = "AMPLIA" Then
            paquete = "1"
        ElseIf ObjData.Paquete = "LIMITADA" Then
            paquete = "2"
        ElseIf ObjData.Paquete = "RC" Then
            paquete = "3"
        ElseIf ObjData.Paquete = "GOLD" Then
            paquete = "4"
        End If


        Dim peticion = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>" &
   "<soapenv:Header xmlns:wsa='http://schemas.xmlsoap.org/ws/2004/08/addressing'><wsse:Security soapenv:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-53215C56EE615B517615373621394271'><wsse:Username>juan.morales</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>Mexico2018.</wsse:Password><wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>UBYk29r4FBtuO4gA3WX0PA==</wsse:Nonce><wsu:Created>2018-09-19T13:02:19.422Z</wsu:Created></wsse:UsernameToken></wsse:Security><wsa:Action>http://tempuri.org/IWsEmision/GenerarCotizacion</wsa:Action><wsa:To>https://www-404.aig.com.mx/EOL.AutoIndividual.WebServices/WSCotizacion.svc</wsa:To></soapenv:Header>" &
        "<soapenv:Body>" &
        "<tem:GenerarCotizacion>" &
            "<tem:codigoPostal>" & ObjData.Cliente.Direccion.CodPostal & "</tem:codigoPostal>" &
            "<tem:SISA>" & ObjData.Vehiculo.Clave & "</tem:SISA>" &
            "<tem:modelo>" & ObjData.Vehiculo.Modelo & "</tem:modelo>" &
            "<tem:deducibleRT>10</tem:deducibleRT>" &
            "<tem:deducibleDM>5</tem:deducibleDM>" &
            "<tem:coberturaRC>3000000</tem:coberturaRC>" &
            "<tem:coberturaGM>50000</tem:coberturaGM>" &
            "<tem:tipoPaquete>" & paquete & "</tem:tipoPaquete>" &
            "<tem:sexo>" & Genero & "</tem:sexo>" &
            "<tem:edad>" & ObjData.Cliente.Edad & "</tem:edad>" &
            "<tem:username>juan.morales</tem:username>" &
            "<tem:password>Mexico2018.</tem:password>" &
            "<tem:formaPago>" & FormaPago & "</tem:formaPago>" &
            "<tem:conductoPago>2</tem:conductoPago>" &
            "<tem:descuento>0</tem:descuento>" &
        "</tem:GenerarCotizacion>" &
    "</soapenv:Body>" &
"</soapenv:Envelope>"

        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>" &
   "<soapenv:Header xmlns:wsa='http://schemas.xmlsoap.org/ws/2004/08/addressing'><wsse:Security soapenv:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-53215C56EE615B517615373621394271'><wsse:Username>juan.morales</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>Mexico2018.</wsse:Password><wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>UBYk29r4FBtuO4gA3WX0PA==</wsse:Nonce><wsu:Created>2018-09-19T13:02:19.422Z</wsu:Created></wsse:UsernameToken></wsse:Security><wsa:Action>http://tempuri.org/IWsEmision/GenerarCotizacion</wsa:Action><wsa:To>https://www-404.aig.com.mx/EOL.AutoIndividual.WebServices/WSCotizacion.svc</wsa:To></soapenv:Header>" &
        "<soapenv:Body>" &
        "<tem:GenerarCotizacion>" &
            "<tem:codigoPostal>" & ObjData.Cliente.Direccion.CodPostal & "</tem:codigoPostal>" &
            "<tem:SISA>" & ObjData.Vehiculo.Clave & "</tem:SISA>" &
            "<tem:modelo>" & ObjData.Vehiculo.Modelo & "</tem:modelo>" &
            "<tem:deducibleRT>10</tem:deducibleRT>" &
            "<tem:deducibleDM>5</tem:deducibleDM>" &
            "<tem:coberturaRC>3000000</tem:coberturaRC>" &
            "<tem:coberturaGM>50000</tem:coberturaGM>" &
            "<tem:tipoPaquete>" & paquete & "</tem:tipoPaquete>" &
            "<tem:sexo>" & Genero & "</tem:sexo>" &
            "<tem:edad>" & ObjData.Cliente.Edad & "</tem:edad>" &
            "<tem:username>juan.morales</tem:username>" &
            "<tem:password>Mexico2018.</tem:password>" &
            "<tem:formaPago>" & FormaPago & "</tem:formaPago>" &
            "<tem:conductoPago>2</tem:conductoPago>" &
            "<tem:descuento>0</tem:descuento>" &
        "</tem:GenerarCotizacion>" &
    "</soapenv:Body>" &
"</soapenv:Envelope>")
        Return soapEnvelop
    End Function

    Private Shared Function CreateSoapEnvelopeGeneraEmision(ObjData As Seguro, ByVal generalesAIG As GeneralesAIG) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        Dim fechaDeNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:eol='http://schemas.datacontract.org/2004/07/EOL.AutoIndividual.BusinessEntity' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>" &
   "<soapenv:Header><wsse:Security soapenv:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-98894CF10AA1BF2FF5154234247496845'><wsse:Username>juan.morales</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>Mexico2018.</wsse:Password><wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>Y1wbifPfVjND2w0sI2wAMQ==</wsse:Nonce></wsse:UsernameToken></wsse:Security></soapenv:Header>" &
       "<soapenv:Body>" &
            "<tem:GenerarEmision140>" &
                "<tem:idCotizacion>" & ObjData.Cotizacion.IDCotizacion & "</tem:idCotizacion>" &
                "<tem:paqueteEmitir>" & generalesAIG.paquete & "</tem:paqueteEmitir>" &
                "<tem:asegurado>" &
                    "<eol:A_Materno>" & ObjData.Cliente.ApellidoMat & "</eol:A_Materno>" &
                    "<eol:A_Paterno>" & ObjData.Cliente.ApellidoPat & "</eol:A_Paterno>" &
                    "<eol:CP>" & ObjData.Cliente.Direccion.CodPostal & "</eol:CP>" &
                    "<eol:Calle>" & ObjData.Cliente.Direccion.Calle & "</eol:Calle>" &
                    "<eol:Cod_Ciudad>" & generalesAIG.codCiudad & "</eol:Cod_Ciudad>" &
                    "<eol:Cod_Colonia>" & generalesAIG.codColonia & "</eol:Cod_Colonia>" &
                    "<eol:Cod_Est_Civil>6</eol:Cod_Est_Civil>" &
                    "<eol:Cod_Estado>" & generalesAIG.codEstado & "</eol:Cod_Estado>" &
                    "<eol:Cod_Municipio>" & generalesAIG.codMunicipio & "</eol:Cod_Municipio>" &
                    "<eol:Cod_Pais>10</eol:Cod_Pais>" &
                    "<eol:Cod_Sexo>" & generalesAIG.genero & "</eol:Cod_Sexo>" &
                    "<eol:Cod_Tipo_Persona>1</eol:Cod_Tipo_Persona>" &
                    "<eol:Curp>" & ObjData.Cliente.CURP & "</eol:Curp>" &
                    "<eol:Edad>" & ObjData.Cliente.Edad & "</eol:Edad>" &
                    "<eol:Email>" & ObjData.Cliente.Email & "</eol:Email>" &
                    "<eol:Fec_Nacimiento>" & fechaDeNacimiento & "T00:00:00Z</eol:Fec_Nacimiento>" &
                    "<eol:Fecha_Fin>" & Date.Now.AddYears(1).ToString("yyyy-MM-dd") & "T00:00:00Z</eol:Fecha_Fin>" &
                    "<eol:Fecha_Ini>" & Date.Now.ToString("yyyy-MM-dd") & "T00:00:00Z</eol:Fecha_Ini>" &
                    "<eol:Nombre>" & ObjData.Cliente.Nombre & "</eol:Nombre>" &
                    "<eol:Num_Exterior>" & ObjData.Cliente.Direccion.NoExt & "</eol:Num_Exterior>" &
                    "<eol:Num_Interior/>" &
                    "<eol:RFC>" & ObjData.Cliente.RFC & "</eol:RFC>" &
                    "<eol:Telefono>" & ObjData.Cliente.Telefono & "</eol:Telefono>" &
                    "<eol:Tipo_Domicilio>3</eol:Tipo_Domicilio>" &
                    "<eol:Tipo_Telefono>1</eol:Tipo_Telefono>" &
                "</tem:asegurado>" &
                "<tem:contratante>" &
                    "<eol:A_Materno>" & ObjData.Cliente.ApellidoMat & "</eol:A_Materno>" &
                    "<eol:A_Paterno>" & ObjData.Cliente.ApellidoPat & "</eol:A_Paterno>" &
                    "<eol:Beneficio_Prop_Cont>1</eol:Beneficio_Prop_Cont>" &
                    "<eol:CURP_Cont/>" &
                    "<eol:Calle>" & ObjData.Cliente.Direccion.Calle & "</eol:Calle>" &
                    "<tem:Cert_Dig_FEA_Cont/>" &
                    "<eol:Certificacion_Cont>0</eol:Certificacion_Cont>" &
                    "<eol:CodCiudad>" & generalesAIG.codCiudad & "</eol:CodCiudad>" &
                    "<eol:CodColonia>" & generalesAIG.codColonia & "</eol:CodColonia>" &
                    "<eol:CodEstado>" & generalesAIG.codEstado & "</eol:CodEstado>" &
                    "<eol:CodMunicipio>" & generalesAIG.codMunicipio & "</eol:CodMunicipio>" &
                    "<eol:CodPostal>" & ObjData.Cliente.Direccion.CodPostal & "</eol:CodPostal>" &
                    "<eol:Cod_Edo_Civil>6</eol:Cod_Edo_Civil>" &
                    "<eol:Cod_Sexo>" & generalesAIG.genero & "</eol:Cod_Sexo>" &
                    "<eol:Cod_Tipo_Persona>1</eol:Cod_Tipo_Persona>" &
                    "<eol:Edad>" & ObjData.Cliente.Edad & "</eol:Edad>" &
                    "<eol:Email>" & ObjData.Cliente.Email & "</eol:Email>" &
                    "<eol:Fec_Nacimiento>" & fechaDeNacimiento & "T00:00:00Z</eol:Fec_Nacimiento>" &
                    "<eol:Jefe_Pol_Conyu_Cont>false</eol:Jefe_Pol_Conyu_Cont>" &
                    "<eol:Jefe_Pol_Conyu_Tiemp_Cont>0</eol:Jefe_Pol_Conyu_Tiemp_Cont>" &
                    "<eol:Jefe_Polico_Cont>false</eol:Jefe_Polico_Cont>" &
                    "<eol:Jefe_Polico_Tiemp_Cont>0</eol:Jefe_Polico_Tiemp_Cont>" &
                    "<eol:Mas_2500USD_Cont>false</eol:Mas_2500USD_Cont>" &
                    "<tem:Nom_Rep_Leg_Cont/>" &
                    "<eol:Nombre>" & ObjData.Cliente.Nombre & "</eol:Nombre>" &
                    "<eol:NumExterior>" & ObjData.Cliente.Direccion.NoExt & "</eol:NumExterior>" &
                    "<eol:NumInterior/>" &
                    "<tem:Num_Empleado/>" &
                    "<tem:Num_Fol_Merc_Acta/>" &
                    "<tem:Num_Ident_Cont/>" &
                    "<eol:Pais_Nacimient_Cont>10</eol:Pais_Nacimient_Cont>" &
                    "<eol:Pais_Nacional_Cont>10</eol:Pais_Nacional_Cont>" &
                    "<eol:RFC>" & ObjData.Cliente.RFC & "</eol:RFC>" &
                    "<eol:Telefono>" & ObjData.Cliente.Telefono & "</eol:Telefono>" &
                    "<eol:Tipo_Comp_Cont>0</eol:Tipo_Comp_Cont>" &
                    "<eol:Tipo_Domicilio>3</eol:Tipo_Domicilio>" &
                    "<eol:Tipo_Giro_Merc_Cont>0</eol:Tipo_Giro_Merc_Cont>" &
                    "<eol:Tipo_Ident_Cont>0</eol:Tipo_Ident_Cont>" &
                    "<eol:Tipo_Ocupa_Prof_Cont>0</eol:Tipo_Ocupa_Prof_Cont>" &
                    "<eol:Tipo_telf>1</eol:Tipo_telf>" &
                "</tem:contratante>" &
                "<tem:conductor1>" &
                   "<eol:A_Materno>" & ObjData.Cliente.ApellidoMat & "</eol:A_Materno>" &
                    "<eol:A_Paterno>" & ObjData.Cliente.ApellidoPat & "</eol:A_Paterno>" &
                    "<eol:Cod_Edo_Civil>6</eol:Cod_Edo_Civil>" &
                    "<eol:Cod_Sexo>" & generalesAIG.genero & "</eol:Cod_Sexo>" &
                    "<eol:Edad>" & ObjData.Cliente.Edad & "</eol:Edad>" &
                    "<eol:Fec_Nacimiento>" & fechaDeNacimiento & "T20:00:00Z</eol:Fec_Nacimiento>" &
                    "<eol:Nombre>" & ObjData.Cliente.Nombre & "</eol:Nombre>" &
                "</tem:conductor1>" &
                "<tem:conductor2>" &
                  "<eol:A_Materno>" & ObjData.Cliente.ApellidoMat & "</eol:A_Materno>" &
                    "<eol:A_Paterno>" & ObjData.Cliente.ApellidoPat & "</eol:A_Paterno>" &
                    "<eol:Cod_Edo_Civil>6</eol:Cod_Edo_Civil>" &
                    "<eol:Cod_Sexo>" & generalesAIG.genero & "</eol:Cod_Sexo>" &
                    "<eol:Edad>" & ObjData.Cliente.Edad & "</eol:Edad>" &
                    "<eol:Fec_Nacimiento>" & fechaDeNacimiento & "T20:00:00Z</eol:Fec_Nacimiento>" &
                    "<eol:Nombre>" & ObjData.Cliente.Nombre & "</eol:Nombre>" &
                "</tem:conductor2>" &
                "<tem:beneficiario>" &
                    "<eol:Nombre>" & ObjData.Cliente.ApellidoPat & " " & ObjData.Cliente.ApellidoMat & " " & ObjData.Cliente.Nombre & "</eol:Nombre>" &
                "</tem:beneficiario>" &
                "<tem:poliza>" &
                    "<eol:Cod_Tipo_Poliza>12</eol:Cod_Tipo_Poliza>" &
                    "<eol:Cod_Tipo_Producto>1</eol:Cod_Tipo_Producto>" &
                    "<eol:Desc_Poliza>12</eol:Desc_Poliza>" &
                    "<eol:Fec_Vigencia_Desde>" & Date.Now.ToString("yyyy-MM-dd") & "T00:00:00Z</eol:Fec_Vigencia_Desde>" &
                    "<eol:Fec_Vigencia_Hasta>" & Date.Now.AddYears(1).ToString("yyyy-MM-dd") & "T00:00:00Z</eol:Fec_Vigencia_Hasta>" &
                "</tem:poliza>" &
                "<tem:vehiculo>" &
                    "<eol:Aaaa_Fabrica>" & ObjData.Vehiculo.Modelo & "</eol:Aaaa_Fabrica>" &
                    "<eol:Cod_Marca>" & generalesAIG.codMarca & "</eol:Cod_Marca>" &
                    "<eol:Cod_Modelo>" & generalesAIG.codModelo & "</eol:Cod_Modelo>" &
                    "<eol:Cod_Tipo_Uso>1</eol:Cod_Tipo_Uso>" &
                    "<eol:Desc_Tipo_Uso>personal</eol:Desc_Tipo_Uso>" &
                    "<eol:Desc_Vehiculo_Extra>" & ObjData.Vehiculo.Descripcion & "</eol:Desc_Vehiculo_Extra>" &
                    "<eol:DescuentoGPS>false</eol:DescuentoGPS>" &
                    "<eol:Imp_Vehiculo_Extra>0</eol:Imp_Vehiculo_Extra>" &
                    "<eol:Motor>" & ObjData.Vehiculo.NoMotor & "</eol:Motor>" &
                    "<eol:NCI/>" &
                    "<eol:Numero_Serie>" & ObjData.Vehiculo.NoSerie & "</eol:Numero_Serie>" &
                    "<eol:Placas>" & ObjData.Vehiculo.NoPlacas & "</eol:Placas>" &
                    "<eol:SISE>" & ObjData.Vehiculo.Clave & "</eol:SISE>" &
                    "<eol:SN_Fronterizo>false</eol:SN_Fronterizo>" &
                "</tem:vehiculo>" &
              "<tem:formaPago> " &
                    "<eol:Anio_Vencimiento>0</eol:Anio_Vencimiento>" &
                    "<eol:AplcPago>false</eol:AplcPago>" &
                    "<eol:Cod_Banco>0</eol:Cod_Banco>" &
                    "<eol:Cod_Conducto_Pago>1</eol:Cod_Conducto_Pago>" &
                    "<eol:Cod_Forma_Pago>1</eol:Cod_Forma_Pago>" &
                    "<eol:Dia_cobro>0</eol:Dia_cobro>" &
                    "<eol:Mes_Vencimiento>0</eol:Mes_Vencimiento>" &
                    "<eol:Nombre_Titular></eol:Nombre_Titular>" &
                    "<eol:Numero_Cuenta></eol:Numero_Cuenta>" &
              "</tem:formaPago>" &
                "<tem:aseguradoIgualContratante>false</tem:aseguradoIgualContratante>" &
                "<tem:aseguradoIgualConductor>true</tem:aseguradoIgualConductor>" &
                "<tem:requiereRecibosFiscales>false</tem:requiereRecibosFiscales>" &
                "<tem:imprimeAvisosCobro>false</tem:imprimeAvisosCobro>" &
                "<tem:username>juan.morales</tem:username>" &
                "<tem:password>Mexico2018.</tem:password>" &
            "</tem:GenerarEmision140>" &
        "</soapenv:Body>" &
    "</soapenv:Envelope>")
        Return soapEnvelop
    End Function

    Private Shared Function CreateSoapEnvelopeConsultaCP(ObjData As Seguro) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>" &
   "<soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-694D27454926B26F8A15426827067371'><wsse:Username>juan.morales</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>Mexico2018.</wsse:Password><wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>KBwSySrjze4smcAq+/CVRw==</wsse:Nonce><wsu:Created>2018-11-20T02:58:26.733Z</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header>" &
   "<soapenv:Body>" &
        "<tem:ObtenerColoniaCp>" &
            "<tem:idPais>10</tem:idPais>" &
            "<tem:cp>" & ObjData.Cliente.Direccion.CodPostal & "</tem:cp>" &
            "<tem:userName>juan.morales</tem:userName>" &
        "</tem:ObtenerColoniaCp>" &
    "</soapenv:Body>" &
"</soapenv:Envelope>")
        Return soapEnvelop
    End Function

    Private Shared Function CreateSoapEnvelopeObtenerModeloSISA(ObjData As Seguro) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'>" &
   "<soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-694D27454926B26F8A154273355664119'><wsse:Username>juan.morales</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>Mexico2018.</wsse:Password><wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>gekzTfjoV/9Pz7JwDLy+Hg==</wsse:Nonce><wsu:Created>2018-11-20T17:05:56.641Z</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header>" &
   "<soapenv:Body>" &
      "<tem:ObtenerModeloSisa>" &
         "<tem:sisa>" & ObjData.Vehiculo.Clave & "</tem:sisa>" &
      "</tem:ObtenerModeloSisa>" &
    "</soapenv:Body>" &
"</soapenv:Envelope>")
        Return soapEnvelop
    End Function

    Private Shared Function CreateSoapEnvelopeProcePago(ObjData As Seguro, ByVal generalesAIG As GeneralesAIG) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        Dim xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/' xmlns:aig='http://schemas.datacontract.org/2004/07/AIG.Business.Payment.Entities'>" &
            "<soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-96378A16AAFACBC25A15512004467881'><wsse:Username>ws.brandon.guadarrama</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>Webserv.aig10$</wsse:Password><wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>v6ko65d7o4MqQkxjqRbN/g==</wsse:Nonce><wsu:Created>2019-02-26T17:00:46.785Z</wsu:Created></wsse:UsernameToken></wsse:Security></soapenv:Header>" &
   "<soapenv:Body>" &
      "<tem:SubmitPayment>" &
         "<tem:subPayment>" &
            "<aig:Username>ws.brandon.guadarrama</aig:Username>" &
            "<aig:currency>MXN</aig:currency>" &
            "<aig:monto>" & ObjData.Emision.PrimaTotal & "</aig:monto>" &
            "<aig:referencia>" & ObjData.Cotizacion.IDCotizacion & "</aig:referencia>" &
            "<aig:tarjeta>" &
               "<aig:_CCS>" & ObjData.Pago.CodigoSeguridad & "</aig:_CCS>" &
               "<aig:_anioExpiracion>" & ObjData.Pago.AnioExp & "</aig:_anioExpiracion>" &
               "<aig:_conductoPago>" & generalesAIG.conducto & "</aig:_conductoPago>" &
               "<aig:_cuenta>" & ObjData.Pago.NoTarjeta & "</aig:_cuenta>" &
               "<aig:_idBanco>" & generalesAIG.codBanco & "</aig:_idBanco>" &
               "<aig:_idPlazo>" & generalesAIG.formaPago & "</aig:_idPlazo>" &
               "<aig:_mesExpiracion>" & ObjData.Pago.MesExp & "</aig:_mesExpiracion>" &
               "<aig:_titular>" & ObjData.Cliente.Nombre & " " & ObjData.Cliente.ApellidoPat & " " & ObjData.Cliente.ApellidoMat & "</aig:_titular>" &
            "</aig:tarjeta>" &
         "</tem:subPayment>" &
      "</tem:SubmitPayment>" &
   "</soapenv:Body>" &
"</soapenv:Envelope>"
        soapEnvelop.LoadXml(xml)
        Return soapEnvelop
    End Function


    Private Shared Function CreateWebRequest(url As String, action As String) As HttpWebRequest
        Dim webRequest__1 As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
        webRequest__1.Headers.Add("SOAPAction", action)
        webRequest__1.ContentType = "text/xml;charset=UTF-8"
        webRequest__1.Accept = "application/xml"
        webRequest__1.Method = "POST"
        Return webRequest__1
    End Function
    Private Shared Sub InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml As XmlDocument, webRequest As HttpWebRequest)
        Using stream As Stream = webRequest.GetRequestStream()
            soapEnvelopeXml.Save(stream)
        End Using
    End Sub

    Private Shared Function BancosAIG(ByVal banco As String, ByVal generalesAIG As GeneralesAIG) As String
        Select Case UCase(banco)
            Case "BANAMEX"
                generalesAIG.codBanco = "2"
            Case "BANCOMER"
                generalesAIG.codBanco = "12"
            Case "BANEJERCITO"
                generalesAIG.codBanco = "19"
            Case "BITAL"
                generalesAIG.codBanco = "21"
            Case "BANORTE"
                generalesAIG.codBanco = "72"
            Case "SANTANDER SERFIN"
                generalesAIG.codBanco = "14"
            Case "SCOTIABANK"
                generalesAIG.codBanco = "44"
            Case "IXE"
                generalesAIG.codBanco = "32"
            Case "INBURSA"
                generalesAIG.codBanco = "36"
            Case "BANCO AZTECA"
                generalesAIG.codBanco = "127"
            Case "BAJIO"
                generalesAIG.codBanco = "30"
            Case "MULTIVALORES"
                generalesAIG.codBanco = "743"
            Case "INTERACCIONES"
                generalesAIG.codBanco = "37"
            Case "INVEX"
                generalesAIG.codBanco = "59"
            Case "AFIRME"
                generalesAIG.codBanco = "62"
        End Select
        Return ""
    End Function

End Class