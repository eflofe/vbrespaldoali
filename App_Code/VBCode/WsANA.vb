﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.VisualBasic
Imports MySql.Data.MySqlClient
Imports SuperObjeto
Public Class WsANA
    Public Shared Function ANACotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Try

            Dim respuesta(10) As String
            Dim FormaPago As String = ""
            Dim cotizaANA As New ANAService.ServiceSoapClient
            Dim cotizaANA2 As New ANA2.ServiceSoapClient


            'Datos del negocio Ref
            Dim Negocio As String = "995"
            Dim Usuario As String = "06010"
            Dim Password As String = "7tUdegub"
            Dim tarifaANA As String = "1603" '"1303"

            'Datos de cotizacion
            Dim plan As String = Nothing
            Dim localizacionID As String = Nothing
            Dim poblacion As String = Nothing
            'Plan
            If UCase(ObjData.Paquete) = "AMPLIA" Then
                plan = "1"
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                plan = "3"
            ElseIf UCase(ObjData.Paquete) = "RC" Then
                plan = "4"
            End If

            Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
            For i = lengthCP To 4
                ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
            Next

            'Forma pago
            If UCase(ObjData.PeriodicidadDePago) = "CONTADO" Then
                FormaPago = "C"
            ElseIf UCase(ObjData.PeriodicidadDePago) = "MENSUAL" Then
                FormaPago = "J"
            End If
            'Fechas
            Dim mes As String = (Now.Month).ToString
            Dim meslargo As Integer = mes.Length
            Dim dia As String = (Now.Day).ToString
            Dim dialargo As Integer = dia.Length

            If meslargo = 1 Then
                mes = "0" & mes
            End If
            If dialargo = 1 Then
                dia = "0" & dia
            End If
            Dim InicioVigTxt As String = (dia & "/" & mes & "/" & Now.Year).ToString
            Dim FinVigTxt As String = (dia & "/" & mes & "/" & Now.Year + 1).ToString

            'Datos generales
            Dim localizacion
            Try
                localizacion = ConsultaMunicipiosANA(ObjData.Cliente.Direccion.CodPostal, ObjData.Cliente.Direccion.Colonia)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de municipios (CP)")
            End Try
            Dim estadoID = localizacion(0)
            If Len(estadoID) = 1 Then
                estadoID = "0" & estadoID
            End If
            Dim municipioID = localizacion(1)
            If Len(municipioID) = 1 Then
                municipioID = "00" & municipioID
            ElseIf Len(municipioID) = 2 Then
                municipioID = "0" & municipioID
            End If
            localizacionID = estadoID & municipioID
            poblacion = localizacion(2)
            Dim DescuentoZona As String

            DescuentoZona = ObjData.Descuento

            Dim DM As String = "5"
            Dim RT As String = "10"
            Dim GM As String = "200000"
            Dim RCB As String = "500000"
            Dim RCP As String = "500000"
            Dim RCM As String = "2000000"
            Dim CRISTALES As String = ""
            Dim TotalCoberturas = (ObjData.Coberturas).Count

            If TotalCoberturas > 0 Then
                If ObjData.Coberturas(0).RoboTotal <> "-" Then
                    RT = ObjData.Coberturas(0).RoboTotal
                End If
                If ObjData.Coberturas(0).DanosMateriales <> "-" Then
                    DM = ObjData.Coberturas(0).DanosMateriales
                End If
                If ObjData.Coberturas(0).GastosMedicosEvento <> "-" Then
                    GM = ObjData.Coberturas(0).GastosMedicosEvento
                End If
                If ObjData.Coberturas(0).RCBienes <> "-" Then
                    RCB = ObjData.Coberturas(0).RCBienes
                End If
                If ObjData.Coberturas(0).RCPersonas <> "-" Then
                    RCP = ObjData.Coberturas(0).RCPersonas
                End If
                If ObjData.Coberturas(0).MuerteAccidental <> "-" Then
                    RCM = ObjData.Coberturas(0).MuerteAccidental
                End If
            End If

            Dim objXmlDocumentCotizacionRes As New XmlDocument()
            Dim vehiculo As XElement
            Dim strXMLDatosCotizacion As XElement = <transacciones xmlns="">
                                                    </transacciones>
            Dim xmlTransaccion As XElement = <transaccion version="1" tipotransaccion="C" cotizacion="" negocio=<%= Negocio %> tiponegocio="">
                                             </transaccion>
            If plan = "1" Then
                vehiculo = <vehiculo id="1" amis=<%= ObjData.Vehiculo.Clave %> modelo=<%= ObjData.Vehiculo.Modelo %> descripcion="" uso="1" servicio="1" plan=<%= plan %> motor="" serie="" repuve="" placas="" conductor="" conductorliciencia="" conductorfecnac="" conductorocupacion="" estado=<%= localizacionID %> poblacion=<%= municipioID %> color="01" dispositivo="" fecdispositivo="" tipocarga="" tipocargadescripcion="">
                               <cobertura id="02" desc="" sa="" tipo="3" ded=<%= DM %> pma=""/>
                               <cobertura id="04" desc="" sa="" tipo="3" ded=<%= RT %> pma=""/>
                               <cobertura id="06" desc="" sa=<%= GM %> tipo="" ded="" pma=""/>
                               <cobertura id="07" desc="" sa="" tipo="" ded="" pma=""/>
                               <cobertura id="10" desc="" sa="" tipo="B" ded="" pma=""/>
                               <cobertura id="25" desc="" sa=<%= RCB %> tipo="" ded="" pma=""/>
                               <cobertura id="26" desc="" sa=<%= RCP %> tipo="" ded="" pma=""/>
                               <cobertura id="34" desc="" sa=<%= RCM %> tipo="" ded="" pma=""/>
                           </vehiculo>
            ElseIf plan = "3" Then
                vehiculo = <vehiculo id="1" amis=<%= ObjData.Vehiculo.Clave %> modelo=<%= ObjData.Vehiculo.Modelo %> descripcion="" uso="1" servicio="1" plan=<%= plan %> motor="" serie="" repuve="" placas="" conductor="" conductorliciencia="" conductorfecnac="" conductorocupacion="" estado=<%= localizacionID %> poblacion=<%= municipioID %> color="01" dispositivo="" fecdispositivo="" tipocarga="" tipocargadescripcion="">
                               <cobertura id="04" desc="" sa="" tipo="3" ded=<%= RT %> pma=""/>
                               <cobertura id="06" desc="" sa=<%= GM %> tipo="" ded="" pma=""/>
                               <cobertura id="07" desc="" sa="" tipo="" ded="" pma=""/>
                               <cobertura id="10" desc="" sa="" tipo="B" ded="" pma=""/>
                               <cobertura id="25" desc="" sa=<%= RCB %> tipo="" ded="" pma=""/>
                               <cobertura id="26" desc="" sa=<%= RCP %> tipo="" ded="" pma=""/>
                               <cobertura id="34" desc="" sa=<%= RCM %> tipo="" ded="" pma=""/>
                           </vehiculo>
            Else
                vehiculo = <vehiculo id="1" amis=<%= ObjData.Vehiculo.Clave %> modelo=<%= ObjData.Vehiculo.Modelo %> descripcion="" uso="1" servicio="1" plan=<%= plan %> motor="" serie="" repuve="" placas="" conductor="" conductorliciencia="" conductorfecnac="" conductorocupacion="" estado=<%= localizacionID %> poblacion=<%= municipioID %> color="01" dispositivo="" fecdispositivo="" tipocarga="" tipocargadescripcion="">
                               <cobertura id="06" desc="" sa=<%= GM %> tipo="" ded="" pma=""/>
                               <cobertura id="07" desc="" sa="" tipo="" ded="" pma=""/>
                               <cobertura id="10" desc="" sa="" tipo="B" ded="" pma=""/>
                               <cobertura id="25" desc="" sa=<%= RCB %> tipo="" ded="" pma=""/>
                               <cobertura id="26" desc="" sa=<%= RCP %> tipo="" ded="" pma=""/>
                               <cobertura id="34" desc="" sa=<%= RCM %> tipo="" ded="" pma=""/>
                           </vehiculo>
            End If
            xmlTransaccion.Add(vehiculo)
            xmlTransaccion.Add(<asegurado id="" nombre="" paterno="" materno="" calle="" numerointerior="" numeroexterior="" colonia="" poblacion="" estado=<%= localizacionID %> cp="" pais="" tipopersona=""/>)
            xmlTransaccion.Add(<poliza id="" tipo="A" endoso="" fecemision=<%= InicioVigTxt %> feciniciovig=<%= InicioVigTxt %> fecterminovig=<%= FinVigTxt %> moneda="0" bonificacion=<%= DescuentoZona %> formapago=<%= FormaPago %> agente=<%= Usuario %> tarifacuotas=<%= tarifaANA %> tarifavalores=<%= tarifaANA %> tarifaderechos=<%= tarifaANA %> beneficiario="" politicacancelacion="1"/>)
            xmlTransaccion.Add(<prima primaneta="" derecho="" recargo="" impuesto="" primatotal="" comision=""/>)
            xmlTransaccion.Add(<recibo id="" feciniciovig="" fecterminovig="" primaneta="" derecho="" recargo="" impuesto="" primatotal="" comision="" cadenaoriginal="" sellodigital="" fecemision="" serie="" folio="" horaemision="" numeroaprobacion="" anoaprobacion="" numseriecertificado=""/>)
            xmlTransaccion.Add(<error/>)
            strXMLDatosCotizacion.Add(xmlTransaccion)
            'Llamada a servicio
            Dim lg As Log = New Log()
            Dim xmldoc As New XmlDocument()
            xmldoc.Load(strXMLDatosCotizacion.CreateReader())
            Dim xml = xmldoc.DocumentElement
            lg.UpdateLogRequest(xml.InnerXml, idLogWSCot)
            'FuncionesGeneral.xmlWS(xml.InnerXml, ObjData, "Cotizacion", "CotizacionEnvio")
            Dim Coberturas As New Coberturas
            Dim nombreCobertura As String = ""
            Try
                Dim x2 As New XmlSerializer(xml.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, xml)
                Dim xmlRespuesta As String = xml2.ToString
                lg.UpdateLogRequest(xmlRespuesta, idLogWSCot)
                'Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Dim resCot = cotizaANA2.Transaccion(xml, 0, Usuario, Password)

            Try
                lg.UpdateLogResponse(resCot, idLogWSCot)
                'Funciones.updateLogWSCot(resCot, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try

            objXmlDocumentCotizacionRes.LoadXml(resCot)
            'Leer respuesta de servicio
            Dim objNodo2 As XmlNode
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("prima")
                ObjData.Cotizacion.PrimaNeta = objXmlNode.Attributes("primaneta").Value
                ObjData.Cotizacion.Derechos = objXmlNode.Attributes("derecho").Value
                ObjData.Cotizacion.Impuesto = objXmlNode.Attributes("impuesto").Value
                ObjData.Cotizacion.PrimaTotal = objXmlNode.Attributes("primatotal").Value
                ObjData.Cotizacion.Recargos = objXmlNode.Attributes("recargo").Value
            Next
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("poliza")
                respuesta(7) = objXmlNode.Attributes("tarifacuotas").Value
                respuesta(8) = objXmlNode.Attributes("tarifavalores").Value
                respuesta(9) = objXmlNode.Attributes("tarifaderechos").Value
            Next
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("vehiculo")
                For Each objNodo2 In objXmlNode
                    Dim cobertura As String = objNodo2.Attributes("desc").Value
                    Dim descripcionCobertura As String = ""
                    Select Case cobertura
                        Case "DAÑOS MATERIALES"
                            nombreCobertura = "DAÑOS MATERIALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.DanosMateriales = descripcionCobertura
                        Case "ROBO TOTAL"
                            nombreCobertura = "ROBO TOTAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RoboTotal = descripcionCobertura
                        Case "RESPONSABILIDAD CIVIL"
                            nombreCobertura = "RESPONSABILIDAD CIVIL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RC = descripcionCobertura
                        Case "  RC BIENES"
                            nombreCobertura = "RC BIENES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCBienes = descripcionCobertura
                        Case "  RC PERSONAS"
                            nombreCobertura = "RC PERSONAS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCPersonas = descripcionCobertura
                        Case "  EXTENSION RC  RC PERSONAS"
                            nombreCobertura = "EXTENSION RC"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCExtension = descripcionCobertura
                        Case "GASTOS MEDICOS"
                            nombreCobertura = "GASTOS MÉDICOS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.GastosMedicosEvento = descripcionCobertura
                        Case "DEF. JUD. Y ASIS. LEGAL"
                            nombreCobertura = "GASTOS LEGALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.DefensaJuridica = descripcionCobertura
                        Case "ANA ASISTENCIA"
                            nombreCobertura = "ASISTENCIA VIAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.AsitenciaCompleta = descripcionCobertura
                    End Select
                Next
            Next
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("recibo")
                If objXmlNode.Attributes("id").Value = "1" Then
                    If objXmlNode.Attributes("primatotal").Value = "" Then
                        respuesta(4) = respuesta(3)
                    Else
                        respuesta(4) = objXmlNode.Attributes("primatotal").Value
                    End If
                Else
                    respuesta(10) = objXmlNode.Attributes("primatotal").Value
                End If
            Next
            If respuesta(3) <> Nothing Then
                If Val(respuesta(3)) <10 Then
                    respuesta(1) = Nothing
                    respuesta(2) = Nothing
                    respuesta(3) = Nothing
                    respuesta(4) = Nothing
                    respuesta(5) = Nothing
                End If
            End If
            ObjData.Coberturas.Add(Coberturas)
            ObjData.Cotizacion.Resultado = "True"
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Dim jsonResponse As String = serializer.Serialize(ObjData)
        Dim log As Log = New Log()
        log.UpdateLogJsonResponse(jsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function ConsultaMunicipiosANA(ByVal CPostal As String, ByVal Colonia As String) As String()
        Dim objConnectionCPostal As SqlConnection
        Dim objCommandCPostal As SqlCommand
        Dim strSQLQueryCPostal As String
        Dim Lista(3) As String
        Dim BusquedaColonia As String = ""
        Dim connectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db"
        Using Connection As New MySqlConnection(connectionString)
            If Colonia <> "" And Colonia <> Nothing Then
                BusquedaColonia = " And Colonia='" & Colonia & "'"
            End If
            strSQLQueryCPostal = "Select * from  `ws_db`.`Ali_R_CatEdosANA` where CPostal = '" & CPostal & "'" & BusquedaColonia

            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim rsCPostal As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (rsCPostal.Read())
                    Lista(0) = rsCPostal("IDEdo_Ana")
                    Lista(1) = rsCPostal("IDMun_Ana")
                    Lista(2) = rsCPostal("Estado")
                End While
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Lista
    End Function

    Public Shared Function ANAEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Try

            Dim respuesta(10) As String
            Dim FormaPago As String = ""
            Dim cotizaANA As New ANAService.ServiceSoapClient
            Dim cotizaANA2 As New ANA2.ServiceSoapClient

            'Datos del negocio Ref
            Dim Negocio As String = "995"
            Dim Usuario As String = "06010"
            Dim Password As String = "7tUdegub"
            Dim tarifaANA As String = "1603" '"1303"

            'Datos de cotizacion
            Dim plan As String = Nothing
            Dim localizacionID As String = Nothing
            Dim poblacion As String = Nothing
            'Plan
            If UCase(ObjData.Paquete) = "AMPLIA" Then
                plan = "1"
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                plan = "3"
            ElseIf UCase(ObjData.Paquete) = "RC" Then
                plan = "4"
            End If

            Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
            For i = lengthCP To 4
                ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
            Next

            'Forma pago
            If UCase(ObjData.PeriodicidadDePago) = Seguro.PeriodicidadPago.ANUAL Then
                FormaPago = "C"
            ElseIf UCase(ObjData.PeriodicidadDePago) = Seguro.PeriodicidadPago.MENSUAL Then
                FormaPago = "J"
            End If
            'Fechas
            Dim mes As String = (Now.Month).ToString
            Dim meslargo As Integer = mes.Length
            Dim dia As String = (Now.Day).ToString
            Dim dialargo As Integer = dia.Length

            If meslargo = 1 Then
                mes = "0" & mes
            End If
            If dialargo = 1 Then
                dia = "0" & dia
            End If
            Dim InicioVigTxt As String = (dia & "/" & mes & "/" & Now.Year).ToString
            Dim FinVigTxt As String = (dia & "/" & mes & "/" & Now.Year + 1).ToString

            'Datos generales
            Dim localizacion
            Try
                localizacion = ConsultaMunicipiosANA(ObjData.Cliente.Direccion.CodPostal, ObjData.Cliente.Direccion.Colonia)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de municipios")
            End Try
            Dim estadoID = localizacion(0)
            If Len(estadoID) = 1 Then
                estadoID = "0" & estadoID
            End If
            Dim municipioID = localizacion(1)
            If Len(municipioID) = 1 Then
                municipioID = "00" & municipioID
            ElseIf Len(municipioID) = 2 Then
                municipioID = "0" & municipioID
            End If
            localizacionID = estadoID & municipioID
            poblacion = localizacion(2)
            Dim DescuentoZona As String

            DescuentoZona = ObjData.Descuento
            'Documento final - Cotizacion

            Dim DM As String = "5"
            Dim RT As String = "10"
            Dim GM As String = "200000"
            Dim RCB As String = "500000"
            Dim RCP As String = "500000"
            Dim RCM As String = "2000000"
            Dim CRISTALES As String = ""
            Dim TotalCoberturas = (ObjData.Coberturas).Count

            If TotalCoberturas > 0 Then
                If ObjData.Coberturas(0).RoboTotal <> "-" Then
                    RT = ObjData.Coberturas(0).RoboTotal
                End If
                If ObjData.Coberturas(0).DanosMateriales <> "-" Then
                    DM = ObjData.Coberturas(0).DanosMateriales
                End If
                If ObjData.Coberturas(0).GastosMedicosEvento <> "-" Then
                    GM = ObjData.Coberturas(0).GastosMedicosEvento
                End If
                If ObjData.Coberturas(0).RCBienes <> "-" Then
                    RCB = ObjData.Coberturas(0).RCBienes
                End If
                If ObjData.Coberturas(0).RCPersonas <> "-" Then
                    RCP = ObjData.Coberturas(0).RCPersonas
                End If
                If ObjData.Coberturas(0).MuerteAccidental <> "-" Then
                    RCM = ObjData.Coberturas(0).MuerteAccidental
                End If
            End If
            Dim Banco As String = BancosANA(ObjData.Pago.Banco)
            Dim Direccion As String = ObjData.Cliente.Direccion.Calle & ", " & ObjData.Cliente.Direccion.Colonia
            Dim FechaExpiracion As String = ObjData.Pago.MesExp & ObjData.Pago.AnioExp.Substring(2, 2)
            Dim objXmlDocumentCotizacionRes As New XmlDocument()
            Dim vehiculo As XElement
            Dim strXMLDatosCotizacion As XElement = <transacciones xmlns="">
                                                    </transacciones>
            Dim xmlTransaccion As XElement = <transaccion version="1" tipotransaccion="E" cotizacion="" negocio=<%= Negocio %> tiponegocio="">
                                             </transaccion>
            If plan = "1" Then
                vehiculo = <vehiculo id="1" amis=<%= ObjData.Vehiculo.Clave %> modelo=<%= ObjData.Vehiculo.Modelo %> descripcion="" uso="1" servicio="1" plan=<%= plan %> motor=<%= ObjData.Vehiculo.Modelo %> serie=<%= ObjData.Vehiculo.NoSerie %> repuve="" placas=<%= ObjData.Vehiculo.NoPlacas %> conductor="" conductorliciencia="" conductorfecnac="" conductorocupacion="" estado=<%= localizacionID %> poblacion=<%= municipioID %> color="01" dispositivo="" fecdispositivo="" tipocarga="" tipocargadescripcion="">
                               <cobertura id="02" desc="" sa="" tipo="3" ded=<%= DM %> pma=""/>
                               <cobertura id="04" desc="" sa="" tipo="3" ded=<%= RT %> pma=""/>
                               <cobertura id="06" desc="" sa=<%= GM %> tipo="" ded="" pma=""/>
                               <cobertura id="07" desc="" sa="" tipo="" ded="" pma=""/>
                               <cobertura id="10" desc="" sa="" tipo="B" ded="" pma=""/>
                               <cobertura id="25" desc="" sa=<%= RCB %> tipo="" ded="" pma=""/>
                               <cobertura id="26" desc="" sa=<%= RCP %> tipo="" ded="" pma=""/>
                               <cobertura id="34" desc="" sa=<%= RCM %> tipo="" ded="" pma=""/>
                           </vehiculo>
            ElseIf plan = "3" Then
                vehiculo = <vehiculo id="1" amis=<%= ObjData.Vehiculo.Clave %> modelo=<%= ObjData.Vehiculo.Modelo %> descripcion="" uso="1" servicio="1" plan=<%= plan %> motor=<%= ObjData.Vehiculo.Modelo %> serie=<%= ObjData.Vehiculo.NoSerie %> repuve="" placas=<%= ObjData.Vehiculo.NoPlacas %> conductor="" conductorliciencia="" conductorfecnac="" conductorocupacion="" estado=<%= localizacionID %> poblacion=<%= municipioID %> color="01" dispositivo="" fecdispositivo="" tipocarga="" tipocargadescripcion="">
                               <cobertura id="04" desc="" sa="" tipo="3" ded=<%= RT %> pma=""/>
                               <cobertura id="06" desc="" sa=<%= GM %> tipo="" ded="" pma=""/>
                               <cobertura id="07" desc="" sa="" tipo="" ded="" pma=""/>
                               <cobertura id="10" desc="" sa="" tipo="B" ded="" pma=""/>
                               <cobertura id="25" desc="" sa=<%= RCB %> tipo="" ded="" pma=""/>
                               <cobertura id="26" desc="" sa=<%= RCP %> tipo="" ded="" pma=""/>
                               <cobertura id="34" desc="" sa=<%= RCM %> tipo="" ded="" pma=""/>
                           </vehiculo>
            Else
                vehiculo = <vehiculo id="1" amis=<%= ObjData.Vehiculo.Clave %> modelo=<%= ObjData.Vehiculo.Modelo %> descripcion="" uso="1" servicio="1" plan=<%= plan %> motor=<%= ObjData.Vehiculo.Modelo %> serie=<%= ObjData.Vehiculo.NoSerie %> repuve="" placas=<%= ObjData.Vehiculo.NoPlacas %> conductor="" conductorliciencia="" conductorfecnac="" conductorocupacion="" estado=<%= localizacionID %> poblacion=<%= municipioID %> color="01" dispositivo="" fecdispositivo="" tipocarga="" tipocargadescripcion="">
                               <cobertura id="06" desc="" sa=<%= GM %> tipo="" ded="" pma=""/>
                               <cobertura id="07" desc="" sa="" tipo="" ded="" pma=""/>
                               <cobertura id="10" desc="" sa="" tipo="B" ded="" pma=""/>
                               <cobertura id="25" desc="" sa=<%= RCB %> tipo="" ded="" pma=""/>
                               <cobertura id="26" desc="" sa=<%= RCP %> tipo="" ded="" pma=""/>
                               <cobertura id="34" desc="" sa=<%= RCM %> tipo="" ded="" pma=""/>
                           </vehiculo>
            End If
            xmlTransaccion.Add(vehiculo)
            xmlTransaccion.Add(<asegurado id="" nombre=<%= ObjData.Cliente.Nombre %> paterno=<%= ObjData.Cliente.ApellidoPat %> materno=<%= ObjData.Cliente.ApellidoMat %> calle=<%= ObjData.Cliente.Direccion.Calle %> numerointerior=<%= ObjData.Cliente.Direccion.NoInt %> numeroexterior=<%= ObjData.Cliente.Direccion.NoExt %> colonia=<%= ObjData.Cliente.Direccion.Colonia %> poblacion=<%= localizacion(1) %> estado=<%= localizacionID %> cp=<%= ObjData.Cliente.Direccion.CodPostal %> pais="MEXICO" tipopersona="1">
                                   <argumento id="2" tipo="" campo="" valor=<%= ObjData.Cliente.Email %>/>
                                   <argumento id="3" tipo="" campo="" valor=<%= ObjData.Cliente.Telefono %>/>
                                   <argumento id="4" tipo="" campo="" valor=<%= ObjData.Cliente.RFC %>/>
                                   <argumento id="5" tipo="" campo="" valor=""/>
                                   <argumento id="6" tipo="" campo="" valor="M1"/>
                                   <argumento id="7" tipo="" campo="" valor="C3"/>
                                   <argumento id="8" tipo="" campo="" valor="220014155750"/>
                                   <argumento id="9" tipo="" campo="" valor="O1"/>
                               </asegurado>)
            xmlTransaccion.Add(<poliza id="" tipo="A" endoso="" fecemision=<%= InicioVigTxt %> feciniciovig=<%= InicioVigTxt %> fecterminovig=<%= FinVigTxt %> moneda="0" bonificacion=<%= DescuentoZona %> formapago=<%= FormaPago %> agente=<%= Usuario %> tarifacuotas=<%= tarifaANA %> tarifavalores=<%= tarifaANA %> tarifaderechos=<%= tarifaANA %> beneficiario="" politicacancelacion="1"/>)
            xmlTransaccion.Add(<prima primaneta="" derecho="" recargo="" impuesto="" primatotal="" comision=""/>)
            xmlTransaccion.Add(<recibo id="" feciniciovig="" fecterminovig="" primaneta="" derecho="" recargo="" impuesto="" primatotal="" comision="" cadenaoriginal="" sellodigital="" fecemision="" serie="" folio="" horaemision="" numeroaprobacion="" anoaprobacion="" numseriecertificado=""/>)
            If UCase(ObjData.Pago.MedioPago) = "DEBITO" Or UCase(ObjData.Pago.MedioPago) = "CREDITO" Then
                xmlTransaccion.Add(<tarjetacredito cliente=<%= ObjData.Pago.NombreTarjeta %> numero=<%= ObjData.Pago.NoTarjeta %> vencimiento=<%= FechaExpiracion %> codigoseguridad=<%= ObjData.Pago.CodigoSeguridad %>/>)
                xmlTransaccion.Add(<domiciliacion banco=<%= Banco %> direcciontajetahabiente=<%= Direccion %> envio="N" rfc="" fiscal="N"/>)
            End If
            xmlTransaccion.Add(<error/>)
            strXMLDatosCotizacion.Add(xmlTransaccion)
            'Llamada a servicio
            Dim xmldoc As New XmlDocument()
            xmldoc.Load(strXMLDatosCotizacion.CreateReader())
            Dim xml = xmldoc.DocumentElement
            Dim Coberturas As New Coberturas
            Dim nombreCobertura As String = ""

            Dim x3 As New XmlSerializer(xml.GetType)
            Dim xml3 As New StringWriter
            x3.Serialize(xml3, xml)
            FuncionesGeneral.xmlWS(xml.InnerXml, ObjData, "Emision", "EmisionEnvio")

            Try
                Funciones.updateLogWS(xml.InnerXml, idLogWS, "RequestWS")
            Catch ex As Exception
            End Try

            Dim resCot = cotizaANA2.Transaccion(xml, 1, Usuario, Password)

            Dim x4 As New XmlSerializer(resCot.GetType)
            Dim xml4 As New StringWriter
            x4.Serialize(xml4, resCot)
            FuncionesGeneral.xmlWS(resCot, ObjData, "Emision", "EmisionRespuesta")

            Try
                Funciones.updateLogWS(resCot, idLogWS, "ResponseWS")
            Catch ex As Exception
            End Try

            objXmlDocumentCotizacionRes.LoadXml(resCot)
            'Leer respuesta de servicio
            Dim objNodo2 As XmlNode
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("prima")
                ObjData.Emision.PrimaNeta = objXmlNode.Attributes("primaneta").Value
                ObjData.Emision.Derechos = objXmlNode.Attributes("derecho").Value
                ObjData.Emision.Impuesto = objXmlNode.Attributes("impuesto").Value
                ObjData.Emision.PrimaTotal = objXmlNode.Attributes("primatotal").Value
                ObjData.Emision.Recargos = objXmlNode.Attributes("recargo").Value
            Next
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("poliza")
                respuesta(7) = objXmlNode.Attributes("tarifacuotas").Value
                respuesta(8) = objXmlNode.Attributes("tarifavalores").Value
                respuesta(9) = objXmlNode.Attributes("tarifaderechos").Value
            Next
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("vehiculo")
                For Each objNodo2 In objXmlNode
                    Dim cobertura As String = objNodo2.Attributes("desc").Value
                    Dim descripcionCobertura As String = ""
                    Select Case cobertura
                        Case "DAÑOS MATERIALES"
                            nombreCobertura = "DAÑOS MATERIALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.DanosMateriales = descripcionCobertura
                        Case "ROBO TOTAL"
                            nombreCobertura = "ROBO TOTAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RoboTotal = descripcionCobertura
                        Case "RESPONSABILIDAD CIVIL"
                            nombreCobertura = "RESPONSABILIDAD CIVIL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RC = descripcionCobertura
                        Case "  RC BIENES"
                            nombreCobertura = "RC BIENES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCBienes = descripcionCobertura
                        Case "  RC PERSONAS"
                            nombreCobertura = "RC PERSONAS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCPersonas = descripcionCobertura
                        Case "  EXTENSION RC  RC PERSONAS"
                            nombreCobertura = "EXTENSION RC"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCExtension = descripcionCobertura
                        Case "  EXTENSION RC"
                            nombreCobertura = "EXTENSION RC"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.RCExtension = descripcionCobertura
                        Case "GASTOS MEDICOS"
                            nombreCobertura = "GASTOS MÉDICOS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.GastosMedicosEvento = descripcionCobertura
                        Case "DEF. JUD. Y ASIS. LEGAL"
                            nombreCobertura = "GASTOS LEGALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.DefensaJuridica = descripcionCobertura
                        Case "ANA ASISTENCIA"
                            nombreCobertura = "ASISTENCIA VIAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2.Attributes("sa").Value & "-D" & objNodo2.Attributes("ded").Value
                            Coberturas.AsitenciaCompleta = descripcionCobertura
                    End Select
                Next
            Next


            ObjData.Coberturas.Add(Coberturas)
            ObjData.Emision.Resultado = "True"

            objXmlDocumentCotizacionRes.LoadXml(resCot)
            For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("poliza")
                ObjData.Emision.Poliza = objXmlNode.Attributes("id").Value
            Next
            ObjData.Emision.Documento = ImprimirANA(ObjData.Emision.Poliza, Usuario, Password, Negocio)
            If resCot.Contains("<error>") Then
                ObjData.CodigoError = Mid(resCot, InStr(resCot, "<error>") + 7, (InStr(resCot, "</error>") - InStr(resCot, "<error>")) - 7)
                If ObjData.CodigoError <> "" Then
                    ObjData.Emision.Resultado = "false"
                Else
                    ObjData.Emision.Resultado = "true"
                End If
            Else
                ObjData.Emision.Resultado = "True"
            End If
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Emision.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function BancosANA(ByVal banco As String) As String
        Select Case UCase(banco)
            Case "BANAMEX"
                Return "0012"
            Case "BANCOMER"
                Return "0013"
            Case "BANEJERCITO"
                Return "0014"
            Case "BITAL"
                Return "0015"
            Case "BANORTE"
                Return "0020"
            Case "BANCEN"
                Return "0022"
            Case "GE CAPITAL"
                Return "0023"
            Case "SANTANDER SERFIN"
                Return "0024"
            Case "BANRURAL"
                Return "0026"
            Case "BANCRECER"
                Return "0027"
            Case "BANSI"
                Return "0028"
            Case "SCOTIABANK"
                Return "0034"
            Case "IXE"
                Return "0036"
            Case "INBURSA"
                Return "0037"
            Case "BANREGIO"
                Return "0058"
            Case "BANK OF TOKIO"
                Return "0108"
            Case "BANCO AZTECA"
                Return "0127"
            Case "BAJIO"
                Return "0130"
            Case "MULTIVALORES"
                Return 132
            Case "ADN AMRO BANK"
                Return "0133"
            Case "INTERACCIONES"
                Return "0134"
            Case "MIFEL"
                Return "0142"
            Case "INVEX"
                Return "0159"
            Case "AFIRME"
                Return "0162"
            Case "BANSEFI"
                Return "0166"
        End Select
        Return "Error EN EL BANCO"
    End Function

    Private Shared Function ImprimirANA(ByVal polizaId As String, ByVal Usuario As String, ByVal Password As String, ByVal Negocio As String) As String
        'Variables
        Dim respuesta As String
        Dim imprimeANA As New ANAService.ServiceSoapClient
        polizaId = Mid(polizaId, 1, 11)

        'Documento final - Impresion
        Dim objXmlDocumentCotizacionRes As New XmlDocument()
        Dim strXMLDatosImpresion As XElement

        strXMLDatosImpresion =
            <transacciones xmlns="">
                <transaccion version="1" tipotransaccion="I" negocio=<%= Negocio %>>
                    <poliza id=<%= polizaId %> endoso="000000" inciso="1" link=""/>
                    <error/>
                </transaccion>
            </transacciones>

        Dim xmldoc As New XmlDocument()
        xmldoc.Load(strXMLDatosImpresion.CreateReader())
        Dim xml = xmldoc.DocumentElement

        Dim resCot = imprimeANA.Transaccion(xml, ANAService.TipoTransaccion.Impresion, Usuario, Password)
        objXmlDocumentCotizacionRes.LoadXml(resCot)
        For Each objXmlNode In objXmlDocumentCotizacionRes.GetElementsByTagName("poliza")
            respuesta = objXmlNode.Attributes("link").Value
        Next
        Return respuesta
    End Function

End Class
