﻿Imports Microsoft.VisualBasic

Imports SuperObjeto
Imports System.IO
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports System.Web.Script.Serialization
Imports System.ServiceModel.Security
Imports UNO.Public
Imports System.Net.Http
Imports System.Xml
Imports System.Threading.Tasks
Imports Newtonsoft.Json
Imports System.Data
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports MySql.Data.MySqlClient

Public Class WSPotosiDesarrollo

    Public Shared Function PotosiCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim respuestaWS As String = ""
        ObjData.Cotizacion.Resultado = Nothing
        ObjData.Cotizacion.IDCotizacion = Nothing
        ObjData.Coberturas = Nothing
        ObjData.Cotizacion.PrimaNeta = Nothing
        ObjData.Cotizacion.Impuesto = Nothing
        ObjData.Cotizacion.PrimerPago = Nothing
        ObjData.Cotizacion.PrimaTotal = Nothing
        ObjData.Cotizacion.PagosSubsecuentes = Nothing
        ObjData.Cotizacion.Derechos = Nothing
        Try
            Dim paquete = ""
            If ObjData.Paquete = "AMPLIA" Then
                paquete = "AMPAHORRASEG"
            ElseIf ObjData.Paquete = "LIMITADA" Then
                paquete = "LIMAHORRASEG"
            ElseIf ObjData.Paquete = "RC" Then
                paquete = "BSAHORRASEG"
            ElseIf ObjData.Paquete = "MIGO" Then
                paquete = "PRODBAHORRASEG"
            End If
            Dim idFormaPago = ""
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                idFormaPago = "000136-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                idFormaPago = "000139-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                idFormaPago = "000137-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                idFormaPago = "000138-2"
            End If
            ObjData.Cotizacion.Resultado = "False"
            Dim datosEntrada = New DatosEntradaPotosi
            datosEntrada.paquetes = New List(Of String)
            datosEntrada.empresa = "AhorraSeguros"
            datosEntrada.usuario = "COTAHORRASEG"
            'Version 1
            'datosEntrada.codigoIntermediario = "COTAHORRASEG"

            'Version 2
            datosEntrada.codigoIntermediario = "012707"

            ObjData.Vehiculo.Clave = FuncionesGeneral.rellenaCerosIzq(ObjData.Vehiculo.Clave)

            datosEntrada.año = ObjData.Vehiculo.Modelo
            datosEntrada.marca = ObjData.Vehiculo.Clave.Substring(0, 3)
            datosEntrada.modelo = ObjData.Vehiculo.Clave.Substring(3, 3)
            datosEntrada.version = ObjData.Vehiculo.Clave.Substring(6, 2)

            Dim estadoMunicipioAna() As String
            Try
                estadoMunicipioAna = ConsultaMunicipiosANA2(ObjData.Cliente.Direccion.CodPostal)
                If estadoMunicipioAna(0) = "Distrito Federal" Then
                    datosEntrada.estado = "009"
                    datosEntrada.municipio = "001"
                Else
                    Dim estadoMunicipioPotosi() As String
                    estadoMunicipioPotosi = ConsultaEstadosPotosi2(estadoMunicipioAna(0), estadoMunicipioAna(1))
                    datosEntrada.estado = estadoMunicipioPotosi(0)
                    datosEntrada.municipio = estadoMunicipioPotosi(1)
                End If
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta catalogo de municipios (CP)")
            End Try

            'A: MERCANCIAS CON REDUCIDO GRADO DE PELIGROSIDAD
            'B: CARGA PESADA COMO MAQUINARIA, TRONCOS, PAPEL, ETC
            'C: SUSTANCIAS Y / O PRODUCTOS TOXICOS, CORROSIVOS, INFLAMABLES, ETC
            'Z: NO APLICA
            datosEntrada.carga = "Z"
            datosEntrada.persona = "F"
            datosEntrada.uso = 1
            datosEntrada.descuento = ObjData.Descuento
            datosEntrada.sumaAsegurada = 1000000
            datosEntrada.addenda = "Información propia del cliente"
            datosEntrada.paquetes.Add(paquete)
            datosEntrada.incluirPlanesPago = True
            datosEntrada.incluirDescuentos = True
            datosEntrada.comparativo = False



            Dim jsonFormat = New JsonSerializerSettings
            jsonFormat.NullValueHandling = NullValueHandling.Ignore
            'Version 1
            'Dim serverUrl = "https://api.elpotosi.com.mx/CotizadorAutos/v1/Cotizar"
            'Version 2
            Dim serverUrl = "https://api.elpotosi.com.mx/Autos/Cotizador/Beta"
            Dim requestUrl = JsonConvert.SerializeObject(datosEntrada, jsonFormat)
            Static soucrecode As String = ""
            Try
                Funciones.updateLogWSCot(requestUrl, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Try
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim Request As Net.HttpWebRequest = Net.WebRequest.Create(serverUrl)
                Request.Accept = "*/*"
                Request.Timeout = 10000
                Request.Method = "POST"
                Request.AllowAutoRedirect = True
                If Request.Method = "POST" AndAlso requestUrl IsNot Nothing Then
                    Dim postBytes() As Byte = New UTF8Encoding().GetBytes(requestUrl)
                    Request.ContentType = "application/json"
                    Request.ContentLength = postBytes.Length
                    Request.GetRequestStream().Write(postBytes, 0, postBytes.Length)
                End If
                Dim Response2 As Net.HttpWebResponse = Request.GetResponse()
                Dim ResponseStream As IO.StreamReader = New IO.StreamReader(Response2.GetResponseStream)
                soucrecode = ResponseStream.ReadToEnd()
                Response2.Close()
                ResponseStream.Close()
                Try
                    Funciones.updateLogWSCot(soucrecode, idLogWSCot, "ResponseWS", "FechaFin")
                Catch ex As Exception
                    Dim errr = ex
                End Try
            Catch exweb As WebException
                Try
                    Using stream = exweb.Response.GetResponseStream()
                        Dim reader = New StreamReader(stream)
                        Dim msnError = reader.ReadToEnd()
                        ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + msnError + "]"
                        ObjData.Cotizacion.Resultado = "false"
                        Funciones.updateLogWSCot(msnError, idLogWSCot, "ResponseWS", "FechaFin")
                    End Using
                Catch ex As Exception
                    ObjData.CodigoError = "WSRequest2: [N/A]" + ", Message: [" + ex.Message + "]"
                End Try
                ObjData.Cotizacion.Resultado = "false"
            Catch e As Exception
                ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + e.Message + "], StackTrace: [" + e.StackTrace.ToString() + "]"
                ObjData.Cotizacion.Resultado = "False"
            End Try
            If soucrecode <> "" Then
                respuestaWS = soucrecode
                Dim cotizacionRespuesta = Newtonsoft.Json.JsonConvert.DeserializeObject(Of PotosiCotizacionResp())("[" & soucrecode & "]")
                If cotizacionRespuesta(0).mensajeError = Nothing Then
                    Dim lstCoberturasPotosi As List(Of CoberturasPotosi)
                    Dim lstPlanesPagoPotosi As List(Of PlanesDePagoPotosi)
                    Dim caseCobertura = ""
                    Dim nombreCobertura = ""
                    Dim descripcionCobertura = ""
                    Dim deducible = ""
                    Dim Coberturas As New Coberturas
                    For Each iterCotizacion As PotosiCotizacionResp In cotizacionRespuesta
                        lstCoberturasPotosi = iterCotizacion.paquetes(0).coberturas
                        lstPlanesPagoPotosi = iterCotizacion.paquetes(0).planesDePago
                        ObjData.Cotizacion.IDCotizacion = iterCotizacion.id
                        ObjData.Cotizacion.PagosSubsecuentes = "0"
                        ObjData.Cotizacion.Resultado = "True"
                        For Each iterCoberturasPotosi As CoberturasPotosi In lstCoberturasPotosi
                            caseCobertura = iterCoberturasPotosi.codigo
                            If String.IsNullOrEmpty(iterCoberturasPotosi.deducible) Then
                                deducible = 0
                            Else
                                deducible = iterCoberturasPotosi.deducible
                            End If
                            Select Case caseCobertura
                                Case "DAMA"
                                    nombreCobertura = "DAÑOS MATERIALES"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.DanosMateriales = descripcionCobertura
                                Case "ROBT"
                                    nombreCobertura = "ROBO TOTAL"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RoboTotal = descripcionCobertura
                                Case "DBPE"
                                    nombreCobertura = "RESPONSABILIDAD CIVIL DAÑOS A BIENES Y PERSONAS"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RC = descripcionCobertura
                                Case "GTMO"
                                    nombreCobertura = "GASTOS MEDICOS OCUPANTES"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.GastosMedicosOcupantes = descripcionCobertura
                                Case "ASDL"
                                    nombreCobertura = "ASESORIA Y DEFENSA LEGAL"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.DefensaJuridica = descripcionCobertura
                                Case "ASVJ"
                                    nombreCobertura = "ASISTENCIA EN VIAJES"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RCExtranjero = descripcionCobertura
                                Case "0005"
                                    nombreCobertura = "MUERTE ACCIDENTAL AL CONDUCTOR"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.MuerteAccidental = descripcionCobertura
                                Case "0006"
                                    nombreCobertura = "PERDIDAS ORGANICAS AL CONDUCTOR"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                Case "RCFA"
                                    nombreCobertura = "RESPONSABILIDAD CIVIL FAMILIAR"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RCFamiliar = descripcionCobertura
                                Case "RCEF"
                                    nombreCobertura = "RESPONSABILIDAD CIVIL EN EXCESO POR FALLECIMIENTO"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                Case "VI39"
                                    nombreCobertura = "VIDA"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                            End Select
                        Next
                        ObjData.Coberturas = New List(Of Coberturas)
                        ObjData.Coberturas.Add(Coberturas)
                        For Each iterPlanes As PlanesDePagoPotosi In lstPlanesPagoPotosi
                            If iterPlanes.id = idFormaPago Then
                                ObjData.Cotizacion.PrimaNeta = iterPlanes.primaNetaSinDescuento
                                ObjData.Cotizacion.Impuesto = iterPlanes.iva
                                ObjData.Cotizacion.PrimerPago = iterPlanes.primerPago
                                ObjData.Cotizacion.PrimaTotal = iterPlanes.pagoTotal
                                ObjData.Cotizacion.PagosSubsecuentes = iterPlanes.pagoSubsecuente
                                ObjData.Cotizacion.Derechos = iterPlanes.gastosDeExpedicion
                                ObjData.Cotizacion.CotID = iterPlanes.folio
                            End If
                        Next
                    Next
                Else
                    ObjData.CodigoError = cotizacionRespuesta(0).mensajeError
                End If
            End If
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "false"
        End Try

        If ObjData.Cotizacion.Resultado = "false" Then
            ObjData.Cotizacion.IDCotizacion = Nothing
            ObjData.Coberturas = Nothing
            ObjData.Cotizacion.PrimaNeta = Nothing
            ObjData.Cotizacion.Impuesto = Nothing
            ObjData.Cotizacion.PrimerPago = Nothing
            ObjData.Cotizacion.PrimaTotal = Nothing
            ObjData.Cotizacion.PagosSubsecuentes = Nothing
            ObjData.Cotizacion.Derechos = Nothing
        End If

        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function PotosiEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim respuestaWS As String = ""
        Dim emisionRequest = New PotosiEmisionRequest
        emisionRequest.Empresa = "AhorraSeguros"
        emisionRequest.Usuario = "COTAHORRASEG"
        Try
            emisionRequest.FolioCotizacion = ObjData.Cotizacion.CotID
            Dim idFormaPago = ""
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                idFormaPago = "000136-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                idFormaPago = "000139-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                idFormaPago = "000137-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                idFormaPago = "000138-2"
            End If
            emisionRequest.PlanDePago = idFormaPago
            emisionRequest.SerialMotor = ObjData.Vehiculo.NoMotor
            emisionRequest.SerialCarroceria = ObjData.Vehiculo.NoSerie
            emisionRequest.Placas = ObjData.Vehiculo.NoPlacas
            emisionRequest.beneficiarioPreferente = "Un beneficiario preferente"
            emisionRequest.Contratante = New Dictionary(Of String, Object)
            emisionRequest.Contratante.Add("Rfc", ObjData.Cliente.RFC.Substring(0, 4))
            emisionRequest.Contratante.Add("RfcHomoclave", ObjData.Cliente.RFC.Substring(ObjData.Cliente.RFC.Length - 3))
            emisionRequest.Contratante.Add("TipoPersona", "F")
            emisionRequest.Contratante.Add("Nombre", ObjData.Cliente.Nombre)
            emisionRequest.Contratante.Add("Apellidos", ObjData.Cliente.ApellidoPat + " " + ObjData.Cliente.ApellidoMat)
            emisionRequest.Contratante.Add("RazonSocial", ObjData.Cliente.Nombre + " " + ObjData.Cliente.ApellidoPat + " " + ObjData.Cliente.ApellidoMat)
            emisionRequest.Contratante.Add("Telefono", ObjData.Cliente.Telefono)
            emisionRequest.Contratante.Add("Telefono2", "5555555555")
            emisionRequest.Contratante.Add("CorreoElectronico", ObjData.Cliente.Email)
            Dim fechaDeNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
            emisionRequest.Contratante.Add("FechaDeNacimiento", fechaDeNacimiento)

            Dim estadoMunicipioAna() As String
            Dim codigoEstado = "", codigoMunicipio = ""
            Try
                estadoMunicipioAna = ConsultaMunicipiosANA2(ObjData.Cliente.Direccion.CodPostal)
                If estadoMunicipioAna(0) = "Distrito Federal" Then
                    codigoEstado = "009"
                    codigoMunicipio = "001"
                Else
                    Dim estadoMunicipioPotosi() As String
                    estadoMunicipioPotosi = ConsultaEstadosPotosi2(estadoMunicipioAna(0), estadoMunicipioAna(1))
                    codigoEstado = estadoMunicipioPotosi(0)
                    codigoMunicipio = estadoMunicipioPotosi(1)
                End If
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta catalogo de municipios (CP)")
            End Try
            emisionRequest.Contratante.Add("Estado", codigoEstado)
            emisionRequest.Contratante.Add("Municipio", codigoMunicipio)
            emisionRequest.Contratante.Add("Ciudad", "028") 'pendiente
            emisionRequest.Contratante.Add("Colonia", ObjData.Cliente.Direccion.Colonia) 'pendiente
            emisionRequest.Contratante.Add("Calle", ObjData.Cliente.Direccion.Calle)
            emisionRequest.Contratante.Add("NumeroExterior", ObjData.Cliente.Direccion.NoExt)
            emisionRequest.Contratante.Add("NumeroInterior", "")
            emisionRequest.Contratante.Add("CodigoPostal", ObjData.Cliente.Direccion.CodPostal)
            emisionRequest.Contratante.Add("EsMexicano", True)
            emisionRequest.Contratante.Add("EstructuraCorporativa", "C")
            emisionRequest.ResponsableDePago = New Dictionary(Of String, Object)
            'Para pruebas usar RFC generico XAXX 
            emisionRequest.ResponsableDePago.Add("Rfc", ObjData.Cliente.RFC.Substring(0, 4))  ' TOMAR SOLO LOS PRIMEROS 4 DIGITOS
            emisionRequest.ResponsableDePago.Add("RfcHomoclave", ObjData.Cliente.RFC.Substring(ObjData.Cliente.RFC.Length - 3))
            emisionRequest.ResponsableDePago.Add("TipoPersona", "F")
            emisionRequest.ResponsableDePago.Add("Nombre", ObjData.Cliente.Nombre)
            emisionRequest.ResponsableDePago.Add("Apellidos", ObjData.Cliente.ApellidoPat + " " + ObjData.Cliente.ApellidoMat)
            emisionRequest.ResponsableDePago.Add("RazonSocial", ObjData.Cliente.Nombre + " " + ObjData.Cliente.ApellidoPat + " " + ObjData.Cliente.ApellidoMat)
            emisionRequest.ResponsableDePago.Add("Telefono", ObjData.Cliente.Telefono)
            emisionRequest.ResponsableDePago.Add("Telefono2", "5555555555")
            emisionRequest.ResponsableDePago.Add("CorreoElectronico", ObjData.Cliente.Email)
            emisionRequest.ResponsableDePago.Add("FechaDeNacimiento", fechaDeNacimiento)
            emisionRequest.ResponsableDePago.Add("Estado", codigoEstado)
            emisionRequest.ResponsableDePago.Add("Municipio", codigoMunicipio)
            emisionRequest.ResponsableDePago.Add("Ciudad", "028")
            emisionRequest.ResponsableDePago.Add("Colonia", ObjData.Cliente.Direccion.Colonia)
            emisionRequest.ResponsableDePago.Add("Calle", ObjData.Cliente.Direccion.Calle)
            emisionRequest.ResponsableDePago.Add("NumeroExterior", ObjData.Cliente.Direccion.NoExt)
            emisionRequest.ResponsableDePago.Add("NumeroInterior", "")
            emisionRequest.ResponsableDePago.Add("CodigoPostal", ObjData.Cliente.Direccion.CodPostal)
            emisionRequest.ResponsableDePago.Add("EsMexicano", True)
            emisionRequest.ResponsableDePago.Add("EstructuraCorporativa", "C")


            'Dim solicitud = "{  'Empresa': 'Advansure',  'Usuario': 773477,  'FolioCotizacion': 1476192,  'PlanDePago': '000137-2',  'SerialMotor': '0123456789',  'SerialCarroceria': '3N1EB31S94K516571',  'Placas': 'XDW-45',  'Contratante': {    'Rfc': 'XXXX',    'RfcHomoclave': '123',    'TipoPersona': 'F',    'Nombre': 'Nombre Contratante',    'Apellidos': 'Apellidos Contratante',    'RazonSocial': 'Razón social contratante',    'Telefono': '4444444444',    'Telefono2': '5555555555',    'CorreoElectronico': 'correo@dominio.com',    'FechaDeNacimiento': '1901-01-01',    'Estado': '024',    'Municipio': '001',    'Ciudad': '028',    'Colonia': 'Colonia contratante',    'Calle': 'Calle contratante',    'NumeroExterior': '000',    'NumeroInterior': '000',    'CodigoPostal': '78000',    'EsMexicano': true,    'EstructuraCorporativa': 'C'  },  'ResponsableDePago': {    'Rfc': 'XXXX',    'RfcHomoclave': '123',    'TipoPersona': 'F',    'Nombre': 'Nombre Responsable',    'Apellidos': 'Apellidos Responsable',    'RazonSocial': 'Razón social Responsable',    'Telefono': '7777777777',    'Telefono2': '8888888888',    'CorreoElectronico': 'correo@dominio.com',    'FechaDeNacimiento': '1901-01-01',    'Estado': '024',    'Municipio': '001',    'Ciudad': '028',    'Colonia': 'Colonia Responsable',    'Calle': 'Calle Responsable',    'NumeroExterior': '111',    'NumeroInterior': '222',    'CodigoPostal': '78130',    'EsMexicano': true,    'EstructuraCorporativa': 'C'  }}"

            Dim jsonFormat = New JsonSerializerSettings
            jsonFormat.NullValueHandling = NullValueHandling.Ignore
            'Version 1
            'Dim serverUrl = "https://api.elpotosi.com.mx/CotizadorAutos/v1/Cotizar"
            'Version 2
            Dim serverUrl = "https://api.elpotosi.com.mx/Autos/Emisor/Beta"
            Dim requestUrl = JsonConvert.SerializeObject(emisionRequest, jsonFormat)
            Static soucrecode As String = ""
            Try
                Funciones.updateLogWS(requestUrl, idLogWS, "RequestWS")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Try
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim Request As Net.HttpWebRequest = Net.WebRequest.Create(serverUrl)
                Request.Accept = "*/*"
                Request.Timeout = 10000
                Request.Method = "POST"
                Request.AllowAutoRedirect = True
                If Request.Method = "POST" AndAlso requestUrl IsNot Nothing Then
                    Dim postBytes() As Byte = New UTF8Encoding().GetBytes(requestUrl)
                    Request.ContentType = "application/json"
                    Request.ContentLength = postBytes.Length
                    Request.GetRequestStream().Write(postBytes, 0, postBytes.Length)
                End If
                Dim Response2 As Net.HttpWebResponse = Request.GetResponse()
                Dim ResponseStream As IO.StreamReader = New IO.StreamReader(Response2.GetResponseStream)
                soucrecode = ResponseStream.ReadToEnd()
                Response2.Close()
                ResponseStream.Close()
                Try
                    respuestaWS = soucrecode
                    Funciones.updateLogWS(soucrecode, idLogWS, "ResponseWS")
                Catch ex As Exception
                    Dim errr = ex
                End Try
            Catch exweb As WebException
                Try
                    Using stream = exweb.Response.GetResponseStream()
                        Dim reader = New StreamReader(stream)
                        Dim msnError = reader.ReadToEnd()
                        ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + msnError + "]"
                        ObjData.Emision.Resultado = "false"
                        respuestaWS = msnError
                        Funciones.updateLogWS(msnError, idLogWS, "ResponseWS")
                    End Using
                Catch ex As Exception
                    ObjData.CodigoError = "WSRequest2: [N/A]" + ", Message: [" + ex.Message + "]"
                End Try
                ObjData.Emision.Resultado = "false"
            Catch e As Exception
                ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + e.Message + "], StackTrace: [" + e.StackTrace.ToString() + "]"
                ObjData.Emision.Resultado = "false"
            End Try
            ObjData.Emision.Resultado = "false"
            If soucrecode <> "" Then
                If soucrecode.Contains("idPoliza") Then
                    respuestaWS = soucrecode
                    Dim emisionRespuesta = Newtonsoft.Json.JsonConvert.DeserializeObject(Of PotosiEmisionResponse())("[" & soucrecode & "]")
                    ObjData.Emision.Documento = emisionRespuesta(0).linkCaratula
                    ObjData.Emision.IDCotizacion = emisionRespuesta(0).idPoliza
                    ObjData.Emision.Poliza = emisionRespuesta(0).numeroPoliza
                    ObjData.Emision.PrimaTotal = ObjData.Cotizacion.PrimaTotal
                    ObjData.Emision.PrimaNeta = ObjData.Cotizacion.PrimaNeta
                    ObjData.Emision.PrimerPago = ObjData.Cotizacion.PrimerPago
                    ObjData.Emision.Impuesto = ObjData.Cotizacion.Impuesto
                    ObjData.Emision.Derechos = ObjData.Cotizacion.Derechos
                    ObjData.Emision.Resultado = "true"
                Else
                    ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]"
                End If
            Else
                ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]"
            End If
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Emision.Resultado = "false"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Private Shared Function ConsultaEstadosPotosi2(ByVal estado As String, ByVal municipio As String)
        Dim strSQLQueryCPostal As String
        Dim Lista(2) As String
        If estado = "Estado de Mexico" Then
            estado = "MEXICO"
        ElseIf estado = "Michoacan de Ocampo" Then
            estado = "MICHOACAN"
        ElseIf estado = "Coahuila de Zaragoza" Then
            estado = "COAHUILA"
        End If

        Dim connectionString1 = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQueryCPostal = "Select CodEstado,CodMunicipio from `Ali_R_CatEdosPotosi` where Estado= '" & estado & "' AND Municipio LIKE'%" & municipio & "%'"
        Dim Connection1 As New MySqlConnection(connectionString1)
        Dim command1 As New MySqlCommand(strSQLQueryCPostal, Connection1)
        Connection1.Open()
        Dim reader1 As MySqlDataReader = command1.ExecuteReader(CommandBehavior.CloseConnection)
        If reader1.HasRows Then

            While (reader1.Read())
                Lista(0) = reader1("CodEstado")
                Lista(1) = reader1("CodMunicipio")
            End While
            Connection1.Close()
        Else
            'Si --No
            Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
            Dim sql = "Select CodEstado,CodMunicipio,Estado,Municipio from `Ali_R_CatEdosPotosi` Where Estado = " + estado + ""
            Using Connection As New MySqlConnection(ConnectionString)
                Dim command As New MySqlCommand(sql, Connection)
                Try
                    Connection.Open()
                    Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                    Dim valorLevenshtein = -1, valorLevenshteinTemp = 0
                    While (reader.Read())
                        valorLevenshteinTemp = FuncionesGeneral.levenshtein(municipio, reader("Municipio"))
                        If valorLevenshtein = -1 Then
                            Lista(0) = reader("CodEstado")
                            Lista(1) = reader("CodMunicipio")
                            valorLevenshtein = valorLevenshteinTemp
                        Else
                            If valorLevenshteinTemp < valorLevenshtein Then
                                Lista(0) = reader("CodEstado")
                                Lista(1) = reader("CodMunicipio")
                                valorLevenshtein = valorLevenshteinTemp
                            End If
                        End If
                    End While
                    Connection.Close()
                Catch ex As Exception
                    Dim errror = ex
                End Try
            End Using
        End If
        Return Lista
    End Function

    Private Shared Function ConsultaMunicipiosANA2(ByVal CPostal As String) As String()
        Dim objConnectionCPostal As MySqlConnection
        Dim objCommandCPostal As MySqlCommand
        Dim strSQLQueryCPostal As String
        Dim Lista(2) As String

        objConnectionCPostal = New MySqlConnection("datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;")
        strSQLQueryCPostal = "Select Estado, Municipio from  `ws_db`.`Ali_R_CatEdosANA`  where CPostal = '" & CPostal & "'"
        objCommandCPostal = New MySqlCommand(strSQLQueryCPostal, objConnectionCPostal)
        objConnectionCPostal.Open()

        Dim rsCPostal As MySqlDataReader = objCommandCPostal.ExecuteReader
        rsCPostal.Read()

        Lista(0) = rsCPostal("Estado")
        Lista(1) = rsCPostal("Municipio")
        rsCPostal.Close()
        objConnectionCPostal.Close()
        Return Lista
    End Function

End Class