﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports Microsoft.VisualBasic
Imports MySql.Data.MySqlClient
Imports SuperObjeto

Public Class WsSURAProduccion
    Public Shared Function CotizacionSURA(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim objSURA = New SURAServiceProd.WSInterface
        Dim IniVig As String = Now.Date.ToString("dd/MM/yyyy")
        Dim FinVig As String = Now.Date.AddYears(1).ToString("dd/MM/yyyy")
        Dim hora As String = Now.Date.ToString("HH:mm")
        Dim periodicidad As String = Now.Date.ToString("HH:mm")
        Dim formaPago = ""
        Dim Genero = ""
        Dim respuestaWS As String = ""
        Dim paquete = ""

        If UCase(ObjData.Paquete) = "AMPLIA" Then
            paquete = "01"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            paquete = "02"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            paquete = "03"
        End If
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            formaPago = "12"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            formaPago = "1"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            formaPago = "6"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            formaPago = "3"
        End If

        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = "H"
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = "M"
        End If

        If ObjData.Vehiculo.Descripcion.Length > 40 Then
            ObjData.Vehiculo.Descripcion = ObjData.Vehiculo.Descripcion.Substring(0, 40)
        End If


        Dim xml = "<rtm>" &
   "<rtm_header>" &
      "<conversationid>1</conversationid>" &
      "<transaction>BB0010</transaction>" &
      "<sourcecountry>MX</sourcecountry>" &
      "<sourcecompany>RSASERVICE</sourcecompany>" &
      "<sourcesystem>RSASERVICE</sourcesystem>" &
   "</rtm_header>" &
   "<rtm_body>" &
      "<information>" &
         "<poliza>" &
            "<cdunieco>1</cdunieco>" &
            "<cdramo>211</cdramo>" &
            "<edoPoliza>W</edoPoliza>" &
            "<nmpoliza/>" &
            "<status>V</status>" &
            "<nmsolici>0</nmsolici>" &
            "<cdmoneda>MXP</cdmoneda>" &
            "<ottempot>R</ottempot>" &
            "<feefecto>" & IniVig & "</feefecto>" &
            "<hhefecto>" & hora & "</hhefecto>" &
            "<fevencim>" & FinVig & "</fevencim>" &
            "<feemisio/>" &
            "<cdperpag>" & formaPago & "</cdperpag>" &
            "<nmcuadro/>" &
            "<porredau>0</porredau>" &
            "<otval01>P</otval01>" &
            "<otval03>S</otval03>" &
            "<otval05>03802</otval05>" &
            "<otval06>0380200001</otval06>" &
            "<otval10>2110000100</otval10>" &
            "<otval11>S</otval11>" &
            "<otval12>105394</otval12>" &
            "<otval13></otval13>" &
            "<otval14>B</otval14>" &
            "<otval19></otval19>" &
            "<otval23/>" &
            "<otval26>" & IniVig & "</otval26>" &
            "<otval29>" & ObjData.Cliente.Telefono & "</otval29>" &
            "<otval33>1</otval33>" &
            "<otval34>0001</otval34>" &
         "</poliza>" &
         "<situaciones>" &
            "<situacion>" &
               "<nmsituac>1</nmsituac>" &
               "<cdagrupa>1</cdagrupa>" &
               "<datosSituacion>" &
                  "<otval01>-" & ObjData.Descuento & "</otval01>" &
                  "<otval02>" & ObjData.Vehiculo.Clave & "</otval02>" &
                  "<otval03>" & ObjData.Vehiculo.Descripcion & "</otval03>" &
                  "<otval05>" & ObjData.Vehiculo.Modelo & "</otval05>" &
                  "<otval07>00</otval07>" &
                  "<otval08>01</otval08>" &
                  "<otval09>5</otval09>" &
                  "<otval10>123</otval10>" &
                  "<otval11>MOT54322</otval11>" &
                  "<otval13>123ABC</otval13>" &
                  "<otval15></otval15>" &
                  "<otval16></otval16>" &
                  "<otval17>N</otval17>" &
                  "<otval21>" & ObjData.Cliente.Edad & "</otval21>" &
                  "<otval22>" & Genero & "</otval22>" &
                  "<otval26>" & ObjData.Cliente.Direccion.CodPostal & "</otval26>" &
               "</datosSituacion>" &
               "<planCobertura>" &
                  "<otval14>" & paquete & "</otval14>"

        If UCase(ObjData.Paquete) = "AMPLIA" Then
            xml &= "<cobertura>" &
                     "<cdgarant>0201</cdgarant>" &
                     "<cdcapita>1</cdcapita>" &
                     "<ptcapita>0</ptcapita>" &
                     "<otval01>0</otval01>" &
                     "<otval03>5</otval03>" &
                  "</cobertura>"
        End If
        If UCase(ObjData.Paquete) = "LIMITADA" Or UCase(ObjData.Paquete) = "AMPLIA" Then
            xml &= "<cobertura>" &
                     "<cdgarant>0202</cdgarant>" &
                     "<cdcapita>2</cdcapita>" &
                     "<ptcapita>0</ptcapita>" &
                     "<otval01>0</otval01>" &
                     "<otval03>10</otval03>" &
                  "</cobertura>"
        End If
        xml &= "<cobertura>" &
                     "<cdgarant>0206</cdgarant>" &
                     "<cdcapita>6</cdcapita>" &
                     "<ptcapita>250000</ptcapita>" &
                     "<otval01>0</otval01>" &
                     "<otval03>0</otval03>" &
                  "</cobertura>" &
                  "<cobertura>" &
                     "<cdgarant>0213</cdgarant>" &
                     "<cdcapita>13</cdcapita>" &
                     "<ptcapita>100000</ptcapita>" &
                     "<otval01>0</otval01>" &
                     "<otval03>0</otval03>" &
                  "</cobertura>" &
                  "<cobertura>" &
                     "<cdgarant>0219</cdgarant>" &
                     "<cdcapita>16</cdcapita>" &
                     "<ptcapita>4000000</ptcapita>" &
                     "<otval01>0</otval01>" &
                     "<otval03>0</otval03>" &
                  "</cobertura>" &
                 "<cobertura>" &
                    "<cdgarant>0235</cdgarant>" &
                    "<cdcapita>31</cdcapita>" &
                    "<ptcapita>0</ptcapita>" &
                    "<otval01>0</otval01>" &
                    "<otval03>0</otval03>" &
                 "</cobertura>" &
                 "<cobertura>" &
                    "<cdgarant>0236</cdgarant>" &
                    "<cdcapita>32</cdcapita>" &
                    "<ptcapita>0</ptcapita>" &
                    "<otval01>0</otval01>" &
                    "<otval03>0</otval03>" &
                 "</cobertura>"
        If UCase(ObjData.Paquete) = "LIMITADA" Or UCase(ObjData.Paquete) = "AMPLIA" Then
            xml &= "<cobertura>" &
                    "<cdgarant>0240</cdgarant>" &
                    "<cdcapita>36</cdcapita>" &
                    "<ptcapita>0</ptcapita>" &
                    "<otval01>0</otval01>" &
                    "<otval03>0</otval03>" &
                 "</cobertura>"
        End If

        xml &= "<cobertura>" &
                    "<cdgarant>0248</cdgarant>" &
                    "<cdcapita>48</cdcapita>" &
                    "<ptcapita>100000</ptcapita>" &
                    "<otval01>0</otval01>" &
                    "<otval03>0</otval03>" &
                 "</cobertura>" &
              "</planCobertura>" &
           "</situacion>" &
        "</situaciones>" &
     "</information>" &
  "</rtm_body>" &
  "<rtm_fault>" &
     "<systemfault/>" &
     "<applicationfault/>" &
  "</rtm_fault>" &
"</rtm>"

        Dim res = ""
        Dim nombreCobertura As String = ""
        Dim Coberturas As New Coberturas
        Dim doc As XmlDocument = New XmlDocument()
        Dim lg As Log = New Log()
        Try
            ' lg.UpdateLogRequest(xml, idLogWSCot)

            ' Funciones.updateLogWSCot(xml, idLogWSCot, "RequestWS", "FechaInicio")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Try
            res = objSURA.doProcess(xml)
            'lg.UpdateLogResponse(res, idLogWSCot)
            ' lg.UpdateLogJsonResponseP(res, idLogWSCot, "time")
            Try
                'Funciones.updateLogWSCot(res, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try
            doc.LoadXml(res)

            Dim errorWS As String = String.Empty
            Try
                Dim colError As XmlNodeList = doc.GetElementsByTagName("systemfault")
                For Each objError In colError
                    errorWS = objError("error").InnerText
                Next
            Catch ex As Exception
            End Try
            If errorWS IsNot String.Empty Then
                respuestaWS = errorWS
                Throw New System.Exception("Error al realizar cotizacion, respuesta no satisfactoria en la invocacion del servicio web")
            End If

            Dim colPoliza As XmlNodeList = doc.GetElementsByTagName("poliza")
            For Each objNodoPoliza In colPoliza
                ObjData.Cotizacion.IDCotizacion = objNodoPoliza("nmpoliza").InnerText
            Next

            Dim colElementos As XmlNodeList = doc.GetElementsByTagName("planCobertura")

            For Each objNodoPaquete In colElementos
                ObjData.Cotizacion.PrimaNeta = objNodoPaquete("primaNeta").InnerText
                ObjData.Cotizacion.Derechos = objNodoPaquete("derechos").InnerText
                ObjData.Cotizacion.Impuesto = objNodoPaquete("impuestos").InnerText
                ObjData.Cotizacion.PrimaTotal = objNodoPaquete("primaTotal").InnerText
                ObjData.Cotizacion.PrimerPago = objNodoPaquete("primerPago").InnerText
                ObjData.Cotizacion.PagosSubsecuentes = objNodoPaquete("pagosSubse").InnerText
                ObjData.Cotizacion.Resultado = "True"
                Dim xmlnode2 As XmlNodeList = objNodoPaquete.GetElementsByTagName("cobertura")
                For Each objnodo4 In xmlnode2
                    Dim cobertura As String = objnodo4("cdgarant").InnerText
                    Dim descripcionCobertura As String = ""
                    Select Case cobertura

                        Case "0201"
                            nombreCobertura = "DAÑOS MATERIALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo4("ptcapita").InnerText & "-D" & objnodo4("otval03").InnerText
                            Coberturas.DanosMateriales = descripcionCobertura
                        Case "0202"
                            nombreCobertura = "ROBO TOTAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo4("ptcapita").InnerText & "-D" & objnodo4("otval03").InnerText
                            Coberturas.RoboTotal = descripcionCobertura
                        Case "0219"
                            nombreCobertura = "RESPONSABILIDAD CIVIL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo4("ptcapita").InnerText & "-D" & objnodo4("otval03").InnerText
                            Coberturas.RCBienes = descripcionCobertura
                        Case "0206"
                            nombreCobertura = "GASTOS MÉDICOS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo4("ptcapita").InnerText & "-D" & objnodo4("otval03").InnerText
                            Coberturas.GastosMedicosOcupantes = descripcionCobertura
                        Case "0236"
                            nombreCobertura = "GASTOS LEGALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo4("ptcapita").InnerText & "-D" & objnodo4("otval03").InnerText
                            Coberturas.DefensaJuridica = descripcionCobertura
                        Case "0235"
                            nombreCobertura = "ASISTENCIA VIAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo4("ptcapita").InnerText & "-D" & objnodo4("otval03").InnerText
                            Coberturas.AsitenciaCompleta = descripcionCobertura
                    End Select

                Next
            Next
            ObjData.Coberturas.Add(Coberturas)
        Catch ex As Exception
            Dim errr = ex
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Dim serializer As New JavaScriptSerializer
        Dim JsonREsponse As String = serializer.Serialize(ObjData)
        ' lg.UpdateLogJsonResponse(JsonREsponse, idLogWSCot)

        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function EmisionSURA(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim objSURA = New SURAServiceProd.WSInterface
        Dim IniVig As String = Now.Date.ToString("dd/MM/yyyy")
        Dim FinVig As String = Now.Date.AddYears(1).ToString("dd/MM/yyyy")
        Dim hora As String = Now.Date.ToString("HH:mm")
        Dim formaPago = ""
        Dim Genero = ""
        Dim paquete = ""
        Dim rolPersona = ""
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            formaPago = "12"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            formaPago = "1"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            formaPago = "6"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            formaPago = "3"
        End If
        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = "H"
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = "M"
        End If

        If ObjData.Vehiculo.Descripcion.Length > 40 Then
            ObjData.Vehiculo.Descripcion = ObjData.Vehiculo.Descripcion.Substring(0, 40)
        End If

        If UCase(ObjData.Paquete) = "AMPLIA" Then
            paquete = "01"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            paquete = "02"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            paquete = "03"
        End If
        Dim provincia As String
        Try
            provincia = ConsultaProvinciaSURA(ObjData.Cliente.Direccion.CodPostal)(0)
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: []" + ", Message: [Error al realizar consulta de estado/municipio], StackTrace: [" + ex.StackTrace.ToString() + "]"
            Dim serializer2 As New JavaScriptSerializer
            Return serializer2.Serialize(ObjData)
        End Try
        Dim xml = "<rtm>" &
          "<rtm_header>" &
             "<conversationid>1</conversationid>" &
             "<transaction>BB0015</transaction>" &
             "<sourcecountry>MX</sourcecountry>" &
             "<sourcecompany>RSASERVICE</sourcecompany>" &
             "<sourcesystem>RSASERVICE</sourcesystem>" &
          "</rtm_header>" &
          "<rtm_body>" &
              "<information>" &
                  "<poliza>" &
                      "<cdunieco>1</cdunieco>" &
                      "<cdramo>211</cdramo>" &
                      "<edoPoliza>W</edoPoliza>" &
                      "<nmpoliza></nmpoliza>" &
                      "<status>V</status>" &
                      "<nmsolici>0</nmsolici>" &
                      "<cdmoneda>MXP</cdmoneda>" &
                      "<ottempot>R</ottempot>" &
                      "<feefecto>" & IniVig & "</feefecto>" &
                      "<hhefecto>" & hora & "</hhefecto>" &
                      "<fevencim>" & FinVig & "</fevencim>" &
                      "<feemisio></feemisio>" &
                      "<cdperpag>" & formaPago & "</cdperpag>" &
                      "<nmcuadro></nmcuadro>" &
                      "<porredau>0</porredau>" &
                      "<otval01>P</otval01>" &
                      "<otval03>S</otval03>" &
                      "<otval05>03802</otval05>" &
                      "<otval06>0380200001</otval06>" &
                      "<otval10>2110000100</otval10>" &
                      "<otval11>S</otval11>" &
                      "<otval12>105394</otval12>" &
                      "<otval13></otval13>" &
                      "<otval14>B</otval14>" &
                      "<otval19>S</otval19>" &
                      "<otval23></otval23>" &
                      "<otval26>" & IniVig & "</otval26>" &
                      "<otval29>" & ObjData.Cliente.Telefono & "</otval29>" &
                      "<otval33>1</otval33>" &
                      "<otval34>0001</otval34>" &
                  "</poliza>" &
                  "<situaciones>" &
                      "<situacion>" &
                          "<nmsituac>1</nmsituac>" &
                          "<cdagrupa>1</cdagrupa>" &
                          "<datosSituacion>" &
                              "<otval01>-" & ObjData.Descuento & "</otval01>" &
                              "<otval02>" & ObjData.Vehiculo.Clave & "</otval02>" &
                              "<otval03>" & ObjData.Vehiculo.Descripcion & "</otval03>" &
                              "<otval05>" & ObjData.Vehiculo.Modelo & "</otval05>" &
                              "<otval07>00</otval07>" &
                              "<otval08>01</otval08>" &
                              "<otval09>5</otval09>" &
                              "<otval10>" & ObjData.Vehiculo.NoSerie & "</otval10>" &
                              "<otval11>" & ObjData.Vehiculo.NoMotor & "</otval11>" &
                              "<otval13>" & ObjData.Vehiculo.NoPlacas & "</otval13>" &
                              "<otval15></otval15>" &
                              "<otval16></otval16>" &
                              "<otval17>N</otval17>" &
                              "<otval21>" & ObjData.Cliente.Edad & "</otval21>" &
                              "<otval22>" & Genero & "</otval22>" &
                              "<otval26>" & ObjData.Cliente.Direccion.CodPostal & "</otval26>" &
                          "</datosSituacion>" &
                          "<planCobertura>" &
                              "<otval14>" & paquete & "</otval14>"
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            xml &= "<cobertura>" &
                                 "<cdgarant>0201</cdgarant>" &
                                 "<cdcapita>1</cdcapita>" &
                                 "<ptcapita>0</ptcapita>" &
                                 "<otval01>0</otval01>" &
                                 "<otval03>5</otval03>" &
                              "</cobertura>"
        End If
        If UCase(ObjData.Paquete) = "LIMITADA" Or UCase(ObjData.Paquete) = "AMPLIA" Then
            xml &= "<cobertura>" &
                                 "<cdgarant>0202</cdgarant>" &
                                 "<cdcapita>2</cdcapita>" &
                                 "<ptcapita>0</ptcapita>" &
                                 "<otval01>0</otval01>" &
                                 "<otval03>10</otval03>" &
                              "</cobertura>"
        End If
        xml &= "<cobertura>" &
                                 "<cdgarant>0206</cdgarant>" &
                                 "<cdcapita>6</cdcapita>" &
                                 "<ptcapita>250000</ptcapita>" &
                                 "<otval01>0</otval01>" &
                                 "<otval03>0</otval03>" &
                              "</cobertura>" &
                              "<cobertura>" &
                                 "<cdgarant>0213</cdgarant>" &
                                 "<cdcapita>13</cdcapita>" &
                                 "<ptcapita>100000</ptcapita>" &
                                 "<otval01>0</otval01>" &
                                 "<otval03>0</otval03>" &
                              "</cobertura>" &
                              "<cobertura>" &
                                 "<cdgarant>0219</cdgarant>" &
                                 "<cdcapita>16</cdcapita>" &
                                 "<ptcapita>4000000</ptcapita>" &
                                 "<otval01>0</otval01>" &
                                 "<otval03>0</otval03>" &
                              "</cobertura>" &
                             "<cobertura>" &
                                "<cdgarant>0235</cdgarant>" &
                                "<cdcapita>31</cdcapita>" &
                                "<ptcapita>0</ptcapita>" &
                                "<otval01>0</otval01>" &
                                "<otval03>0</otval03>" &
                             "</cobertura>" &
                             "<cobertura>" &
                                "<cdgarant>0236</cdgarant>" &
                                "<cdcapita>32</cdcapita>" &
                                "<ptcapita>0</ptcapita>" &
                                "<otval01>0</otval01>" &
                                "<otval03>0</otval03>" &
                             "</cobertura>"
        If UCase(ObjData.Paquete) = "LIMITADA" Or UCase(ObjData.Paquete) = "AMPLIA" Then
            xml &= "<cobertura>" &
                                "<cdgarant>0240</cdgarant>" &
                                "<cdcapita>36</cdcapita>" &
                                "<ptcapita>0</ptcapita>" &
                                "<otval01>0</otval01>" &
                                "<otval03>0</otval03>" &
                             "</cobertura>"
        End If
        xml &= "<cobertura>" &
                                "<cdgarant>0248</cdgarant>" &
                                "<cdcapita>48</cdcapita>" &
                                "<ptcapita>100000</ptcapita>" &
                                "<otval01>0</otval01>" &
                                "<otval03>0</otval03>" &
                             "</cobertura>" &
                          "</planCobertura>" &
                      "</situacion>" &
                  "</situaciones>" &
                  "<personas>"
        For index As Integer = 1 To 4
            If index = 1 Then
                rolPersona = "AS"
            ElseIf index = 2 Then
                rolPersona = "TO"
            ElseIf index = 3 Then
                rolPersona = "XX"
            ElseIf index = 4 Then
                rolPersona = "CA"
            End If
            xml &= "<rolpersona>" &
                          "<cdrol>" & rolPersona & "</cdrol>" &
                          "<cdperson/>" &
                          "<cdtipide>1</cdtipide>" &
                          "<cdideper>" & ObjData.Cliente.RFC & "</cdideper>" &
                          "<dsnombr1>" & ObjData.Cliente.Nombre & "</dsnombr1>" &
                          "<dsnombr2/>" &
                          "<dsapell1>" & ObjData.Cliente.ApellidoPat & "</dsapell1>" &
                          "<dsapell2>" & ObjData.Cliente.ApellidoMat & "</dsapell2>" &
                          "<cdtipper>01</cdtipper>" &
                          "<otfisjur>1</otfisjur>" &
                          "<otsexo>" & Genero & "</otsexo>" &
                          "<fenacimi>" & ObjData.Cliente.FechaNacimiento & "</fenacimi>" &
                          "<otval02>" & ObjData.Cliente.CURP & "</otval02>" &
                          "<otval14>" & ObjData.Cliente.Email & "</otval14>" &
                          "<otval15/>" &
                          "<otval30 />" &
                          "<otval31 />" &
                          "<otval32 />" &
                          "<otval33 />" &
                          "<otval34 />" &
                          "<otval35 />" &
                          "<otval36 />" &
                          "<otval37 />" &
                          "<otval38 />" &
                          "<otval39 />" &
                          "<otval40 />" &
                          "<otval41 />" &
                          "<otval42 />" &
                          "<otval43 />" &
                          "<otval44 />" &
                          "<otval45 />" &
                          "<otval46 />" &
                          "<otval47 />" &
                          "<datosDomicilio>" &
                              "<nmorddom>1</nmorddom>" &
                              "<cdtipdom>1</cdtipdom>" &
                              "<dsdomici>" & ObjData.Cliente.Direccion.Calle & "</dsdomici>" &
                              "<cdsiglas>CA</cdsiglas>" &
                              "<nmtelefo>" & ObjData.Cliente.Telefono & "</nmtelefo>" &
                              "<cdpostal>" & ObjData.Cliente.Direccion.CodPostal & "</cdpostal>" &
                              "<otpoblac></otpoblac>" &
                              "<cdpais>052</cdpais>" &
                              "<otpiso/>" &
                              "<nmnumero>" & ObjData.Cliente.Direccion.NoExt & "</nmnumero>" &
                              "<cdprovin>" & provincia & "</cdprovin>" &
                              "<dszona/>" &
                              "<cdcoloni></cdcoloni>" &
                              "<dscoloni/>" &
                          "</datosDomicilio>" &
                      "</rolpersona>"
        Next
        Dim codigoBanco = bancosSURA(ObjData.Pago.Banco)
        xml &= "</personas>" &
                  "<cobranza>" &
                      "<agrupador>" &
                          "<cdagrupa>1</cdagrupa>" &
                          "<cdperson/>" &
                          "<paglinea>true</paglinea>" &
                          "<cdforpag>3</cdforpag>" &
                          "<cdbanco>" & codigoBanco & "</cdbanco>" &
                          "<cdsucurs/>" &
                          "<cdcuenta/>" &
                          "<cdgestor>RDACRED</cdgestor>" &
                          "<fevencim>01/" & ObjData.Pago.MesExp & "/" & ObjData.Pago.AnioExp & "</fevencim>" &
                          "<cdtarcre>" & ObjData.Pago.NoTarjeta & "</cdtarcre>" &
                          "<cvv>" & ObjData.Pago.CodigoSeguridad & "</cvv>" &
                      "</agrupador>" &
                  "</cobranza>" &
              "</information>" &
          "</rtm_body>" &
           "<rtm_fault>" &
               "<systemfault/>" &
               "<applicationfault/>" &
            "</rtm_fault>" &
          "</rtm>"

        Dim res = ""
        Dim nombreCobertura As String = ""
        Dim Coberturas As New Coberturas
        Dim doc As XmlDocument = New XmlDocument()

        Try
            Funciones.updateLogWS(xml, idLogWS, "RequestWS")
        Catch ex As Exception
            Dim errr = ex
        End Try

        Try
            res = objSURA.doProcess(xml)
            Try
                Funciones.updateLogWS(res, idLogWS, "ResponseWS")
            Catch ex As Exception
                Dim errr = ex
            End Try
            doc.LoadXml(res)
            Dim systemfault As XmlNodeList = doc.GetElementsByTagName("systemfault")
            Dim errorMensaje As String = Nothing
            Try
                For Each errorElement In systemfault
                    errorMensaje = errorElement("error").InnerText
                Next
            Catch ex As Exception
            End Try
            If errorMensaje Is Nothing Then
                Dim planCobertura As XmlNodeList = doc.GetElementsByTagName("planCobertura")
                ObjData.Emision.Resultado = "False"
                For Each objNodoPaquete In planCobertura
                    ObjData.Emision.PrimaTotal = objNodoPaquete("primaTotal").InnerText
                    ObjData.Emision.PrimerPago = objNodoPaquete("primerPago").InnerText
                    ObjData.Emision.PagosSubsecuentes = objNodoPaquete("pagosSubse").InnerText
                    ObjData.Emision.PrimaNeta = objNodoPaquete("primaNeta").InnerText
                    ObjData.Emision.Impuesto = objNodoPaquete("impuestos").InnerText
                    ObjData.Emision.Derechos = objNodoPaquete("derechos").InnerText
                Next
                Dim colElementos As XmlNodeList = doc.GetElementsByTagName("poliza")
                For Each objNodoPaquete In colElementos
                    ObjData.Emision.Poliza = objNodoPaquete("nmpoliza").InnerText
                Next
                ObjData.Emision.Resultado = "True"
                ObjData.Emision.Documento = ImprimeSURA(ObjData.Cotizacion.IDCotizacion, "W")
            Else
                ObjData.CodigoError = errorMensaje
                ObjData.Emision.Resultado = "False"
            End If
        Catch ex As Exception
            ObjData.CodigoError = ex.ToString
            ObjData.Emision.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Private Shared Function ConsultaProvinciaSURA(ByVal CPostal As String) As String()
        Dim strSQLQueryCPostal As String
        Dim Lista(1) As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQueryCPostal = "SELECT CdProvin FROM  Ali_R_CatCPostalSuraTemp where CPostal = '" & CPostal & "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Lista(0) = reader("CdProvin")
                End While
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Lista
    End Function

    Public Shared Function ImprimeSURA(ByVal poliza As String, ByVal tipoDocumento As String) As String
        Dim doc As XmlDocument = New XmlDocument()
        Dim objSURA = New SURAService.WSInterface
        Dim documento As String = ""
        Dim xml = "<rtm>" &
            "<rtm_header>" &
                "<conversationid>1</conversationid>" &
                "<transaction>BB0200</transaction>" &
                "<sourcecountry>MX</sourcecountry>" &
                "<sourcecompany>RSASERVICE</sourcecompany>" &
                "<sourcesystem>RSASERVICE</sourcesystem>" &
            "</rtm_header>" &
            "<rtm_body>" &
                    "<impresion>" &
                        "<tipo>BB0015</tipo>" &
                        "<cdunieco>1</cdunieco>" &
                        "<cdramo>211</cdramo>" &
                        "<edoPoliza>" & tipoDocumento & "</edoPoliza>" &
                        "<nmpoliza>" & poliza & "</nmpoliza>" &
                    "</impresion>" &
            "</rtm_body>" &
            "<rtm_fault>" &
                "<systemfault/>" &
                "<applicationfault/>" &
            "</rtm_fault>" &
        "</rtm>"
        Dim res = objSURA.doProcess(xml)
        doc.LoadXml(res)
        Dim systemfault As XmlNodeList = doc.GetElementsByTagName("systemfault")
        Dim errorMensaje As String = Nothing
        Try
            For Each errorElement In systemfault
                errorMensaje = errorElement("error").InnerText
            Next
        Catch ex As Exception
        End Try
        If errorMensaje Is Nothing Then
            Dim impresion As XmlNodeList = doc.GetElementsByTagName("impresion")
            For Each documentoResp In impresion
                documento = documentoResp("url").InnerText
                Exit For
            Next
        End If
        Return documento
    End Function

    Private Shared Function bancosSURA(ByVal banco As String) As String
        Select Case UCase(banco)
            Case "BANAMEX"
                Return "002"
            Case "SCOTIABANK"
                Return "003"
            Case "BITAL HSBC"
                Return "006"
            Case "CITIBANCK"
                Return "007"
            Case "NACIONAL FINACI"
                Return "008"
            Case "BANCREMI"
                Return "010"
            Case "SANTANDER"
                Return "011"
            Case "BBVA BANCOMER"
                Return "012"
            Case "BANCOMEXT"
                Return "013"
            Case "TOKIO"
                Return "017"
            Case "JP MORGAN"
                Return "018"
            Case "GE CAPITAL BANK"
                Return "022"
            Case "BANJERCITO"
                Return "026"
            Case "BANORTE"
                Return "028"
            Case "BAJIO"
                Return "030"
            Case "BANCEN "
                Return "031"
            Case "INBURSA"
                Return "033"
            Case "BANINTER"
                Return "034"
            Case "MIFEL"
                Return "036"
            Case "BAN NAL COMER"
                Return "037"
            Case "BANOBRAS"
                Return "038"
            Case "BANREGIO"
                Return "042"
            Case "INVEX"
                Return "043"
            Case "AFIRME"
                Return "044"
            Case "BANCOS DEL EXTR"
                Return "046"
            Case "BANK OF MONTREA"
                Return "049"
            Case "NATIONS BANK"
                Return "050"
            Case "GECAPITAL"
                Return "051"
            Case "IXE"
                Return "052"
            Case "BANSI"
                Return "054"
            Case "ABN AMRO MEXICO"
                Return "056"
            Case "AMERICAN EXPRESS"
                Return "057"
            Case "BANK OF AMER"
                Return "058"
            Case "BANK BOSTON"
                Return "059"
            Case "DRESDNER BANK"
                Return "061"
            Case "FIRST CHICAGO"
                Return "062"
            Case "FUJI BANK"
                Return "063"
            Case "ING BANK"
                Return "064"
            Case "HSBC NO UTILIZA"
                Return "065"
            Case "BANXICO"
                Return "067"
            Case "COMERICA BANK"
                Return "069"
            Case "DEUTSCHE BANK"
                Return "070"
            Case "GRUPO F. BANORT"
                Return "071"
            Case "AZTECA"
                Return "072"
            Case "HIPOTECARIA"
                Return "073"
            Case "BANREGIO"
                Return "074"
            Case "AUTOFIN"
                Return "075"
            Case "FAMSA"
                Return "088"
            Case "BMULTIVA"
                Return "089"
            Case "WAL-MART"
                Return "105"
        End Select
        Return "Error EN EL BANCO"
    End Function

End Class
