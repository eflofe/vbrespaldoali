﻿Imports Microsoft.VisualBasic

Imports SuperObjeto
Imports System.IO
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports System.Web.Script.Serialization
Imports System.ServiceModel.Security
Imports UNO.Public
Imports System.Net.Http
Imports System.Xml
Imports System.Threading.Tasks
Imports Newtonsoft.Json
Imports System.Data
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports MySql.Data.MySqlClient

Public Class WSPotosi

    Public Shared Function PotosiCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim respuestaWS As String = ""
        ObjData.Cotizacion.Resultado = Nothing
        ObjData.Cotizacion.IDCotizacion = Nothing
        ObjData.Coberturas = Nothing
        ObjData.Cotizacion.PrimaNeta = Nothing
        ObjData.Cotizacion.Impuesto = Nothing
        ObjData.Cotizacion.PrimerPago = Nothing
        ObjData.Cotizacion.PrimaTotal = Nothing
        ObjData.Cotizacion.PagosSubsecuentes = Nothing
        ObjData.Cotizacion.Derechos = Nothing
        Try
            Dim paquete = ""
            If ObjData.Paquete = "AMPLIA" Then
                paquete = "AMPAHORRASEG"
            ElseIf ObjData.Paquete = "LIMITADA" Then
                paquete = "LIMAHORRASEG"
            ElseIf ObjData.Paquete = "RC" Then
                paquete = "BSAHORRASEG"
            ElseIf ObjData.Paquete = "MIGO" Then
                paquete = "PRODBAHORRASEG"
            End If
            Dim idFormaPago = ""
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                idFormaPago = "000136-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                idFormaPago = "000139-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                idFormaPago = "000137-2"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                idFormaPago = "000138-2"
            End If
            ObjData.Cotizacion.Resultado = "False"
            Dim datosEntrada = New DatosEntradaPotosi
            datosEntrada.paquetes = New List(Of String)
            datosEntrada.empresa = "AhorraSeguros"
            datosEntrada.usuario = "COTAHORRASEG"
            'Version 1
            'datosEntrada.codigoIntermediario = "COTAHORRASEG"

            'Version 2
            datosEntrada.codigoIntermediario = "012707"

            ObjData.Vehiculo.Clave = FuncionesGeneral.rellenaCerosIzq(ObjData.Vehiculo.Clave)

            datosEntrada.año = ObjData.Vehiculo.Modelo
            datosEntrada.marca = ObjData.Vehiculo.Clave.Substring(0, 3)
            datosEntrada.modelo = ObjData.Vehiculo.Clave.Substring(3, 3)
            datosEntrada.version = ObjData.Vehiculo.Clave.Substring(6, 2)

            Dim estadoMunicipioAna() As String
            Try
                estadoMunicipioAna = ConsultaMunicipiosANA(ObjData.Cliente.Direccion.CodPostal)
                If estadoMunicipioAna(0) = "Distrito Federal" Then
                    datosEntrada.estado = "009"
                    datosEntrada.municipio = "001"
                Else
                    Dim estadoMunicipioPotosi() As String
                    estadoMunicipioPotosi = ConsultaEstadosPotosi(estadoMunicipioAna(0), estadoMunicipioAna(1))
                    datosEntrada.estado = estadoMunicipioPotosi(0)
                    datosEntrada.municipio = estadoMunicipioPotosi(1)
                End If
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta catalogo de municipios (CP)")
            End Try

            'A: MERCANCIAS CON REDUCIDO GRADO DE PELIGROSIDAD
            'B: CARGA PESADA COMO MAQUINARIA, TRONCOS, PAPEL, ETC
            'C: SUSTANCIAS Y / O PRODUCTOS TOXICOS, CORROSIVOS, INFLAMABLES, ETC
            'Z: NO APLICA
            datosEntrada.carga = "Z"
            datosEntrada.persona = "F"
            datosEntrada.uso = 1
            datosEntrada.descuento = ObjData.Descuento
            datosEntrada.sumaAsegurada = 1000000
            datosEntrada.addenda = "Información propia del cliente"
            datosEntrada.paquetes.Add(paquete)
            datosEntrada.incluirPlanesPago = True
            datosEntrada.incluirDescuentos = True
            datosEntrada.comparativo = False



            Dim jsonFormat = New JsonSerializerSettings
            jsonFormat.NullValueHandling = NullValueHandling.Ignore
            'Version 1
            'Dim serverUrl = "https://api.elpotosi.com.mx/CotizadorAutos/v1/Cotizar"
            'Version 2
            Dim serverUrl = "https://api.elpotosi.com.mx/Autos/Cotizador/v2"
            Dim requestUrl = JsonConvert.SerializeObject(datosEntrada, jsonFormat)
            Static soucrecode As String = ""
            Try
                Funciones.updateLogWSCot(requestUrl, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Try
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                Dim Request As Net.HttpWebRequest = Net.WebRequest.Create(serverUrl)
                Request.Accept = "*/*"
                Request.Timeout = 60000
                Request.Method = "POST"
                Request.AllowAutoRedirect = True
                If Request.Method = "POST" AndAlso requestUrl IsNot Nothing Then
                    Dim postBytes() As Byte = New UTF8Encoding().GetBytes(requestUrl)
                    Request.ContentType = "application/json"
                    Request.ContentLength = postBytes.Length
                    Request.GetRequestStream().Write(postBytes, 0, postBytes.Length)
                End If
                Dim Response2 As Net.HttpWebResponse = Request.GetResponse()
                Dim ResponseStream As IO.StreamReader = New IO.StreamReader(Response2.GetResponseStream)
                soucrecode = ResponseStream.ReadToEnd()
                Response2.Close()
                ResponseStream.Close()
                Try
                    Funciones.updateLogWSCot(soucrecode, idLogWSCot, "ResponseWS", "FechaFin")
                Catch ex As Exception
                    Dim errr = ex
                End Try
            Catch exweb As WebException
                Try
                    Using stream = exweb.Response.GetResponseStream()
                        Dim reader = New StreamReader(stream)
                        Dim msnError = reader.ReadToEnd()
                        ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + msnError + "]"
                        ObjData.Cotizacion.Resultado = "false"
                        Funciones.updateLogWSCot(msnError, idLogWSCot, "ResponseWS", "FechaFin")
                    End Using
                Catch ex As Exception
                    ObjData.CodigoError = "WSRequest2: [N/A]" + ", Message: [" + ex.Message + "]"
                End Try
                ObjData.Cotizacion.Resultado = "false"
            Catch e As Exception
                ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + e.Message + "], StackTrace: [" + e.StackTrace.ToString() + "]"
                ObjData.Cotizacion.Resultado = "False"
            End Try
            If soucrecode <> "" Then
                respuestaWS = soucrecode
                Dim cotizacionRespuesta = Newtonsoft.Json.JsonConvert.DeserializeObject(Of PotosiCotizacionResp())("[" & soucrecode & "]")
                If cotizacionRespuesta(0).mensajeError = Nothing Then
                    Dim lstCoberturasPotosi As List(Of CoberturasPotosi)
                    Dim lstPlanesPagoPotosi As List(Of PlanesDePagoPotosi)
                    Dim caseCobertura = ""
                    Dim nombreCobertura = ""
                    Dim descripcionCobertura = ""
                    Dim deducible = ""
                    Dim Coberturas As New Coberturas
                    For Each iterCotizacion As PotosiCotizacionResp In cotizacionRespuesta
                        lstCoberturasPotosi = iterCotizacion.paquetes(0).coberturas
                        lstPlanesPagoPotosi = iterCotizacion.paquetes(0).planesDePago
                        ObjData.Cotizacion.IDCotizacion = iterCotizacion.id
                        ObjData.Cotizacion.PagosSubsecuentes = "0"
                        ObjData.Cotizacion.Resultado = "True"
                        For Each iterCoberturasPotosi As CoberturasPotosi In lstCoberturasPotosi
                            caseCobertura = iterCoberturasPotosi.codigo
                            If String.IsNullOrEmpty(iterCoberturasPotosi.deducible) Then
                                deducible = 0
                            Else
                                deducible = iterCoberturasPotosi.deducible
                            End If
                            Select Case caseCobertura
                                Case "DAMA"
                                    nombreCobertura = "DAÑOS MATERIALES"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.DanosMateriales = descripcionCobertura
                                Case "ROBT"
                                    nombreCobertura = "ROBO TOTAL"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RoboTotal = descripcionCobertura
                                Case "DBPE"
                                    nombreCobertura = "RESPONSABILIDAD CIVIL DAÑOS A BIENES Y PERSONAS"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RC = descripcionCobertura
                                Case "GTMO"
                                    nombreCobertura = "GASTOS MEDICOS OCUPANTES"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.GastosMedicosOcupantes = descripcionCobertura
                                Case "ASDL"
                                    nombreCobertura = "ASESORIA Y DEFENSA LEGAL"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.DefensaJuridica = descripcionCobertura
                                Case "ASVJ"
                                    nombreCobertura = "ASISTENCIA EN VIAJES"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RCExtranjero = descripcionCobertura
                                Case "0005"
                                    nombreCobertura = "MUERTE ACCIDENTAL AL CONDUCTOR"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.MuerteAccidental = descripcionCobertura
                                Case "0006"
                                    nombreCobertura = "PERDIDAS ORGANICAS AL CONDUCTOR"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                Case "RCFA"
                                    nombreCobertura = "RESPONSABILIDAD CIVIL FAMILIAR"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                    Coberturas.RCFamiliar = descripcionCobertura
                                Case "RCEF"
                                    nombreCobertura = "RESPONSABILIDAD CIVIL EN EXCESO POR FALLECIMIENTO"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                                Case "VI39"
                                    nombreCobertura = "VIDA"
                                    descripcionCobertura = "-N" & nombreCobertura & "-S" & iterCoberturasPotosi.sumaAsegurada & "-D" & deducible
                            End Select
                        Next
                        ObjData.Coberturas = New List(Of Coberturas)
                        ObjData.Coberturas.Add(Coberturas)
                        For Each iterPlanes As PlanesDePagoPotosi In lstPlanesPagoPotosi
                            If iterPlanes.id = idFormaPago Then
                                ObjData.Cotizacion.PrimaNeta = iterPlanes.primaNetaSinDescuento
                                ObjData.Cotizacion.Impuesto = iterPlanes.iva
                                ObjData.Cotizacion.PrimerPago = iterPlanes.primerPago
                                ObjData.Cotizacion.PrimaTotal = iterPlanes.pagoTotal
                                ObjData.Cotizacion.PagosSubsecuentes = iterPlanes.pagoSubsecuente
                                ObjData.Cotizacion.Derechos = iterPlanes.gastosDeExpedicion
                                ObjData.Cotizacion.CotID = iterPlanes.folio
                            End If
                        Next
                    Next
                Else
                    ObjData.CodigoError = cotizacionRespuesta(0).mensajeError
                End If
            End If
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "false"
        End Try

        If ObjData.Cotizacion.Resultado = "false" Then
            ObjData.Cotizacion.IDCotizacion = Nothing
            ObjData.Coberturas = Nothing
            ObjData.Cotizacion.PrimaNeta = Nothing
            ObjData.Cotizacion.Impuesto = Nothing
            ObjData.Cotizacion.PrimerPago = Nothing
            ObjData.Cotizacion.PrimaTotal = Nothing
            ObjData.Cotizacion.PagosSubsecuentes = Nothing
            ObjData.Cotizacion.Derechos = Nothing
        End If

        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function PotosiEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim solicitud = "{  'Empresa': 'Advansure',  'Usuario': 773477,  'FolioCotizacion': 1476192,  'PlanDePago': '000137-2',  'SerialMotor': '0123456789',  'SerialCarroceria': '3N1EB31S94K516571',  'Placas': 'XDW-45',  'Contratante': {    'Rfc': 'XXXX',    'RfcHomoclave': '123',    'TipoPersona': 'F',    'Nombre': 'Nombre Contratante',    'Apellidos': 'Apellidos Contratante',    'RazonSocial': 'Razón social contratante',    'Telefono': '4444444444',    'Telefono2': '5555555555',    'CorreoElectronico': 'correo@dominio.com',    'FechaDeNacimiento': '1901-01-01',    'Estado': '024',    'Municipio': '001',    'Ciudad': '028',    'Colonia': 'Colonia contratante',    'Calle': 'Calle contratante',    'NumeroExterior': '000',    'NumeroInterior': '000',    'CodigoPostal': '78000',    'EsMexicano': true,    'EstructuraCorporativa': 'C'  },  'ResponsableDePago': {    'Rfc': 'XXXX',    'RfcHomoclave': '123',    'TipoPersona': 'F',    'Nombre': 'Nombre Responsable',    'Apellidos': 'Apellidos Responsable',    'RazonSocial': 'Razón social Responsable',    'Telefono': '7777777777',    'Telefono2': '8888888888',    'CorreoElectronico': 'correo@dominio.com',    'FechaDeNacimiento': '1901-01-01',    'Estado': '024',    'Municipio': '001',    'Ciudad': '028',    'Colonia': 'Colonia Responsable',    'Calle': 'Calle Responsable',    'NumeroExterior': '111',    'NumeroInterior': '222',    'CodigoPostal': '78130',    'EsMexicano': true,    'EstructuraCorporativa': 'C'  }}"

        Dim jsonFormat = New JsonSerializerSettings
        jsonFormat.NullValueHandling = NullValueHandling.Ignore
        'Version 1
        'Dim serverUrl = "https://api.elpotosi.com.mx/CotizadorAutos/v1/Cotizar"
        'Version 2
        Dim serverUrl = "https://api.elpotosi.com.mx/Autos/Emisor/Beta"
        'Dim requestUrl = JsonConvert.SerializeObject(potosiEmisionRequest, jsonFormat)
        Dim requestUrl = "{}"
        Static soucrecode As String = ""
        Try
            Funciones.updateLogWSCot(requestUrl, idLogWS, "RequestWS", "FechaInicio")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            Dim Request As Net.HttpWebRequest = Net.WebRequest.Create(serverUrl)
            Request.Accept = "*/*"
            Request.Timeout = 10000
            Request.Method = "POST"
            Request.AllowAutoRedirect = True
            If Request.Method = "POST" AndAlso requestUrl IsNot Nothing Then
                Dim postBytes() As Byte = New UTF8Encoding().GetBytes(solicitud)
                Request.ContentType = "application/json"
                Request.ContentLength = postBytes.Length
                Request.GetRequestStream().Write(postBytes, 0, postBytes.Length)
            End If
            Dim Response2 As Net.HttpWebResponse = Request.GetResponse()
            Dim ResponseStream As IO.StreamReader = New IO.StreamReader(Response2.GetResponseStream)
            soucrecode = ResponseStream.ReadToEnd()
            Response2.Close()
            ResponseStream.Close()
            Try
                Funciones.updateLogWS(soucrecode, idLogWS, "ResponseWS")
            Catch ex As Exception
                Dim errr = ex
            End Try
        Catch exweb As WebException
            Try
                Using stream = exweb.Response.GetResponseStream()
                    Dim reader = New StreamReader(stream)
                    Dim msnError = reader.ReadToEnd()
                    ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + msnError + "]"
                    ObjData.Cotizacion.Resultado = "false"
                    Funciones.updateLogWS(msnError, idLogWS, "ResponseWS")
                End Using
            Catch ex As Exception
                ObjData.CodigoError = "WSRequest2: [N/A]" + ", Message: [" + ex.Message + "]"
            End Try
            ObjData.Cotizacion.Resultado = "false"
        Catch e As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + e.Message + "], StackTrace: [" + e.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "false"
        End Try

    End Function

    Private Shared Function ConsultaEstadosPotosi(ByVal estado As String, ByVal municipio As String)
        Dim strSQLQueryCPostal As String
        Dim Lista(2) As String
        If estado = "Estado de Mexico" Then
            estado = "MEXICO"
        ElseIf estado = "Michoacan de Ocampo" Then
            estado = "MICHOACAN"
        ElseIf estado = "Coahuila de Zaragoza" Then
            estado = "COAHUILA"
        End If
        Dim connectionString1 = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db"
        strSQLQueryCPostal = "Select CodEstado,CodMunicipio from `Ali_R_CatEdosPotosi` where Estado= '" & estado & "' AND Municipio LIKE'%" & municipio & "%'"
        Dim Connection1 As New MySqlConnection(connectionString1)
        Dim command1 As New MySqlCommand(strSQLQueryCPostal, Connection1)
        Connection1.Open()
        Dim reader1 As MySqlDataReader = command1.ExecuteReader(CommandBehavior.CloseConnection)
        If reader1.HasRows Then

            While (reader1.Read())
                Lista(0) = reader1("CodEstado")
                Lista(1) = reader1("CodMunicipio")
            End While
            Connection1.Close()
        Else
            'Si --No
            Dim ConnectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db"
            Dim sql = "Select CodEstado,CodMunicipio,Estado,Municipio from `Ali_R_CatEdosPotosi` Where Estado = " + estado + ""
            Using Connection As New MySqlConnection(ConnectionString)
                Dim command As New MySqlCommand(sql, Connection)
                Try
                    Connection.Open()
                    Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                    Dim valorLevenshtein = -1, valorLevenshteinTemp = 0
                    While (reader.Read())
                        valorLevenshteinTemp = FuncionesGeneral.levenshtein(municipio, reader("Municipio"))
                        If valorLevenshtein = -1 Then
                            Lista(0) = reader("CodEstado")
                            Lista(1) = reader("CodMunicipio")
                            valorLevenshtein = valorLevenshteinTemp
                        Else
                            If valorLevenshteinTemp < valorLevenshtein Then
                                Lista(0) = reader("CodEstado")
                                Lista(1) = reader("CodMunicipio")
                                valorLevenshtein = valorLevenshteinTemp
                            End If
                        End If
                    End While
                    Connection.Close()
                Catch ex As Exception
                    Dim errror = ex
                End Try
            End Using
        End If
        Return Lista
    End Function

    Private Shared Function ConsultaMunicipiosANA(ByVal CPostal As String) As String()
        Dim objConnectionCPostal As MySqlConnection
        Dim objCommandCPostal As MySqlCommand
        Dim strSQLQueryCPostal As String
        Dim Lista(2) As String
        objConnectionCPostal = New MySqlConnection("datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db")
        strSQLQueryCPostal = "Select Estado, Municipio from  `ws_db`.`Ali_R_CatEdosANA`  where CPostal = '" & CPostal & "'"
        objCommandCPostal = New MySqlCommand(strSQLQueryCPostal, objConnectionCPostal)
        objConnectionCPostal.Open()

        Dim rsCPostal As MySqlDataReader = objCommandCPostal.ExecuteReader
        rsCPostal.Read()

        Lista(0) = rsCPostal("Estado")
        Lista(1) = rsCPostal("Municipio")
        rsCPostal.Close()
        objConnectionCPostal.Close()
        Return Lista
    End Function

End Class

Class Marca
    Public Property ID As String
    Public Property DESCRIPCION As String
End Class

Class Modelo
    Public Property ID As String
    Public Property DESCRIPCION As String
End Class

Class Version
    Public Property ID As String
    Public Property DESCRIPCION As String
End Class



'------ Conjuto de clases que permiten almacenar la respuesta a la solicitud de cotizacion   ------
Class PotosiCotizacionResp
    Public Property id As String
    Public Property datosEntrada As DatosEntradaPotosi
    Public Property descripcionDelVehiculo As String
    Public Property lugarDeCirculacion As String
    Public Property fechaCotizacion As String
    Public Property fechaVigencia As String
    Public Property paquetes As List(Of PaquetesPotosi)
    Public Property mensajeError As String
End Class

Class DatosEntradaPotosi
    Public Property empresa As String
    Public Property usuario As String
    Public Property codigoIntermediario As String
    Public Property año As String
    Public Property marca As String
    Public Property modelo As String
    Public Property version As String
    Public Property estado As String
    Public Property municipio As String
    Public Property paquetes As List(Of String)
    Public Property carga As String
    Public Property persona As String
    Public Property uso As String
    Public Property sumaAsegurada As String
    Public Property descuento As String
    Public Property addenda As String
    Public Property clientIp As String
    Public Property tipoVehiculo As String
    Public Property incluirDatosDeEntrada As Boolean
    Public Property incluirDescuentos As Boolean
    Public Property multianual As String
    Public Property comparativo As Boolean
    '------ Campo que se agrega para las solicitudes de tipo cotizacion
    Public Property incluirPlanesPago As Boolean
End Class

Class PaquetesPotosi
    Public Property codigoPaquete As String
    Public Property paquete As String
    Public Property coberturas As List(Of CoberturasPotosi)
    Public Property planesDePago As List(Of PlanesDePagoPotosi)
    Public Property descuentos As DescuentosPotosi
    Public Property primaNetaConDescuento As String
    Public Property primaNetaSinDescuento As String
    Public Property primaVida As String
    Public Property mensajeError As String
End Class

Class CoberturasPotosi
    Public Property ramo As String
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
    Public Property sumaAsegurada As String
    Public Property primaSinDescuento As String
    Public Property primaConDescuento As String
    Public Property deducible As String
End Class

Class PlanesDePagoPotosi
    Public Property id As String
    Public Property descripcion As String
    Public Property primaNetaSinDescuento As String
    Public Property primaNetaConDescuento As String
    Public Property gastosDeExpedicion As String
    Public Property iva As String
    Public Property numeroDePagos As String
    Public Property primerPago As String
    Public Property pagoSubsecuente As String
    Public Property factorPagoFraccionado As String
    Public Property pagoTotal As String
    Public Property folio As String
End Class

Class DescuentosPotosi
    Public Property conceptos As List(Of ConceptosPotosi)
    Public Property maximo As String
    Public Property solicitado As String
    Public Property autorizado As String
End Class

Class ConceptosPotosi
    Public Property descripcion As String
    Public Property porcentaje As String
End Class
'------ Fin conjunto de clases que permiten almacenar la respuesta a la solicitud de cotizacion ------


'------ Conjunto de clases que permiten almacenar la respuesta a la solicitud de emision ------

Class PotosiEmisionRequest
    Public Property Empresa As String
    Public Property Usuario As String
    Public Property FolioCotizacion As String
    Public Property PlanDePago As String
    Public Property SerialMotor As String
    Public Property SerialCarroceria As String
    Public Property Placas As String
    Public Property beneficiarioPreferente As String
    Public Property Contratante As Dictionary(Of String, Object)
    Public Property ResponsableDePago As Dictionary(Of String, Object)
    Public Property mensajeError As String
End Class

Class PotosiEmisionResponse
    Public Property datosDeEntrada As PotosiEmisionDatosEntradas
    Public Property idPoliza As String
    Public Property linkCaratula As String
    Public Property linkRecibo As String
    Public Property linkBeneficiarioPreferente As String
    Public Property numeroPoliza As String
    Public Property uuidPoliza As String
End Class

Class PotosiEmisionDatosEntradas
    Public Property contratante As PotosiEmisionContrate
    Public Property empresa As String
    Public Property fechaEmision As String
    Public Property folioCotizacion As String
    Public Property placas As String
    Public Property planDePago As String
    Public Property responsableDePago As PotosiEmisionResponsablePago
    Public Property serialCarroceria As String
    Public Property serialMotor As String
    Public Property usuario As String
    Public Property beneficiarioPreferente As String
End Class

Class PotosiEmisionContrate
    Public Property apellidos As String
    Public Property calle As String
    Public Property codigoPostal As String
    Public Property colonia As String
    Public Property correoElectronico As String
    Public Property esMexicano As String
    Public Property estado As String
    Public Property estructuraCorporativa As String
    Public Property fechaDeNacimiento As String
    Public Property municipio As String
    Public Property nombre As String
    Public Property numeroExterior As String
    Public Property numeroInterior As String
    Public Property rfc As String
    Public Property rfcHomoclave As String
    Public Property telefono As String
    Public Property telefono2 As String
    Public Property tipoPersona As String
End Class

Class PotosiEmisionResponsablePago
    Public Property apellidos As String
    Public Property calle As String
    Public Property codigoPostal As String
    Public Property colonia As String
    Public Property correoElectronico As String
    Public Property esMexicano As String
    Public Property estado As String
    Public Property estructuraCorporativa As String
    Public Property fechaDeNacimiento As String
    Public Property municipio As String
    Public Property nombre As String
    Public Property numeroExterior As String
    Public Property numeroInterior As String
    Public Property rfc As String
    Public Property rfcHomoclave As String
    Public Property telefono As String
    Public Property telefono2 As String
    Public Property tipoPersona As String
End Class

'------ Fin conjunto de clases que permiten almacenar la respuesta a la solicitud de emision ------