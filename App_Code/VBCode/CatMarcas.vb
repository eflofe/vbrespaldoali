﻿Imports Microsoft.VisualBasic

Public Class CatMarcas
    Private _ListadoMarcas As New List(Of ListadoMarcas)
    Public Property ListadoMarcas As List(Of ListadoMarcas)
        Get
            Return Me._ListadoMarcas
        End Get
        Set(ByVal value As List(Of ListadoMarcas))
            Me._ListadoMarcas = value
        End Set
    End Property
End Class

Public Class ListadoMarcas
    Private _Marca As String
    Private Sub Constructor()
        Me._Marca = ""
    End Sub
    Public Property Marca As String
        Get
            Return Me._Marca
        End Get
        Set(ByVal value As String)
            Me._Marca = value
        End Set
    End Property
End Class

