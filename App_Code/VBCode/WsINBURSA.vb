﻿Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports Newtonsoft.Json.Linq

Public Class WsINBURSA
    Public Shared Function InbursaCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)

        Try
            Dim wsOTP = New InbursaService.generate
            Dim WSCot = New InbursaCotizacionService.BrokerServiceImplService
            Dim ObjInbursa = New InbursaCotizacionService.negocioVO
            Dim Otp As String = ""
            Dim identificador As String = ""
            Dim resOtp = wsOTP.generateOtp("origen1", "H2TdzVteY8JTt/gRnDrwwQ==", "Fi/e9BybJYU=")
            Dim jsonRespuesta As JObject = JObject.Parse(resOtp)
            For Each Row In jsonRespuesta
                If Row.Key = "otp" Then
                    Otp = Row.Value.ToString
                ElseIf Row.Key = "identificador" Then
                    identificador = Row.Value.ToString
                End If
            Next

            Dim Genero = ""
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                Genero = "H"
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                Genero = "M"
            End If

            ObjInbursa.producto = "AUTOTAL"
            ObjInbursa.canal = "AUTTRIGA"
            ObjInbursa.paquete = "A"
            ObjInbursa.otp = New InbursaCotizacionService.wsOtp
            ObjInbursa.otp.otp = Otp
            ObjInbursa.otp.identificador = identificador
            ObjInbursa.parametros = New InbursaCotizacionService.wsParametro(8) {}
            ObjInbursa.parametros(0) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(0).codigo = "CLAVE_VEHICULAR"
            ObjInbursa.parametros(0).valor = ObjData.Vehiculo.Clave

            ObjInbursa.parametros(1) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(1).codigo = "MODELO"
            ObjInbursa.parametros(1).valor = ObjData.Vehiculo.Modelo

            ObjInbursa.parametros(2) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(2).codigo = "LOCALIDAD"
            ObjInbursa.parametros(2).valor = "22202"

            ObjInbursa.parametros(3) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(3).codigo = "CODUSOS"
            ObjInbursa.parametros(3).valor = "101"

            ObjInbursa.parametros(4) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(4).codigo = "DOM_INBURSA"
            ObjInbursa.parametros(4).valor = "SI"

            ObjInbursa.parametros(5) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(5).codigo = "OCUPHABI"
            ObjInbursa.parametros(5).valor = "12"

            ObjInbursa.parametros(6) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(6).codigo = "AUTEDAD"
            ObjInbursa.parametros(6).valor = ObjData.Cliente.Edad

            ObjInbursa.parametros(7) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(7).codigo = "AUTSEXO"
            ObjInbursa.parametros(7).valor = Genero

            ObjInbursa.parametros(8) = New InbursaCotizacionService.wsParametro()
            ObjInbursa.parametros(8).codigo = "COD_POSTAL"
            ObjInbursa.parametros(8).valor = ObjData.Cliente.Direccion.CodPostal

            Dim resCot = WSCot.negocioAutos(ObjInbursa)
            Dim paquete = ""
            Dim periodicidad = ""
            If ObjData.Paquete = "AMPLIA" Then
                paquete = "AMP"
            ElseIf ObjData.Paquete = "LIMITADA" Then
                paquete = "LIM"
            End If

            If ObjData.PeriodicidadDePago.ToString = "ANUAL" Then
                periodicidad = "CONTADO"
            Else
                periodicidad = ObjData.PeriodicidadDePago.ToString
            End If
            Dim descripcionPrima = paquete & "_" & periodicidad
            For i = 0 To resCot.Length - 1
                Dim codigo As String = resCot(i).codigo
                Dim prima = resCot(i).valor
                Select Case codigo
                    Case "REC_INI_" & descripcionPrima
                        ObjData.Cotizacion.PrimerPago = prima
                    Case "REC_POS_" & descripcionPrima
                        ObjData.Cotizacion.PagosSubsecuentes = prima
                    Case "PMA_" & descripcionPrima
                        ObjData.Cotizacion.Resultado = "True"
                        ObjData.Cotizacion.PrimaTotal = prima
                        ObjData.Cotizacion.Impuesto = (CInt(ObjData.Cotizacion.PrimaTotal) * 0.16).ToString
                        ObjData.Cotizacion.PrimaNeta = (CInt(ObjData.Cotizacion.PrimaTotal) - CInt(ObjData.Cotizacion.PrimaTotal) * 0.16).ToString
                End Select
            Next
            Dim Coberturas As New Coberturas
            Coberturas.DanosMateriales = "-NDAÑOS MATERIALES-S0-D5%"
            Coberturas.RoboTotal = "-NROBO TOTAL-S0-D10%"
            Coberturas.RCBienes = "-NRESPONSABILIDAD CIVIL BIENES-S250,000.00-D0"
            Coberturas.RCPersonas = "-NRESPONSABILIDAD CIVIL PERSONAS-S250,000.00-D0"
            Coberturas.GastosMedicosOcupantes = "-NGASTOS MÉDICOS-S125,000.00-D0"
            Coberturas.RCExtension = "-NGASTOS MÉDICOS-SAMPARADA-D0"
            Coberturas.RCExtranjero = "-NRC EN EL EXTRANJERO-SAMPARADA-D0"
            Coberturas.DefensaJuridica = "-NASISTENCIA LEGAL-S45,000.00-D0"
            Coberturas.AsitenciaCompleta = "-NASISTENCIA EN VIAJES Y VIAL KM-SAMPARADA-D0"
            ObjData.Coberturas.Add(Coberturas)
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

End Class
