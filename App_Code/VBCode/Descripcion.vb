﻿Imports Microsoft.VisualBasic

Public Class General
    Private _Catalogo As New List(Of Catalogo)
    Public Property Catalogo As List(Of Catalogo)
        Get
            Return Me._Catalogo
        End Get
        Set(ByVal value As List(Of Catalogo))
            Me._Catalogo = value
        End Set
    End Property
End Class

Public Class Catalogo
    Private _Aseguradora As String
    Private _Marca As String
    Private _Modelo As String
    Private _CatDescripciones As New List(Of CatDescripciones)

    Private Sub Constructor()
        Me._Aseguradora = ""
        Me._Marca = ""
        Me._Modelo = ""
    End Sub
    Public Property Aseguradora As String
        Get
            Return Me._Aseguradora
        End Get
        Set(ByVal value As String)
            Me._Aseguradora = value
        End Set
    End Property
    Public Property Marca As String
        Get
            Return Me._Marca
        End Get
        Set(ByVal value As String)
            Me._Marca = value
        End Set
    End Property
    Public Property Modelo As String
        Get
            Return Me._Modelo
        End Get
        Set(ByVal value As String)
            Me._Modelo = value
        End Set
    End Property
    Public Property CatDescripciones As List(Of CatDescripciones)
        Get
            Return Me._CatDescripciones
        End Get
        Set(ByVal value As List(Of CatDescripciones))
            Me._CatDescripciones = value
        End Set
    End Property
End Class

Public Class CatDescripciones
    Private _clave As String
    Private _Descripcion As String
    Private Sub Constructor()
        Me._clave = ""
        Me._Descripcion = ""
    End Sub
    Public Property clave As String
        Get
            Return Me._clave
        End Get
        Set(ByVal value As String)
            Me._clave = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property
End Class
