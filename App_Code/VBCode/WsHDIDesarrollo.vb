Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Xml.Serialization
Imports SuperObjeto
Imports System.ServiceModel.Security
Imports Newtonsoft.Json

Public Class WsHDIDesarrollo
    Private Shared user As String = "0682480001"
    Private Shared psw As String = "COmPARAgurUIMP*1"
    Public Shared Function HDICotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Try
            Dim objCotizacion As New HDIServicePruebas.PublicServicesAutosContractClient()
            objCotizacion.ClientCredentials.UserName.UserName = user
            objCotizacion.ClientCredentials.UserName.Password = psw
            objCotizacion.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.PeerTrust
            objCotizacion.ClientCredentials.CreateSecurityTokenManager()
            objCotizacion.ClientCredentials.Clone()
            Dim ObjParam As New HDIServicePruebas.ObtenerPaquetesRequest
            Dim ObjVehiculo As New HDIServicePruebas.CaracteristicasVehiculo
            Dim ObjDatosAdi As New HDIServicePruebas.DatosAdicionalesGLM
            Dim ObjDatosConductor As New HDIServicePruebas.DatosConductor
            Dim ObjMarcas As New HDIServicePruebas.ObtenerMarcasRequest
            Dim Ajuste As New HDIServicePruebas.Ajuste
            Dim ObjSubmarcas As New HDIServicePruebas.ObtenerTiposRequest
            Dim ObjTransmision As New HDIServicePruebas.ObtenerTransmisionesRequest
            Dim ObjPaquetesCam(0) As HDIServicePruebas.PaqueteCoberturas
            Dim ObjPaquetesCam1 As HDIServicePruebas.PaqueteCoberturas = New HDIServicePruebas.PaqueteCoberturas
            Dim ListarPaquetes As New HDIServicePruebas.StringArrayContract
            Dim Objversiones As New HDIServicePruebas.ObtenerVersionesRequest
            Dim ObjUso As New HDIServicePruebas.ObtenerUsoRequest
            Dim ObjNombres As New HDIServicePruebas.NombresConducor

            Dim IdTipoVehiculo As Integer = 0
            Dim IdMarca As Integer
            Dim FormaPago As String = ""
            Dim IdDescripcion As String = ""
            Dim IdVersion As String = ""
            Dim IdTransmision As String = ""
            Dim IdUso As String = ""
            Dim paquete As String = ObjData.Paquete
            Dim DatosVehiculo = GetDatosVehiculo(ObjData.Vehiculo.Clave, ObjData.Vehiculo.Modelo)
            Dim TipoVehiculo = DatosVehiculo(1)
            IdMarca = DatosVehiculo(3)
            IdDescripcion = DatosVehiculo(4)
            IdVersion = DatosVehiculo(5)
            IdTransmision = DatosVehiculo(6)
            Dim IdFormaPago As Integer = 0
            If ObjData.PeriodicidadDePago = 0 Then
                FormaPago = "Contado"
            ElseIf ObjData.PeriodicidadDePago = 4 Then
                FormaPago = "Mensual"
            End If
            Dim tipvhc = GetTipoVehiculo(ObjData.Vehiculo.Clave)
            Dim plan(0) As String
            If UCase(paquete) = "AMPLIA" And UCase(tipvhc(0)) = "AUT" Then
                plan(0) = "19"
            ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc(0)) = "AUT" Then
                plan(0) = "21"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc(0)) = "AUT" Then
                plan(0) = "22"
            ElseIf UCase(paquete) = "AMPLIA" And UCase(tipvhc(0)) = "PKP" Then
                plan(0) = "23"
            ElseIf UCase(paquete) = "LIMITADA" And IdTransmision = "PKP" Then
                plan(0) = "24"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc(0)) = "PKP" Then
                plan(0) = "25"
            End If


            Dim FormasPago = objCotizacion.ObtenerFormasPago()
            For i = 0 To FormasPago.Count - 1
                If FormasPago(i).Descripcion = UCase(FormaPago) Then
                    IdFormaPago = FormasPago(i).Clave
                End If
            Next

            IdUso = 4581

            Dim EdoCivil = objCotizacion.ObtenerEstadoCivil()
            ' ------------------------------------- 
            '       Inicio de Objeto Cotizacion 
            '-------------------------------------
            Dim Nombres = Split(ObjData.Cliente.Nombre)
            Dim numeroNom = Nombres.Length
            ObjData.Cliente.FechaNacimiento = "01/01/" & (Now.Year - ObjData.Cliente.Edad).ToString
            Dim fechaNac As String = ObjData.Cliente.FechaNacimiento
            Dim sexo As Integer = 0

            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = 1
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = 2
            End If

            ObjParam.IDTipoSumaAsegurada = 4452
            ObjParam.IvaPorcentaje = 0
            ObjParam.SumaAsegurada = 0


            ObjVehiculo.idVehiculo = ObjData.Vehiculo.Clave
            ObjVehiculo.idMarca = IdMarca
            ObjVehiculo.idModelo = CInt(ObjData.Vehiculo.Modelo)
            ObjVehiculo.idTipo = IdDescripcion
            ObjVehiculo.idVersion = IdVersion
            ObjVehiculo.idTransmision = IdTransmision
            ObjVehiculo.idUso = IdUso
            ObjVehiculo.numeroSerie = 0
            ObjVehiculo.idZonaCirculacion = 0
            ObjVehiculo.idTonelaje = 0
            ObjVehiculo.idServicio = 0
            ObjDatosAdi.CPCirculacion = ObjData.Cliente.Direccion.CodPostal
            ObjDatosAdi.Renovaciones = 0
            ObjDatosAdi.idRemolque = 4603
            ObjVehiculo.DatosAdicionales = ObjDatosAdi
            ObjParam.datosVehiculo = ObjVehiculo
            ObjDatosConductor.ApellidoPaterno = "PEREZ"
            ObjDatosConductor.ApellidoMaterno = "GOMEZ"
            ObjNombres.PrimerNombre = "LUIS"
            If numeroNom = 2 Then
                ObjNombres.SegundoNombre = Nombres(1)
            End If
            ObjDatosConductor.Nombres = ObjNombres
            ObjDatosConductor.FechaDeNacimiento = Convert.ToDateTime(fechaNac, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            ObjDatosConductor.RFC = "PEGL881119"
            ObjDatosConductor.Sexo = sexo
            ObjDatosConductor.EstadoCivil = "C"
            ObjDatosConductor.Ocupacion = "96"
            ObjDatosConductor.CuentaConCochera = True
            ObjVehiculo.Conductor = ObjDatosConductor

            '-------------------------------------------
            '                 Formas pago
            '-------------------------------------------

            ObjParam.idFormaPago = IdFormaPago
            ListarPaquetes.StringArray = plan
            ObjParam.listaPaquetesACalcular = ListarPaquetes
            ObjParam.obtenerTodosPaquetes = False

            ObjParam.usuario = user
            Try
                Dim x As New XmlSerializer(ObjParam.GetType)
                Dim xml As New StringWriter
                x.Serialize(xml, ObjParam)
                Dim errorws2 As String = xml.ToString
                Funciones.updateLogWSCot(errorws2, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception

            End Try

            Dim respuestaFinal = objCotizacion.ObtenerPaquetes(ObjParam)

            ObjData.Cotizacion.Resultado = "True"

            Try
                Dim x2 As New XmlSerializer(respuestaFinal.ListaPaquetes(0).GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, respuestaFinal.ListaPaquetes(0))
                Dim errorws4 As String = xml2.ToString
                Funciones.updateLogWSCot(errorws4, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception

            End Try

            ObjData.Cotizacion.PrimaNeta = respuestaFinal.ListaPaquetes(0).Totales.PrimaNeta
            ObjData.Cotizacion.Derechos = respuestaFinal.ListaPaquetes(0).Totales.DerechoPoliza
            ObjData.Cotizacion.Impuesto = respuestaFinal.ListaPaquetes(0).Totales.IVA
            ObjData.Cotizacion.PrimaTotal = respuestaFinal.ListaPaquetes(0).Totales.PrimaTotal
            ObjData.Cotizacion.PrimerPago = respuestaFinal.ListaPaquetes(0).Recibos.PrimeraExhibicion
            ObjData.Cotizacion.PagosSubsecuentes = respuestaFinal.ListaPaquetes(0).Recibos.RecibosSubsecuentes


            Dim Coberturas As New Coberturas

            Coberturas.DanosMateriales = "-NDAÑOS MATERIALES-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(0).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(0).Deducible
            Coberturas.RoboTotal = "-NROBO TOTAL-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(1).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(1).Deducible
            Coberturas.RC = "-NRESPONSABILIDAD CIVIL-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(2).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(2).Deducible
            Coberturas.DefensaJuridica = "-NDEF. JUD. Y ASIS. LEGAL-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(3).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatorias(3).Deducible
            Coberturas.GastosMedicosOcupantes = "-NGASTOS MEDICOS-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(0).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(0).Deducible
            Coberturas.RCExtension = "-NEXTENSION RC-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(1).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(1).Deducible
            Coberturas.MuerteAccidental = "-NMUERTE DEL CONDUCTOR POR ACCIDENTE AUTOMOVILISTICO-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(2).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(2).Deducible
            Coberturas.RCFamiliar = "-NRESPONSABILIDAD CIVIL FAMILIAR-S" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(3).SumaAsegurada & "-D" & respuestaFinal.ListaPaquetes(0).CoberturasObligatoriasOpcionales(3).Deducible
            ObjData.Coberturas.Add(Coberturas)

        Catch ex As Exception
            ObjData.CodigoError = ex.Message.ToString
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function GetTipoVehiculo(ByVal Clave As String) As String()
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "HDI", Clave)
        Dim mapaVehiculo = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim Tipo(1) As String
        Tipo(0) = mapaVehiculo.Item("vehicle_type")
        Tipo(1) = mapaVehiculo.Item("providers_trim")
        Return Tipo
    End Function
    Public Shared Function HDIEmision(ByVal ObjData As Seguro, ByVal idLog As String)
        Try
            Dim objCotizacion As New HDIServicePruebas.PublicServicesAutosContractClient()
            objCotizacion.ClientCredentials.UserName.UserName = user
            objCotizacion.ClientCredentials.UserName.Password = psw
            objCotizacion.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.PeerTrust
            objCotizacion.ClientCredentials.CreateSecurityTokenManager()
            objCotizacion.ClientCredentials.Clone()
            Dim ObjParam As New HDIServicePruebas.DatosCotizacion
            Dim ObjParamEmi As New HDIServicePruebas.EmitirCotizacionRequest
            Dim ObjPaqueteCoberturas As New HDIServicePruebas.PaqueteCoberturas
            Dim ObjVehiculo As New HDIServicePruebas.CaracteristicasVehiculo
            Dim ObjDatosAdi As New HDIServicePruebas.DatosAdicionalesGLM
            Dim ObjDatosConductor As New HDIServicePruebas.DatosConductor
            Dim ObjMarcas As New HDIServicePruebas.ObtenerMarcasRequest
            Dim Ajuste As New HDIServicePruebas.Ajuste
            Dim ObjSubmarcas As New HDIServicePruebas.ObtenerTiposRequest
            Dim ObjTransmision As New HDIServicePruebas.ObtenerTransmisionesRequest
            Dim ObjPaquetesCam(0) As HDIServicePruebas.PaqueteCoberturas
            Dim ObjPaquetesCam1 As HDIServicePruebas.PaqueteCoberturas = New HDIServicePruebas.PaqueteCoberturas
            Dim ListarPaquetes As New HDIServicePruebas.StringArrayContract
            Dim Objversiones As New HDIServicePruebas.ObtenerVersionesRequest
            Dim ObjUso As New HDIServicePruebas.ObtenerUsoRequest
            Dim ObjNombres As New HDIServicePruebas.NombresConducor
            Dim objCliente As New HDIServicePruebas.Cliente
            Dim ObjDireccion As New HDIServicePruebas.Direccion
            Dim ObjInfoPago As New HDIServicePruebas.InformacionPago
            Dim ObjBenef As New HDIServicePruebas.BeneficiarioPreferente
            Dim ObjTarifExpres As New HDIServicePruebas.PaqueteTarificarExpress
            Dim ObjInfCorredor As New HDIServicePruebas.InformacionAdicionalCorredor
            Dim vigencia As New HDIServicePruebas.Vigencia

            Dim fechaNacCond As String = CDate(ObjData.Cliente.FechaNacimiento).ToString("dd/MM/yyyy")
            Dim Nombres = Split(ObjData.Cliente.Nombre)
            Dim numeroNom = Nombres.Length
            Dim fechaNac As String = ObjData.Cliente.FechaNacimiento.Replace("\", "")
            Dim sexo As Integer = 0
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = 1
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = 2
            End If

            Dim IdTipoVehiculo As Integer = 0
            Dim IdMarca As Integer
            Dim FormaPago As String = ""
            Dim IdDescripcion As String = ""
            Dim IdVersion As String = ""
            Dim IdTransmision As String = ""
            Dim IdUso As String = ""
            Dim paquete As String = ObjData.Paquete
            Dim DatosVehiculo = GetDatosVehiculo(ObjData.Vehiculo.Clave, ObjData.Vehiculo.Modelo)
            Dim TipoVehiculo = DatosVehiculo(1)
            IdMarca = DatosVehiculo(3)
            IdDescripcion = DatosVehiculo(4)
            IdVersion = DatosVehiculo(5)
            IdTransmision = DatosVehiculo(6)
            IdUso = 4581

            Dim IdFormaPago As Integer = 0
            If ObjData.PeriodicidadDePago = 0 Then
                FormaPago = "Contado"
            ElseIf ObjData.PeriodicidadDePago = 4 Then
                FormaPago = "Mensual"
            End If
            Dim plan As String
            Dim tipvhc = GetTipoVehiculo(ObjData.Vehiculo.Clave)
            If UCase(paquete) = "AMPLIA" And UCase(tipvhc(0)) = "AUT" Then
                plan = "19"
            ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc(0)) = "AUT" Then
                plan = "21"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc(0)) = "AUT" Then
                plan = "22"
            ElseIf UCase(paquete) = "AMPLIA" And UCase(tipvhc(0)) = "PKP" Then
                plan = "23"
            ElseIf UCase(paquete) = "LIMITADA" And UCase(tipvhc(0)) = "PKP" Then
                plan = "24"
            ElseIf UCase(paquete) = "RC" And UCase(tipvhc(0)) = "PKP" Then
                plan = "25"
            End If
            Dim FormasPago = objCotizacion.ObtenerFormasPago()
            For i = 0 To FormasPago.Count - 1
                If FormasPago(i).Descripcion = UCase(FormaPago) Then
                    IdFormaPago = FormasPago(i).Clave
                    Exit For
                End If
            Next
            Dim IdBanco = obtenerCveBanco(ObjData.Pago.Banco)

            If ObjData.Cliente.Direccion.Ciudad = "DISTRITO FEDERAL" Then
                ObjData.Cliente.Direccion.Ciudad = "CIUDAD DE MÉXICO"
            End If
            Dim Estados = objCotizacion.ObtenerEstados("00007")
            Dim IdEstado = ""
            For g = 0 To Estados.Count - 1
                If Estados(g).Descripcion = UCase(ObjData.Cliente.Direccion.Ciudad) Then
                    IdEstado = Estados(g).Clave
                    Exit For
                End If
            Next

            Dim ciudades = objCotizacion.ObtenerCiudades(IdEstado)
            Dim IdCiudad = ""

            For g = 0 To ciudades.Count - 1
                If ciudades(g).Descripcion = UCase(ObjData.Cliente.Direccion.Poblacion) Then
                    IdCiudad = ciudades(g).Clave
                    Exit For
                End If
            Next

            Dim ben = objCotizacion.ObtenerTiposBeneficiarioPreferente()

            Dim ocupacion = ""

            If UCase(ObjData.Cliente.Ocupacion) = "COMERCIANTE" Then
                ocupacion = "710A"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "ESTUDIANTE" Then
                ocupacion = "995A"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "AMA DE CASA" Then
                ocupacion = "996A"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "EMPLEADO" Then
                ocupacion = "9BJG"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "EMPRESARIO" Then
                ocupacion = "9BJK"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "JUBILADO" Then
                ocupacion = "9BOL"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = "PROFESIONISTA" Then
                ocupacion = "9D01"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = UCase("Actividades Agrícolas y Ganaderas") Then
                ocupacion = "9D02"
            ElseIf UCase(ObjData.Cliente.Ocupacion) = UCase("Fuerzas de seguridad y Militares") Then
                ocupacion = "9D03"
            Else
                ocupacion = "9BJG"
            End If

            Dim CCobro = 0
            If ObjData.Pago.MedioPago = "CREDITO" Then
                CCobro = 4
            ElseIf ObjData.Pago.MedioPago = "DEBITO" Then
                CCobro = 5
            End If


            Ajuste.PorcentajeAjuste = 0
            Ajuste.TipoAjuste = 4612

            vigencia.Inicial = Date.Now
            vigencia.Inicial = Date.Now.AddYears(1)


            ObjPaqueteCoberturas.Clave = plan
            ObjPaqueteCoberturas.Vigencia = vigencia
            ObjPaqueteCoberturas.ListaDatosFormasPagos = New HDIServicePruebas.DatosFormasPagos(1) {}
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0) = New HDIServicePruebas.DatosFormasPagos()
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdFormaPago = IdFormaPago
            ObjPaqueteCoberturas.ListaDatosFormasPagos(0).IdPaquete = plan

            Dim Coberturas = New HDIServicePruebas.Coberturas
            Dim cobertura1 = New HDIServicePruebas.Cobertura()
            cobertura1.Regla = 287
            cobertura1.Clave = 233
            cobertura1.Descripcion = "Daños Materiales"
            cobertura1.SumaAsegurada = 0
            cobertura1.Deducible = 5
            cobertura1.ProveedorAsistencia = 0
            cobertura1.PrimaNeta = 12158.1190263578
            cobertura1.Calculada = True
            Coberturas.Add(cobertura1)

            Dim cobertura2 = New HDIServicePruebas.Cobertura()
            cobertura2.Regla = 339
            cobertura2.Clave = 236
            cobertura2.Descripcion = "Robo Total"
            cobertura2.SumaAsegurada = 0
            cobertura2.Deducible = 10
            cobertura2.ProveedorAsistencia = 0
            cobertura2.PrimaNeta = 3972.6375140569
            cobertura2.Calculada = True
            Coberturas.Add(cobertura2)

            Dim cobertura3 = New HDIServicePruebas.Cobertura()
            cobertura3.Regla = 295
            cobertura3.Clave = 242
            cobertura3.Descripcion = "Responsabilidad Civil (Límite Único y Co"
            cobertura3.SumaAsegurada = 1500000
            cobertura3.Deducible = 0
            cobertura3.ProveedorAsistencia = 6923
            cobertura3.PrimaNeta = 312
            cobertura3.Calculada = True
            Coberturas.Add(cobertura3)

            ObjPaqueteCoberturas.CoberturasObligatorias = Coberturas

            Dim CoberturasOpcionales = New HDIServicePruebas.Coberturas

            Dim CoberturaOpcional1 = New HDIServicePruebas.Cobertura()
            CoberturaOpcional1.Regla = 292
            CoberturaOpcional1.Clave = 239
            CoberturaOpcional1.Descripcion = "Gastos Médicos Ocupantes (Límite Único C"
            CoberturaOpcional1.SumaAsegurada = 20000
            CoberturaOpcional1.Deducible = 0
            CoberturaOpcional1.ProveedorAsistencia = 0
            CoberturaOpcional1.PrimaNeta = 401.901562730822
            CoberturaOpcional1.Calculada = True
            CoberturasOpcionales.Add(CoberturaOpcional1)

            Dim CoberturaOpcional2 = New HDIServicePruebas.Cobertura()
            CoberturaOpcional2.Regla = 317
            CoberturaOpcional2.Clave = 264
            CoberturaOpcional2.Descripcion = "Extensión de Responsabilidad Civil para Automóvil Particular"
            CoberturaOpcional2.SumaAsegurada = 0
            CoberturaOpcional2.Deducible = 0
            CoberturaOpcional2.ProveedorAsistencia = 0
            CoberturaOpcional2.PrimaNeta = 0.00
            CoberturaOpcional2.Calculada = True
            CoberturasOpcionales.Add(CoberturaOpcional2)

            Dim CoberturaOpcional3 = New HDIServicePruebas.Cobertura()
            CoberturaOpcional3.Regla = 653
            CoberturaOpcional3.Clave = 366
            CoberturaOpcional3.Descripcion = "Responsabilidad Civil Exceso por Muerte"
            CoberturaOpcional3.SumaAsegurada = 2000000
            CoberturaOpcional3.Deducible = 0
            CoberturaOpcional3.ProveedorAsistencia = 0
            CoberturaOpcional3.PrimaNeta = 251.72414
            CoberturaOpcional3.Calculada = True
            CoberturasOpcionales.Add(CoberturaOpcional3)

            Dim CoberturaOpcional4 = New HDIServicePruebas.Cobertura()
            CoberturaOpcional4.Regla = 398
            CoberturaOpcional4.Clave = 267
            CoberturaOpcional4.Descripcion = "Responsabilidad Civil Familiar"
            CoberturaOpcional4.SumaAsegurada = 100000
            CoberturaOpcional4.Deducible = 0
            CoberturaOpcional4.ProveedorAsistencia = 0
            CoberturaOpcional4.PrimaNeta = 41.0
            CoberturaOpcional4.Calculada = True
            CoberturasOpcionales.Add(CoberturaOpcional4)

            ObjPaqueteCoberturas.CoberturasObligatoriasOpcionales = CoberturasOpcionales
            ObjPaqueteCoberturas.condiciones = New HDIServicePruebas.CondicionesTarificar
            ObjPaqueteCoberturas.condiciones.DeducibleDM = 5
            ObjPaqueteCoberturas.condiciones.DeducibleRT = 10

            ObjDatosAdi.CPCirculacion = ObjData.Cliente.Direccion.CodPostal
            ObjDatosAdi.Renovaciones = 0
            ObjDatosAdi.idRemolque = 4603

            ObjVehiculo.idVehiculo = ObjData.Vehiculo.Clave
            ObjVehiculo.idMarca = IdMarca
            ObjVehiculo.idModelo = CInt(ObjData.Vehiculo.Modelo)


            ObjVehiculo.idTransmision = IdTransmision
            ObjVehiculo.idUso = IdUso

            ObjVehiculo.tipoVehiculo = TipoVehiculo
            ObjVehiculo.numeroMotor = ObjData.Vehiculo.NoMotor
            ObjVehiculo.placas = ObjData.Vehiculo.NoPlacas
            ObjVehiculo.numeroSerie = ObjData.Vehiculo.NoSerie
            ObjVehiculo.pasajeros = 0
            ObjVehiculo.idZonaCirculacion = 0
            ObjVehiculo.idTonelaje = 0
            ObjVehiculo.idServicio = 4601
            ObjVehiculo.idRiesgoCarga = 0
            ObjVehiculo.DatosAdicionales = ObjDatosAdi

            ObjDireccion.calle = ObjData.Cliente.Direccion.Calle
            ObjDireccion.ciudad = IdCiudad
            ObjDireccion.codigoPostal = ObjData.Cliente.Direccion.CodPostal
            ObjDireccion.colonia = ObjData.Cliente.Direccion.Colonia
            ObjDireccion.correoElectronico = ObjData.Cliente.Email
            ObjDireccion.nacionalidad = "002"
            ObjDireccion.estado = IdEstado
            ObjDireccion.pais = "00007"
            ObjDireccion.telefonoCasa = ObjData.Cliente.Telefono
            ObjDireccion.numeroExterior = ObjData.Cliente.Direccion.NoExt
            objCliente.actividadPolitica = False

            objCliente.apellidoMaterno = ObjData.Cliente.ApellidoMat
            objCliente.apellidoPaterno = ObjData.Cliente.ApellidoPat


            objCliente.entidadGubernamental = 0
            objCliente.estadoCivil = "C"
            objCliente.fechaNacimiento = Convert.ToDateTime(fechaNac, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX"))
            objCliente.giroActividad = "EMPLEADO"
            objCliente.ocupacion = UCase(ocupacion)
            objCliente.primerNombre = Nombres(0)
            If numeroNom = 2 Then
                objCliente.segundoNombre = Nombres(1)
            End If
            objCliente.rfc = ObjData.Cliente.RFC
            objCliente.sexo = sexo
            objCliente.Direccion = ObjDireccion

            ObjBenef.tipoBeneficiario = ben(0).Clave
            ObjBenef.nombreCompleto = ObjData.Cliente.Nombre & ObjData.Cliente.ApellidoPat & ObjData.Cliente.ApellidoMat

            ObjTarifExpres.IdPaquete = plan

            Dim CondicionesTar = New HDIServicePruebas.CondicionesTarificar()
            CondicionesTar.DeducibleDM = 5
            CondicionesTar.DeducibleRT = 10
            Dim ListaCondicionesT = New HDIServicePruebas.ListaCondicionesTarificar
            ListaCondicionesT.Add(CondicionesTar)
            ObjTarifExpres.ListaCondiciones = ListaCondicionesT
            ObjTarifExpres.IdPromocion = 0

            Dim configPaquete = New HDIServicePruebas.ConfiguracionPaquete()
            Dim ListaconfigPaquete = New HDIServicePruebas.ListaConfiguracionesPaquete
            configPaquete.UsuarioCotiza = user
            configPaquete.InicioVigencia = Date.Now
            configPaquete.FinVigencia = Date.Now.AddYears(1)
            configPaquete.EsCotizacionDiasPorMes = False
            configPaquete.Descuento = CInt(ObjData.Descuento)
            configPaquete.AgenteCotiza = user
            ListaconfigPaquete.Add(configPaquete)
            ObjTarifExpres.ListaConfiguraciones = ListaconfigPaquete

            ObjInfCorredor.FechaEmisionFactura = Date.Now

            ObjParam.PaqueteCoberturas = ObjPaqueteCoberturas
            ObjParam.CaracteristicasVehiculo = ObjVehiculo
            ObjParam.Cliente = objCliente
            ObjParam.fechaInicioVigencia = Date.Now
            ObjParam.idFormaPago = IdFormaPago
            ObjParam.idConductoCobro = 1
            ObjParam.idConductoCobro = CCobro
            ObjParam.idEstado = IdEstado
            ObjParam.idCiudad = IdCiudad
            ObjParam.BeneficiarioPreferente = ObjBenef
            ObjParam.PaqueteEmitir = ObjTarifExpres

            ObjParam.IDTipoSumaAsegurada = 4452
            ObjParam.SumaAsegurada = 0
            ObjParam.InformacionAdicionalCorredor = ObjInfCorredor
            ObjParam.porcentajeIva = 0


            ObjInfoPago.idBanco = IdBanco
            ObjInfoPago.idTipoTarjeta = Pago.CatCarrier.VISA
            ObjInfoPago.nombreTitularTarjeta = ObjData.Pago.NombreTarjeta
            ObjInfoPago.numCuentaHabiente = ObjData.Pago.NoTarjeta
            ObjInfoPago.numCodigoSeguridad = ObjData.Pago.CodigoSeguridad
            ObjInfoPago.numVencimientoTarjeta = ObjData.Pago.MesExp & ObjData.Pago.AnioExp

            ObjParam.InformacionPago = ObjInfoPago
            ObjParamEmi.datosCotizacion = ObjParam
            ObjParamEmi.usuario = user




            Try
                Dim x2 As New XmlSerializer(ObjParamEmi.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, ObjParamEmi)
                Dim xmlRequest As String = xml2.ToString
                Funciones.updateLogWS(xmlRequest, idLog, "RequestWS")
            Catch ex As Exception

            End Try

            Dim res = objCotizacion.EmitirCotizacion(ObjParamEmi)

            Try
                Dim x3 As New XmlSerializer(res.GetType)
                Dim xml3 As New StringWriter
                x3.Serialize(xml3, res)
                Dim xmlRespuesta As String = xml3.ToString
                Funciones.updateLogWS(xmlRespuesta, idLog, "ResponseWS")
            Catch ex As Exception

            End Try
            ObjData.Emision.Poliza = res.idPoliza
            Dim oficina As String = res.idOficina
            Dim cotizacion = res.idCotizacion

            Dim ObjImprimir As New HDIServicePruebas.PolizaAImprimir

            ObjImprimir.Cotizacion = cotizacion
            ObjImprimir.NumeroPoliza = ObjData.Emision.Poliza
            ObjImprimir.usuario = user
            ObjImprimir.Agencia = oficina
            If ObjData.Emision.Poliza <> Nothing And ObjData.Emision.Poliza <> "" Then
                Dim URL = objCotizacion.ObtenerPolizaPDF(ObjImprimir)
                Dim ruta As String = "W:\Documentos\Proyecto\ws-ali-ahorraseguros-prod\Archivo\"
                File.WriteAllBytes(ruta + ObjData.Emision.Poliza + ".pdf", URL.ByteArray)
                ObjData.Emision.Documento = "https://ws-se.com/Archivo/" + ObjData.Emision.Poliza + ".pdf"
                ObjData.Emision.Resultado = "True"
                ObjData.Emision.PrimaTotal = ObjData.Cotizacion.PrimaTotal
                ObjData.Emision.PrimaNeta = ObjData.Cotizacion.PrimaNeta
                ObjData.Emision.Derechos = ObjData.Cotizacion.Derechos
                ObjData.Emision.Impuesto = ObjData.Cotizacion.Impuesto
                ObjData.Emision.PrimerPago = ObjData.Cotizacion.PrimerPago
                ObjData.Emision.PagosSubsecuentes = ObjData.Cotizacion.PagosSubsecuentes
            Else
                For i = 0 To res.errores.Count - 1
                    ObjData.CodigoError = ObjData.CodigoError + " " + res.errores(i).descripcion
                Next
                ObjData.Emision.Resultado = "False"
            End If

        Catch ex As Exception
            ObjData.Emision.Resultado = "False"

            ObjData.CodigoError = ex.Message.ToString
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)

    End Function
    Private Shared Function obtenerCveBanco(ByVal nBanco As String) As Integer
        Select Case UCase(nBanco)
            Case "BANAMEX"
                Return 2
            Case "HSBC"
                Return 6
            Case "CITIBANK"
                Return 6
            Case "NAFIN"
                Return 8
            Case "SANTANDER"
                Return 11
            Case "BBVA BANCOMER"
                Return 12
            Case "BNCI"
                Return 14
            Case "ATLANTICO"
                Return 15
            Case "JP MORGAN"
                Return 20
            Case "BANCO INDUSTRIAL"
                Return 21
            Case "PROMEX"
                Return 25
            Case "BANJERCITO"
                Return 26
            Case "BANORTE"
                Return 28
            Case "BAJIO"
                Return 30
            Case "BANCEN"
                Return 31
            Case "INBURSA"
                Return 33
            Case "INTERACCIONES"
                Return 34
            Case "BANOBRAS"
                Return 38
            Case "INVEX"
                Return 43
            Case "AFIRME"
                Return 44
            Case "IXE"
                Return 52
            Case "GE MONEY"
                Return 87
            Case "BANSI"
                Return 88
            Case "BANK BOSTON"
                Return 95
            Case "SCOTIABANK"
                Return 96
            Case "BANK ONE"
                Return 98
            Case "DEUTSCHE BANK"
                Return 108
            Case "BMULTIVA"
                Return 111
            Case "CITIBANK"
                Return 116
            Case "BANREGIO"
                Return 117
            Case "ALBERTA"
                Return 120
            Case "SOVEREIGN BANK"
                Return 123
            Case "ING BANK"
                Return 127
            Case "VE POR MAS"
                Return 129
            Case "WAL-MART"
                Return 135
            Case "BANCOPPEL"
                Return 137
        End Select
        Return "ERROR EN EL BANCO"
    End Function

    Private Shared Function GetDatosVehiculo(ByVal Clave As String, ByVal modelo As String) As String()
        Dim objConnection As SqlConnection
        Dim objCommand As SqlCommand
        Dim strSQLQueryCPostal As String
        Dim Tipo(7) As String
        objConnection = New SqlConnection("Data Source=10.20.18.149;Initial Catalog=ws_seguros_auto;User Id=mmendoza;Password=gL@0jAOS123;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn")
        strSQLQueryCPostal = "Select * from CatalogoHDI where amis = '" & Clave & "' and modelo='" & modelo & "'"
        objCommand = New SqlCommand(strSQLQueryCPostal, objConnection)
        objConnection.Open()

        Dim rsCPostal As SqlDataReader = objCommand.ExecuteReader
        rsCPostal.Read()
        Tipo(0) = rsCPostal("amis")
        Tipo(1) = rsCPostal("tipovehiculo")
        Tipo(2) = rsCPostal("modelo")
        Tipo(3) = rsCPostal("IdMarca")
        Tipo(4) = rsCPostal("tipo")
        Tipo(5) = rsCPostal("version")
        Tipo(6) = rsCPostal("transmision")
        Return Tipo
    End Function
End Class
