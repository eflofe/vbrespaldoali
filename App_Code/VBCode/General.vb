﻿Imports Microsoft.VisualBasic
Imports SuperObjeto
Public Class FuncionesGeneral
    Public Shared Sub xmlWS(ByVal xml As String, ByVal ObjData As Seguro, ByVal transaccion As String, ByVal tipoxml As String)
        Dim exists As Boolean = True
        Dim fecha = Now.ToString("ddMMyyyy")
        Dim Contador = 0
        Try
            Dim URL As String = "W:\inetpub\ahorraSeguros\Archivos\xml\" & ObjData.Aseguradora & "\" & transaccion & "\"
            Dim Archivo = URL & fecha & "-" & tipoxml & "-" & ObjData.Cliente.Direccion.CodPostal & ObjData.Vehiculo.Marca.Replace("/", "") & ObjData.Vehiculo.Descripcion.Replace("/", "") & ObjData.Vehiculo.Modelo
            exists = System.IO.Directory.Exists(URL)
            If exists = False Then
                System.IO.Directory.CreateDirectory(URL)
            End If
            Dim ArchivoFinal = Archivo
            While (exists)
                Archivo = ArchivoFinal
                exists = System.IO.File.Exists(Archivo & "_" & Contador & ".xml")
                'If exists = False Then
                '    System.IO.Directory.CreateDirectory(Archivo & ".xml")
                'End If

                'If exists = True Then
                '    'Archivo = Mid(Archivo, 1, InStr(Archivo, "_") - 1)
                '    Archivo += "_" & Contador & ".xml"
                'Else
                Archivo += "_" & Contador & ".xml"
                'End If
                Contador += 1
            End While
            Dim File As System.IO.StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Archivo, True)
            File.Write(xml)
            File.Close()
        Catch ex As Exception
            Dim err = ex
        End Try
    End Sub

    Public Shared Function levenshtein(ByVal s1 As String, ByVal s2 As String) As Integer
        Dim coste As Integer = 0
        Dim n1 As Integer = s1.Length
        Dim n2 As Integer = s2.Length
        Dim m As Integer(,) = New Integer(n1, n2) {}
        For i As Integer = 0 To n1
            m(i, 0) = i
        Next
        For i As Integer = 0 To n2
            m(0, i) = i
        Next
        For i1 As Integer = 1 To n1
            For i2 As Integer = 1 To n2
                coste = If((s1(i1 - 1) = s2(i2 - 1)), 0, 1)
                m(i1, i2) = Math.Min(Math.Min(m(i1 - 1, i2) + 1, m(i1, i2 - 1) + 1), m(i1 - 1, i2 - 1) + coste)
            Next
        Next
        Return m(n1, n2)
    End Function

    Public Shared Function rellenaCerosIzq(ByVal cadena As String)
        For index As Integer = cadena.Length To 7
            cadena = "0" + cadena
        Next
        Return cadena
    End Function

End Class
