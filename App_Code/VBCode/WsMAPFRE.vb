﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Xml.Serialization
Imports System.Net
Imports Newtonsoft.Json
Imports MySql.Data.MySqlClient

Public Class WsMAPFRE
    Public Shared Function CotizacionMAPFRE(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim respuesta As String = ""
        Try
            Dim paquete As String = ObjData.Paquete
            Dim formaPago As String = ObjData.PeriodicidadDePago
            Dim Descripcion As String = ObjData.Vehiculo.Descripcion
            Dim modelo As String = ObjData.Vehiculo.Modelo
            Dim marca As String = ObjData.Vehiculo.Marca
            Dim CPostal As String = ObjData.Cliente.Direccion.CodPostal
            Dim clave As String = ObjData.Vehiculo.Clave
            Dim Edad As String = ObjData.Cliente.Edad
            Dim Genero As String = ObjData.Cliente.Genero
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
            Dim CotizaMapfre As New MAPFREService.CatalogosAutosSoapClient
            Dim token = "94403460-fb76-4e9d-b1d4-244fa7c52c55"

            Dim User As String = "GMAC0106"
            Dim Password As String = "MXMIFNLKYZAQH5PRPHRLLA=="
            Dim Negocio As String = "CANON"
            Dim PolizaGrupo As String = "4ECOMMERCE001"
            Dim Sector As String = "4"
            Dim IniVig As Date = Now.Date
            Dim IniVigTxt = IniVig.ToString("dd/MM/yyyy")
            Dim Finvig As Date
            Finvig = IniVig.AddYears(1)
            Dim FinvigTxt = Finvig.ToString("dd/MM/yyyy")
            Dim CuadroComision As String = "1"
            Dim TipoGestor As String = "AG"
            Dim CodigoNivel3 As String = "0"
            Dim TipoDocumento As String = "RFC"
            Dim CodigoDocumento As String = "PRUEBA"
            Dim CodigoEstado As String = ""
            Dim CodigoPoblacion As String = ""
            Dim PorcentajeAgente As String = "100"
            Dim paquete2 As String = "40999"
            Dim CodMarca As String
            Dim CodModelo As String
            Dim CodTipoVehiculo As String
            Dim tipoVehic
            Try
                tipoVehic = GetTipoVehiculo(ObjData.Vehiculo.Clave)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta tipoVehiculo (clave vehiculo)")
            End Try


            Dim descuentoCot As String

            descuentoCot = 0

            If clave.Length = 8 And UCase(tipoVehic) = "PKP" Then
                CodMarca = Mid(clave, 1, 3)
                CodModelo = Mid(clave, 4, 3)
                CodTipoVehiculo = IIf(Mid(clave, 7, 2) = "01", Mid(Mid(clave, 7, 2), 2, 1), Mid(clave, 7, 2))
            ElseIf clave.Length = 8 Then
                CodMarca = Mid(clave, 1, 3)
                CodModelo = Mid(clave, 4, 3)
                CodTipoVehiculo = Mid(clave, 8, 2)
            ElseIf UCase(tipoVehic) = "PKP" Then
                CodMarca = Mid(clave, 1, 2)
                CodModelo = Mid(clave, 3, 3)
                CodTipoVehiculo = IIf(Mid(clave, 6, 2) = "01", Mid(Mid(clave, 6, 2), 2, 1), Mid(clave, 6, 2))
            Else
                clave = "0" & clave
                CodMarca = Mid(clave, 1, 2)
                CodModelo = Mid(clave, 3, 3)
                CodTipoVehiculo = Mid(clave, 7, 2)
            End If

            Dim ramo As String = "401"
            If CodTipoVehiculo = "12" Then
                ramo = "405"
            End If

            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                formaPago = "1"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                formaPago = "4"
            End If
            Dim strXMLDatosEstados As String = "<xml><data><valor cod_ramo='401' id_negocio='AHORRA_SEGUROS'/></data></xml>"

            Dim DatosDir
            Try
                DatosDir = ConsultaMunicipios(ObjData.Cliente.Direccion.CodPostal)
            Catch ex As Exception
                Throw New System.Exception("Error al realzar consulta de catalogos municipios (CP)")
            End Try

            Dim colElementos As XmlNodeList
            Dim colElementos2 As XmlNodeList
            Dim colElementos3 As XmlNodeList
            Dim colElementos4 As XmlNodeList
            Dim objNodo As XmlNode
            Dim objNodo2 As XmlNode
            Dim objNodo3 As XmlNode
            Dim objNodo4 As XmlNode
            Dim objXmlDocumentEdoRes As New XmlDocument
            Dim objXmlDocumentPobRes As New XmlDocument
            Dim Estado = ""
            CodigoEstado = DatosDir(0)
            CodigoPoblacion = DatosDir(1)

            Dim strXMLDatosCotizacion = ""

            If UCase(ObjData.Paquete) = "AMPLIA" Then
                strXMLDatosCotizacion = "<XML>" +
                    "<SEGURIDAD><USER ID='" + User + "' PWD='" + Password + "'/></SEGURIDAD>" +
                    "<DATA><COTIZACION><DATOS_POLIZA ID_NEGOCIO='AHORRA_SEGUROS' NUM_POLIZA_GRUPO='4ECOMMERCE001' NUM_CONTRATO='47735' COD_SECTOR='4' COD_RAMO='" + ramo + "' FEC_EFEC_POLIZA='" + IniVigTxt + "' FEC_VCTO_POLIZA='" + FinvigTxt + "' COD_FRACC_PAGO='" + formaPago + "' COD_CUADRO_COM='415' COD_AGT='66064' COD_USR='GMAC0106' COD_NIVEL3_CAPTURA='0' TIP_DOCUM='RFC' COD_DOCUM='PRUEBA' COD_ESTADO='" +
                    CodigoEstado + "' COD_PROV='" + CodigoPoblacion + "' PCT_AGT='100' COD_GESTOR='66064' TIP_GESTOR='AG'/>" +
                    " <DATOS_VARIABLES NUM_RIESGO='1' TIP_NIVEL='3'><CAMPO COD_CAMPO='COD_MATERIA' VAL_CAMPO='1'/></DATOS_VARIABLES>" +
                    "<DATOS_VARIABLES19 NUM_RIESGO='1' TIP_NIVEL='2'><CAMPO9 COD_CAMPO='COD_MODALIDAD' VAL_CAMPO='40999'/><CAMPO11 COD_CAMPO='ANIO_FABRICA' VAL_CAMPO='" +
                    modelo + "'/><CAMPO12 COD_CAMPO='COD_MARCA' VAL_CAMPO='" + CodMarca + "'/><CAMPO13 COD_CAMPO='COD_MODELO' VAL_CAMPO='" + CodModelo + "'/><CAMPO14 COD_CAMPO='COD_TIP_VEHI' VAL_CAMPO='" +
                    CodTipoVehiculo + "'/><CAMPO15 COD_CAMPO='COD_USO_VEHI' VAL_CAMPO='401'/><CAMPO16 COD_CAMPO='MCA_FACTURA' VAL_CAMPO='N'/><CAMPO17 COD_CAMPO='MCA_ACTUAL' VAL_CAMPO='N'/><CAMPO18 COD_CAMPO='MCA_COMERCIAL' VAL_CAMPO='S'/>" +
                    "</DATOS_VARIABLES19>" +
                    "<DATOS_VARIABLES22 NUM_RIESGO='0' TIP_NIVEL='1'><CAMPO19 COD_CAMPO='COD_BONI_RECA' VAL_CAMPO='999'/><CAMPO21 COD_CAMPO='PCT_COD_REC_ESP' VAL_CAMPO='" +
                    descuentoCot + "'/></DATOS_VARIABLES22>" +
                    "<COBERTURAS>" +
                "<COBERTURA COD_COB='4000' SUMA_ASEG='C' COD_FRANQUICIA='5'/>" +
                "<COBERTURA24 COD_COB='4001' SUMA_ASEG='C' COD_FRANQUICIA='10'/>" +
                "<COBERTURA25 COD_COB='4010' SUMA_ASEG='1500000'/>" +
                "<COBERTURA26 COD_COB='4011' SUMA_ASEG='1500000'/>" +
                "<COBERTURA27 COD_COB='4006' SUMA_ASEG='200000'/>" +
                "<COBERTURA28 COD_COB='4003' SUMA_ASEG='1'/>" +
                "<COBERTURA29 COD_COB='4004' SUMA_ASEG='1'/>" +
                "<COBERTURA30 COD_COB='4012' SUMA_ASEG='0'/>" +
                "<COBERTURA31 COD_COB='4015' SUMA_ASEG='0'/>" +
                "<COBERTURA32 COD_COB='4013' SUMA_ASEG='100000'/>" +
                "<COBERTURA33 COD_COB='4022' SUMA_ASEG='0'/>" +
                "<COBERTURA34 COD_COB='4023' SUMA_ASEG='0'/>" +
                "<COBERTURA35 COD_COB='4024' SUMA_ASEG='0'/>" +
                "<COBERTURA36 COD_COB='4027' SUMA_ASEG='0'/>" +
                "<COBERTURA37 COD_COB='4028' SUMA_ASEG='0'/>" +
                "<COBERTURA38 COD_COB='4048' SUMA_ASEG='0'/>" +
                "<COBERTURA40 COD_COB='4068' SUMA_ASEG='0' COD_FRANQUICIA='0'/>" +
                "</COBERTURAS></COTIZACION></DATA></XML>"
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                strXMLDatosCotizacion = "<XML>" +
                    "<SEGURIDAD><USER ID='" + User + "' PWD='" + Password + "'/></SEGURIDAD>" +
                    "<DATA><COTIZACION><DATOS_POLIZA ID_NEGOCIO='AHORRA_SEGUROS' NUM_POLIZA_GRUPO='4ECOMMERCE001' NUM_CONTRATO='47735' COD_SECTOR='4' COD_RAMO='" + ramo + "' FEC_EFEC_POLIZA='" + IniVigTxt + "' FEC_VCTO_POLIZA='" + FinvigTxt + "' COD_FRACC_PAGO='" + formaPago + "' COD_CUADRO_COM='415' COD_AGT='66064' COD_USR='GMAC0106' COD_NIVEL3_CAPTURA='0' TIP_DOCUM='RFC' COD_DOCUM='PRUEBA' COD_ESTADO='" +
                    CodigoEstado + "' COD_PROV='" + CodigoPoblacion + "' PCT_AGT='100' COD_GESTOR='66064' TIP_GESTOR='AG'/>" +
                    " <DATOS_VARIABLES NUM_RIESGO='1' TIP_NIVEL='3'><CAMPO COD_CAMPO='COD_MATERIA' VAL_CAMPO='1'/></DATOS_VARIABLES>" +
                    "<DATOS_VARIABLES19 NUM_RIESGO='1' TIP_NIVEL='2'><CAMPO9 COD_CAMPO='COD_MODALIDAD' VAL_CAMPO='40999'/><CAMPO11 COD_CAMPO='ANIO_FABRICA' VAL_CAMPO='" +
                    modelo + "'/><CAMPO12 COD_CAMPO='COD_MARCA' VAL_CAMPO='" + CodMarca + "'/><CAMPO13 COD_CAMPO='COD_MODELO' VAL_CAMPO='" + CodModelo + "'/><CAMPO14 COD_CAMPO='COD_TIP_VEHI' VAL_CAMPO='" +
                    CodTipoVehiculo + "'/><CAMPO15 COD_CAMPO='COD_USO_VEHI' VAL_CAMPO='401'/><CAMPO16 COD_CAMPO='MCA_FACTURA' VAL_CAMPO='N'/><CAMPO17 COD_CAMPO='MCA_ACTUAL' VAL_CAMPO='N'/><CAMPO18 COD_CAMPO='MCA_COMERCIAL' VAL_CAMPO='S'/>" +
                    "</DATOS_VARIABLES19>" +
                    "<DATOS_VARIABLES22 NUM_RIESGO='0' TIP_NIVEL='1'><CAMPO19 COD_CAMPO='COD_BONI_RECA' VAL_CAMPO='999'/><CAMPO21 COD_CAMPO='PCT_COD_REC_ESP' VAL_CAMPO='" +
                    descuentoCot + "'/></DATOS_VARIABLES22>" +
                    "<COBERTURAS>" +
                 "<COBERTURA COD_COB='4000' SUMA_ASEG='0' COD_FRANQUICIA='5'/>" +
                "<COBERTURA24 COD_COB='4001' SUMA_ASEG='C' COD_FRANQUICIA='10'/>" +
                "<COBERTURA25 COD_COB='4010' SUMA_ASEG='1500000'/>" +
                "<COBERTURA26 COD_COB='4011' SUMA_ASEG='1500000'/>" +
                "<COBERTURA27 COD_COB='4006' SUMA_ASEG='100000'/>" +
                "<COBERTURA28 COD_COB='4003' SUMA_ASEG='1'/>" +
                "<COBERTURA29 COD_COB='4004' SUMA_ASEG='1'/>" +
                "<COBERTURA30 COD_COB='4012' SUMA_ASEG='0'/>" +
                "<COBERTURA31 COD_COB='4015' SUMA_ASEG='0'/>" +
                "<COBERTURA32 COD_COB='4013' SUMA_ASEG='100000'/>" +
                "<COBERTURA33 COD_COB='4022' SUMA_ASEG='0'/>" +
                "<COBERTURA34 COD_COB='4023' SUMA_ASEG='0'/>" +
                "<COBERTURA35 COD_COB='4024' SUMA_ASEG='0'/>" +
                "<COBERTURA36 COD_COB='4027' SUMA_ASEG='0'/>" +
                "<COBERTURA37 COD_COB='4028' SUMA_ASEG='0'/>" +
                "<COBERTURA38 COD_COB='4048' SUMA_ASEG='0'/>" +
                "<COBERTURA40 COD_COB='4068' SUMA_ASEG='2000000' COD_FRANQUICIA='0'/>" +
                "</COBERTURAS></COTIZACION></DATA></XML>"
            ElseIf UCase(ObjData.Paquete) = "RC" Then
                strXMLDatosCotizacion = "<XML>" +
                    "<SEGURIDAD><USER ID='" + User + "' PWD='" + Password + "'/></SEGURIDAD>" +
                    "<DATA><COTIZACION><DATOS_POLIZA ID_NEGOCIO='AHORRA_SEGUROS' NUM_POLIZA_GRUPO='4ECOMMERCE001' NUM_CONTRATO='47735' COD_SECTOR='4' COD_RAMO='" + ramo + "' FEC_EFEC_POLIZA='" + IniVigTxt + "' FEC_VCTO_POLIZA='" + FinvigTxt + "' COD_FRACC_PAGO='" + formaPago + "' COD_CUADRO_COM='415' COD_AGT='66064' COD_USR='GMAC0106' COD_NIVEL3_CAPTURA='0' TIP_DOCUM='RFC' COD_DOCUM='PRUEBA' COD_ESTADO='" +
                    CodigoEstado + "' COD_PROV='" + CodigoPoblacion + "' PCT_AGT='100' COD_GESTOR='66064' TIP_GESTOR='AG'/>" +
                    " <DATOS_VARIABLES NUM_RIESGO='1' TIP_NIVEL='3'><CAMPO COD_CAMPO='COD_MATERIA' VAL_CAMPO='1'/></DATOS_VARIABLES>" +
                    "<DATOS_VARIABLES19 NUM_RIESGO='1' TIP_NIVEL='2'><CAMPO9 COD_CAMPO='COD_MODALIDAD' VAL_CAMPO='40999'/><CAMPO11 COD_CAMPO='ANIO_FABRICA' VAL_CAMPO='" +
                    modelo + "'/><CAMPO12 COD_CAMPO='COD_MARCA' VAL_CAMPO='" + CodMarca + "'/><CAMPO13 COD_CAMPO='COD_MODELO' VAL_CAMPO='" + CodModelo + "'/><CAMPO14 COD_CAMPO='COD_TIP_VEHI' VAL_CAMPO='" +
                    CodTipoVehiculo + "'/><CAMPO15 COD_CAMPO='COD_USO_VEHI' VAL_CAMPO='401'/><CAMPO16 COD_CAMPO='MCA_FACTURA' VAL_CAMPO='N'/><CAMPO17 COD_CAMPO='MCA_ACTUAL' VAL_CAMPO='N'/><CAMPO18 COD_CAMPO='MCA_COMERCIAL' VAL_CAMPO='S'/>" +
                    "</DATOS_VARIABLES19>" +
                    "<DATOS_VARIABLES22 NUM_RIESGO='0' TIP_NIVEL='1'><CAMPO19 COD_CAMPO='COD_BONI_RECA' VAL_CAMPO='999'/><CAMPO21 COD_CAMPO='PCT_COD_REC_ESP' VAL_CAMPO='" +
                    descuentoCot + "'/></DATOS_VARIABLES22>" +
                    "<COBERTURAS>" +
                 "<COBERTURA COD_COB='4000' SUMA_ASEG='0' COD_FRANQUICIA='5'/>" +
                "<COBERTURA24 COD_COB='4001' SUMA_ASEG='0' COD_FRANQUICIA='10'/>" +
                "<COBERTURA25 COD_COB='4010' SUMA_ASEG='1500000'/>" +
                "<COBERTURA26 COD_COB='4011' SUMA_ASEG='1500000'/>" +
                "<COBERTURA27 COD_COB='4006' SUMA_ASEG='100000'/>" +
                "<COBERTURA29 COD_COB='4004' SUMA_ASEG='1'/>" +
                "<COBERTURA30 COD_COB='4012' SUMA_ASEG='0'/>" +
                "<COBERTURA31 COD_COB='4015' SUMA_ASEG='0'/>" +
                "<COBERTURA32 COD_COB='4013' SUMA_ASEG='0'/>" +
                "<COBERTURA33 COD_COB='4022' SUMA_ASEG='0'/>" +
                "<COBERTURA34 COD_COB='4023' SUMA_ASEG='0'/>" +
                "<COBERTURA35 COD_COB='4024' SUMA_ASEG='0'/>" +
                "<COBERTURA36 COD_COB='4027' SUMA_ASEG='0'/>" +
                "<COBERTURA37 COD_COB='4028' SUMA_ASEG='0'/>" +
                "<COBERTURA38 COD_COB='4048' SUMA_ASEG='0'/>" +
                "<COBERTURA40 COD_COB='4068' SUMA_ASEG='2000000' COD_FRANQUICIA='0'/>" +
                "</COBERTURAS></COTIZACION></DATA></XML>"
            End If



            Dim shortDescripcion As String
            Try
                shortDescripcion = Replace(Mid(Descripcion, 1, InStr(Descripcion, " ")), "/", "")
            Catch ex As Exception
                shortDescripcion = Replace(Descripcion, "/", "")
            End Try

            Try
                Funciones.updateLogWSCot(strXMLDatosCotizacion, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Dim res As String = CotizaMapfre.WS_TW_ACotiza(strXMLDatosCotizacion, token).OuterXml


            Try
                Funciones.updateLogWSCot(res, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Dim objXmlDocumentRes As New XmlDocument
            Dim objXmlDocumentRes2 As New XmlDocument

            objXmlDocumentRes.LoadXml(res)

            'Num de cotizacion
            ObjData.Cotizacion.Resultado = "False"

            For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("param")
                Dim resultado = objXmlNode("result").InnerText
                If resultado.Equals("false") Then
                    respuesta = objXmlNode("error").InnerText
                    Throw New System.Exception("Error al realizar cotizacion")
                End If
            Next

            'DAtos generales
            For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("Totales")
                ObjData.Cotizacion.PrimaNeta = objXmlNode("total_prima_neta").InnerText
                ObjData.Cotizacion.Derechos = objXmlNode("total_derechos").InnerText
                ObjData.Cotizacion.Recargos = objXmlNode("total_recargos").InnerText
                ObjData.Cotizacion.PrimaTotal = objXmlNode("prima_total").InnerText
                ObjData.Cotizacion.Impuesto = objXmlNode("total_iva").InnerText
                ObjData.Cotizacion.Resultado = "True"

            Next
            'Primer Pago

            For Each objXmlNode2 In objXmlDocumentRes.GetElementsByTagName("FormaPago")
                For Each objXmlNode3 In objXmlNode2.GetElementsByTagName("Recibos")
                    ObjData.Cotizacion.PrimerPago = objXmlNode3("PrimaTotalPrimerRecibo").InnerText
                    ObjData.Cotizacion.PagosSubsecuentes = "0"
                    If formaPago = "4" Then
                        ObjData.Cotizacion.PagosSubsecuentes = ObjData.Cotizacion.PrimerPago
                    End If
                Next
            Next
            'Coberturas que mostraremos en el portal
            Dim coberNum = ""
            Dim nombreCobertura = ""
            Dim descripcionCobertura = ""
            Dim Coberturas As New Coberturas
            For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("cotizacion")
                colElementos3 = objXmlNode.GetElementsByTagName("coberturas")
                For Each objXmlNode2 In colElementos3
                    coberNum = objXmlNode2("COBERTURA").InnerText
                    descripcionCobertura = ""

                    Select Case coberNum
                        Case "DANOS MATERIALES"
                            nombreCobertura = "DAÑOS MATERIALES"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & "VALOR COMERCIAL-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.DanosMateriales = descripcionCobertura
                        Case "ROBO TOTAL"
                            nombreCobertura = "ROBO TOTAL"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & "VALOR COMERCIAL-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.RoboTotal = descripcionCobertura
                        Case "ASISTENCIA COMPLETA"
                            nombreCobertura = "ASISTENCIA VIAL"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & objXmlNode2("LIMMAXRESP").InnerText & "-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.AsitenciaCompleta = descripcionCobertura
                        Case "DEFENSA JURIDICA"
                            nombreCobertura = "GASTOS LEGALES"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & objXmlNode2("LIMMAXRESP").InnerText & "-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.DefensaJuridica = descripcionCobertura
                        Case "RC A TERCEROS EN SUS BIENES"
                            nombreCobertura = "RC BIENES"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & objXmlNode2("LIMMAXRESP").InnerText & "-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.RCBienes = descripcionCobertura
                        Case "RC A TERCEROS EN SUS PERSONAS"
                            nombreCobertura = "RC PERSONAS"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & objXmlNode2("LIMMAXRESP").InnerText & "-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.RCPersonas = descripcionCobertura
                        Case "RC CATASTROFICA POR MUERTE ACC"
                            nombreCobertura = "MUERTE DEL CONDUCTOR POR ACCIDENTE AUTOMOVILISTICO"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & objXmlNode2("LIMMAXRESP").InnerText & "-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.MuerteAccidental = descripcionCobertura
                        Case "GASTOS MEDICOS"
                            nombreCobertura = "GASTOS MÉDICOS"
                            descripcionCobertura = "-N" & nombreCobertura & "-S" & objXmlNode2("LIMMAXRESP").InnerText & "-D" & objXmlNode2("DEDUCIBLE").InnerText
                            Coberturas.GastosMedicosOcupantes = descripcionCobertura
                    End Select
                Next
            Next

            'Pagos subsecuentes
            For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("Recibo")
                If objXmlNode("Serie").InnerText = "1/12" Then
                    ObjData.Cotizacion.PrimerPago = objXmlNode("PrimaTotal").InnerText
                    Exit For
                Else
                    ObjData.Cotizacion.PagosSubsecuentes = objXmlNode("PrimaTotal").InnerText
                    Exit For
                End If
            Next
            ObjData.Coberturas.Add(Coberturas)
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [" + respuesta + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "False"
        End Try

        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function EmisionMAPFRE(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim paquete As String = ObjData.Paquete
        Dim formaPago As String = ObjData.PeriodicidadDePago
        Dim Descripcion As String = ObjData.Vehiculo.Descripcion
        Dim modelo As String = ObjData.Vehiculo.Modelo
        Dim marca As String = ObjData.Vehiculo.Marca
        Dim CPostal As String = ObjData.Cliente.Direccion.CodPostal
        Dim clave As String = ObjData.Vehiculo.Clave
        Dim Edad As String = ObjData.Cliente.Edad
        Dim Genero As String = ObjData.Cliente.Genero
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        Dim CotizaMapfre As New MAPFREService.CatalogosAutosSoapClient
        Dim token = "94403460-fb76-4e9d-b1d4-244fa7c52c55"

        Dim User As String = "GMAC0106"
        Dim Password As String = "MXMIFNLKYZAQH5PRPHRLLA=="
        Dim Negocio As String = "CANON"
        Dim PolizaGrupo As String = "4ECOMMERCE001"
        Dim Contrato As String = "44763"
        Dim Sector As String = "4"
        Dim IniVig As Date = Now.Date
        Dim IniVigTxt = IniVig.ToString("dd/MM/yyyy")
        Dim Finvig As Date
        Finvig = IniVig.AddYears(1)
        Dim FinvigTxt = Finvig.ToString("dd/MM/yyyy")
        Dim CuadroComision As String = "1"
        Dim tipoGestor = ObjData.Pago.MedioPago
        If tipoGestor = "DEBITO" Then
            tipoGestor = "BA"
        ElseIf tipoGestor = "CREDITO" Then
            tipoGestor = "TA"
        Else
            tipoGestor = "AG"
        End If
        Dim CodigoNivel3 As String = "0"
        Dim TipoDocumento As String = "RFC"
        Dim CodigoDocumento As String = "PRUEBA"

        Dim CodigoEstado As String = ""
        Dim CodigoPoblacion As String = ""
        Dim PorcentajeAgente As String = "100"
        Dim paquete2 As String = "40999"

        Dim CodMarca As String
        Dim CodModelo As String
        Dim CodTipoVehiculo As String
        Dim tipoVehic = GetTipoVehiculo(ObjData.Vehiculo.Clave)

        Dim descuentoCot As String

        Dim DatosDir = ConsultaMunicipios(ObjData.Cliente.Direccion.CodPostal)

        descuentoCot = 0
        CodigoEstado = DatosDir(0)
        CodigoPoblacion = DatosDir(1)

        If clave.Length = 8 And UCase(tipoVehic) = "PKP" Then
            CodMarca = Mid(clave, 1, 3)
            CodModelo = Mid(clave, 4, 3)
            CodTipoVehiculo = IIf(Mid(clave, 7, 2) = "01", Mid(Mid(clave, 7, 2), 2, 1), Mid(clave, 7, 2))
        ElseIf clave.Length = 8 Then
            CodMarca = Mid(clave, 1, 3)
            CodModelo = Mid(clave, 4, 3)
            CodTipoVehiculo = Mid(clave, 8, 2)
        ElseIf UCase(tipoVehic) = "PKP" Then
            CodMarca = Mid(clave, 1, 2)
            CodModelo = Mid(clave, 3, 3)
            CodTipoVehiculo = IIf(Mid(clave, 6, 2) = "01", Mid(Mid(clave, 6, 2), 2, 1), Mid(clave, 6, 2))
        Else
            clave = "0" & clave
            CodMarca = Mid(clave, 1, 2)
            CodModelo = Mid(clave, 3, 3)
            CodTipoVehiculo = Mid(clave, 7, 2)
        End If

        Dim ramo As String = "401"
        If CodTipoVehiculo = "12" Then
            ramo = "405"
        End If

        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            formaPago = "1"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            formaPago = "4"
        End If

        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = 1
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = 0
        End If

        Dim banco = bancosMAPFRE(ObjData.Pago.Banco)


        Dim tipotarjeta = ""

        Dim fNacimiento = ObjData.Cliente.FechaNacimiento.Replace("/", "-")

        If ObjData.Pago.Carrier = Pago.CatCarrier.VISA Then
            tipotarjeta = 1
        ElseIf ObjData.Pago.Carrier = Pago.CatCarrier.MASTERCARD Then
            tipotarjeta = 2
        ElseIf ObjData.Pago.Carrier = Pago.CatCarrier.AMEX Then
            tipotarjeta = 3
        End If
        Dim nombreTar As String
        Dim apellidoPTar As String
        Dim apellidoMTar As String
        Dim nombreMat = Split(ObjData.Pago.NombreTarjeta, " ")
        If nombreMat.Count > 3 Then
            nombreTar = nombreMat(0) + " " + nombreMat(1)
            apellidoPTar = nombreMat(2)
            apellidoMTar = nombreMat(3)
        Else
            nombreTar = nombreMat(0)
            apellidoPTar = nombreMat(1)
            apellidoMTar = nombreMat(2)
        End If

        Dim strXMLDatosEmision = ""

        'COD_CUADRO_COM='369' pruebas
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            strXMLDatosEmision = "<XML><SEGURIDAD><USER ID='" + User + "' PWD='" + Password + "' /></SEGURIDAD>" +
            "<DATA><POLIZA><DATOS_POLIZA ID_NEGOCIO='AHORRA_SEGUROS' NUM_POLIZA_GRUPO='4ECOMMERCE001' NUM_CONTRATO='47735' COD_SECTOR='4' COD_RAMO='" + ramo + "' FEC_EFEC_POLIZA='" + IniVigTxt + "' FEC_VCTO_POLIZA='" + FinvigTxt + "' COD_FRACC_PAGO='" + formaPago + "' COD_CUADRO_COM='415' COD_AGT='66064' COD_USR='GMAC0106' COD_NIVEL3_CAPTURA='0' TIP_DOCUM='RFC' COD_DOCUM='PRUEBA' COD_ESTADO='" +
                CodigoEstado + "' COD_PROV='" + CodigoPoblacion + "' PCT_AGT='100'" +
            " FEC_CORTE='" + Now.ToString("dd/MM/yyyy") + "' COD_ENTIDAD='" + banco + "' COD_OFICINA='" + banco + "' NOM_TERCERO='" + nombreTar + "' APE1_TERCERO='" + apellidoPTar + "' APE2_TERCERO='" + apellidoMTar + "' EMAIL='" + ObjData.Cliente.Email + "' COD_GESTOR='" + banco + banco + "' TIP_GESTOR='" + tipoGestor + "' NUM_TARJETA='" + ObjData.Pago.NoTarjeta + "' TIP_TARJETA='" + tipotarjeta + "' FEC_VCTO_TARJETA='01/" + ObjData.Pago.MesExp + "/" + ObjData.Pago.AnioExp + "' COD_TARJETA='" + ObjData.Pago.CodigoSeguridad + "'/>" +
            "<DATOS_VARIABLES NUM_RIESGO='1' TIP_NIVEL='3'><CAMPO COD_CAMPO='COD_MATERIA' VAL_CAMPO='1'/></DATOS_VARIABLES>" +
            "<DATOS_VARIABLES44 TIP_NIVEL='2' NUM_RIESGO='1'>" +
                "<CAMPO9 VAL_CAMPO='" + ObjData.Vehiculo.NoSerie + "' COD_CAMPO='NUM_SERIE'/>" +
                "<CAMPO11 VAL_CAMPO='40999' COD_CAMPO='COD_MODALIDAD'/>" +
                "<CAMPO12 VAL_CAMPO='" + ObjData.Vehiculo.Modelo + "' COD_CAMPO='ANIO_FABRICA'/>" +
                "<CAMPO13 VAL_CAMPO='" + CodMarca + "' COD_CAMPO='COD_MARCA'/>" +
                "<CAMPO14 VAL_CAMPO='" + CodModelo + "' COD_CAMPO='COD_MODELO'/>" +
                "<CAMPO15 VAL_CAMPO='" + CodTipoVehiculo + "' COD_CAMPO='COD_TIP_VEHI'/>" +
                "<CAMPO16 VAL_CAMPO='401' COD_CAMPO='COD_USO_VEHI'/>" +
                "<CAMPO17 VAL_CAMPO='N' COD_CAMPO='MCA_FACTURA'/>" +
                "<CAMPO18 VAL_CAMPO='N' COD_CAMPO='MCA_ACTUAL'/>" +
                "<CAMPO19 VAL_CAMPO='S' COD_CAMPO='MCA_COMERCIAL'/>" +
                "<CAMPO23 VAL_CAMPO='" + ObjData.Vehiculo.NoPlacas + "' COD_CAMPO='NUM_MATRICULA'/>" +
                "<CAMPO24 VAL_CAMPO='" + ObjData.Vehiculo.NoMotor + "' COD_CAMPO='NUM_MOTOR'/>" +
                "<CAMPO26 VAL_CAMPO='5' COD_CAMPO='NUM_PASAJEROS'/>" +
            "</DATOS_VARIABLES44>" +
            "<DATOS_VARIABLES49 TIP_NIVEL='1' NUM_RIESGO='0'>" +
                "<CAMPO44 VAL_CAMPO='999' COD_CAMPO='COD_BONI_RECA'/>" +
                "<CAMPO46 VAL_CAMPO='0' COD_CAMPO='PCT_COD_REC_ESP'/>" +
                "<CAMPO47 VAL_CAMPO='100' COD_CAMPO='PCT_CESION_COM_AGT'/>" +
                "<CAMPO48 VAL_CAMPO='1' COD_CAMPO='MEDIO_CAPTACION'/>" +
            "</DATOS_VARIABLES49>" +
            "<COBERTURAS>" +
             "<COBERTURA COD_COB='4000' SUMA_ASEG='C' COD_FRANQUICIA='5'/>" +
            "<COBERTURA51 COD_COB='4001' SUMA_ASEG='C' COD_FRANQUICIA='10'/>" +
            "<COBERTURA52 COD_COB='4010' SUMA_ASEG='1500000'/>" +
            "<COBERTURA53 COD_COB='4011' SUMA_ASEG='1500000'/>" +
            "<COBERTURA54 COD_COB='4006' SUMA_ASEG='200000'/>" +
            "<COBERTURA55 COD_COB='4003' SUMA_ASEG='1'/>" +
            "<COBERTURA56 COD_COB='4004' SUMA_ASEG='1'/>" +
            "<COBERTURA57 COD_COB='4012' SUMA_ASEG='0'/>" +
            "<COBERTURA58 COD_COB='4015' SUMA_ASEG='0'/>" +
            "<COBERTURA59 COD_COB='4013' SUMA_ASEG='100000'/>" +
            "<COBERTURA60 COD_COB='4022' SUMA_ASEG='0'/>" +
            "<COBERTURA61 COD_COB='4023' SUMA_ASEG='0'/>" +
            "<COBERTURA62 COD_COB='4024' SUMA_ASEG='0'/>" +
            "<COBERTURA63 COD_COB='4027' SUMA_ASEG='0'/>" +
            "<COBERTURA64 COD_COB='4028' SUMA_ASEG='0'/>" +
            "<COBERTURA65 COD_COB='4048' SUMA_ASEG='0'/>" +
            "<COBERTURA67 COD_COB='4068' SUMA_ASEG='0'/>" +
            "</COBERTURAS>" +
            "<TERCEROS>" +
                "<CONTRATANTE COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<CONDUCTOR COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<BENEFICIARIO COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<YO COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
            "</TERCEROS>" +
        "</POLIZA>" +
    "</DATA>" +
"</XML>"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            strXMLDatosEmision = "<XML><SEGURIDAD><USER ID='" + User + "' PWD='" + Password + "' /></SEGURIDAD>" +
            "<DATA><POLIZA><DATOS_POLIZA ID_NEGOCIO='AHORRA_SEGUROS' NUM_POLIZA_GRUPO='4ECOMMERCE001' NUM_CONTRATO='47735' COD_SECTOR='4' COD_RAMO='" + ramo + "' FEC_EFEC_POLIZA='" + IniVigTxt + "' FEC_VCTO_POLIZA='" + FinvigTxt + "' COD_FRACC_PAGO='" + formaPago + "' COD_CUADRO_COM='415' COD_AGT='66064' COD_USR='GMAC0106' COD_NIVEL3_CAPTURA='0' TIP_DOCUM='RFC' COD_DOCUM='PRUEBA' COD_ESTADO='" +
                CodigoEstado + "' COD_PROV='" + CodigoPoblacion + "' PCT_AGT='100' COD_GESTOR='66064' TIP_GESTOR='" + tipoGestor + "'" +
            " FEC_CORTE='" + Now.ToString("dd/MM/yyyy") + "' COD_ENTIDAD='" + banco + "' COD_OFICINA='" + banco + "' NOM_TERCERO='" + nombreTar + "' APE1_TERCERO='" + apellidoPTar + "' APE2_TERCERO='' EMAIL='" + ObjData.Cliente.Email + "' NUM_TARJETA='" + ObjData.Pago.NoTarjeta + "' TIP_TARJETA='" + tipotarjeta + "' FEC_VCTO_TARJETA='01/" + ObjData.Pago.MesExp + "/" + ObjData.Pago.AnioExp + "' COD_TARJETA='" + ObjData.Pago.CodigoSeguridad + "'/>" +
            "<DATOS_VARIABLES NUM_RIESGO='1' TIP_NIVEL='3'><CAMPO COD_CAMPO='COD_MATERIA' VAL_CAMPO='1'/></DATOS_VARIABLES>" +
            "<DATOS_VARIABLES44 TIP_NIVEL='2' NUM_RIESGO='1'>" +
                "<CAMPO9 VAL_CAMPO='" + ObjData.Vehiculo.NoSerie + "' COD_CAMPO='NUM_SERIE'/>" +
                "<CAMPO11 VAL_CAMPO='40999' COD_CAMPO='COD_MODALIDAD'/>" +
                "<CAMPO12 VAL_CAMPO='" + ObjData.Vehiculo.Modelo + "' COD_CAMPO='ANIO_FABRICA'/>" +
                "<CAMPO13 VAL_CAMPO='" + CodMarca + "' COD_CAMPO='COD_MARCA'/>" +
                "<CAMPO14 VAL_CAMPO='" + CodModelo + "' COD_CAMPO='COD_MODELO'/>" +
                "<CAMPO15 VAL_CAMPO='" + CodTipoVehiculo + "' COD_CAMPO='COD_TIP_VEHI'/>" +
                "<CAMPO16 VAL_CAMPO='401' COD_CAMPO='COD_USO_VEHI'/>" +
                "<CAMPO17 VAL_CAMPO='N' COD_CAMPO='MCA_FACTURA'/>" +
                "<CAMPO18 VAL_CAMPO='N' COD_CAMPO='MCA_ACTUAL'/>" +
                "<CAMPO19 VAL_CAMPO='S' COD_CAMPO='MCA_COMERCIAL'/>" +
                "<CAMPO23 VAL_CAMPO='" + ObjData.Vehiculo.NoPlacas + "' COD_CAMPO='NUM_MATRICULA'/>" +
                "<CAMPO24 VAL_CAMPO='" + ObjData.Vehiculo.NoMotor + "' COD_CAMPO='NUM_MOTOR'/>" +
                "<CAMPO26 VAL_CAMPO='5' COD_CAMPO='NUM_PASAJEROS'/>" +
            "</DATOS_VARIABLES44>" +
            "<DATOS_VARIABLES49 TIP_NIVEL='1' NUM_RIESGO='0'>" +
                "<CAMPO44 VAL_CAMPO='999' COD_CAMPO='COD_BONI_RECA'/>" +
                "<CAMPO46 VAL_CAMPO='0' COD_CAMPO='PCT_COD_REC_ESP'/>" +
                "<CAMPO47 VAL_CAMPO='100' COD_CAMPO='PCT_CESION_COM_AGT'/>" +
                "<CAMPO48 VAL_CAMPO='1' COD_CAMPO='MEDIO_CAPTACION'/>" +
            "</DATOS_VARIABLES49>" +
            "<COBERTURAS>" +
            "<COBERTURA COD_COB='4000' SUMA_ASEG='0' COD_FRANQUICIA='5'/>" +
            "<COBERTURA51 COD_COB='4001' SUMA_ASEG='C' COD_FRANQUICIA='10'/>" +
            "<COBERTURA52 COD_COB='4010' SUMA_ASEG='1500000'/>" +
            "<COBERTURA53 COD_COB='4011' SUMA_ASEG='1500000'/>" +
            "<COBERTURA54 COD_COB='4006' SUMA_ASEG='100000'/>" +
            "<COBERTURA55 COD_COB='4003' SUMA_ASEG='1'/>" +
            "<COBERTURA56 COD_COB='4004' SUMA_ASEG='1'/>" +
            "<COBERTURA57 COD_COB='4012' SUMA_ASEG='0'/>" +
            "<COBERTURA58 COD_COB='4015' SUMA_ASEG='0'/>" +
            "<COBERTURA59 COD_COB='4013' SUMA_ASEG='100000'/>" +
            "<COBERTURA60 COD_COB='4022' SUMA_ASEG='0'/>" +
            "<COBERTURA61 COD_COB='4023' SUMA_ASEG='0'/>" +
            "<COBERTURA62 COD_COB='4024' SUMA_ASEG='0'/>" +
            "<COBERTURA63 COD_COB='4027' SUMA_ASEG='0'/>" +
            "<COBERTURA64 COD_COB='4028' SUMA_ASEG='0'/>" +
            "<COBERTURA65 COD_COB='4048' SUMA_ASEG='0'/>" +
            "<COBERTURA67 COD_COB='4068' SUMA_ASEG='0'/>" +
            "</COBERTURAS>" +
            "<TERCEROS>" +
                "<CONTRATANTE COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<CONDUCTOR COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<BENEFICIARIO COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<YO COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
            "</TERCEROS>" +
        "</POLIZA>" +
    "</DATA>" +
"</XML>"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            strXMLDatosEmision = "<XML><SEGURIDAD><USER ID='" + User + "' PWD='" + Password + "' /></SEGURIDAD>" +
            "<DATA><POLIZA><DATOS_POLIZA ID_NEGOCIO='AHORRA_SEGUROS' NUM_POLIZA_GRUPO='4ECOMMERCE001' NUM_CONTRATO='47735' COD_SECTOR='4' COD_RAMO='" + ramo + "' FEC_EFEC_POLIZA='" + IniVigTxt + "' FEC_VCTO_POLIZA='" + FinvigTxt + "' COD_FRACC_PAGO='" + formaPago + "' COD_CUADRO_COM='415' COD_AGT='66064' COD_USR='GMAC0106' COD_NIVEL3_CAPTURA='0' TIP_DOCUM='RFC' COD_DOCUM='PRUEBA' COD_ESTADO='" +
                CodigoEstado + "' COD_PROV='" + CodigoPoblacion + "' PCT_AGT='100' COD_GESTOR='66064' TIP_GESTOR='" + tipoGestor + "'" +
            " FEC_CORTE='" + Now.ToString("dd/MM/yyyy") + "' COD_ENTIDAD='" + banco + "' COD_OFICINA='" + banco + "' NOM_TERCERO='" + nombreTar + "' APE1_TERCERO='" + apellidoPTar + "' APE2_TERCERO='' EMAIL='" + ObjData.Cliente.Email + "' NUM_TARJETA='" + ObjData.Pago.NoTarjeta + "' TIP_TARJETA='" + tipotarjeta + "' FEC_VCTO_TARJETA='01/" + ObjData.Pago.MesExp + "/" + ObjData.Pago.AnioExp + "' COD_TARJETA='" + ObjData.Pago.CodigoSeguridad + "'/>" +
            "<DATOS_VARIABLES NUM_RIESGO='1' TIP_NIVEL='3'><CAMPO COD_CAMPO='COD_MATERIA' VAL_CAMPO='1'/></DATOS_VARIABLES>" +
            "<DATOS_VARIABLES44 TIP_NIVEL='2' NUM_RIESGO='1'>" +
                "<CAMPO9 VAL_CAMPO='" + ObjData.Vehiculo.NoSerie + "' COD_CAMPO='NUM_SERIE'/>" +
                "<CAMPO11 VAL_CAMPO='40999' COD_CAMPO='COD_MODALIDAD'/>" +
                "<CAMPO12 VAL_CAMPO='" + ObjData.Vehiculo.Modelo + "' COD_CAMPO='ANIO_FABRICA'/>" +
                "<CAMPO13 VAL_CAMPO='" + CodMarca + "' COD_CAMPO='COD_MARCA'/>" +
                "<CAMPO14 VAL_CAMPO='" + CodModelo + "' COD_CAMPO='COD_MODELO'/>" +
                "<CAMPO15 VAL_CAMPO='" + CodTipoVehiculo + "' COD_CAMPO='COD_TIP_VEHI'/>" +
                "<CAMPO16 VAL_CAMPO='401' COD_CAMPO='COD_USO_VEHI'/>" +
                "<CAMPO17 VAL_CAMPO='N' COD_CAMPO='MCA_FACTURA'/>" +
                "<CAMPO18 VAL_CAMPO='N' COD_CAMPO='MCA_ACTUAL'/>" +
                "<CAMPO19 VAL_CAMPO='S' COD_CAMPO='MCA_COMERCIAL'/>" +
                "<CAMPO23 VAL_CAMPO='" + ObjData.Vehiculo.NoPlacas + "' COD_CAMPO='NUM_MATRICULA'/>" +
                "<CAMPO24 VAL_CAMPO='" + ObjData.Vehiculo.NoMotor + "' COD_CAMPO='NUM_MOTOR'/>" +
                "<CAMPO26 VAL_CAMPO='5' COD_CAMPO='NUM_PASAJEROS'/>" +
            "</DATOS_VARIABLES44>" +
            "<DATOS_VARIABLES49 TIP_NIVEL='1' NUM_RIESGO='0'>" +
                "<CAMPO44 VAL_CAMPO='999' COD_CAMPO='COD_BONI_RECA'/>" +
                "<CAMPO46 VAL_CAMPO='0' COD_CAMPO='PCT_COD_REC_ESP'/>" +
                "<CAMPO47 VAL_CAMPO='100' COD_CAMPO='PCT_CESION_COM_AGT'/>" +
                "<CAMPO48 VAL_CAMPO='1' COD_CAMPO='MEDIO_CAPTACION'/>" +
            "</DATOS_VARIABLES49>" +
            "<COBERTURAS>" +
             "<COBERTURA COD_COB='4000' SUMA_ASEG='0' COD_FRANQUICIA='5'/>" +
            "<COBERTURA24 COD_COB='4001' SUMA_ASEG='0' COD_FRANQUICIA='10'/>" +
            "<COBERTURA52 COD_COB='4010' SUMA_ASEG='1500000'/>" +
            "<COBERTURA53 COD_COB='4011' SUMA_ASEG='1500000'/>" +
            "<COBERTURA54 COD_COB='4006' SUMA_ASEG='100000'/>" +
            "<COBERTURA56 COD_COB='4004' SUMA_ASEG='1'/>" +
            "<COBERTURA57 COD_COB='4012' SUMA_ASEG='0'/>" +
            "<COBERTURA58 COD_COB='4015' SUMA_ASEG='0'/>" +
            "<COBERTURA59 COD_COB='4013' SUMA_ASEG='0'/>" +
            "<COBERTURA60 COD_COB='4022' SUMA_ASEG='0'/>" +
            "<COBERTURA61 COD_COB='4023' SUMA_ASEG='0'/>" +
            "<COBERTURA62 COD_COB='4024' SUMA_ASEG='0'/>" +
            "<COBERTURA63 COD_COB='4027' SUMA_ASEG='0'/>" +
            "<COBERTURA64 COD_COB='4028' SUMA_ASEG='0'/>" +
            "<COBERTURA65 COD_COB='4048' SUMA_ASEG='0'/>" +
            "<COBERTURA67 COD_COB='4068' SUMA_ASEG='0'/>" +
            "</COBERTURAS>" +
            "<TERCEROS>" +
                "<CONTRATANTE COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<CONDUCTOR COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<BENEFICIARIO COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
                "<YO COD_PROV='" + CodigoPoblacion + "' COD_ESTADO='" + CodigoEstado + "' COD_DOCUM='" + ObjData.Cliente.RFC + "' MODIFICADO='N' TLF_MOVIL='" + ObjData.Cliente.Telefono + "' EMAIL='" + ObjData.Cliente.Email + "' TLF_NUMERO='" + ObjData.Cliente.Telefono + "' COD_POSTAL='" + ObjData.Cliente.Direccion.CodPostal + "' FEC_NACIMIENTO='" + fNacimiento + "' MCA_SEXO='" + Genero + "' COD_LOCALIDAD='" + CodigoPoblacion + "' NOM_DOMICILIO3='" + ObjData.Cliente.Direccion.Colonia + "' NOM_DOMICILIO1='" + ObjData.Cliente.Direccion.Calle + "' NOM_TERCERO='" + ObjData.Cliente.Nombre + "' APE2_TERCERO='" + ObjData.Cliente.ApellidoMat + "' APE1_TERCERO='" + ObjData.Cliente.ApellidoPat + "' MCA_FISICO='S'/>" +
            "</TERCEROS>" +
        "</POLIZA>" +
    "</DATA>" +
"</XML>"
        End If



        Dim shortDescripcion As String
        Try
            shortDescripcion = Replace(Mid(Descripcion, 1, InStr(Descripcion, " ")), "/", "")
        Catch ex As Exception
            shortDescripcion = Replace(Descripcion, "/", "")
        End Try

        Try
            Funciones.updateLogWS(strXMLDatosEmision, idLogWS, "RequestWS")
        Catch ex As Exception
        End Try

        Try
            FuncionesGeneral.xmlWS(strXMLDatosEmision, ObjData, "Emision", "EmisionEnvio")
        Catch ex As Exception
        End Try
        Dim res As String = CotizaMapfre.WS_TW_AEmite(strXMLDatosEmision, token).OuterXml
        Try
            FuncionesGeneral.xmlWS(res, ObjData, "Emision", "EmisionSalida")
        Catch ex As Exception

        End Try

        Try
            Funciones.updateLogWS(res, idLogWS, "ResponseWS")
        Catch ex As Exception
        End Try

        Dim objXmlDocumentRes As New XmlDocument
        objXmlDocumentRes.LoadXml(res)
        Dim objXmlNode As XmlNode
        Try
            For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("PrimasPoliza")
                ObjData.Emision.Poliza = objXmlNode("Poliza").InnerText
                ObjData.Emision.PrimaNeta = objXmlNode("PrimaNeta").InnerText
                ObjData.Emision.Derechos = objXmlNode("Derecho").InnerText
                ObjData.Emision.Recargos = objXmlNode("Recargo").InnerText
                ObjData.Emision.PrimaTotal = objXmlNode("PrimaTotal").InnerText
            Next

            If ObjData.Emision.Poliza <> Nothing And ObjData.Emision.Poliza <> "" Then
                ObjData.Emision.Resultado = "True"
            Else
                If res.Contains("<poliza error=""true""") Then
                    Dim objXmlNodeList As XmlNodeList = objXmlDocumentRes.GetElementsByTagName("data")
                    ObjData.CodigoError = objXmlNodeList(0).InnerText
                ElseIf res.Contains("<result>false</result>") Then
                    For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("param")
                        ObjData.CodigoError = objXmlNode("error").InnerText
                    Next
                End If
                ObjData.Emision.Resultado = "False"
            End If

            If res.Contains("<EstatusID>PENDIENTE</EstatusID>") Then
                ObjData.Emision.Resultado = "False"
                ObjData.CodigoError = "El estatus de la emision se encuentra como PENDIENTE"
            End If

        Catch ex As Exception
            ObjData.CodigoError = ex.ToString
            ObjData.Emision.Resultado = "Flase"
        End Try


        If ObjData.Emision.Poliza <> Nothing Or ObjData.Emision.Poliza <> "" Then
            ObjData.Emision.Documento = "https://negocios.mapfre.com.mx/vip/emision/PolizaALFA.aspx?poli=" & ObjData.Emision.Poliza & "&strEndoso=0&strNegocio=WS_GENERAL&NMI=1"

        Else
            ObjData.Emision.Documento = ""
        End If
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function GetTipoVehiculo(ByVal Clave As String)
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "MAPFRE", Clave)
        Dim mapaVehiculo = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim tipo = mapaVehiculo.Item("vehicle_type")
        Return tipo
    End Function
    Private Shared Function ConsultaMunicipios(ByVal CPostal As String) As String()
        Dim objConnectionCPostal As SqlConnection
        Dim objCommandCPostal As SqlCommand
        Dim strSQLQueryCPostal As String
        Dim Lista(2) As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQueryCPostal = "Select * from `Ali_R_CatCPostalMapfre` where CPostal = '" & CPostal & "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Lista(0) = reader("IdEdo")
                    Lista(1) = reader("Idmun")
                End While
                Connection.Close()
            Catch ex As Exception

            End Try
        End Using
        Return Lista
    End Function

    Private Shared Function bancosMAPFRE(ByVal banco As String) As String
        Dim abr As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        Dim sql As String = "SELECT idBanco From `Ali_R_CatBancos` where Abreviacion=" + banco + ""
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(sql, Connection)
            Connection.Open()
            Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
            While (reader.Read())
                abr = reader("idBanco")
            End While
            Connection.Close()
        End Using
        If abr.Length = 1 Then
            banco = "000" & abr
        ElseIf abr.Length = 2 Then
            banco = "00" & abr
        ElseIf abr.Length = 3 Then
            banco = "0" & abr
        End If
        Return banco
    End Function
End Class
