﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.Xml
Imports System.IO
Imports System.Xml.Serialization
Imports AXA_Bancs_QA_CS
Imports MySql.Data.MySqlClient

Partial Class WsAXA
    Inherits System.Web.UI.Page
    Public Shared AGENTE As String = ""
    Public Shared ObjData As Seguro = New Seguro
    Public Shared Function AXACotizacion(ByVal ObjData2 As Seguro, ByVal idLogWSCot As String) As String
        ObjData = ObjData2
        Dim respuestaerror As String
        Dim respuestaWS As String = ""
        If ObjData.Vehiculo.Clave.Length < 9 Then
            respuestaerror = "La clave de vehículo no se encuentra en el catálogo"
        Else
            Dim Plan = ""
            If UCase(ObjData.Paquete) = "AMPLIA" Then
                Plan = "AM"
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                Plan = "ES"
            ElseIf UCase(ObjData.Paquete) = "RC" Then
                Plan = "BA"
            End If
            Dim inputData As issuanceRequest
            Try
                inputData = getExpressData(ObjData)
            Catch ex As Exception
                ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
                Dim serializer2 As New JavaScriptSerializer
                Return serializer2.Serialize(ObjData)
            End Try

            ' Se crea un vector con 1 posición donde se agrega la clase quickQuoteInfo
            inputData.issuance.quickQuoteInfo = New quickQuoteInfo(0) {}
            'Se cargan los valores PaymentFrecuency y Package en la posicion 0
            inputData.issuance.quickQuoteInfo(0) = New quickQuoteInfo()
            inputData.issuance.quickQuoteInfo(0).paymentFrequency = ObjData.PeriodicidadDePago
            inputData.issuance.quickQuoteInfo(0).package = Plan

            'Declaramos el objeto ClienteWS y cargamos los valores USERNAME y PASSWORD
            Dim clienteWS As integratorPortClient = preparaEndPoint()

            'Declaramos el objeto request y cargamos los valores de inputData
            Dim request As getIssuanceDetailsRequest = New getIssuanceDetailsRequest()
            request.issuanceRequest = inputData
            Dim lg As Log = New Log()
            Try
                Dim x As New XmlSerializer(request.GetType)
                Dim xml As New StringWriter
                x.Serialize(xml, request)
                Dim errorws2 As String = xml.ToString
                lg.UpdateLogRequest(errorws2, idLogWSCot)
                'Funciones.updateLogWSCot(errorws2, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception

            End Try
            Try
                '    'Declaramos el objero Respuesta con los datos de la cotización
                Dim respuesta As getIssuanceDetailsResponse = clienteWS.getIssuanceDetails(request)
                Try
                    Dim x2 As New XmlSerializer(respuesta.GetType)
                    Dim xml2 As New StringWriter
                    x2.Serialize(xml2, respuesta)
                    Dim errorws4 As String = xml2.ToString
                    lg.UpdateLogResponse(errorws4, idLogWSCot)
                    'Funciones.updateLogWSCot(errorws4, idLogWSCot, "ResponseWS", "FechaFin")
                Catch ex As Exception

                End Try
                '    'Si al recibir respuesta del WS tenemos que esta satisfactorio o sin errores, continuamos con el proceso de llenado de nuestro Obj
                If respuesta.issuanceResponse.errorDetails(0).errorMessage = "SUCCESS" Or respuesta.issuanceResponse.errorDetails(0).errorMessage = "" Then
                    ObjData.Cotizacion.PrimaNeta = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.netPremium
                    ObjData.Cotizacion.Impuesto = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.tax
                    ObjData.Cotizacion.PrimaTotal = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.totalPremium
                    ObjData.Cotizacion.Derechos = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.policyFee
                    ObjData.Cotizacion.Recargos = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.installmentSurcharge
                    Dim _calculaPagoMensual(1) As Integer 'llama a funcion para calcular primer pago y pago subsecuente SI EL PAGO ES MENSUAL
                    If ObjData.PeriodicidadDePago = 4 Then
                        Dim PrimaNetaIva As Integer = ObjData.Cotizacion.PrimaNeta * 1.16
                        Dim Recargo As Integer = ObjData.Cotizacion.Recargos * 1.16
                        Dim Derechos As Integer = ObjData.Cotizacion.Derechos * 1.16
                        ObjData.Cotizacion.PagosSubsecuentes = (PrimaNetaIva + Recargo) / 12
                        ObjData.Cotizacion.PrimerPago = ObjData.Cotizacion.PagosSubsecuentes + Derechos
                    End If
                    ObjData.Cotizacion.Resultado = "True"
                Else
                    ObjData.Cotizacion.Resultado = "False"

                End If

            Catch ex As System.Exception
                ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
                ObjData.Cotizacion.Resultado = "False"
            End Try
            Dim serializer As New JavaScriptSerializer
            Dim jsonResponse As String = serializer.Serialize(ObjData)
            lg.UpdateLogJsonResponse(jsonResponse, idLogWSCot)
            Return serializer.Serialize(ObjData)
        End If
        Return respuestaerror
    End Function

    Private Shared Function getExpressData(ByVal ObjData As Seguro) As issuanceRequest
        AGENTE = "AGT00607034"
        Dim claveVersion As Integer = 0
        Dim LineCod As Integer = 0
        Dim IdMarca As String = ""
        Dim Genero = ""
        Dim Plan = ""
        If ObjData.Cliente.Genero = "MASCULINO" Then
            Genero = "M"
        Else
            Genero = "F"
        End If
        Dim Municipios As String()
        Try
            Municipios = ConsultaMunicipiosQualitas(ObjData.Cliente.Direccion.CodPostal)
        Catch ex As Exception
            Throw New System.Exception("Error al realizar consulta de municipios")
        End Try
        Dim Colonia As Integer = CInt(Municipios(3))
        Dim Ciudad As Integer = CInt(Municipios(2))
        Dim Estado As Integer = CInt(Municipios(0))
        Dim CodigoPostal = CInt(ObjData.Cliente.Direccion.CodPostal)
        claveVersion = CInt(Mid(ObjData.Vehiculo.Clave, 5, 2))
        LineCod = CInt(Mid(ObjData.Vehiculo.Clave, 8, 2))
        IdMarca = Mid(ObjData.Vehiculo.Clave, 1, 2)
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            Plan = "AM"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            Plan = "ES"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            Plan = "BA"
        End If



        Dim FormaPago As String = ObjData.PeriodicidadDePago

        Dim dtHoy As DateTime = Date.Now
        Dim dtUnAnio As DateTime = dtHoy.AddYears(1)
        'Declaro de inputData como un nuevo issuanceRequest (Peticion de Emision)
        Dim inputData As issuanceRequest = New issuanceRequest()
        'objeto issuance 
        inputData.issuance = New issuance()
        inputData.issuance.productCode = productCode.Item1002
        inputData.issuance.typeOperation = issuanceTypeOperation.ExpressQuote '----------------------------Tipo de operacion Enum (Cotizacion Expres, Cotizacion, Issuance, Recotizacion)

        'objeto issuance.issuePolicyInfo 
        inputData.issuance.issuePolicyInfo = New issuePolicyInfo()
        'objeto issuance.issuePolicyInfo.issuancePolicyInfo 
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo = New policyInfo()
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyStartDate = dtHoy     '---------------------------------Fecha Inicio Poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyStartDateSpecified = True '-----------------------------Fecha especificada para iniciar poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyExpiryDate = dtUnAnio '---------------------------------Fecha de expiracion Póliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyExpiryDateSpecified = True '----------------------------Fecha especificada para Expirar poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.currency = currency.MXN '---------------------Moneda Enum (MXN o USD)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.currencySpecified = True '------------------------------------Confirmacion de Moneda especifica
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyDurationUnit = policyDurationUnit.Days 'Duracion poliza Enum (Dias, Mes, Año)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyDurationUnitSpecified = True '--------------------------Confirmacion de duracion poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.campaignCode = {"TRAD012013"}
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.netPremium = 0
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.netPremiumSpecified = True


        'inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentMode = paymentMode.MasterCard
        'inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentModeSpecified = True


        'Nuevo Objeto issuance.issuePolicyInfo.issuanceRiskInfo Arreglo (posicion 0)
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo = New riskInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0) = New riskInfo()
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).subBranch = "A"  '-------------------------------------------subRamo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).vehicleType = "RESIDENTES"  '-------------------------------Tipo de Vehiculo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).vehicleCode = ObjData.Vehiculo.Clave    '-------------------------------Codigo de vehicular
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).description = ObjData.Vehiculo.Descripcion '---------------------------Descripcion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).brand = ObjData.Vehiculo.Marca  '-------------------------------------------Marca
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).versionCode = claveVersion    '---------------------------------------Codigo de Version
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).model = ObjData.Vehiculo.Modelo   '-------------------------------------------Modelo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).lineCode = LineCod  '-------------------------------------------Codigo de linea
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).use = "N"    '-----------------------------------------------Uso
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).frequencyOfUse = "NA" '--------------------------------------Frecuencia de Uso
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).service = "PR"   '-------------------------------------------Servicio
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).kmsrecorded = 0  '-------------------------------------------Km recorridos
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).kmsrecordedSpecified = True  '-------------------------------Confirmacion de km recorridos
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).antiTheftDevice = "NONE" '-----------------------------------Dispositivo antirobo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).trailerUsed = "false"    '-----------------------------------Uso de remolque
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).smallTrailerUsed = False '-----------------------------------uso de remolque pequeño
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).smallTrailerUsedSpecified = True '---------------------------confirmacion de uso de remolque
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).brandId = IdMarca '---------------------------------------------ID de Marca
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).deductibleType = deductibleType.variable '---Tipo de deducieble Enum (Fijo o Variable)
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).deductibleTypeSpecified = True   '---------------------------Confirmacion de tipo de deducible

        Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
        'Nuevo objeto issuance.issuePolicyInfo.issuanceCustomerInfo
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo = New partyInfo(1) {}
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0) = New partyInfo()
        'inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyCode = ""   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyRole = "CONTRATO"   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyType = "I"  '---------------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).name = "Pedro" '-----------------------------------------Nombre Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).fatherLastName = "Perez"    '---------------------------Apellido Paterno 
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).motherLastName = "Gomez"    '---------------------------Apellido Materno
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).gender = Genero '-------------------------------------------Genero
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).dateofBirth = CDate(fechaNacimiento) '-------------------------------Fecha Nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).dateofBirthSpecified = True '----------------------------Confirmacion de fecha de nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).taxId = "BASC881119UA7"  '-------------------------------RFC
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).isAlsoPolicyHolder = "Y" '-------------------------------Es Titular de la poliza

        'Nuevo objeto para conductor principal
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1) = New partyInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).partyRole = "CONDUCTOR"   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).partyType = "I"  '---------------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).name = "Pedro" '-----------------------------------------Nombre Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).fatherLastName = "Perez"    '---------------------------Apellido Paterno 
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).motherLastName = "Gomez"     '---------------------------Apellido Materno
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).gender = Genero '-------------------------------------------Genero
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).dateofBirth = CDate(fechaNacimiento) '-------------------------------Fecha Nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).dateofBirthSpecified = True '----------------------------Confirmacion de fecha de nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).taxId = "BASC881119UA7"  '-------------------------------RFC
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).mainDriver = "Y"    '----------------------------------Conductor principal                          'Actualizacion AXA

        'Nuevo objeto ContactInfo
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo = New contactInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0) = New contactInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).contactType = "Permanent" '---------------Tipo de Contacto
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).neighborhood = Colonia ' "732"  '-------------------Colonia
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).city = Ciudad ' "4"    '---------------------------Ciudad
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).state = Estado  '"9"   '---------------------------Estado
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).country = "MEXICO"    '-------------------Pais
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).postalCode = CodigoPostal   '-------------------Codigo Postal

        'nuevo Objeto IssuanceAgente Info
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo = New agentInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0) = New agentInfo()
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).agentCode = AGENTE  '--------------------------------------Usuario Agente
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).mainAgent = "Y" '------------------------------------------Agente Principal
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).participationPercentage = "100" '--------------------------participacion porcentual
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentage = "0" '----------------------------------Porcentaje de cesion
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentageSpecified = True   '----------------------Confirmacion de Porcentaje de cesion
        'Borrar este codigo
        'Si es Cotizacion Completa no tomar en cuenta
        inputData.issuance.packageCoverInfo = New packageCoverInfo() {}
        ReDim inputData.issuance.packageCoverInfo(0)
        inputData.issuance.packageCoverInfo(0) = New packageCoverInfo
        If FormaPago = 0 Then
            inputData.issuance.packageCoverInfo(0).paymentFrequency = paymentFrequency.Cash
        Else
            inputData.issuance.packageCoverInfo(0).paymentFrequency = paymentFrequency.Monthly
        End If
        inputData.issuance.packageCoverInfo(0).package = Plan   '----------------------------------------------------------Paquete  Checar!!!!!!!!!!!!!!!!!!!!
        inputData.issuance.packageCoverInfo(0).riskCoverDetails = getAPCovers(Plan) '------------------------------------------Detalle de Cobertura de Riesgos
        Return inputData
    End Function

    Private Shared Function getExpressData2(ByVal ObjData As Seguro) As issuanceRequest
        AGENTE = "AGT00607034"
        Dim claveVersion As Integer = 0
        Dim LineCod As Integer = 0
        Dim IdMarca As String = ""
        Dim Genero = ""
        Dim Plan = ""
        If ObjData.Cliente.Genero = "MASCULINO" Then
            Genero = "M"
        Else
            Genero = "F"
        End If
        Dim Municipios As String()
        Try
            Municipios = ConsultaMunicipiosQualitas(ObjData.Cliente.Direccion.CodPostal)
        Catch ex As Exception
            Throw New System.Exception("Error al realizar consulta de municipios")
        End Try
        Dim Colonia As Integer = CInt(Municipios(3))
        Dim Ciudad As Integer = CInt(Municipios(2))
        Dim Estado As Integer = CInt(Municipios(0))
        Dim CodigoPostal = CInt(ObjData.Cliente.Direccion.CodPostal)
        claveVersion = CInt(Mid(ObjData.Vehiculo.Clave, 5, 2))
        LineCod = CInt(Mid(ObjData.Vehiculo.Clave, 8, 2))
        IdMarca = Mid(ObjData.Vehiculo.Clave, 1, 2)
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            Plan = "AM"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            Plan = "ES"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            Plan = "BA"
        End If

        Dim paymentMode As String = ""

        If (ObjData.Pago.MedioPago).ToUpper = "DEBITO" Then
            paymentMode = AXA_Bancs_QA_CS.paymentMode.Cash
        Else
            paymentMode = AXA_Bancs_QA_CS.paymentMode.Cash
        End If

        Dim FormaPago As String = ObjData.PeriodicidadDePago

        Dim dtHoy As DateTime = Date.Now
        Dim dtUnAnio As DateTime = dtHoy.AddYears(1)
        'Declaro de inputData como un nuevo issuanceRequest (Peticion de Emision)
        Dim inputData As issuanceRequest = New issuanceRequest()
        'objeto issuance 
        inputData.issuance = New issuance()
        inputData.issuance.productCode = productCode.Item1002
        inputData.issuance.typeOperation = issuanceTypeOperation.Quotation '----------------------------Tipo de operacion Enum (Cotizacion Expres, Cotizacion, Issuance, Recotizacion)

        'objeto issuance.issuePolicyInfo 
        inputData.issuance.issuePolicyInfo = New issuePolicyInfo()
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo = New policyInfo()
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyStartDate = dtHoy     '---------------------------------Fecha Inicio Poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyStartDateSpecified = True '-----------------------------Fecha especificada para iniciar poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyExpiryDate = dtUnAnio '---------------------------------Fecha de expiracion Póliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyExpiryDateSpecified = True '----------------------------Fecha especificada para Expirar poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.currency = currency.MXN '---------------------Moneda Enum (MXN o USD)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.currencySpecified = True '------------------------------------Confirmacion de Moneda especifica
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyDurationUnit = policyDurationUnit.Days 'Duracion poliza Enum (Dias, Mes, Año)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyDurationUnitSpecified = True '--------------------------Confirmacion de duracion poliza
        'inputData.issuance.issuePolicyInfo.issuancePolicyInfo.campaignCode = {"TRAD012013"}
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.netPremium = 0
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.netPremiumSpecified = True

        'inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentMode = AXA_Bancs_QA_CS.paymentMode.MasterCard
        'inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentModeSpecified = True


        'Nuevo Objeto issuance.issuePolicyInfo.issuanceRiskInfo Arreglo (posicion 0)
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo = New riskInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0) = New riskInfo()
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).subBranch = "A"  '-------------------------------------------subRamo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).vehicleType = "RESIDENTES"  '-------------------------------Tipo de Vehiculo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).vehicleCode = ObjData.Vehiculo.Clave    '-------------------------------Codigo de vehicular
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).description = ObjData.Vehiculo.Descripcion '---------------------------Descripcion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).brand = ObjData.Vehiculo.Marca  '-------------------------------------------Marca
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).versionCode = claveVersion    '---------------------------------------Codigo de Version

        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).model = ObjData.Vehiculo.Modelo   '-------------------------------------------Modelo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).lineCode = LineCod  '-------------------------------------------Codigo de linea
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).use = "N"    '-----------------------------------------------Uso
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).frequencyOfUse = "NA" '--------------------------------------Frecuencia de Uso
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).service = "PR"   '-------------------------------------------Servicio
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).kmsrecorded = 0  '-------------------------------------------Km recorridos
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).kmsrecordedSpecified = True  '-------------------------------Confirmacion de km recorridos
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).antiTheftDevice = "NONE" '-----------------------------------Dispositivo antirobo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).trailerUsed = "False"    '-----------------------------------Uso de remolque
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).smallTrailerUsed = False '-----------------------------------uso de remolque pequeño
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).smallTrailerUsedSpecified = True '---------------------------confirmacion de uso de remolque
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).brandId = IdMarca '---------------------------------------------ID de Marca
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).deductibleType = deductibleType.variable '---Tipo de deducieble Enum (Fijo o Variable)
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).deductibleTypeSpecified = True   '---------------------------Confirmacion de tipo de deducible

        Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
        'Nuevo objeto issuance.issuePolicyInfo.issuanceCustomerInfo
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo = New partyInfo(1) {}
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0) = New partyInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyCode = ""   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyRole = "CONTRATO"   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyType = "I"  '---------------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).name = ObjData.Cliente.Nombre '-----------------------------------------Nombre Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).fatherLastName = ObjData.Cliente.ApellidoPat    '---------------------------Apellido Paterno 
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).motherLastName = ObjData.Cliente.ApellidoMat    '---------------------------Apellido Materno
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).gender = Genero '-------------------------------------------Genero
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).dateofBirth = CDate(fechaNacimiento) '-------------------------------Fecha Nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).dateofBirthSpecified = True '----------------------------Confirmacion de fecha de nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).taxId = ObjData.Cliente.RFC  '-------------------------------RFC
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).isAlsoPolicyHolder = "Y" '-------------------------------Es Titular de la poliza

        'Nuevo objeto para conductor principal
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1) = New partyInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).partyRole = "CONDUCTOR"   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).partyType = "I"  '---------------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).name = ObjData.Cliente.Nombre '-----------------------------------------Nombre Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).fatherLastName = ObjData.Cliente.ApellidoPat    '---------------------------Apellido Paterno 
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).motherLastName = ObjData.Cliente.ApellidoMat     '---------------------------Apellido Materno
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).gender = Genero '-------------------------------------------Genero
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).dateofBirth = CDate(fechaNacimiento) '-------------------------------Fecha Nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).dateofBirthSpecified = True '----------------------------Confirmacion de fecha de nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).taxId = ObjData.Cliente.RFC  '-------------------------------RFC
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).mainDriver = "Y"    '----------------------------------Conductor principal                          'Actualizacion AXA

        'Nuevo objeto ContactInfo
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo = New contactInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0) = New contactInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).contactType = "Permanent" '---------------Tipo de Contacto
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).neighborhood = Colonia ' "732"  '-------------------Colonia
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).city = Ciudad ' "4"    '---------------------------Ciudad
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).state = Estado  '"9"   '---------------------------Estado
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).country = "MEXICO"    '-------------------Pais
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).postalCode = CodigoPostal   '-------------------Codigo Postal

        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).contactInfo = inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo

        'nuevo Objeto IssuanceAgente Info
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo = New agentInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0) = New agentInfo()
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).agentCode = AGENTE  '--------------------------------------Usuario Agente
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).mainAgent = "Y" '------------------------------------------Agente Principal
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).participationPercentage = "100" '--------------------------participacion porcentual
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentage = "0" '----------------------------------Porcentaje de cesion
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentageSpecified = True   '----------------------Confirmacion de Porcentaje de cesion

        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentMode = paymentMode     '--Modo de Pago Enum (Efectivo, cheque, Debito, AMEX, Visa, Mastercard, descuento por nomina)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentModeSpecified = True '-------------------------------Confirmacion de modo de pago

        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).VIN = ObjData.Vehiculo.NoSerie     '----------------------------------VIN 
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).description = ObjData.Vehiculo.Descripcion '--------------------------Descripcion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).licencePlate = ObjData.Vehiculo.NoPlacas '----------------------------------Numero de placa
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).engineNumber = ObjData.Vehiculo.NoMotor  '--------------------------------------Numero de Motor
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).zoneOfCirculation = "NA"    '------------------------------Zona Circulacion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).plateIssueState = Estado   '----------------------------------Estado de placas
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).RFV = "0"   '----------------------------------------------RFV
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).typeOfSumInsured = "CV" '----------------------------------Tipo de suma Asegurada         'Checar !!!!!!!!!!!!!!!!!!!
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).commercialPercentage = 100  '------------------------------Porcentaje comercial
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).commercialPercentageSpecified = True    '------------------Confirmacion de porcentaje comercial
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).invoiceAgreedValue = "1" '----------------------------------Factura valor convenido
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).invoiceAgreedValueSpecified = True  '----------------------Confirmacion de factura valor convenido
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).registro = "0"  '------------------------------------------registro
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).salvaged = "N"  '------------------------------------------Rescatado
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).riskCoverageInfo = getAPCovers(Plan) '------------------------------------------Detalle de Cobertura de Riesgos

        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).occupation = "INDP"     '------------------------------Ocupacion de Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).houseNumber = ObjData.Cliente.Direccion.NoExt '--------------Nimero Casa
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).street = ObjData.Cliente.Direccion.Calle   '------Calle
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).interiorNumber = ObjData.Cliente.Direccion.NoInt 'No debe ir vacio 'Numero interior
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).phoneNumber = ObjData.Cliente.Telefono   '----------Telefono
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).email = ObjData.Cliente.Email

        'Borrar este codigo
        'Si es Cotizacion Completa no tomar en cuenta
        inputData.issuance.packageCoverInfo = New packageCoverInfo() {}
        ReDim inputData.issuance.packageCoverInfo(0)
        inputData.issuance.packageCoverInfo(0) = New packageCoverInfo
        If FormaPago = 0 Then
            inputData.issuance.packageCoverInfo(0).paymentFrequency = paymentFrequency.Cash
        Else
            inputData.issuance.packageCoverInfo(0).paymentFrequency = paymentFrequency.Monthly
        End If
        inputData.issuance.packageCoverInfo(0).package = Plan   '----------------------------------------------------------Paquete  Checar!!!!!!!!!!!!!!!!!!!!

        Return inputData
    End Function

    Private Shared Function getExpressDataTC(ByVal ObjData As Seguro) As issuanceRequest
        AGENTE = "AGT00607034"
        Dim claveVersion As Integer = 0
        Dim LineCod As Integer = 0
        Dim IdMarca As String = ""
        Dim Genero = ""
        Dim Plan = ""
        If ObjData.Cliente.Genero = "MASCULINO" Then
            Genero = "M"
        Else
            Genero = "F"
        End If
        Dim Municipios As String()
        Try
            Municipios = ConsultaMunicipiosQualitas(ObjData.Cliente.Direccion.CodPostal)
        Catch ex As Exception
            Throw New System.Exception("Error al realizar consulta de municipios")
        End Try
        Dim Colonia As Integer = CInt(Municipios(3))
        Dim Ciudad As Integer = CInt(Municipios(2))
        Dim Estado As Integer = CInt(Municipios(0))
        Dim CodigoPostal = CInt(ObjData.Cliente.Direccion.CodPostal)
        claveVersion = CInt(Mid(ObjData.Vehiculo.Clave, 5, 2))
        LineCod = CInt(Mid(ObjData.Vehiculo.Clave, 8, 2))
        IdMarca = Mid(ObjData.Vehiculo.Clave, 1, 2)
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            Plan = "AM"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            Plan = "ES"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            Plan = "BA"
        End If

        Dim paymentMode As String = ""

        If ObjData.Pago.Carrier = Pago.CatCarrier.VISA Then
            paymentMode = AXA_Bancs_QA_CS.paymentMode.VISA
        ElseIf ObjData.Pago.Carrier = Pago.CatCarrier.MASTERCARD Then
            paymentMode = AXA_Bancs_QA_CS.paymentMode.MasterCard
        ElseIf ObjData.Pago.Carrier = Pago.CatCarrier.AMEX Then
            paymentMode = AXA_Bancs_QA_CS.paymentMode.AMEX
        End If

        Dim FormaPago As String = ObjData.PeriodicidadDePago

        Dim dtHoy As DateTime = Date.Now
        Dim dtUnAnio As DateTime = dtHoy.AddYears(1)
        'Declaro de inputData como un nuevo issuanceRequest (Peticion de Emision)
        Dim inputData As issuanceRequest = New issuanceRequest()
        'objeto issuance 
        inputData.issuance = New issuance()
        inputData.issuance.productCode = productCode.Item1002
        inputData.issuance.typeOperation = issuanceTypeOperation.Quotation '----------------------------Tipo de operacion Enum (Cotizacion Expres, Cotizacion, Issuance, Recotizacion)

        'objeto issuance.issuePolicyInfo 
        inputData.issuance.issuePolicyInfo = New issuePolicyInfo()
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo = New policyInfo()
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyStartDate = dtHoy     '---------------------------------Fecha Inicio Poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyStartDateSpecified = True '-----------------------------Fecha especificada para iniciar poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyExpiryDate = dtUnAnio '---------------------------------Fecha de expiracion Póliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyExpiryDateSpecified = True '----------------------------Fecha especificada para Expirar poliza
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.currency = currency.MXN '---------------------Moneda Enum (MXN o USD)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.currencySpecified = True '------------------------------------Confirmacion de Moneda especifica
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyDurationUnit = policyDurationUnit.Days 'Duracion poliza Enum (Dias, Mes, Año)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.policyDurationUnitSpecified = True '--------------------------Confirmacion de duracion poliza
        'inputData.issuance.issuePolicyInfo.issuancePolicyInfo.campaignCode = {"TRAD012013"}
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.netPremium = 0
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.netPremiumSpecified = True

        'Nuevo Objeto issuance.issuePolicyInfo.issuanceRiskInfo Arreglo (posicion 0)
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo = New riskInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0) = New riskInfo()
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).subBranch = "A"  '-------------------------------------------subRamo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).vehicleType = "RESIDENTES"  '-------------------------------Tipo de Vehiculo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).vehicleCode = ObjData.Vehiculo.Clave    '-------------------------------Codigo de vehicular
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).description = ObjData.Vehiculo.Descripcion '---------------------------Descripcion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).brand = ObjData.Vehiculo.Marca  '-------------------------------------------Marca
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).versionCode = claveVersion    '---------------------------------------Codigo de Version

        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).model = ObjData.Vehiculo.Modelo   '-------------------------------------------Modelo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).lineCode = LineCod  '-------------------------------------------Codigo de linea
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).use = "N"    '-----------------------------------------------Uso
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).frequencyOfUse = "NA" '--------------------------------------Frecuencia de Uso
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).service = "PR"   '-------------------------------------------Servicio
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).kmsrecorded = 0  '-------------------------------------------Km recorridos
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).kmsrecordedSpecified = True  '-------------------------------Confirmacion de km recorridos
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).antiTheftDevice = "NONE" '-----------------------------------Dispositivo antirobo
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).trailerUsed = "False"    '-----------------------------------Uso de remolque
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).smallTrailerUsed = False '-----------------------------------uso de remolque pequeño
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).smallTrailerUsedSpecified = True '---------------------------confirmacion de uso de remolque
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).brandId = IdMarca '---------------------------------------------ID de Marca
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).deductibleType = deductibleType.variable '---Tipo de deducieble Enum (Fijo o Variable)
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).deductibleTypeSpecified = True   '---------------------------Confirmacion de tipo de deducible

        Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyy-MM-dd")
        'Nuevo objeto issuance.issuePolicyInfo.issuanceCustomerInfo
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo = New partyInfo(1) {}
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0) = New partyInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyCode = ""   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyRole = "CONTRATO"   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).partyType = "I"  '---------------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).name = ObjData.Cliente.Nombre '-----------------------------------------Nombre Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).fatherLastName = ObjData.Cliente.ApellidoPat    '---------------------------Apellido Paterno 
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).motherLastName = ObjData.Cliente.ApellidoMat    '---------------------------Apellido Materno
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).gender = Genero '-------------------------------------------Genero
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).dateofBirth = CDate(fechaNacimiento) '-------------------------------Fecha Nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).dateofBirthSpecified = True '----------------------------Confirmacion de fecha de nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).taxId = ObjData.Cliente.RFC  '-------------------------------RFC
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).isAlsoPolicyHolder = "Y" '-------------------------------Es Titular de la poliza

        'Nuevo objeto para conductor principal
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1) = New partyInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).partyRole = "CONDUCTOR"   '-------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).partyType = "I"  '---------------------------------------
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).name = ObjData.Cliente.Nombre '-----------------------------------------Nombre Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).fatherLastName = ObjData.Cliente.ApellidoPat    '---------------------------Apellido Paterno 
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).motherLastName = ObjData.Cliente.ApellidoMat     '---------------------------Apellido Materno
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).gender = Genero '-------------------------------------------Genero
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).dateofBirth = CDate(fechaNacimiento) '-------------------------------Fecha Nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).dateofBirthSpecified = True '----------------------------Confirmacion de fecha de nacimiento
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).taxId = ObjData.Cliente.RFC  '-------------------------------RFC
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).mainDriver = "Y"    '----------------------------------Conductor principal                          'Actualizacion AXA

        'Nuevo objeto ContactInfo
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo = New contactInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0) = New contactInfo()
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).contactType = "Permanent" '---------------Tipo de Contacto
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).neighborhood = Colonia ' "732"  '-------------------Colonia
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).city = Ciudad ' "4"    '---------------------------Ciudad
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).state = Estado  '"9"   '---------------------------Estado
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).country = "MEXICO"    '-------------------Pais
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).postalCode = CodigoPostal   '-------------------Codigo Postal

        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(1).contactInfo = inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo

        'nuevo Objeto IssuanceAgente Info
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo = New agentInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0) = New agentInfo()
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).agentCode = AGENTE  '--------------------------------------Usuario Agente
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).mainAgent = "Y" '------------------------------------------Agente Principal
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).participationPercentage = "100" '--------------------------participacion porcentual
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentage = "0" '----------------------------------Porcentaje de cesion
        inputData.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentageSpecified = True   '----------------------Confirmacion de Porcentaje de cesion

        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentMode = paymentMode     '--Modo de Pago Enum (Efectivo, cheque, Debito, AMEX, Visa, Mastercard, descuento por nomina)
        inputData.issuance.issuePolicyInfo.issuancePolicyInfo.paymentModeSpecified = True '-------------------------------Confirmacion de modo de pago

        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).VIN = ObjData.Vehiculo.NoSerie     '----------------------------------VIN 
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).description = ObjData.Vehiculo.Descripcion '--------------------------Descripcion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).licencePlate = ObjData.Vehiculo.NoPlacas '----------------------------------Numero de placa
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).engineNumber = ObjData.Vehiculo.NoMotor  '--------------------------------------Numero de Motor
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).zoneOfCirculation = "NA"    '------------------------------Zona Circulacion
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).plateIssueState = Estado   '----------------------------------Estado de placas
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).RFV = "0"   '----------------------------------------------RFV
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).typeOfSumInsured = "CV" '----------------------------------Tipo de suma Asegurada         'Checar !!!!!!!!!!!!!!!!!!!
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).commercialPercentage = 100  '------------------------------Porcentaje comercial
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).commercialPercentageSpecified = True    '------------------Confirmacion de porcentaje comercial
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).invoiceAgreedValue = "1" '----------------------------------Factura valor convenido
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).invoiceAgreedValueSpecified = True  '----------------------Confirmacion de factura valor convenido
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).registro = "0"  '------------------------------------------registro
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).salvaged = "N"  '------------------------------------------Rescatado
        inputData.issuance.issuePolicyInfo.issuanceRiskInfo(0).riskCoverageInfo = getAPCovers(Plan) '------------------------------------------Detalle de Cobertura de Riesgos

        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).occupation = "INDP"     '------------------------------Ocupacion de Cliente
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).houseNumber = ObjData.Cliente.Direccion.NoExt '--------------Nimero Casa
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).street = ObjData.Cliente.Direccion.Calle   '------Calle
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).interiorNumber = ObjData.Cliente.Direccion.NoInt 'No debe ir vacio 'Numero interior
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).phoneNumber = ObjData.Cliente.Telefono   '----------Telefono
        inputData.issuance.issuePolicyInfo.issuanceCustomerInfo(0).contactInfo(0).email = ObjData.Cliente.Email

        inputData.issuance.issuePolicyInfo.issuanceCardInfo = New cardInfo(0) {}
        inputData.issuance.issuePolicyInfo.issuanceCardInfo(0) = New cardInfo()
        inputData.issuance.issuePolicyInfo.issuanceCardInfo(0).cardNumber = ObjData.Pago.NoTarjeta
        inputData.issuance.issuePolicyInfo.issuanceCardInfo(0).expiryMonthYear = ObjData.Pago.MesExp + ObjData.Pago.AnioExp.Substring(2, 2)
        inputData.issuance.issuePolicyInfo.issuanceCardInfo(0).firstName = ObjData.Pago.NombreTarjeta
        inputData.issuance.issuePolicyInfo.issuanceCardInfo(0).securityCode = ObjData.Pago.CodigoSeguridad
        inputData.issuance.issuePolicyInfo.issuanceCardInfo(0).inmediatePayment = "N"

        Return inputData
    End Function

    Private Shared Function preparaEndPoint() As integratorPortClient
        Dim USERNAME As String = "MXS00101483A"
        Dim PASSWORD As String = "Soporte18*"
        Dim clienteWS As New integratorPortClient("integratorBancsSoap")
        clienteWS.ClientCredentials.UserName.UserName = USERNAME
        clienteWS.ClientCredentials.UserName.Password = PASSWORD
        Return clienteWS
    End Function

    Private Shared Function getAPCovers(ByVal Cobertura As String) As coverageInfo()
        Dim cober As New Coberturas

        Dim toReturn() As coverageInfo = New coverageInfo() {}
        If Cobertura = "AM" Then
            ReDim Preserve toReturn(7)
        ElseIf Cobertura = "ES" Then
            ReDim Preserve toReturn(5)
        ElseIf Cobertura = "BA" Then
            ReDim Preserve toReturn(6)
        End If

        toReturn(0) = New coverageInfo()
        toReturn(0).coverCode = "RCP"
        toReturn(0).coverApplicable = "YES"
        toReturn(0).sumInsured = 3000000
        toReturn(0).sumInsuredSpecified = True

        If Cobertura = "AM" Then
            toReturn(1) = New coverageInfo()
            toReturn(1).coverCode = "RC"
            toReturn(1).coverApplicable = "YES"
            toReturn(1).deductible = 0
            toReturn(1).deductibleSpecified = True
            toReturn(1).sumInsured = 1000000
            toReturn(1).sumInsuredSpecified = True
        ElseIf Cobertura = "ES" Or Cobertura = "BA" Then
            toReturn(1) = New coverageInfo()
            toReturn(1).coverCode = "RC"
            toReturn(1).coverApplicable = "YES"
            toReturn(1).deductible = 0
            toReturn(1).deductibleSpecified = True
            toReturn(1).sumInsured = 1000000
            toReturn(1).sumInsuredSpecified = True
        End If

        If Cobertura = "AM" Then
            toReturn(2) = New coverageInfo()
            toReturn(2).coverCode = "GMO"
            toReturn(2).coverApplicable = "YES"
            toReturn(2).sumInsured = 200000
            toReturn(2).sumInsuredSpecified = True
        ElseIf Cobertura = "ES" Or Cobertura = "BA" Then
            toReturn(2) = New coverageInfo()
            toReturn(2).coverCode = "GMO"
            toReturn(2).coverApplicable = "YES"
            toReturn(2).sumInsured = 200000
            toReturn(2).sumInsuredSpecified = True
        End If

        toReturn(3) = New coverageInfo()
        toReturn(3).coverCode = "SERVAS"
        toReturn(3).coverApplicable = "YES"

        toReturn(4) = New coverageInfo()
        toReturn(4).coverCode = "DL"
        toReturn(4).coverApplicable = "YES"

        If Cobertura = "AM" Then
            toReturn(5) = New coverageInfo()
            toReturn(5).coverCode = "RT"
            toReturn(5).coverApplicable = "YES"
            toReturn(5).deductible = 10
            toReturn(5).deductibleSpecified = True

            toReturn(6) = New coverageInfo()
            toReturn(6).coverCode = "DM"
            toReturn(6).coverApplicable = "YES"
            toReturn(6).deductible = 5
            toReturn(6).deductibleSpecified = True

            toReturn(7) = New coverageInfo()
            toReturn(7).coverCode = "CRI"
            toReturn(7).coverApplicable = "YES"
            toReturn(7).deductible = 20
            toReturn(7).deductibleSpecified = True

            cober.DanosMateriales = "-NDAÑOS MATERIALES-SV. COMERCIAL-D" & toReturn(6).deductible.ToString & "%"
            cober.RoboTotal = "-NROBO TOTAL-SV. COMERCIAL-D" & toReturn(5).deductible.ToString & "%"
            cober.Cristales = "-NCRISTALES-SAMPARADA" & "-D0"

        ElseIf Cobertura = "ES" Then
            toReturn(5) = New coverageInfo()
            toReturn(5).coverCode = "RT"
            toReturn(5).coverApplicable = "YES"
            toReturn(5).deductible = 10
            toReturn(5).deductibleSpecified = True
            cober.RoboTotal = "-NROBO TOTAL-SV. COMERCIAL-D" & toReturn(5).deductible.ToString & "%"

        ElseIf Cobertura = "BA" Then

        End If
        cober.RC = "-NRESPONSABILIDAD CIVIL-S" & toReturn(1).sumInsured.ToString & "-D" & toReturn(1).deductible.ToString
        cober.GastosMedicosOcupantes = "-NGASTOS MÉDICOS-S" & toReturn(2).sumInsured.ToString & "-D0"
        cober.AsitenciaCompleta = "-NASISTENCIA VIAL-SAMPARADA" & "-D0"
        cober.DefensaJuridica = "-NGASTOS LEGALES-SAMPARADA" & "-D0"
        cober.RCPExtra = "-NRESPONSABILIDAD CIVIL PERSONAS EXCESO-S" & toReturn(0).sumInsured.ToString & "-D0"
        ObjData.Coberturas.Add(cober)
        Return toReturn
    End Function

    'Emision WS AXA 
    Public Shared Function AXAEmision(ByVal ObjData2 As Seguro, ByVal idLogWS As String) As String
        ObjData = ObjData2
        Dim respuestaWS As String = ""
        Dim respuestaerror As String
        If ObjData.Vehiculo.Clave.Length < 9 Then
            respuestaerror = "La clave de vehículo no se encuentra en el catálogo"
        Else
            Dim Plan = ""
            If UCase(ObjData.Paquete) = "AMPLIA" Then
                Plan = "AM"
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                Plan = "ES"
            ElseIf UCase(ObjData.Paquete) = "RC" Then
                Plan = "BA"
            End If
            Dim FormaPago As String = ObjData.PeriodicidadDePago

            Dim inputData As issuanceRequest

            Try
                inputData = getExpressDataTC(ObjData)
            Catch ex As Exception
                ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
                Dim serializer2 As New JavaScriptSerializer
                Return serializer2.Serialize(ObjData)
            End Try

            'Se crea un vector con 1 posición donde se agrega la clase quickQuoteInfo
            inputData.issuance.quickQuoteInfo = New quickQuoteInfo(0) {}
            'Se cargan los valores PaymentFrecuency y Package en la posicion 0
            inputData.issuance.quickQuoteInfo(0) = New quickQuoteInfo()
            'inputData.issuance.quickQuoteInfo(0).paymentFrequency = ObjData.PeriodicidadDePago
            If FormaPago = 0 Then
                inputData.issuance.quickQuoteInfo(0).paymentFrequency = paymentFrequency.Cash
            Else
                inputData.issuance.quickQuoteInfo(0).paymentFrequency = paymentFrequency.Monthly
            End If
            inputData.issuance.quickQuoteInfo(0).package = Plan

            'Declaramos el objeto ClienteWS y cargamos los valores USERNAME y PASSWORD
            Dim clienteWS As integratorPortClient = preparaEndPoint()

            'Declaramos el objeto request y cargamos los valores de inputData
            Dim request As getIssuanceDetailsRequest = New getIssuanceDetailsRequest()
            request.issuanceRequest = inputData

            Try


                Try
                    Dim x As New XmlSerializer(request.GetType)
                    Dim xml As New StringWriter
                    x.Serialize(xml, request)
                    Dim errorws2 As String = xml.ToString
                    Funciones.updateLogWS(errorws2, idLogWS, "RequestWS")
                Catch ex As Exception

                End Try
                '    'Declaramos el objero Respuesta con los datos de la cotización
                Dim respuesta As getIssuanceDetailsResponse = clienteWS.getIssuanceDetails(request)


                Try
                    Dim x2 As New XmlSerializer(respuesta.GetType)
                    Dim xml2 As New StringWriter
                    x2.Serialize(xml2, respuesta)
                    Dim errorws4 As String = xml2.ToString
                    Funciones.updateLogWS(errorws4, idLogWS, "ResponseWS")
                Catch ex As Exception

                End Try

                '    'Si al recibir respuesta del WS tenemos que esta satisfactorio o sin errores, continuamos con el proceso de llenado de nuestro Obj
                If respuesta.issuanceResponse.errorDetails(0).errorMessage = "SUCCESS" Or respuesta.issuanceResponse.errorDetails(0).errorMessage = "" Then
                    ObjData.Cotizacion.PrimaNeta = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.netPremium
                    ObjData.Cotizacion.Impuesto = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.tax
                    ObjData.Cotizacion.PrimaTotal = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.totalPremium
                    ObjData.Cotizacion.Derechos = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.policyFee
                    ObjData.Cotizacion.Recargos = respuesta.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.installmentSurcharge
                    ObjData.Cotizacion.IDCotizacion = respuesta.issuanceResponse.issuanceDetails(0).policyNumber
                    If ObjData.PeriodicidadDePago = 4 Then
                        Dim PrimaNetaIva As Integer = ObjData.Cotizacion.PrimaNeta * 1.16
                        Dim Recargo As Integer = ObjData.Cotizacion.Recargos * 1.16
                        Dim Derechos As Integer = ObjData.Cotizacion.Derechos * 1.16
                        ObjData.Cotizacion.PagosSubsecuentes = (PrimaNetaIva + Recargo) / 12
                        ObjData.Cotizacion.PrimerPago = ObjData.Cotizacion.PagosSubsecuentes + Derechos
                    End If

                    Dim inputData2 As issuanceRequest = New issuanceRequest()
                    inputData2.issuance = New issuance()
                    inputData2.issuance.productCode = productCode.Item1002
                    inputData2.issuance.typeOperation = issuanceTypeOperation.Issuance

                    inputData2.issuance.issuePolicyInfo = New issuePolicyInfo()
                    inputData2.issuance.issuePolicyInfo.issuancePolicyInfo = New policyInfo()
                    inputData2.issuance.issuePolicyInfo.issuancePolicyInfo.policyNumber = ObjData.Cotizacion.IDCotizacion

                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo = New agentInfo(0) {}
                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo(0) = New agentInfo()
                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo(0).agentCode = AGENTE  '--------------------------------------Usuario Agente
                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo(0).mainAgent = "Y" '------------------------------------------Agente Principal
                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo(0).participationPercentage = "100" '--------------------------participacion porcentual
                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentage = "0" '----------------------------------Porcentaje de cesion
                    inputData2.issuance.issuePolicyInfo.issuanceAgentInfo(0).cessionPercentageSpecified = True   '----------------------Confirmacion de Porcentaje de cesion

                    inputData2.issuance.quickQuoteInfo = New quickQuoteInfo(0) {}
                    inputData2.issuance.quickQuoteInfo(0) = New quickQuoteInfo()

                    If FormaPago = 0 Then
                        inputData2.issuance.quickQuoteInfo(0).paymentFrequency = paymentFrequency.Cash
                    Else
                        inputData2.issuance.quickQuoteInfo(0).paymentFrequency = paymentFrequency.Monthly
                    End If

                    '<!--AP AM ES BA PE CT-->
                    inputData2.issuance.quickQuoteInfo(0).package = Plan


                    'Declaramos el objeto request y cargamos los valores de inputData
                    Dim request2 As getIssuanceDetailsRequest = New getIssuanceDetailsRequest()
                    request.issuanceRequest = inputData2

                    Try
                        Dim x2 As New XmlSerializer(request.GetType)
                        Dim xml2 As New StringWriter
                        x2.Serialize(xml2, request)
                        Dim errorws3 As String = xml2.ToString
                        Funciones.updateLogWS(errorws3, idLogWS, "RequestWS")
                        'FuncionesGeneral.xmlWS(errorws3, ObjData, "Emision", "EmisionEnvio")
                    Catch ex As Exception

                    End Try

                    Dim respuesta2 As getIssuanceDetailsResponse = clienteWS.getIssuanceDetails(request)

                    Try
                        Dim x3 As New XmlSerializer(respuesta2.GetType)
                        Dim xml3 As New StringWriter
                        x3.Serialize(xml3, respuesta2)
                        Dim errorws4 As String = xml3.ToString
                        Funciones.updateLogWS(errorws4, idLogWS, "ResponseWS")
                        'FuncionesGeneral.xmlWS(errorws4, ObjData, "Emision", "EmisionRespuesta")
                    Catch ex As Exception

                    End Try

                    ObjData.CodigoError = respuesta2.issuanceResponse.errorDetails(0).errorMessage

                    Threading.Thread.Sleep(10000)

                    If respuesta2.issuanceResponse.errorDetails(0).errorMessage = "Successful" Then
                        ObjData.Emision.PrimaNeta = respuesta2.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.netPremium
                        ObjData.Emision.Impuesto = respuesta2.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.tax
                        ObjData.Emision.PrimaTotal = respuesta2.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.totalPremium
                        ObjData.Emision.Derechos = respuesta2.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.policyFee
                        ObjData.Emision.Recargos = respuesta2.issuanceResponse.issuanceDetails(0).quoteInfo(0).receiptsInfo.installmentSurcharge
                        ObjData.Emision.Poliza = respuesta2.issuanceResponse.issuanceDetails(0).policyNumber.Trim
                        If ObjData.PeriodicidadDePago = 4 Then
                            Dim PrimaNetaIva As Integer = ObjData.Emision.PrimaNeta * 1.16
                            Dim Recargo As Integer = ObjData.Emision.Recargos * 1.16
                            Dim Derechos As Integer = ObjData.Emision.Derechos * 1.16
                            ObjData.Emision.PagosSubsecuentes = (PrimaNetaIva + Recargo) / 12
                            ObjData.Emision.PrimerPago = ObjData.Emision.PagosSubsecuentes + Derechos
                        End If
                        Dim urlPoliza As getPolicyDocumentRequest = New getPolicyDocumentRequest
                        urlPoliza.Item = New downloadDocument
                        urlPoliza.Item.UserId = "BancsMCS"
                        urlPoliza.Item.Pwd = "b2a4a375f20a323bdd523462988dc070"
                        urlPoliza.Item.ProyectoId = "3"
                        urlPoliza.Item.ExpId = "3"
                        urlPoliza.Item.AgentId = AGENTE
                        urlPoliza.Item.ConfiguracionXml = "3"
                        urlPoliza.Item.filtros = New abstractDocumentFiltros
                        urlPoliza.Item.filtros.filtrop = "numero_poliza=[" & ObjData.Emision.Poliza.Trim & "]"
                        urlPoliza.Item.filtros.filtroh = ""
                        urlPoliza.Item.Rama = "1"
                        urlPoliza.agentInfo = New agentInfo
                        urlPoliza.agentInfo.agentCode = AGENTE
                        urlPoliza.agentInfo.participationPercentage = "0"
                        urlPoliza.agentInfo.cessionPercentage = "0"
                        urlPoliza.agentInfo.cessionPercentageSpecified = True


                        Dim x3 As New XmlSerializer(urlPoliza.GetType)
                        Dim xml3 As New StringWriter
                        x3.Serialize(xml3, urlPoliza)
                        Dim errorws4 As String = xml3.ToString

                        Dim urlPolizarespuesta As getPolicyDocumentResponse = clienteWS.getPolicyDocument(urlPoliza)
                        ObjData.Emision.Documento = DirectCast(urlPolizarespuesta.Item, downloadDocumentResponse).DownloadDocumentUrl
                        ObjData.Emision.Resultado = "True"
                    Else
                        ObjData.Emision.Resultado = "False"
                    End If
                Else
                    ObjData.Emision.Resultado = "False"
                    ObjData.CodigoError = respuesta.issuanceResponse.errorDetails(0).errorMessage
                End If

            Catch excep As System.Exception
                Dim errror = excep
                ObjData.Emision.Resultado = "False"
            End Try
            Dim serializer As New JavaScriptSerializer
            Return serializer.Serialize(ObjData)
        End If
        Return respuestaerror

    End Function

    Private Shared Function ConsultaMunicipiosQualitas(ByVal CPostal As String) As String()

        Dim strSQLQueryCPostal As String
        Dim Lista(3) As String
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQueryCPostal = "Select * from `ws_db`.`Ali_R_CatEdosQUA` where CPostal = '" & CPostal & "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim rsCPostal As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (rsCPostal.Read())

                    Lista(0) = rsCPostal("IDEdo_Qua")
                    Lista(1) = rsCPostal("Municipio")
                    Lista(2) = rsCPostal("IDMun_Qua")
                    Lista(3) = rsCPostal("IDColonia_Qua")
                End While
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Lista
    End Function

End Class
