﻿Imports Microsoft.VisualBasic

Public Class CatModelo
    Private _ListadoModelos As New List(Of ListadoModelos)
    Private _Marca As String
    Private Sub Constructor()
        Me._Marca = ""
    End Sub
    Public Property Marca As String
        Get
            Return Me._Marca
        End Get
        Set(ByVal value As String)
            Me._Marca = value
        End Set
    End Property
    Public Property ListadoDescripciones As List(Of ListadoModelos)
        Get
            Return Me._ListadoModelos
        End Get
        Set(ByVal value As List(Of ListadoModelos))
            Me._ListadoModelos = value
        End Set
    End Property
End Class
Public Class ListadoModelos
    Private _Modelo As String
    Private Sub Constructor()
        Me._Modelo = ""
    End Sub
    Public Property Modelo As String
        Get
            Return Me._Modelo
        End Get
        Set(ByVal value As String)
            Me._Modelo = value
        End Set
    End Property
End Class
