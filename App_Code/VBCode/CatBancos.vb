﻿Imports Microsoft.VisualBasic

Public Class CatBancos
    Private _Aseguradora As String
    Private _ListadoBancos As New List(Of ListadoBancos)
    Private Sub Constructor()
        Me._Aseguradora = ""
    End Sub
    Public Property Aseguradora As String
        Get
            Return Me._Aseguradora
        End Get
        Set(ByVal value As String)
            Me._Aseguradora = value
        End Set
    End Property
    Public Property Bancos As List(Of ListadoBancos)
        Get
            Return Me._ListadoBancos
        End Get
        Set(ByVal value As List(Of ListadoBancos))
            Me._ListadoBancos = value
        End Set
    End Property
End Class
Public Class ListadoBancos
    Private _Nombre As String
    Private _Abreviacion As String
    Private Sub Constructor()
        Me._Nombre = ""
        Me._Abreviacion = ""
    End Sub
    Public Property Abreviacion As String
        Get
            Return Me._Abreviacion
        End Get
        Set(ByVal value As String)
            Me._Abreviacion = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property
End Class
