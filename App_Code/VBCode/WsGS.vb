﻿Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Serialization

Public Class WsGS
    Public Shared Function GSCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim serializer As New JavaScriptSerializer
        Dim ObjGSResponse As New GSService.CotizacionEmisionWSService
        Dim ObjGSCoberturasResponse As New GSCoberturas.CatalogoCoberturasWSService
        Dim ObjGS As New GSService.cotizacionRequest
        Dim ObjGSCoberturas As New GSCoberturas.obtenerCoberturasCotizacionRequest
        Dim FormaPago As String = ""
        Dim Paquete As String = ""
        Dim IdPaquete As Int16 = 0
        Dim respuesta = New GSService.guardarCotizacionResponse
        Try

            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                FormaPago = "CONTADO"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                FormaPago = "SEMESTRAL"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                FormaPago = "TRIMESTRAL"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                FormaPago = "MENSUAL"
            End If
            If UCase(ObjData.Paquete) = "AMPLIA" Then
                Paquete = "CONFORT AMPLIA"
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                Paquete = "CONFORT LIMITADA"
            ElseIf UCase(ObjData.Paquete) = "RC" Then
                Paquete = "CONFORT BASICA"
            End If
            ObjGS.token = "a32de857bd9666ffb3894829abfcc3d0"
            ObjGS.configuracionProducto = GSService.configuracionProductoEnum.RESIDENTE_INDIVIDUAL
            ObjGS.configuracionProductoSpecified = True
            ObjGS.cp = ObjData.Cliente.Direccion.CodPostal
            ObjGS.descuento = ObjData.Descuento
            ObjGS.vigencia = GSService.vigencia.ANUAL
            ObjGS.inciso = New GSService.incisoDTO
            ObjGS.inciso.claveGs = ObjData.Vehiculo.Clave
            ObjGS.inciso.conductorMenor30 = 1
            ObjGS.inciso.modelo = ObjData.Vehiculo.Modelo
            ObjGS.inciso.tipoServicio = GSService.tipoServicio.PARTICULAR
            ObjGS.inciso.tipoServicioSpecified = True
            ObjGS.inciso.tipoValor = GSService.tipoValor.VALOR_COMERCIAL
            ObjGS.inciso.tipoValorSpecified = True
            ObjGS.inciso.tipoVehiculo = GSService.tipoVehiculo.AUTO_PICKUP
            ObjGS.inciso.tipoVehiculoSpecified = True
            Try
                Dim x2 As New XmlSerializer(ObjGS.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, ObjGS)
                Dim xmlRespuesta As String = xml2.ToString
                Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            respuesta = ObjGSResponse.generarCotizacion(ObjGS)
            Try
                Dim x2 As New XmlSerializer(respuesta.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, respuesta)
                Dim errorws4 As String = xml2.ToString
                Funciones.updateLogWSCot(errorws4, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception

            End Try
            For i = 0 To respuesta.paquetes.Length - 1
                If respuesta.paquetes(i).nombre = Paquete Then
                    IdPaquete = respuesta.paquetes(i).id
                    For j = 0 To respuesta.paquetes(i).formasPagoDTO.Length - 1
                        If respuesta.paquetes(i).formasPagoDTO(j).nombre = FormaPago Then
                            ObjData.Cotizacion.IDCotizacion = respuesta.idCotizacion
                            ObjData.Cotizacion.PrimaNeta = respuesta.paquetes(i).formasPagoDTO(j).primaNeta
                            ObjData.Cotizacion.PrimaTotal = respuesta.paquetes(i).formasPagoDTO(j).primaTotal
                            ObjData.Cotizacion.Recargos = respuesta.paquetes(i).formasPagoDTO(j).recargo
                            ObjData.Cotizacion.PagosSubsecuentes = respuesta.paquetes(i).formasPagoDTO(j).recibosub
                            ObjData.Cotizacion.Impuesto = respuesta.paquetes(i).formasPagoDTO(j).iva
                            ObjData.Cotizacion.Derechos = respuesta.paquetes(i).formasPagoDTO(j).derechos
                        End If
                    Next
                End If
            Next
            ObjGSCoberturas.cotizacion = respuesta.idCotizacion
            ObjGSCoberturas.token = "d819ff02cb8eaa57ab60f8b03ab9a2c6"
            ObjGSCoberturas.paquete = IdPaquete
            Dim respuestaCoberturas = ObjGSCoberturasResponse.wsObtenerCoberturasCotizacion(ObjGSCoberturas)
            Dim Coberturas As New Coberturas
            For k = 0 To respuestaCoberturas.coberturas.Length - 1
                Dim cobertura As String = respuestaCoberturas.coberturas(k).descripcion
                Dim descripcionCobertura As String = ""
                Dim nombreCobertura = ""
                Dim Monto = respuestaCoberturas.coberturas(k).monto
                Select Case cobertura

                    Case "Daños Materiales Pérdida Total"
                        nombreCobertura = "DAÑOS MATERIALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & Monto
                        Coberturas.DanosMateriales = descripcionCobertura
                    Case "Robo Total"
                        nombreCobertura = "ROBO TOTAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S0-D" & Monto
                        Coberturas.RoboTotal = descripcionCobertura
                    Case "Responsabilidad Civil por Daños a Terceros (LUC)"
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & Monto & "-D0"
                        Coberturas.RCBienes = descripcionCobertura
                    Case "Gastos Médicos"
                        nombreCobertura = "GASTOS MÉDICOS"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & Monto & "-D0"
                        Coberturas.GastosMedicosOcupantes = descripcionCobertura
                    Case "Asistencia Jurídica GS"
                        nombreCobertura = "GASTOS LEGALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & Monto & "-D0"
                        Coberturas.DefensaJuridica = descripcionCobertura
                    Case "Asistencia Vial y en Viajes GS"
                        nombreCobertura = "ASISTENCIA VIAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & Monto & "-D0"
                        Coberturas.AsitenciaCompleta = descripcionCobertura
                End Select
            Next
            ObjData.Coberturas.Add(Coberturas)
            ObjData.Cotizacion.Resultado = "True"
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = respuesta.mensaje
        End Try
        Return serializer.Serialize(ObjData)
    End Function
End Class
