﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Script.Serialization
Imports Microsoft.VisualBasic
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports SuperObjeto

Public Class WsBANORTEPProd
    Public Shared Function BANORTECotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        'Dim urlCotizacion = "https://api-pre.segurosbanorte.com/cotizadores/api/v1/auto/cotizacion"
        Dim urlCotizacion = "https://api-pre.segurosbanorte.com/cotizadores/api/v1/auto/recalcular/cotizacion"

        ObjData.Cotizacion.Resultado = "False"
        Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
        For i = lengthCP To 4
            ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
        Next

        Dim banorteCotizacionRequest = New BANORTECotizacionRequest
        banorteCotizacionRequest.nombreProducto = "SEGURO DE AUTOMÓVILES RESIDENTES"
        banorteCotizacionRequest.claveIntermediario = "9023"
        banorteCotizacionRequest.nombreCategoria = "AUTOS RESIDENTES"
        banorteCotizacionRequest.codigoPostal = ObjData.Cliente.Direccion.CodPostal
        'banorteCotizacionRequest.municipio = edoMun(1)
        'banorteCotizacionRequest.estado = edoMun(0)
        banorteCotizacionRequest.claveBanorte = ObjData.Vehiculo.Clave
        banorteCotizacionRequest.anio = ObjData.Vehiculo.Modelo
        banorteCotizacionRequest.nombreUso = "PARTICULAR"
        banorteCotizacionRequest.nombreServicio = "PARTICULAR"
        banorteCotizacionRequest.respuestas = New List(Of BANORTERespuesta)
        Dim tipoVehi As String
        Try
            tipoVehi = getTipoVehi(ObjData.Vehiculo.Clave)
        Catch ex As Exception
            Throw New System.Exception("Error al reaizar consulta tipo vehiculo (clave de vehiculo)")
        End Try

        Dim Paquete As String = ""
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            Paquete = "PROTEGIDO"
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            Paquete = "PREVENTIVO"
        ElseIf UCase(ObjData.Paquete) = "RC" Then
            Paquete = "PROTECCIÓN BÁSICA"
        End If

        Dim tipoVehiDig As String = IIf(tipoVehi = "AUT", "1", "2")
        Dim banorteRespuesta1 = New BANORTERespuesta
        banorteRespuesta1.enunciado = "TIPO VALOR VEHICULO"
        banorteRespuesta1.valor = tipoVehiDig
        banorteRespuesta1.indice = "2"
        banorteCotizacionRequest.respuestas.Add(banorteRespuesta1)
        Dim banorteRespuesta2 = New BANORTERespuesta
        banorteRespuesta2.enunciado = "¿SU AUTO ARRASTRA REMOLQUE?"
        banorteRespuesta2.valor = "false"
        banorteRespuesta2.indice = "4"
        banorteCotizacionRequest.respuestas.Add(banorteRespuesta2)
        Dim banorteRespuesta3 = New BANORTERespuesta
        banorteRespuesta3.enunciado = "¿EQUIPO ESPECIAL A ASEGURAR?"
        banorteRespuesta3.valor = "false"
        banorteRespuesta3.indice = "7"
        banorteCotizacionRequest.respuestas.Add(banorteRespuesta3)
        Dim banorteRespuesta4 = New BANORTERespuesta
        banorteRespuesta4.enunciado = "INDIQUE EL SEXO DEL ASEGURADO"
        banorteRespuesta4.valor = ObjData.Cliente.Genero
        banorteRespuesta4.indice = "10"
        banorteCotizacionRequest.respuestas.Add(banorteRespuesta4)
        Dim banorteRespuesta5 = New BANORTERespuesta
        banorteRespuesta5.enunciado = "¿DESEA COTIZAR PLANES COMPLEMENTARIOS?"
        banorteRespuesta5.valor = "NO"
        banorteRespuesta5.indice = "11"
        banorteCotizacionRequest.respuestas.Add(banorteRespuesta5)
        Dim banorteRespuesta6 = New BANORTERespuesta
        banorteRespuesta6.enunciado = "NO. POLIZA / NO. OFICINA"
        banorteRespuesta6.valor = "0BB"
        banorteRespuesta6.indice = "12"
        banorteCotizacionRequest.respuestas.Add(banorteRespuesta6)
        banorteCotizacionRequest.nombrePaquete = Paquete

        'Coberturas PROTEGIDO (AMPLIA)
        banorteCotizacionRequest.items = New List(Of BANORTEItems)
        Dim itemRoboTotal = New BANORTEItems
        itemRoboTotal.indice = "84"
        itemRoboTotal.habilitada = "true"
        itemRoboTotal.nombreTipoCobertura = "ROBO TOTAL"
        itemRoboTotal.montoSumaAsegurada = "540082"
        itemRoboTotal.montoDeducible = "10"
        banorteCotizacionRequest.items.Add(itemRoboTotal)
        Dim itemRCDanosTerceros = New BANORTEItems
        itemRCDanosTerceros.indice = "85"
        itemRCDanosTerceros.habilitada = "true"
        itemRCDanosTerceros.nombreTipoCobertura = "RESPONSABILIDAD CIVIL DAÑOS A TERCEROS"
        itemRCDanosTerceros.montoSumaAsegurada = "4000000"
        itemRCDanosTerceros.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemRCDanosTerceros)
        Dim itemGM = New BANORTEItems
        itemGM.indice = "86"
        itemGM.habilitada = "true"
        itemGM.nombreTipoCobertura = "GASTOS MÉDICOS OCUPANTES"
        itemGM.montoSumaAsegurada = "150000"
        itemGM.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemGM)
        Dim itemExRC = New BANORTEItems
        itemExRC.indice = "87"
        itemExRC.habilitada = "true"
        itemExRC.nombreTipoCobertura = "EXTENSIÓN DE RESPONSABILIDAD CIVIL"
        itemExRC.montoSumaAsegurada = "4000000"
        itemExRC.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemExRC)
        Dim itemProteccionUSA = New BANORTEItems
        itemProteccionUSA.indice = "88"
        itemProteccionUSA.habilitada = "true"
        itemProteccionUSA.nombreTipoCobertura = "PROTECCIÓN EN ESTADOS UNIDOS"
        itemProteccionUSA.montoSumaAsegurada = "100000"
        itemProteccionUSA.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemProteccionUSA)
        Dim itemAsistenciaJuridica = New BANORTEItems
        itemAsistenciaJuridica.indice = "89"
        itemAsistenciaJuridica.habilitada = "true"
        itemAsistenciaJuridica.nombreTipoCobertura = "ASISTENCIA JURÍDICA"
        itemAsistenciaJuridica.montoSumaAsegurada = "1000000"
        itemAsistenciaJuridica.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemAsistenciaJuridica)
        Dim itemAsistenciaVehicular = New BANORTEItems
        itemAsistenciaVehicular.indice = "90"
        itemAsistenciaVehicular.habilitada = "true"
        itemAsistenciaVehicular.nombreTipoCobertura = "ASISTENCIA VEHICULAR"
        itemAsistenciaVehicular.montoSumaAsegurada = "750000"
        itemAsistenciaVehicular.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemAsistenciaVehicular)
        Dim itemEme = New BANORTEItems
        itemEme.indice = "1631"
        itemEme.habilitada = "false"
        itemEme.nombreTipoCobertura = "EMME AUTO PROTEGIDO"
        itemEme.montoSumaAsegurada = "15000"
        itemEme.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemEme)
        Dim itemMuerteConductor = New BANORTEItems
        itemMuerteConductor.indice = "92"
        itemMuerteConductor.habilitada = "false"
        itemMuerteConductor.nombreTipoCobertura = "MUERTE DEL CONDUCTOR X ACCID. AUTOM."
        itemMuerteConductor.montoSumaAsegurada = "100000"
        itemMuerteConductor.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemMuerteConductor)
        Dim itemPremium = New BANORTEItems
        itemPremium.indice = "93"
        itemPremium.habilitada = "false"
        itemPremium.nombreTipoCobertura = "SERVICIOS PREMIUM"
        itemPremium.montoSumaAsegurada = "1"
        itemPremium.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemPremium)
        Dim itemAutoSustituto = New BANORTEItems
        itemAutoSustituto.indice = "94"
        itemAutoSustituto.habilitada = "false"
        itemAutoSustituto.nombreTipoCobertura = "AUTO SUSTITUTO POR ROBO TOTAL"
        itemAutoSustituto.montoSumaAsegurada = "6000"
        itemAutoSustituto.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemAutoSustituto)
        Dim itemEliminaDedu = New BANORTEItems
        itemEliminaDedu.indice = "95"
        itemEliminaDedu.habilitada = "false"
        itemEliminaDedu.nombreTipoCobertura = "ELIMINAR DEDUC./DEVOL. PRIMA X ROBO T"
        itemEliminaDedu.montoSumaAsegurada = "0"
        itemEliminaDedu.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemEliminaDedu)
        Dim itemRcOcupantes = New BANORTEItems
        itemRcOcupantes.indice = "99"
        itemRcOcupantes.habilitada = "false"
        itemRcOcupantes.nombreTipoCobertura = "RC OCUPANTES"
        itemRcOcupantes.montoSumaAsegurada = "300000"
        itemRcOcupantes.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemRcOcupantes)
        Dim itemFunerario = New BANORTEItems
        itemFunerario.indice = "100"
        itemFunerario.habilitada = "false"
        itemFunerario.nombreTipoCobertura = "SERVICIOS FUNERARIOS"
        itemFunerario.montoSumaAsegurada = "1"
        itemFunerario.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemFunerario)
        Dim itemRoboParcial = New BANORTEItems
        itemRoboParcial.indice = "5204"
        itemRoboParcial.habilitada = "false"
        itemRoboParcial.nombreTipoCobertura = "ROBO PARCIAL"
        itemRoboParcial.montoSumaAsegurada = "100000"
        itemRoboParcial.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemRoboParcial)
        Dim itemLlantaSegura = New BANORTEItems
        itemLlantaSegura.indice = "5210"
        itemLlantaSegura.habilitada = "false"
        itemLlantaSegura.nombreTipoCobertura = "LLANTA SEGURA"
        itemLlantaSegura.montoSumaAsegurada = "100000"
        itemLlantaSegura.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemLlantaSegura)
        Dim itemValorGarant = New BANORTEItems
        itemValorGarant.indice = "5216"
        itemValorGarant.habilitada = "false"
        itemValorGarant.nombreTipoCobertura = "VALOR GARANTIZADO"
        itemValorGarant.montoSumaAsegurada = "10000"
        itemValorGarant.montoDeducible = "0"
        banorteCotizacionRequest.items.Add(itemValorGarant)

        banorteCotizacionRequest.nombreVigencia = "ANUAL"
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            banorteCotizacionRequest.nombreFormaPago = "PAGO ÚNICO"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            banorteCotizacionRequest.nombreFormaPago = "SEMESTRAL"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            banorteCotizacionRequest.nombreFormaPago = "TRIMESTRAL"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            banorteCotizacionRequest.nombreFormaPago = "MENSUAL"
        End If
        Dim lg As Log = New Log()

        Try
            Dim jsonFormat = New JsonSerializerSettings
            jsonFormat.NullValueHandling = NullValueHandling.Ignore
            Dim requestUrl = JsonConvert.SerializeObject(banorteCotizacionRequest, jsonFormat)
            lg.UpdateLogRequest(requestUrl, idLogWSCot)
            Static soucrecode As String = ""
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            Dim Request As Net.HttpWebRequest = Net.WebRequest.Create(urlCotizacion)
            Request.Accept = "*/*"
            Request.Timeout = 10000
            Request.Method = "POST"
            Request.AllowAutoRedirect = True
            Request.Headers.Add("usuario", "BROKER")
            Request.Headers.Add("numOficina", "0BB")
            Request.Headers.Add("nombreRamo", "Autos")
            Request.Credentials = New NetworkCredential("Br_0bb0000", "iCpqFsgj9k")
            If Request.Method = "POST" AndAlso requestUrl IsNot Nothing Then
                Dim postBytes() As Byte = New UTF8Encoding().GetBytes(requestUrl)
                Request.ContentType = "application/json"
                Request.ContentLength = postBytes.Length
                Request.GetRequestStream().Write(postBytes, 0, postBytes.Length)
            End If
            Dim Response2 As Net.HttpWebResponse = Request.GetResponse()
            Dim ResponseStream As IO.StreamReader = New IO.StreamReader(Response2.GetResponseStream)
            soucrecode = ResponseStream.ReadToEnd()
            Response2.Close()
            ResponseStream.Close()
            Try
                lg.UpdateLogResponse(soucrecode, idLogWSCot)
            Catch ex As Exception
                Dim errr = ex
            End Try

            Dim jsonBanorteRequest = JsonConvert.DeserializeObject(Of JObject)(soucrecode)
            If (jsonBanorteRequest.SelectToken("operacion").SelectToken("codigoOperacion") = "0") Then
                ObjData.CodigoError = "Message: [" + jsonBanorteRequest.SelectToken("operacion").SelectToken("mensaje").ToString + "]"
            Else
                ObjData.Cotizacion.Resultado = "True"
                Dim polizas = jsonBanorteRequest.SelectToken("data").SelectToken("polizas")(0)
                ObjData.Cotizacion.PrimaNeta = polizas.SelectToken("resumenCotizacion").SelectToken("primaNeta")
                ObjData.Cotizacion.Derechos = polizas.SelectToken("resumenCotizacion").SelectToken("montoDerecho")
                ObjData.Cotizacion.Recargos = polizas.SelectToken("resumenCotizacion").SelectToken("montoRecargo")
                ObjData.Cotizacion.Impuesto = polizas.SelectToken("resumenCotizacion").SelectToken("iva")
                ObjData.Cotizacion.PrimaTotal = polizas.SelectToken("resumenCotizacion").SelectToken("primaTotal")
                Dim nombreCobertura = ""
                Dim Coberturas As New Coberturas
                For Each iter In polizas.SelectToken("certificado").SelectToken("items").ToList()
                    If iter.SelectToken("nombreTipoCobertura") = "ROBO TOTAL" Then
                        Coberturas.RoboTotal = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    ElseIf iter.SelectToken("nombreTipoCobertura") = "RESPONSABILIDAD CIVIL DAÑOS A TERCEROS" Then
                        Coberturas.RCBienes = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    ElseIf iter.SelectToken("nombreTipoCobertura") = "GASTOS MÉDICOS OCUPANTES" Then
                        Coberturas.GastosMedicosOcupantes = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    ElseIf iter.SelectToken("nombreTipoCobertura") = "EXTENSIÓN DE RESPONSABILIDAD CIVIL" Then
                        Coberturas.RCExtension = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    ElseIf iter.SelectToken("nombreTipoCobertura") = "ASISTENCIA JURÍDICA" Then
                        Coberturas.DefensaJuridica = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    ElseIf iter.SelectToken("nombreTipoCobertura") = "ASISTENCIA VEHICULAR" Then
                        Coberturas.AsitenciaCompleta = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    ElseIf iter.SelectToken("nombreTipoCobertura") = "MUERTE DEL CONDUCTOR X ACCID. AUTOM." Then
                        Coberturas.MuerteAccidental = "-N" & iter.SelectToken("nombreTipoCobertura").ToString & "-S" & iter.SelectToken("montoSumaAsegurada").ToString & "-D" & iter.SelectToken("montoDeducible").ToString
                    End If
                Next
                ObjData.Coberturas.Add(Coberturas)
            End If
        Catch exweb As WebException
            Try
                Using stream = exweb.Response.GetResponseStream()
                    Dim reader = New StreamReader(stream)
                    Dim msnError = reader.ReadToEnd()
                    ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + msnError + "]"
                    ObjData.Cotizacion.Resultado = "false"
                    lg.UpdateLogResponse(msnError, idLogWSCot)
                End Using
            Catch ex As Exception
                ObjData.CodigoError = "WSRequest2: [N/A]" + ", Message: [" + ex.Message + "]"
            End Try
            ObjData.Cotizacion.Resultado = "false"
        Catch e As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + e.Message + "], StackTrace: [" + e.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Dim JsonResponse As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(JsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function BANORTEemision(ByVal ObjData As Seguro, ByVal idLogWS As String)

    End Function

    Private Shared Function getTipoVehi(ByVal clave As String) As String
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "BANORTE", clave)
        Dim serializer As New JavaScriptSerializer
        Dim mapaVehiculo = serializer.Deserialize(Of Dictionary(Of String, String))(jsonVehiculo)
        Dim tipoVehi = mapaVehiculo.Item("vehicle_type")
        Return tipoVehi
    End Function

    Class BANORTECotizacionRequest
        Public Property nombreProducto As String
        Public Property claveIntermediario As String
        Public Property nombreCategoria As String
        Public Property codigoPostal As String
        Public Property municipio As String
        Public Property estado As String
        Public Property claveBanorte As String
        Public Property anio As String
        Public Property nombreUso As String
        Public Property nombreServicio As String
        Public Property respuestas As List(Of BANORTERespuesta)
        Public Property nombrePaquete As String
        Public Property items As List(Of BANORTEItems)
        Public Property nombreVigencia As String
        Public Property nombreFormaPago As String
    End Class
    Class BANORTEItems
        Public Property indice As String
        Public Property habilitada As String
        Public Property nombreTipoCobertura As String
        Public Property montoSumaAsegurada As String
        Public Property montoDeducible As String
    End Class
    Class BANORTERespuesta
        Public Property enunciado As String
        Public Property valor As String
        Public Property indice As String
    End Class

End Class
