﻿Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports System.Net.Http
Imports System.Globalization
Imports System.Data
Imports MySql.Data.MySqlClient

Public Class WsGNPNuevo

    Private Shared apiUrl As String = "https://api.service.gnp.com.mx"
    Public Shared totalSegundosRequest As String

    Public Shared Function GNPCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        'Contar tiempo de  response cotizador
        Dim inicioTotal As DateTime
        Dim pararTotal As DateTime
        Dim lapsoTiempoTotal As TimeSpan
        inicioTotal = Now

        Dim respuestaWS As String = ""
        Try
            Dim lg As Log = New Log
            Dim tipoVehi = ""
            Dim claveArmadora = ""
            Dim carroceria = ""
            Dim version = ""
            Dim client As New HttpClient
            Dim Genero = ""
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                Genero = "M"
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                Genero = "F"
            End If
            Dim marca = ObjData.Vehiculo.Marca
            If ObjData.Vehiculo.Clave.Contains("GM") Then
                marca = "GENERAL MOTORS"
            ElseIf ObjData.Vehiculo.Clave.Contains("CH") Then
                marca = "CHRYSLER"
            ElseIf ObjData.Vehiculo.Clave.Contains("FR") Then
                marca = "FORD"
            ElseIf ObjData.Vehiculo.Clave.Contains("NI") Then
                marca = "NISSAN"
            End If
            tipoVehi = ObjData.Vehiculo.Clave.Substring(0, 3)
            claveArmadora = ObjData.Vehiculo.Clave.Substring(3, 2)
            carroceria = ObjData.Vehiculo.Clave.Substring(5, 2)
            version = ObjData.Vehiculo.Clave.Substring(7, 2)
            Dim fInicio = Date.Now.ToString("yyyyMMdd")
            Dim fFin = Date.Now.AddYears(1).ToString("yyyyMMdd")
            Dim poblacion
            Try
                poblacion = ConsultaPoblacionGNP(ObjData.Cliente.Direccion.CodPostal)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de poblaciones")
            End Try
            Dim dataXML As New XmlDocument
            Dim cvePaquete = ""
            Dim periodicidad = ""
            If ObjData.Paquete = "AMPLIA" And tipoVehi = "AUT" Then
                cvePaquete = "PRP0002746"
            ElseIf ObjData.Paquete = "AMPLIA" And tipoVehi = "CA1" Then
                cvePaquete = "PRP0002790"
            ElseIf ObjData.Paquete = "AMPLIA" And tipoVehi = "CA2" Then
                cvePaquete = "PRP0002834"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehi = "AUT" Then
                cvePaquete = "PRP0002747"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehi = "CA1" Then
                cvePaquete = "PRP0002791"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehi = "CA2" Then
                cvePaquete = "PRP0002835"
            ElseIf ObjData.Paquete = "RC" And tipoVehi = "AUT" Then
                cvePaquete = "PRP0002748"
            ElseIf ObjData.Paquete = "RC" And tipoVehi = "CA1" Then
                cvePaquete = "PRP0002792"
            ElseIf ObjData.Paquete = "RC" And tipoVehi = "CA2" Then
                cvePaquete = "PRP0002836"
            End If

            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                periodicidad = "A"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                periodicidad = "M"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                periodicidad = "T"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                periodicidad = "S"
            End If


            'Contar tiempo de request a response cotizador
            Dim inicioResponseT As DateTime
            Dim pararResponseT As DateTime
            Dim lapsoTiempoResponseT As TimeSpan
            inicioResponseT = Now
            'Requets
            Dim inicio As DateTime
            Dim parar As DateTime
            Dim lapsoTiempo As TimeSpan
            inicio = Now

            Dim requestXML = "<COTIZACION>" &
                        "<SOLICITUD>" +
                        "<NUM_COTIZACION/>" &
                        "<USUARIO>IPEREZ050031</USUARIO>" &
                        "<PASSWORD>Junipg2017</PASSWORD>" &
                        "<ID_UNIDAD_OPERABLE>NOP0000059</ID_UNIDAD_OPERABLE>" &
                        "<FCH_INICIO_VIGENCIA>" & fInicio & "</FCH_INICIO_VIGENCIA>" &
                        "<FCH_FIN_VIGENCIA>" & fFin & "</FCH_FIN_VIGENCIA>" &
                        "<VIA_PAGO>IN</VIA_PAGO>" &
                        "<PERIODICIDAD>" & periodicidad & "</PERIODICIDAD>" &
                        "<ELEMENTOS>" &
                        "<ELEMENTO>" &
                        "<NOMBRE>CODIGO_PROMOCION</NOMBRE>" &
                        "<CLAVE>COP0000425</CLAVE>" &
                        "<VALOR>COP0000425</VALOR>" &
                        "</ELEMENTO>" &
                        "</ELEMENTOS>" &
                        "</SOLICITUD>" &
                        "<VEHICULO>" &
                        "<SUB_RAMO>01</SUB_RAMO>" &
                        "<TIPO_VEHICULO>" & tipoVehi & "</TIPO_VEHICULO>" &
                        "<MODELO>" & ObjData.Vehiculo.Modelo & "</MODELO>" &
                        "<ARMADORA>" & claveArmadora & "</ARMADORA>" &
                        "<DESC_ARMADORA>" & marca & "</DESC_ARMADORA>" &
                        "<CARROCERIA>" & carroceria & "</CARROCERIA>" &
                        "<VERSION>" & version & "</VERSION>" &
                        "<USO>01</USO>" &
                        "<DESC_FORMA_INDEMNIZACION/>" &
                        "<FORMA_INDEMNIZACION>03</FORMA_INDEMNIZACION>" &
                        "<VALOR_VEHICULO/>" &
                        "<VALOR_FACTURA/>" &
                        "<PLACAS/> " &
                        "<ALTO_RIESGO/>" &
                        "<VALOR_REFERENCIA/>" &
                        "<VALOR_CONVENIDO/>" &
                        "<TIPO_CARGA/>" &
                        "<REGISTRO_FEDERAL/>" &
                        "<NUM_PASAJEROS/>" &
                        "<ESTADO_CIRCULACION>" & poblacion & "</ESTADO_CIRCULACION>" &
                        "<MOTOR/>" &
                        "<SERIE/>" &
                        "<REFERENCIA/>" &
                        "<CODIGO_POSTAL>" & ObjData.Cliente.Direccion.CodPostal & "</CODIGO_POSTAL>" &
                        "<DESCRIPCION_FACTURA_VEHICULO/>" &
                        "<ADAPTACIONES_CONVERSIONES>" &
                        "<ADAPTACION_CONVERSION>" &
                        "<DESCRIPCION_EQUIPAMIENTO/>" &
                        "<MTO_FACTURACION/>" &
                        "<FCH_FACTURA_EQUIPAMIENTO/>" &
                        "<MTO_SUMA_ASEGURADA/>" &
                        "<BAN_EQUIPAMIENTO_BLINDAJE/>" &
                        "</ADAPTACION_CONVERSION>" &
                        "</ADAPTACIONES_CONVERSIONES>" &
                        "</VEHICULO>" &
                        "<CONTRATANTE>" &
                        "<TIPO_PERSONA>F</TIPO_PERSONA>" &
                        "<APELLIDO_PATERNO/>" &
                        "<APELLIDO_MATERNO/>" &
                        "<NOMBRE/>" &
                        "<CODIGO_POSTAL/>" &
                        "</CONTRATANTE>" &
                        "<CONDUCTOR>" &
                        "<FCH_NACIMIENTO/>" &
                        "<SEXO>" & Genero & "</SEXO>" &
                        "<EDAD>" & ObjData.Cliente.Edad & "</EDAD>" &
                        "<CODIGO_POSTAL>" & ObjData.Cliente.Direccion.CodPostal & "</CODIGO_POSTAL>" &
                        "</CONDUCTOR>" &
                        "<PAQUETES>" &
                        "<PAQUETE>" &
                        "<CVE_PAQUETE>" & cvePaquete & "</CVE_PAQUETE>" &
                        "<DESC_PAQUETE/>" &
                        "<COBERTURAS/>" &
                        "</PAQUETE>" &
                        "</PAQUETES>" &
                        "</COTIZACION>"
            Try
                parar = Now
                lapsoTiempo = parar.Subtract(inicio)
                totalSegundosRequest = lapsoTiempo.TotalSeconds.ToString()
                lg.UpdateLogRequestWithTime(requestXML, idLogWSCot, totalSegundosRequest)
                'Funciones.updateLogWSCot(requestXML.ToString, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try

            'Contar tiempo de  response cotizador
            Dim inicioResponse As DateTime
            Dim pararResponse As DateTime
            Dim lapsoTiempoResponse As TimeSpan
            inicioResponse = Now
            Dim inputContent As HttpContent = New StringContent(requestXML, Encoding.UTF8, "application/xml")
            Dim coberturas As New Coberturas
            Dim response As HttpResponseMessage = client.PostAsync(apiUrl & "/autos/wsp/cotizador/cotizar", inputContent).Result
            'fin tiempo Response
            Try
                Dim xmlRespuesta = response.Content.ReadAsStringAsync().Result
                'Fin de termino Response
                pararResponse = Now
                lapsoTiempoResponse = pararResponse.Subtract(inicioResponse)
                Dim totalSegundosResponse As Integer = lapsoTiempoResponse.TotalSeconds.ToString()
                Dim lge As Log = New Log()
                'fin de Total Request
                pararResponseT = Now
                lapsoTiempoResponseT = parar.Subtract(inicioResponseT)
                Dim totalSegundos = lapsoTiempoResponseT.TotalSeconds.ToString()
                Dim sumaSegundos As Double = CDbl(totalSegundosResponse) + CDbl(totalSegundosRequest)
                lge.UpdateLogResponseWithTimeP(xmlRespuesta, idLogWSCot, totalSegundosResponse, (sumaSegundos).ToString())
                ' lg.UpdateLogResponse(xmlRespuesta, idLogWSCot)
                'fin tiempo Response, request

                '                Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try

            If response.IsSuccessStatusCode = True Then
                dataXML.LoadXml(response.Content.ReadAsStringAsync().Result)
                Dim colElementos As XmlNodeList = dataXML.GetElementsByTagName("TOTAL_PRIMA")
                Dim colElementos2 As XmlNodeList = dataXML.GetElementsByTagName("COBERTURAS")
                Dim colElementos3 As XmlNodeList = dataXML.GetElementsByTagName("SOLICITUD")
                For Each objNodoPaquete In colElementos
                    Dim xmlnode = objNodoPaquete.GetElementsByTagName("CONCEPTO_ECONOMICO")
                    For Each objnodo2 In xmlnode
                        Dim concepto = objnodo2("NOMBRE").InnerText
                        If concepto = "PRIMA_NETA" Then
                            ObjData.Cotizacion.PrimaNeta = objnodo2("MONTO").InnerText
                        ElseIf concepto = "DERECHOS_POLIZA" Then
                            ObjData.Cotizacion.Derechos = objnodo2("MONTO").InnerText
                        ElseIf concepto = "IVA" Then
                            ObjData.Cotizacion.Impuesto = objnodo2("MONTO").InnerText
                        ElseIf concepto = "TOTAL_PAGAR" Then
                            ObjData.Cotizacion.PrimaTotal = objnodo2("MONTO").InnerText
                            ObjData.Cotizacion.Resultado = "True"
                        ElseIf concepto = "PRIMER_RECIBO" Then
                            ObjData.Cotizacion.PrimerPago = objnodo2("MONTO").InnerText
                        ElseIf concepto = "RECIBO_SUBSECUENTE" Then
                            ObjData.Cotizacion.PagosSubsecuentes = objnodo2("MONTO").InnerText
                        ElseIf concepto = "RECARGO_FRACCIONAMIENTO" Then
                            ObjData.Cotizacion.Recargos = objnodo2("MONTO").InnerText
                        End If
                    Next
                Next
                For Each objNodoPaquete2 In colElementos2
                    Dim xmlnode = objNodoPaquete2.GetElementsByTagName("COBERTURA")
                    For Each objnodo2 In xmlnode
                        Dim nombreCobertura = objnodo2("NOMBRE").InnerText
                        Dim descripcionCobertura As String = ""
                        Select Case (nombreCobertura)
                            Case "Daños Materiales Pérdida Total"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.DanosMateriales = descripcionCobertura.ToUpper
                            Case "Daños Materiales Pérdida Parcial"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.DanosMaterialesPP = descripcionCobertura.ToUpper
                            Case "Robo Total"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.RoboTotal = descripcionCobertura.ToUpper
                            Case "Club GNP Autos"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.AsitenciaCompleta = descripcionCobertura.ToUpper
                            Case "Protección Legal"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.DefensaJuridica = descripcionCobertura.ToUpper
                            Case "Gastos Médicos Ocupantes"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.GastosMedicosOcupantes = descripcionCobertura.ToUpper
                            Case "Responsabilidad Civil por Daños a Terceros"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D"
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.RCBienes = descripcionCobertura.ToUpper
                            Case "Extensión Cobertura Resp. Civil"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.RCExtension = descripcionCobertura.ToUpper
                            Case "Cristales"
                                descripcionCobertura &= "-N" & nombreCobertura & "-S" & objnodo2("SUMA_ASEGURADA").InnerText & "-D" & objnodo2("DEDUCIBLE").InnerText
                                descripcionCobertura.Replace("No aplica", "").ToUpper()
                                coberturas.Cristales = descripcionCobertura.ToUpper
                        End Select
                    Next
                Next
                For Each objNodoPaquete3 In colElementos3
                    ObjData.Cotizacion.IDCotizacion = objNodoPaquete3("NUM_COTIZACION").InnerText
                Next
            Else
                respuestaWS = response.ReasonPhrase
                Throw New System.Exception("Error al realizar cotizacion, respuesta del servicio no satisfactorio")
            End If
            ObjData.Coberturas.Add(coberturas)
            Dim rs = response
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Dim serializer As New JavaScriptSerializer
        Dim JsonRespose As String = serializer.Serialize(ObjData)
        Dim log As Log = New Log
        pararTotal = Now
        lapsoTiempoTotal = pararTotal.Subtract(inicioTotal)
        Dim totalSegundosTotal = lapsoTiempoTotal.TotalSeconds.ToString()
        'log.UpdateLogJsonResponse(JsonRespose, idLogWSCot)
        Dim lgh As Log = New Log()
        lgh.UpdateLogJsonResponseP(JsonRespose, idLogWSCot, totalSegundosTotal)

        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function ConsultaPoblacionGNP(ByVal CPostal As String) As String
        Try
            Dim objCotizaGNP As New GNPService.CotizadorGNPSoapClient
            Dim Municipio As XmlNode = objCotizaGNP.ObtenerCatalogo("<SolicitudCatalogo><Catalogo><Usuario>TRIGARANTEWS</Usuario><Password>Trig_WS</Password><Tipo>codigopostal</Tipo><Parametro1>" & CPostal & "</Parametro1><Parametro2>estado</Parametro2></Catalogo></SolicitudCatalogo>")
            Dim Municipios As String = Municipio.OuterXml
            Dim objXmlDocumentMun As New XmlDocument
            Dim colElmentosMun As XmlNodeList
            objXmlDocumentMun.LoadXml(Municipios)
            Dim Poblacion As String = ""
            colElmentosMun = objXmlDocumentMun.GetElementsByTagName("Elemento")
            For Each Municipio In colElmentosMun
                Poblacion = Municipio("Valor").InnerText
            Next
            Return Poblacion
        Catch ex As Exception

        End Try

    End Function


    Public Shared Function GNPEmisionOffline(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim respuestaWS As String = ""
        Try
            Dim fechaInicioVigencia = Date.Now.ToString("yyyyMMdd")
            Dim fechaFinVigencia = Date.Now.AddYears(1).ToString("yyyyMMdd")
            Dim client As New HttpClient
            Dim dataXML As New XmlDocument

            'Informacion del Vehiculo
            Dim noMotor = ObjData.Vehiculo.NoMotor
            Dim tipoVehiculo = ObjData.Vehiculo.Clave.Substring(0, 3)
            Dim modeloVehiculo = ObjData.Vehiculo.Modelo
            Dim claveArmadora = ObjData.Vehiculo.Clave.Substring(3, 2)
            Dim carroceria = ObjData.Vehiculo.Clave.Substring(5, 2)
            Dim version = ObjData.Vehiculo.Clave.Substring(7, 2)
            Dim poblacion
            Dim codigoEstado
            Try
                poblacion = ConsultaPoblacionGNP(ObjData.Cliente.Direccion.CodPostal)
                codigoEstado = GetCodEdo(poblacion)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de poblacion/estado (cp)")
            End Try
            Dim placas = ObjData.Vehiculo.NoPlacas
            Dim motor = ObjData.Vehiculo.NoMotor
            Dim serie = ObjData.Vehiculo.NoSerie
            Dim codigoPostal = ObjData.Cliente.Direccion.CodPostal

            'Informacion del contratante 
            Dim tipoPersona = ObjData.Cliente.TipoPersona
            Dim rfc = ObjData.Cliente.RFC
            Dim nombreCliente = ObjData.Cliente.Nombre
            Dim apellidoPaterno = ObjData.Cliente.ApellidoPat
            Dim apellidoMaterno = ObjData.Cliente.ApellidoMat
            Dim sexo = ""
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = "M"
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = "F"
            End If
            Dim estadoCivil = "S"
            Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyyMMdd")
            Dim calle = ObjData.Cliente.Direccion.Calle
            Dim noExterior = ObjData.Cliente.Direccion.NoExt
            Dim noInterior = ObjData.Cliente.Direccion.NoInt
            Dim colonia = ObjData.Cliente.Direccion.Colonia
            Dim delegacionMunicipio = ObjData.Cliente.Direccion.Poblacion
            Dim pais = ObjData.Cliente.Direccion.Pais
            Dim telefono = "55000000", lada = "55"
            If ObjData.Cliente.Telefono IsNot Nothing And ObjData.Cliente.Telefono.Length = 10 Then
                telefono = ObjData.Cliente.Telefono.Substring(2, 8)
                lada = ObjData.Cliente.Telefono.Substring(0, 2)
            End If
            Dim correo = ObjData.Cliente.Email
            Dim edad = ObjData.Cliente.Edad
            Dim cvePaquete = ""
            If ObjData.Paquete = "AMPLIA" And tipoVehiculo = "AUT" Then
                cvePaquete = "PRP0002746"
            ElseIf ObjData.Paquete = "AMPLIA" And tipoVehiculo = "CA1" Then
                cvePaquete = "PRP0002790"
            ElseIf ObjData.Paquete = "AMPLIA" And tipoVehiculo = "CA2" Then
                cvePaquete = "PRP0002834"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehiculo = "AUT" Then
                cvePaquete = "PRP0002747"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehiculo = "CA1" Then
                cvePaquete = "PRP0002791"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehiculo = "CA2" Then
                cvePaquete = "PRP0002835"
            ElseIf ObjData.Paquete = "RC" And tipoVehiculo = "AUT" Then
                cvePaquete = "PRP0002748"
            ElseIf ObjData.Paquete = "RC" And tipoVehiculo = "CA1" Then
                cvePaquete = "PRP0002792"
            ElseIf ObjData.Paquete = "RC" And tipoVehiculo = "CA2" Then
                cvePaquete = "PRP0002836"
            End If
            Dim periodicidad = ""
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                periodicidad = "A"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                periodicidad = "M"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                periodicidad = "T"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                periodicidad = "S"
            End If
            Dim requestXML = "<EMISION>" &
                            "<SOLICITUD>" &
                                "<USUARIO>IPEREZ050031</USUARIO>" &
                                "<PASSWORD>Junipg2017</PASSWORD>" &
                                "<ID_UNIDAD_OPERABLE>NOP0000059</ID_UNIDAD_OPERABLE>" &
                                "<NUM_COTIZACION>" & ObjData.Cotizacion.IDCotizacion & "</NUM_COTIZACION>" &
                                "<FCH_INICIO_VIGENCIA>" & fechaInicioVigencia & "</FCH_INICIO_VIGENCIA>" &
                                "<FCH_FIN_VIGENCIA>" & fechaFinVigencia & "</FCH_FIN_VIGENCIA>" &
                                "<FCH_EFECTO_MOVIMIENTO>" & fechaInicioVigencia & "</FCH_EFECTO_MOVIMIENTO>" &
                                "<FCH_FIN_EFECTO_MOVIMIENTO>" & fechaInicioVigencia & "</FCH_FIN_EFECTO_MOVIMIENTO>" &
                                "<VIA_PAGO>CL</VIA_PAGO>" &
                                "<VIA_PAGO_SUCESIVOS>IN</VIA_PAGO_SUCESIVOS>" &
                                "<PERIODICIDAD>" & periodicidad & "</PERIODICIDAD>" &
                                "<CVE_MONEDA>MXN</CVE_MONEDA>" &
                                "<BAN_RENOVACION_AUTOMATICA>1</BAN_RENOVACION_AUTOMATICA>" &
                                "<BAN_URL_IMPRESION>0</BAN_URL_IMPRESION>" &
                                "<CVE_FORMA_AJUSTE_IRREGULAR>PR</CVE_FORMA_AJUSTE_IRREGULAR>" &
                                "<BAN_CONTRA_IGUAL_CONDUCTOR>1</BAN_CONTRA_IGUAL_CONDUCTOR>" &
                                "<BAN_CONTRA_IGUAL_BENEFICIARIO>1</BAN_CONTRA_IGUAL_BENEFICIARIO>" &
                                "<BAN_AFECTA_BONO>0</BAN_AFECTA_BONO>" &
                                "<OPERACION>P</OPERACION>" &
                                "<ELEMENTOS>" &
                                    "<ELEMENTO>" &
                                        "<NOMBRE>CODIGO_PROMOCION</NOMBRE>" &
                                        "<CLAVE>COP0000425</CLAVE>" &
                                        "<VALOR>COP0000425</VALOR>" &
                                    "</ELEMENTO>" &
                                "</ELEMENTOS>" &
                            "</SOLICITUD>" &
                            "<AGENTES/>" &
                             "<VEHICULO>" &
                                "<SUB_RAMO>01</SUB_RAMO>" &
                                "<TIPO_VEHICULO>" & tipoVehiculo & "</TIPO_VEHICULO>" &
                                "<MODELO>" & modeloVehiculo & "</MODELO>" &
                                "<ARMADORA>" & claveArmadora & "</ARMADORA>" &
                                "<CARROCERIA>" & carroceria & "</CARROCERIA>" &
                                "<VERSION>" & version & "</VERSION>" &
                                "<USO>01</USO>" &
                                "<VALOR_VEHICULO></VALOR_VEHICULO>" &
                                "<ESTADO_CIRCULACION>" & codigoEstado & "</ESTADO_CIRCULACION>" &
                                "<FORMA_INDEMNIZACION>03</FORMA_INDEMNIZACION>" &
                                "<TIPO_CARGA></TIPO_CARGA>" &
                                "<PLACAS>PERMISO</PLACAS>" &
                                "<ALTO_RIESGO>0</ALTO_RIESGO>" &
                                "<MOTOR>" & motor & "</MOTOR>" &
                                "<SERIE>" & serie & "</SERIE>" &
                                "<CODIGO_POSTAL>" & codigoPostal & "</CODIGO_POSTAL>" &
                            "</VEHICULO>" &
                             "<CONTRATANTE>" &
                                "<TIPO_PERSONA>F</TIPO_PERSONA>" &
                                "<RFC>" & rfc & "</RFC>" &
                                "<NOMBRES>" & nombreCliente & "</NOMBRES>" &
                                "<APELLIDO_PATERNO>" & apellidoPaterno & "</APELLIDO_PATERNO>" &
                                "<APELLIDO_MATERNO>" & apellidoMaterno & "</APELLIDO_MATERNO>" &
                                "<SEXO>" & sexo & "</SEXO>" &
                                "<ESTADO_CIVIL>" & estadoCivil & "</ESTADO_CIVIL>" &
                                "<FCH_NACIMIENTO>" & fechaNacimiento & "</FCH_NACIMIENTO>" &
                                "<NACIONALIDAD>MEX</NACIONALIDAD>" &
                                "<PAIS_NACIMIENTO>MEX</PAIS_NACIMIENTO>" &
                                "<DIRECCION>" &
                                    "<CVE_TIPO_VIA>CL</CVE_TIPO_VIA>" &
                                    "<CALLE>" & calle & "</CALLE>" &
                                    "<NUMERO_EXTERIOR>" & noExterior & "</NUMERO_EXTERIOR>" &
                                    "<COLONIA>" & colonia & "</COLONIA>" &
                                    "<DELEGACION_MCPIO>" & delegacionMunicipio & "</DELEGACION_MCPIO>" &
                                    "<ESTADO>" & codigoEstado & "</ESTADO>" &
                                    "<CODIGO_POSTAL>" & codigoPostal & "</CODIGO_POSTAL>" &
                                    "<PAIS_DOMICILIO>MEX</PAIS_DOMICILIO>" &
                                "</DIRECCION>" &
                                "<TELEFONOS>" &
                                    "<TELEFONO>" &
                                        "<CVE_LADA>" & lada & "</CVE_LADA>" &
                                        "<CVE_LADA_NACIONAL>" & lada & "</CVE_LADA_NACIONAL>" &
                                        "<NUMERO_TELEFONO>" & telefono & "</NUMERO_TELEFONO>" &
                                    "</TELEFONO>" &
                                "</TELEFONOS>" &
                                "<CORREOS>" &
                                    "<CORREO>" &
                                        "<CORREO_ELECTRONICO>" & correo & "</CORREO_ELECTRONICO>" &
                                    "</CORREO>" &
                                "</CORREOS>" &
                            "</CONTRATANTE>" &
                            "<CONDUCTOR>" &
                                "<RFC>" & rfc & "</RFC>" &
                                "<NOMBRES>" & nombreCliente & "</NOMBRES>" &
                                "<APELLIDO_PATERNO>" & apellidoPaterno & "</APELLIDO_PATERNO>" &
                                "<APELLIDO_MATERNO>" & apellidoMaterno & "</APELLIDO_MATERNO>" &
                                "<SEXO>" & sexo & "</SEXO>" &
                                "<ESTADO_CIVIL>" & estadoCivil & "</ESTADO_CIVIL>" &
                                "<FCH_NACIMIENTO>" & fechaNacimiento & "</FCH_NACIMIENTO>" &
                                "<EDAD_CONDUCTOR_HABITUAL>" & edad & "</EDAD_CONDUCTOR_HABITUAL>" &
                            "</CONDUCTOR>" &
                            "<BENEFICIARIOS>" &
                                "<BENEFICIARIO>" &
                                    "<BAN_IRREVOCABLE></BAN_IRREVOCABLE>" &
                                    "<NOMBRES></NOMBRES>" &
                                    "<APELLIDO_PATERNO></APELLIDO_PATERNO>" &
                                    "<APELLIDO_MATERNO></APELLIDO_MATERNO>" &
                                    "<PCT_BENEFICIO></PCT_BENEFICIO>" &
                                    "<TIPO_PERSONA></TIPO_PERSONA>" &
                                "</BENEFICIARIO>" &
                            "</BENEFICIARIOS>" &
                            "<PAQUETE>" &
                                 "<CVE_PAQUETE>" & cvePaquete & "</CVE_PAQUETE>" &
                            "</PAQUETE>" &
                            "<IMPORTES>" &
                                "<PRIMA_TOTAL>" & ObjData.Cotizacion.PrimaTotal.Replace("$", "").Replace(",", "") & "</PRIMA_TOTAL>" &
                                "<IMP_IVA>" & ObjData.Cotizacion.Impuesto.Replace("$", "").Replace(",", "") & "</IMP_IVA>" &
                                "<PRIMA_NETA>" & ObjData.Cotizacion.PrimaNeta.Replace("$", "").Replace(",", "") & "</PRIMA_NETA>" &
                            "</IMPORTES>" &
                        "</EMISION>"

            Try
                Dim id = Funciones.updateLogWS(requestXML, idLogWS, "RequestWS")
            Catch ex As Exception
            End Try

            Dim inputContent As HttpContent = New StringContent(requestXML, Encoding.UTF8, "application/xml")
            Dim response As HttpResponseMessage = client.PostAsync(apiUrl & "/autos/wsp/emisor/emisor/emitir/offline", inputContent).Result
            respuestaWS = response.Content.ReadAsStringAsync().Result
            Try
                Funciones.updateLogWS(respuestaWS, idLogWS, "ResponseWS")
            Catch ex As Exception
            End Try
            ObjData.Emision.Resultado = "False"
            If response.IsSuccessStatusCode = True Then
                dataXML.LoadXml(respuestaWS)
                Dim nodoSolicitud As XmlNodeList = dataXML.GetElementsByTagName("SOLICITUD")
                Dim nodoConceptosEconomicos As XmlNodeList = dataXML.GetElementsByTagName("CONCEPTOS_ECONOMICOS")
                For Each objNodoPaquete In nodoConceptosEconomicos
                    Dim xmlnode = objNodoPaquete.GetElementsByTagName("CONCEPTO_ECONOMICO")
                    For Each objnodo2 In xmlnode
                        Dim concepto = objnodo2("NOMBRE").InnerText
                        If concepto = "PRIMA_TOTAL" Then
                            ObjData.Emision.PrimaTotal = objnodo2("MONTO").InnerText
                        ElseIf concepto = "IMP_PRIMER_RECIBO" Then
                            ObjData.Emision.PrimerPago = objnodo2("MONTO").InnerText
                        ElseIf concepto = "IMP_RECIBO_SUBSECUENTE" Then
                        ElseIf concepto = "RECARGO_DESCUENTO" Then
                        ElseIf concepto = "PRIMA_TECNICA" Then
                        ElseIf concepto = "PRIMA_NETA" Then
                            ObjData.Emision.PrimaNeta = objnodo2("MONTO").InnerText
                        ElseIf concepto = "PRIMA_CEDIDA" Then
                        ElseIf concepto = "RECARGO_PAGO_FRACC" Then
                        ElseIf concepto = "IVA" Then
                            ObjData.Emision.Impuesto = objnodo2("MONTO").InnerText
                        ElseIf concepto = "DERECHOS_POLIZA" Then
                            ObjData.Emision.Derechos = objnodo2("MONTO").InnerText
                        ElseIf concepto = "DESCUENTO" Then
                        ElseIf concepto = "NUM_RECIBOS_SUB" Then
                        ElseIf concepto = "NUM_PAGOS" Then
                        End If
                    Next
                Next
                For Each objNodoPaquete3 In nodoSolicitud
                    ObjData.Emision.IDCotizacion = objNodoPaquete3("NUM_COTIZACION").InnerText
                Next
                Dim key = "5uPx0PzUxp7xOFVZg5TITA=="
                Dim codPromocion = "COP0000425"
                ObjData.Emision.Recargos = 0
                ObjData.Emision.PagosSubsecuentes = 0.0
                ObjData.Emision.Terminal = "https://gnpventamasiva.com.mx/Portal/pagotv.aspx?TipOP=WSP&IdT=" & ObjData.Emision.IDCotizacion & "&Mac=" & ObjData.Emision.PrimerPago & "&codPromo=" & codPromocion & "&idLlave=" & key & "&URLResp=" & ObjData.urlRedireccion
                ObjData.Emision.Terminal = Replace(ObjData.Emision.Terminal, " ", "")

                'Try
                '    Dim bytesDocumento = "", nombrePdfPoliza = ""
                '    Dim nodoDocumento As XmlNodeList = dataXML.GetElementsByTagName("DOCUMENTO")
                '    For Each nodoObjDocumento In nodoDocumento
                '        bytesDocumento = nodoObjDocumento("BYTES_DOCUMENTO").InnerText
                '        nombrePdfPoliza = nodoObjDocumento("NOMBRE_DOCUMENTO").InnerText
                '    Next
                '    Dim binaryData As Byte()
                '    binaryData = Convert.FromBase64String(bytesDocumento)
                '    File.WriteAllBytes("W:\Documentos\Proyecto\ws-ali-ahorraseguros\Archivo\" + nombrePdfPoliza, binaryData)
                '    ObjData.Emision.Documento = "core.alimx.mx/Archivo/" + nombrePdfPoliza
                'Catch ex As Exception
                '    ObjData.CodigoError = "Message: [Error al imprimir la poliza]"
                '    ObjData.Emision.Resultado = "false"
                'End Try
            Else
                Dim colElementos As XmlNodeList
                dataXML.LoadXml(respuestaWS)
                colElementos = dataXML.GetElementsByTagName("ERROR")
                For Each iterElementos In colElementos
                    respuestaWS = iterElementos("DESCRIPCION").InnerText
                    Exit For
                Next
                Throw New System.Exception("Error al realizar emision, respuesta del servicio no satisfactorio")
            End If
        Catch ex As Exception
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function GNPEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim respuestaWS As String = ""
        Try
            Dim fechaInicioVigencia = Date.Now.ToString("yyyyMMdd")
            Dim fechaFinVigencia = Date.Now.AddYears(1).ToString("yyyyMMdd")
            Dim client As New HttpClient
            Dim dataXML As New XmlDocument

            'Informacion del Vehiculo
            Dim noMotor = ObjData.Vehiculo.NoMotor
            Dim tipoVehiculo = ObjData.Vehiculo.Clave.Substring(0, 3)
            Dim modeloVehiculo = ObjData.Vehiculo.Modelo
            Dim claveArmadora = ObjData.Vehiculo.Clave.Substring(3, 2)
            Dim carroceria = ObjData.Vehiculo.Clave.Substring(5, 2)
            Dim version = ObjData.Vehiculo.Clave.Substring(7, 2)
            Dim poblacion
            Dim codigoEstado
            Try
                poblacion = ConsultaPoblacionGNP(ObjData.Cliente.Direccion.CodPostal)
                codigoEstado = GetCodEdo(poblacion)
            Catch ex As Exception
                Throw New System.Exception("Error al realizar consulta de poblacion/estado (cp)")
            End Try
            Dim placas = ObjData.Vehiculo.NoPlacas
            Dim motor = ObjData.Vehiculo.NoMotor
            Dim serie = ObjData.Vehiculo.NoSerie
            Dim codigoPostal = ObjData.Cliente.Direccion.CodPostal

            'Informacion del contratante 
            Dim tipoPersona = ObjData.Cliente.TipoPersona
            Dim rfc = ObjData.Cliente.RFC
            Dim nombreCliente = ObjData.Cliente.Nombre
            Dim apellidoPaterno = ObjData.Cliente.ApellidoPat
            Dim apellidoMaterno = ObjData.Cliente.ApellidoMat
            Dim sexo = ""
            If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
                sexo = "M"
            ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
                sexo = "F"
            End If
            Dim estadoCivil = "S"
            Dim fechaNacimiento = Convert.ToDateTime(ObjData.Cliente.FechaNacimiento, System.Globalization.CultureInfo.CreateSpecificCulture("es-MX")).ToString("yyyyMMdd")
            Dim calle = ObjData.Cliente.Direccion.Calle
            Dim noExterior = ObjData.Cliente.Direccion.NoExt
            Dim noInterior = ObjData.Cliente.Direccion.NoInt
            Dim colonia = ObjData.Cliente.Direccion.Colonia
            Dim delegacionMunicipio = ObjData.Cliente.Direccion.Poblacion
            Dim pais = ObjData.Cliente.Direccion.Pais
            Dim telefono = ObjData.Cliente.Telefono
            Dim correo = ObjData.Cliente.Email
            Dim edad = ObjData.Cliente.Edad
            Dim cvePaquete = ""
            If ObjData.Paquete = "AMPLIA" And tipoVehiculo = "AUT" Then
                cvePaquete = "PRP0002746"
            ElseIf ObjData.Paquete = "AMPLIA" And tipoVehiculo = "CA1" Then
                cvePaquete = "PRP0002790"
            ElseIf ObjData.Paquete = "AMPLIA" And tipoVehiculo = "CA2" Then
                cvePaquete = "PRP0002834"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehiculo = "AUT" Then
                cvePaquete = "PRP0002747"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehiculo = "CA1" Then
                cvePaquete = "PRP0002791"
            ElseIf ObjData.Paquete = "LIMITADA" And tipoVehiculo = "CA2" Then
                cvePaquete = "PRP0002835"
            ElseIf ObjData.Paquete = "RC" And tipoVehiculo = "AUT" Then
                cvePaquete = "PRP0002748"
            ElseIf ObjData.Paquete = "RC" And tipoVehiculo = "CA1" Then
                cvePaquete = "PRP0002792"
            ElseIf ObjData.Paquete = "RC" And tipoVehiculo = "CA2" Then
                cvePaquete = "PRP0002836"
            End If
            Dim periodicidad = ""
            If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
                periodicidad = "A"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
                periodicidad = "M"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
                periodicidad = "T"
            ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
                periodicidad = "S"
            End If
            Dim requestXML = "<EMISION>" &
                        "<SOLICITUD>" &
                            "<USUARIO>IPEREZ050031</USUARIO>" &
                            "<PASSWORD>Junipg2017</PASSWORD>" &
                            "<ID_UNIDAD_OPERABLE>NOP0000059</ID_UNIDAD_OPERABLE>" &
                            "<NUM_COTIZACION>" & ObjData.Cotizacion.IDCotizacion & "</NUM_COTIZACION>" &
                            "<FCH_INICIO_VIGENCIA>" & fechaInicioVigencia & "</FCH_INICIO_VIGENCIA>" &
                            "<FCH_FIN_VIGENCIA>" & fechaFinVigencia & "</FCH_FIN_VIGENCIA>" &
                            "<FCH_EFECTO_MOVIMIENTO>" & fechaInicioVigencia & "</FCH_EFECTO_MOVIMIENTO>" &
                            "<FCH_FIN_EFECTO_MOVIMIENTO>" & fechaInicioVigencia & "</FCH_FIN_EFECTO_MOVIMIENTO>" &
                            "<VIA_PAGO>CL</VIA_PAGO>" &
                            "<VIA_PAGO_SUCESIVOS>IN</VIA_PAGO_SUCESIVOS>" &
                            "<PERIODICIDAD>" & periodicidad & "</PERIODICIDAD>" &
                            "<CVE_MONEDA>MXN</CVE_MONEDA>" &
                            "<BAN_RENOVACION_AUTOMATICA>1</BAN_RENOVACION_AUTOMATICA>" &
                            "<BAN_URL_IMPRESION>0</BAN_URL_IMPRESION>" &
                            "<CVE_FORMA_AJUSTE_IRREGULAR>PR</CVE_FORMA_AJUSTE_IRREGULAR>" &
                            "<BAN_CONTRA_IGUAL_CONDUCTOR>1</BAN_CONTRA_IGUAL_CONDUCTOR>" &
                            "<BAN_CONTRA_IGUAL_BENEFICIARIO>1</BAN_CONTRA_IGUAL_BENEFICIARIO>" &
                            "<BAN_AFECTA_BONO>N</BAN_AFECTA_BONO>" &
                            "<OPERACION>P</OPERACION>" &
                            "<ELEMENTOS>" &
                                "<ELEMENTO>" &
                                    "<NOMBRE>CODIGO_PROMOCION</NOMBRE>" &
                                    "<CLAVE>COP0000425</CLAVE>" &
                                    "<VALOR>COP0000425</VALOR>" &
                                "</ELEMENTO>" &
                            "</ELEMENTOS>" &
                        "</SOLICITUD>" &
                        "<AGENTES/>" &
                         "<VEHICULO>" &
                            "<SUB_RAMO>01</SUB_RAMO>" &
                            "<TIPO_VEHICULO>" & tipoVehiculo & "</TIPO_VEHICULO>" &
                            "<MODELO>" & modeloVehiculo & "</MODELO>" &
                            "<ARMADORA>" & claveArmadora & "</ARMADORA>" &
                            "<CARROCERIA>" & carroceria & "</CARROCERIA>" &
                            "<VERSION>" & version & "</VERSION>" &
                            "<USO>01</USO>" &
                            "<VALOR_VEHICULO></VALOR_VEHICULO>" &
                            "<ESTADO_CIRCULACION>" & codigoEstado & "</ESTADO_CIRCULACION>" &
                            "<FORMA_INDEMNIZACION>03</FORMA_INDEMNIZACION>" &
                            "<TIPO_CARGA></TIPO_CARGA>" &
                            "<PLACAS>PERMISO</PLACAS>" &
                            "<ALTO_RIESGO>0</ALTO_RIESGO>" &
                            "<MOTOR>" & motor & "</MOTOR>" &
                            "<SERIE>" & serie & "</SERIE>" &
                            "<CODIGO_POSTAL>" & codigoPostal & "</CODIGO_POSTAL>" &
                        "</VEHICULO>" &
                         "<CONTRATANTE>" &
                            "<TIPO_PERSONA>F</TIPO_PERSONA>" &
                            "<RFC>" & rfc & "</RFC>" &
                            "<NOMBRES>" & nombreCliente & "</NOMBRES>" &
                            "<APELLIDO_PATERNO>" & apellidoPaterno & "</APELLIDO_PATERNO>" &
                            "<APELLIDO_MATERNO>" & apellidoMaterno & "</APELLIDO_MATERNO>" &
                            "<SEXO>" & sexo & "</SEXO>" &
                            "<ESTADO_CIVIL>" & estadoCivil & "</ESTADO_CIVIL>" &
                            "<FCH_NACIMIENTO>" & fechaNacimiento & "</FCH_NACIMIENTO>" &
                            "<NACIONALIDAD>MEX</NACIONALIDAD>" &
                            "<PAIS_NACIMIENTO>MEX</PAIS_NACIMIENTO>" &
                            "<DIRECCION>" &
                                "<CVE_TIPO_VIA>CL</CVE_TIPO_VIA>" &
                                "<CALLE>" & calle & "</CALLE>" &
                                "<NUMERO_EXTERIOR>" & noExterior & "</NUMERO_EXTERIOR>" &
                                "<COLONIA>" & colonia & "</COLONIA>" &
                                "<DELEGACION_MCPIO>" & delegacionMunicipio & "</DELEGACION_MCPIO>" &
                                "<ESTADO>" & codigoEstado & "</ESTADO>" &
                                "<CODIGO_POSTAL>" & codigoPostal & "</CODIGO_POSTAL>" &
                                "<PAIS_DOMICILIO>MEX</PAIS_DOMICILIO>" &
                            "</DIRECCION>" &
                            "<TELEFONOS>" &
                                "<TELEFONO>" &
                                    "<CVE_LADA>55</CVE_LADA>" &
                                    "<CVE_LADA_NACIONAL>55</CVE_LADA_NACIONAL>" &
                                    "<NUMERO_TELEFONO>" & telefono & "</NUMERO_TELEFONO>" &
                                "</TELEFONO>" &
                            "</TELEFONOS>" &
                            "<CORREOS>" &
                                "<CORREO>" &
                                    "<CORREO_ELECTRONICO>" & correo & "</CORREO_ELECTRONICO>" &
                                "</CORREO>" &
                            "</CORREOS>" &
                        "</CONTRATANTE>" &
                        "<CONDUCTOR>" &
                            "<RFC>" & rfc & "</RFC>" &
                            "<NOMBRES>" & nombreCliente & "</NOMBRES>" &
                            "<APELLIDO_PATERNO>" & apellidoPaterno & "</APELLIDO_PATERNO>" &
                            "<APELLIDO_MATERNO>" & apellidoMaterno & "</APELLIDO_MATERNO>" &
                            "<SEXO>" & sexo & "</SEXO>" &
                            "<ESTADO_CIVIL>" & estadoCivil & "</ESTADO_CIVIL>" &
                            "<FCH_NACIMIENTO>" & fechaNacimiento & "</FCH_NACIMIENTO>" &
                            "<EDAD_CONDUCTOR_HABITUAL>" & edad & "</EDAD_CONDUCTOR_HABITUAL>" &
                        "</CONDUCTOR>" &
                        "<BENEFICIARIOS>" &
                            "<BENEFICIARIO>" &
                                "<BAN_IRREVOCABLE></BAN_IRREVOCABLE>" &
                                "<NOMBRES></NOMBRES>" &
                                "<APELLIDO_PATERNO></APELLIDO_PATERNO>" &
                                "<APELLIDO_MATERNO></APELLIDO_MATERNO>" &
                                "<PCT_BENEFICIO></PCT_BENEFICIO>" &
                                "<TIPO_PERSONA></TIPO_PERSONA>" &
                            "</BENEFICIARIO>" &
                        "</BENEFICIARIOS>" &
                        "<PAQUETE>" &
                             "<CVE_PAQUETE>" & cvePaquete & "</CVE_PAQUETE>" &
                        "</PAQUETE>" &
                        "<IMPORTES>" &
                            "<PRIMA_TOTAL>" & ObjData.Cotizacion.PrimaTotal.Replace("$", "").Replace(",", "") & "</PRIMA_TOTAL>" &
                            "<IMP_IVA>" & ObjData.Cotizacion.Impuesto.Replace("$", "").Replace(",", "") & "</IMP_IVA>" &
                            "<PRIMA_NETA>" & ObjData.Cotizacion.PrimaNeta.Replace("$", "").Replace(",", "") & "</PRIMA_NETA>" &
                        "</IMPORTES>" &
                    "</EMISION>"

            Try
                Dim id = Funciones.updateLogWS(requestXML, idLogWS, "RequestWS")
            Catch ex As Exception
            End Try

            Dim inputContent As HttpContent = New StringContent(requestXML, Encoding.UTF8, "application/xml")
            Dim response As HttpResponseMessage = client.PostAsync(apiUrl & "/autos/wsp/emisor/emisor/emitir", inputContent).Result
            respuestaWS = response.Content.ReadAsStringAsync().Result
            Try
                Funciones.updateLogWS(respuestaWS, idLogWS, "ResponseWS")
            Catch ex As Exception
            End Try
            If response.IsSuccessStatusCode = True Then
                dataXML.LoadXml(respuestaWS)
                Dim nodoSolicitud As XmlNodeList = dataXML.GetElementsByTagName("SOLICITUD")
                Dim nodoConceptosEconomicos As XmlNodeList = dataXML.GetElementsByTagName("CONCEPTOS_ECONOMICOS")
                For Each objNodoPaquete In nodoConceptosEconomicos
                    Dim xmlnode = objNodoPaquete.GetElementsByTagName("CONCEPTO_ECONOMICO")
                    For Each objnodo2 In xmlnode
                        Dim concepto = objnodo2("NOMBRE").InnerText
                        If concepto = "TOTAL_PAGAR" Then
                            ObjData.Emision.PrimaTotal = objnodo2("MONTO").InnerText
                            ObjData.Emision.Resultado = "False"
                        ElseIf concepto = "IMP_PRIMER_RECIBO" Then
                            ObjData.Emision.PrimerPago = objnodo2("MONTO").InnerText
                        ElseIf concepto = "IMP_RECIBO_SUBSECUENTE" Then
                        ElseIf concepto = "RECARGO_DESCUENTO" Then
                        ElseIf concepto = "PRIMA_TECNICA" Then
                        ElseIf concepto = "PRIMA_NETA" Then
                            ObjData.Emision.PrimaNeta = objnodo2("MONTO").InnerText
                        ElseIf concepto = "PRIMA_CEDIDA" Then
                        ElseIf concepto = "RECARGO_PAGO_FRACC" Then
                        ElseIf concepto = "IVA" Then
                            ObjData.Emision.Impuesto = objnodo2("MONTO").InnerText
                        ElseIf concepto = "DERECHOS_POLIZA" Then
                            ObjData.Emision.Derechos = objnodo2("MONTO").InnerText
                        ElseIf concepto = "DESCUENTO" Then
                        ElseIf concepto = "NUM_RECIBOS_SUB" Then
                        ElseIf concepto = "NUM_PAGOS" Then
                        End If
                    Next
                Next
                For Each objNodoPaquete3 In nodoSolicitud
                    ObjData.Emision.IDCotizacion = objNodoPaquete3("NUM_COTIZACION").InnerText
                Next
                Dim key = "5uPx0PzUxp7xOFVZg5TITA=="
                Dim codPromocion = "COP0000425"
                ObjData.Emision.Terminal = "https://gnpventamasiva.com.mx/Portal/pagotv.aspx?TipOP=WSP&IdT=" & ObjData.Emision.IDCotizacion & "&Mac=" & ObjData.Emision.PrimerPago & "&codPromo=" & codPromocion & "&idLlave=" & key & "&URLResp=" & ObjData.urlRedireccion
                ObjData.Emision.Terminal = Replace(ObjData.Emision.Terminal, " ", "")
            Else
                Dim colElementos As XmlNodeList
                dataXML.LoadXml(respuestaWS)
                colElementos = dataXML.GetElementsByTagName("ERROR")
                For Each iterElementos In colElementos
                    respuestaWS = iterElementos("DESCRIPCION").InnerText
                    Exit For
                Next
                Throw New System.Exception("Error al realizar emision, respuesta del servicio no satisfactorio")
            End If
        Catch ex As Exception
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Dim serializer As New JavaScriptSerializer
        Return serializer.Serialize(ObjData)
    End Function


    Public Shared Function GetCodEdo(ByVal Estado As String) As String

        Dim CodEdo As String = ""
        Dim val As String = "False"
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        Dim sql = "SELECT * FROM `ws_db`.`Ali_R_CatEdosGNP` where Edo = '" + Estado + "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(sql, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    val = "True"
                End While
                CodEdo = reader("IdEdo")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return CodEdo

    End Function

    Public Shared Function GNPImpresion(ByVal poliza As String)
        Dim respuesta = ""
        Try
            Dim apiUrl As String = "https://api.service.gnp.com.mx"
            Dim client As New HttpClient
            Dim requestXML = "<IMPRESION_POLIZA>" &
                            "<USUARIO>IPEREZ050031</USUARIO>" &
                            "<PASSWORD>Junipg2017</PASSWORD>" &
                            "<NUM_POLIZA>" + poliza + "</NUM_POLIZA>" &
                            "<NUM_VERSION>0</NUM_VERSION>" &
                            "<EXTENSION_ARCHIVO>PDF</EXTENSION_ARCHIVO>" &
                            "</IMPRESION_POLIZA>"
            Dim inputContent As HttpContent = New StringContent(requestXML, Encoding.UTF8, "application/xml")
            Dim coberturas As New Coberturas
            Dim dataXML As New XmlDocument
            Dim response As HttpResponseMessage = client.PostAsync(apiUrl & "/autos/wsp/impresion/buscarPoliza", inputContent).Result
            If response.IsSuccessStatusCode = True Then
                Dim jsonImpresion As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(response.Content.ReadAsStringAsync().Result)
                For Each Row3 In jsonImpresion
                    For Each Row4 In Row3.Value
                        If DirectCast(Row4, Newtonsoft.Json.Linq.JProperty).Name = "URL_DOCUMENTO" Then
                            respuesta = DirectCast(Row4.First, Newtonsoft.Json.Linq.JValue).Value
                        End If
                    Next
                Next
            Else
                Dim rs = response
            End If
        Catch ex As Exception
        End Try
        Dim serializer As New JavaScriptSerializer
        Return respuesta
    End Function

End Class
