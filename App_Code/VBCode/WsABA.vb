﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.Xml

Public Class WsABA
    Public Shared usuario As String = "WSTRIGARANTE"
    Public Shared password As String = "VIRTUAL1$"
    Public Shared inicioTotal As DateTime
    Public Shared parar As DateTime
    Public Shared lapsoTiempo As TimeSpan
    Public Shared Function ABACotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim xmlIn As String = ""
        Dim xmlOut As String = ""
        Dim ObjABA As New ABACotProd.ACCotizacionClient
        Dim agrupacion As String = "207115"
        Dim token As New ABACotProd.Token()
        token.usuario = usuario 'usuario con permisos para usar el servicio de conexión
        token.password = password 'password del usuario con permisos para usar el servicio de conexión

        Dim Paquete As String = ObjData.Paquete
        Dim FormaPago As String = ObjData.PeriodicidadDePago.ToString 'ObtenerFormaPago(Datos(1)) 
        Dim Descripcion As String = ObjData.Vehiculo.Descripcion
        Dim Modelo As String = ObjData.Vehiculo.Modelo
        Dim Marca As String = ObjData.Vehiculo.Marca
        Dim CPostal As String = ObjData.Cliente.Direccion.CodPostal
        Dim ClaveBase As String = ObjData.Vehiculo.Clave
        Dim Edad As String = ObjData.Cliente.Edad
        Dim Genero As String = ObjData.Cliente.Genero


        Dim Agente = "93197"
        Dim Negocio = ObtenerNegocios()
        Dim Conducto = ObtenerConducto(Negocio, Agente)
        Dim Tarifa = "170"
        Dim Municipios = ObtnerMunicipio(Negocio, CPostal)

        Dim objCotizacion As New ABACotProd.ACCotizacionClient
        Dim objToken As New ABACotProd.Token

        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim colElementos2 As XmlNodeList
        Dim colElementos4 As XmlNodeList
        Dim colElementos5 As XmlNodeList
        Dim objNodo As XmlNode
        Dim objNodo2 As XmlNode
        Dim objNodo4 As XmlNode
        Dim objNodo5 As XmlNode

        Dim objDataSet As New DataSet()
        Dim strCotizacion As String
        Dim iIndice As Integer = 0
        Dim Respuesta(10) As String

        objToken.usuario = usuario
        objToken.password = password

        Dim descuento As String = ObjData.Descuento
        'Descuento
        Dim InicioVigencia = ""
        InicioVigencia = DateTime.Now().ToString("yyyy-MM-dd")

        Dim diaFinal As Date = CDate(InicioVigencia).AddYears(1) 'hoy.AddYears(1)
        Dim FinVigencia = diaFinal.ToString("yyyy-MM-dd")
        'tiempo de request
        inicioTotal = Now
        'Paquete
        If Paquete = "Amplia" Or Paquete = "AMPLIA" Then
            Paquete = "1"
        ElseIf Paquete = "Limitada" Or Paquete = "LIMITADA" Then
            Paquete = "2"
        End If
        'FormaPago
        If FormaPago = "Mensual" Or FormaPago = "MENSUAL" Or FormaPago = "mensual" Then
            FormaPago = "34" ' 34 mensual sin recargos con cobro de derechos
        ElseIf FormaPago = "Semestral" Or FormaPago = "SEMESTRAL" Then
            FormaPago = "2"
        ElseIf FormaPago = "Contado" Or FormaPago = "CONTADO" Or FormaPago = "contado" Or FormaPago = "ANUAL" Then
            FormaPago = "12"
        End If
        Dim DM As String = "0.05"
        Dim RT As String = "0.10"
        Dim RC As String = "1500000"
        Dim RCFAMILIAR As String = "500000"
        Dim RCPF As String = "3000000"
        Dim RCPersonas As String = "100000"
        Dim GMO As String = "400000"
        Dim AAC As String = "100000"
        Dim AL As String = "4500000"
        Dim TotalCoberturas = (ObjData.Coberturas).Count

        Dim Cobs As XElement
        If Paquete = "1" Then  ' Amplia
            Cobs = <COBS>
                       <COB>
                           <COBID>1</COBID><!-- DAÑOS MATERIALES -->
                           <SUMAASEG>0</SUMAASEG>
                           <DADIC/>
                           <DPCT><%= DM %></DPCT>
                       </COB>
                       <COB>
                           <COBID>2</COBID><!-- ROBO TOTAL -->
                           <SUMAASEG>0</SUMAASEG>
                           <DADIC/>
                           <DPCT><%= RT %></DPCT>
                       </COB>
                       <COB>
                           <COBID>3</COBID><!-- RESPONSABILIDAD CIVIL TERCEROS  -->
                           <SUMAASEG><%= RC %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>110</COBID><!-- RESPONSABILIDAD CIVIL FAMILIAR-->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>486</COBID><!-- RESPONSABILIDAD CIVIL POR FALLECIMIENTO PLUS  -->
                           <SUMAASEG><%= RCPF %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>552</COBID><!-- RESPONSABILIDAD CIVIL EN USA Y CANADÁ -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>561</COBID><!-- RESPONSABILIDAD CIVIL PERSONAS -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>732</COBID><!-- ACCIDENTES PERSONALES -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>4</COBID><!-- GASTOS MÉDICOS OCUPANTES -->
                           <SUMAASEG><%= GMO %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>49</COBID><!-- ACCIDENTES AUTOMOVILÍSTICOS AL CONDUCTOR -->
                           <SUMAASEG><%= AAC %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>78</COBID><!-- ASISTENCIA LEGAL PROVIAL -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>114</COBID><!-- GESTORIA VIAL -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>116</COBID><!-- ASISTENCIA EN VIAJE IKE -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>393</COBID><!-- AUTO RELEVO PLUS -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                   </COBS>
        ElseIf Paquete = "2" Then  ' Limitada
            Cobs = <COBS>
                       <COB>
                           <COBID>2</COBID><!-- ROBO TOTAL -->
                           <SUMAASEG>0</SUMAASEG>
                           <DADIC/>
                           <DPCT><%= RT %></DPCT>
                       </COB>
                       <COB>
                           <COBID>3</COBID><!-- RESPONSABILIDAD CIVIL TERCEROS  -->
                           <SUMAASEG><%= RC %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>110</COBID><!-- RESPONSABILIDAD CIVIL FAMILIAR-->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>486</COBID><!-- RESPONSABILIDAD CIVIL POR FALLECIMIENTO PLUS  -->
                           <SUMAASEG><%= RCPF %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>552</COBID><!-- RESPONSABILIDAD CIVIL EN USA Y CANADÁ -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>561</COBID><!-- RESPONSABILIDAD CIVIL PERSONAS -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>732</COBID><!-- ACCIDENTES PERSONALES -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>4</COBID><!-- GASTOS MÉDICOS OCUPANTES -->
                           <SUMAASEG><%= GMO %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>49</COBID><!-- ACCIDENTES AUTOMOVILÍSTICOS AL CONDUCTOR -->
                           <SUMAASEG><%= AAC %></SUMAASEG>
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>78</COBID><!-- ASISTENCIA LEGAL PROVIAL -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>114</COBID><!-- GESTORIA VIAL -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>116</COBID><!-- ASISTENCIA EN VIAJE IKE -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                       <COB>
                           <COBID>393</COBID><!-- AUTO RELEVO PLUS -->
                           <DADIC/>
                           <DPCT>0</DPCT>
                       </COB>
                   </COBS>
        Else

        End If
        Dim strXMLDatosCot As XElement =
        <COT><DG><NEG><%= Negocio %></NEG>
            <AGE><%= Agente %></AGE>
            <CON><%= Conducto %></CON>
            <TAR><%= Tarifa %></TAR>

            <INIVIG><%= InicioVigencia %></INIVIG>
            <FINVIG><%= FinVigencia %></FINVIG>
            <TS>2</TS><!-- Tipo de Subscripcion obtener de catalogos se cambia a dos -->
            <AGRUPA><%= agrupacion %></AGRUPA>
            <TC>1</TC><!-- Tipo de Cobro Obtener de Catalogos -->
            <FP><%= FormaPago %></FP><!-- Forma de PAgo Obtener de catalogos-->
            </DG>
            <INCISOS>
                <INCISO>
                    <!--<ID> <%= IDDesc %> </ID>-->
                    <CVEVEH><%= ClaveBase %></CVEVEH>
                    <MOD><%= Modelo %></MOD>
                    <PAQ><%= Paquete %></PAQ><!--Paquetes Seleccionar de catalogo-->
                    <EDO><%= Municipios(1) %></EDO>
                    <MUN><%= Municipios(0) %></MUN>
                    <SERV>1</SERV>
                    <USO>1</USO>
                    <TDED>1</TDED>
                    <TSA>1</TSA><!--Tipo de Suma Asegurada Seleccionar del catalogo-->
                    <PD><%= descuento %></PD>
                    <PB>0</PB>
                    <%= Cobs %>
                </INCISO>
            </INCISOS>
        </COT>
        Dim lg As Log = New Log()
        Try
            lg.UpdateLogRequest(strXMLDatosCot.ToString, idLogWSCot)

            'Funciones.updateLogWSCot(strXMLDatosCot.ToString, idLogWSCot, "RequestWS", "FechaInicio")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Try
            xmlOut = objCotizacion.CotizaAuto(objToken, strXMLDatosCot.ToString)
            parar = Now
            lapsoTiempo = parar.Subtract(inicioTotal)
            Dim totalSegundos = lapsoTiempo.TotalSeconds.ToString("0.000000")
            Try
                lg.UpdateLogResponseWithTime(xmlOut, idLogWSCot, totalSegundos)

                ' Funciones.updateLogWSCot(xmlOut, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try
            ObjData.Cotizacion.Resultado = "True"
        Catch ex As ServiceModel.FaultException(Of ABACotProd.Error)
            xmlOut = String.Format("Ocurrio un error en el WCF:\n " +
        "Mensaje: {0}\n " + ":: Origen: {1}\n " +
            "Stack: {2}", ex.Detail.Mensaje, ex.Detail.Origen, ex.Detail.StackTrace)
            ObjData.Cotizacion.Resultado = "False"
        End Try

        objXmlDocument.LoadXml(xmlOut)
        colElementos = objXmlDocument.GetElementsByTagName("COT")

        For Each objNodo In colElementos
            ObjData.Cotizacion.CotID = objNodo("COTID").InnerText()
            ObjData.Cotizacion.IDCotizacion = objNodo("COTID").InnerText()
            ObjData.Cotizacion.VerID = objNodo("VERID").InnerText()
        Next
        colElementos2 = objXmlDocument.GetElementsByTagName("INCISO")

        For Each objNodo2 In colElementos2
            ObjData.Cotizacion.CotIncID = objNodo2("COTINCID").InnerText()
            ObjData.Cotizacion.VerIncID = objNodo2("VERINCID").InnerText()
            ObjData.Cotizacion.PrimaNeta = objNodo2("PNETA").InnerText() - objNodo2("DES").InnerText()
            Respuesta(4) = objNodo2("PNETA").InnerText() - objNodo2("DES").InnerText()
            ObjData.Cotizacion.Impuesto = objNodo2("IVA").InnerText()
            Respuesta(5) = objNodo2("IVA").InnerText()
            ObjData.Cotizacion.PrimaTotal = objNodo2("PTOTAL").InnerText()
            ObjData.Cotizacion.Derechos = objNodo2("DER").InnerText()
        Next

        ' Obtiene Valor de costo para primer recibo a pagar
        Dim reciboObtenido As String = ""
        Dim primaTotalRecibo As String = ""
        colElementos4 = objXmlDocument.GetElementsByTagName("RECIBOS")
        colElementos5 = objXmlDocument.GetElementsByTagName("RECIBO")

        For Each objNodo4 In colElementos4
            For Each objNodo5 In colElementos5
                reciboObtenido = objNodo5("NUM").InnerText()
                If reciboObtenido = 1 Then
                    primaTotalRecibo = objNodo5("PTOTAL").InnerText()
                ElseIf reciboObtenido = 2 Then
                    Respuesta(10) = objNodo5("PTOTAL").InnerText()
                    Exit For
                End If
            Next
        Next

        If (FormaPago = "12" And reciboObtenido = "1") Or (FormaPago = "34" And (reciboObtenido = "1" Or reciboObtenido = "2")) Then
            ObjData.Cotizacion.PrimerPago = primaTotalRecibo
        Else
            ObjData.Cotizacion.PrimerPago = Respuesta(6)
        End If
        Dim coberturas As New Coberturas
        Dim objXmlDocumentRes As New XmlDocument
        objXmlDocumentRes.LoadXml(xmlOut)
        Dim objXmlNode As XmlNode
        For Each objXmlNode In objXmlDocumentRes.GetElementsByTagName("COB")
            Dim descripcionCobertura As String = ""
            If objXmlNode("SEL").InnerText = "1" Then
                If objXmlNode("DEDESC").InnerText <> "" Then
                    descripcionCobertura &= "-N" & objXmlNode("DESC").InnerText & "-SAmparada-D" & objXmlNode("DEDESC").InnerText
                Else
                    descripcionCobertura &= "-N" & objXmlNode("DESC").InnerText & "-SAmparada-D" & "No aplica"
                End If
            Else
                If objXmlNode("DEDESC").InnerText <> "" And (objXmlNode("DESC").InnerText = "DAÑOS MATERIALES" Or objXmlNode("DESC").InnerText = "ROBO TOTAL") Then
                    descripcionCobertura &= "-N" & objXmlNode("DESC").InnerText & "-SV. Comercial-D" & objXmlNode("DEDESC").InnerText & ""
                ElseIf objXmlNode("DEDESC").InnerText <> "" Then
                    descripcionCobertura &= "-N" & objXmlNode("DESC").InnerText & "-S" & objXmlNode("SUMAASEG").InnerText & "-D" & objXmlNode("DEDESC").InnerText
                Else
                    descripcionCobertura &= "-N" & objXmlNode("DESC").InnerText & "-S" & objXmlNode("SUMAASEG").InnerText & "-D" & "No aplica"
                End If
            End If

            Select Case (objXmlNode("DESC").InnerText)
                Case "DAÑOS MATERIALES"
                    coberturas.DanosMateriales = descripcionCobertura.Replace("DANOS MATERIALES", "DAÑOS MATERIALES")
                Case "ROBO TOTAL"
                    coberturas.RoboTotal = descripcionCobertura
                Case "ASISTENCIA EN VIAJE IKE *"
                    coberturas.AsitenciaCompleta = descripcionCobertura.Replace("NO APLICA", "")
                Case "ASISTENCIA LEGAL PROVIAL *"
                    coberturas.DefensaJuridica = descripcionCobertura.Replace("NO APLICA", "")
                Case "GASTOS MÉDICOS OCUPANTES"
                    coberturas.GastosMedicosOcupantes = descripcionCobertura.Replace("NO APLICA", "")
                Case "RESPONSABILIDAD CIVIL POR DAÑOS A TERCEROS"
                    coberturas.RCBienes = descripcionCobertura.Replace("-D0", "-D")
                Case "RESPONSABILIDAD CIVIL PERSONAS"
                    coberturas.RCPersonas = descripcionCobertura.Replace("-D0", "-D")
                Case "RESPONSABILIDAD CIVIL POR FALLECIMIENTO"
                    coberturas.MuerteAccidental = descripcionCobertura.Replace("NO APLICA", "")
                Case "RESPONSABILIDAD  CIVIL  FAMILIAR"
                    coberturas.RCFamiliar = descripcionCobertura.Replace("NO APLICA", "")
                Case "RESPONSABILIDAD CIVIL USA ACE"
                    coberturas.RCExtranjero = descripcionCobertura.Replace("NO APLICA", "")
            End Select
        Next
        ObjData.Coberturas.Add(coberturas)

        Dim serializer As New JavaScriptSerializer
        Dim jsonResponse As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(jsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)

    End Function
    Private Shared Function ObtnerMunicipio(ByVal Negocio As String, ByVal Cpostal As String) As String()
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim objNodo As XmlNode
        Dim Respuesta(2) As String
        Dim ObjCatalogo = New ABACatalogueServiceProd.ACCatalogosClient
        Dim ObjToken = New ABACatalogueServiceProd.Token
        ObjToken.usuario = usuario
        ObjToken.password = password
        Dim StrEntrada = "<CAT><NEG>" & Negocio & "</NEG><CP>" & Cpostal & "</CP></CAT>"
        Dim strRespuesta = ObjCatalogo.ObtenerMunicipioPorCP(ObjToken, StrEntrada)
        objXmlDocument.LoadXml(strRespuesta)
        colElementos = objXmlDocument.GetElementsByTagName("CAT")
        For Each objNodo In colElementos
            Respuesta(0) = objNodo("MPO").InnerText()
            Respuesta(1) = objNodo("EDO").InnerText()
        Next
        Return Respuesta
    End Function
    Private Shared Function ObtenerNegocios() As String
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim objNodo As XmlNode
        Dim Respuesta As String
        Dim ObjCatalogo = New ABACatalogueServiceProd.ACCatalogosClient
        Dim ObjToken = New ABACatalogueServiceProd.Token
        ObjToken.usuario = usuario
        ObjToken.password = password
        Dim StrEntrada = ""
        Dim strRespuesta = ObjCatalogo.ObtenerNegocios(ObjToken, StrEntrada)
        objXmlDocument.LoadXml(strRespuesta)
        colElementos = objXmlDocument.GetElementsByTagName("NEGOCIO")
        For Each objNodo In colElementos
            Respuesta = objNodo("NEG").InnerText()
        Next
        Return Respuesta
    End Function
    Private Shared Function ObtenerConducto(ByVal Negocio As String, ByVal Agente As String) As String
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim objNodo As XmlNode
        Dim Respuesta As String
        Dim ObjCatalogo = New ABACatalogueServiceProd.ACCatalogosClient
        Dim ObjToken = New ABACatalogueServiceProd.Token
        ObjToken.usuario = usuario
        ObjToken.password = password
        Dim StrEntrada = "<CAT><NEG>" & Negocio & "</NEG><ID>" & Agente & "</ID></CAT>"
        Dim strRespuesta = ObjCatalogo.ObtenerConductos(ObjToken, StrEntrada)
        Respuesta = "0"
        Return Respuesta
    End Function
    Public Shared Function ABAEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim Tipo As String = ObjData.Cliente.TipoPersona
        If Tipo = "F" Then
            Tipo = 0
        Else
            Tipo = 1
        End If
        Dim nombreComp = ObjData.Cliente.Nombre & " " & ObjData.Cliente.ApellidoPat & " " & ObjData.Cliente.ApellidoMat 'ClienteObj(3) 'Split(ClienteObj(3), " ")

        Dim NomCom As String
        Dim RFC As String = ObjData.Cliente.RFC
        Dim HClave As String

        Dim MyRFC As String
        Dim MyNumero As String
        If Tipo = 0 Then
            If RFC.Length > 10 Then
                MyRFC = RFC
                RFC = RFC.Substring(0, RFC.Length - 3)
                HClave = MyRFC.Substring(10, 3)
            End If
        Else
            If RFC.Length > 9 Then
                MyRFC = RFC
                RFC = RFC.Substring(0, RFC.Length - 3)
                HClave = MyRFC.Substring(9, 3)
            End If
        End If

        Dim modelo As String = ObjData.Vehiculo.Modelo 'CotizacionObj(46)
        Dim marca As String = ObjData.Vehiculo.Marca 'C1otizacionObj(10)
        Dim PNom As String
        Dim SNom As String
        Dim App As String
        Dim Apm As String
        Dim Sexo As String
        Dim EdoCivil As String
        Dim tPersona As String = Tipo
        Dim RazonSocial As String
        Dim Giro As String = "" ' Definir            
        Dim Regimen As String = "" ' Definir         
        Dim TipoSociedad As String = "" ' Definir   
        Dim Descripcion = ObjData.Vehiculo.Descripcion 'CotizacionObj(47)

        If tPersona = "0" Then          ' Persona Física
            RFC = RFC
            HClave = HClave
            PNom = ObjData.Cliente.Nombre
            SNom = "" 'nombreComp(1)
            App = ObjData.Cliente.ApellidoPat 'ClienteObj(4)
            Apm = ObjData.Cliente.ApellidoMat 'ClienteObj(5)
            Sexo = ObjData.Cliente.Genero
            EdoCivil = "SOLTERO"
        ElseIf tPersona = "1" Then      ' Persona Moral
            RFC = RFC
            HClave = HClave
            RazonSocial = ObjData.Cliente.Nombre 'AGREGAR RAZON SOCIAL AL SUPER OBJETO
            NomCom = nombreComp
            Giro = ""
            Regimen = ""
            TipoSociedad = ""
        End If

        Dim TipoDir As String = "1" ' Definir
        Dim Calle As String = ObjData.Cliente.Direccion.Calle 'ClienteObj(13)
        Dim NumExt As String = ObjData.Cliente.Direccion.NoExt 'ClienteObj(15)
        Dim NumInt As String = ObjData.Cliente.Direccion.NoInt 'ClienteObj(16)
        Dim Col As String = ObjData.Cliente.Direccion.Colonia 'ClienteObj(14)
        Dim CP As String = ObjData.Cliente.Direccion.CodPostal ' ClienteObj(17)
        Dim Pob As String = ObjData.Cliente.Direccion.Poblacion 'ClienteObj(19)
        Dim Lada As String = "" 'ClienteObj(6)
        Dim Numero As String = ObjData.Cliente.Telefono 'ClienteObj(6)

        Dim CelLada As String
        Dim CelNumero As String = ObjData.Cliente.Telefono 'ClienteObj(8)
        Dim Correo As String = ObjData.Cliente.Email
        Dim FechaConst As String = DateTime.Now ' Definir

        Dim CotID As String = ObjData.Cotizacion.CotID
        Dim VerID As String = ObjData.Cotizacion.VerID
        Dim CotIncID As String = ObjData.Cotizacion.CotIncID
        Dim VerIncID As String = ObjData.Cotizacion.VerIncID

        Dim Serie As String = ObjData.Vehiculo.NoSerie
        Dim Ref As String = ""
        Dim Motor As String = ObjData.Vehiculo.NoMotor
        Dim Placas As String = ObjData.Vehiculo.NoPlacas

        If Numero.Length > 8 Then
            MyNumero = Numero
            Numero = Numero.Substring(2, 8)
            Lada = MyNumero.Substring(0, 2)
        End If
        If CelNumero.Length > 8 Then
            MyNumero = CelNumero
            CelNumero = CelNumero.Substring(2, 8)
            CelLada = MyNumero.Substring(0, 2)
        End If

        'Parseo de variables
        If Sexo = "MASCULINO" Or Sexo = "masculino" Or Sexo = "Masculino" Or Sexo = "0" Then
            Sexo = "1"
        Else
            Sexo = "0"
        End If
        If EdoCivil = "VIUDO" Then
            EdoCivil = "3"
        ElseIf EdoCivil = "CASADO" Then
            EdoCivil = "1"
        Else
            EdoCivil = "2"
        End If

        Dim TranID As String
        Dim PID As String
        Dim DirID As String
        Dim CPId As String = ObjData.Cliente.Direccion.CodPostal  ' CP de Cotización
        Dim ExistePersona = ConsultarPersonas(Tipo, RFC, HClave)
        If ExistePersona <> "" Then
            Dim Direcciones = ConsultarDireccion(Tipo, ExistePersona)
            PID = ExistePersona
            TranID = Direcciones(1)
            DirID = Direcciones(0)
            CP = Direcciones(2) ' CP de dirección registrada inicialmente

            If CP.Length = 4 Then
                CP = "0" + CP
            End If

            Dim nCP = ObjData.Cliente.Direccion.CodPostal  ' Nuevo cp en caso de requerir nueva dirección
            If CPId <> CP Then
                Dim DireccionesPersonas = RegistraDireccionPersona(Tipo, PID, 1, Calle, NumExt, NumInt, Col, nCP, Pob)
                TranID = DireccionesPersonas(0)   ' Identificador de la transacción. Este valor debe conservarse ya que es necesario para emitir la póliza. 
                DirID = DireccionesPersonas(2)    ' TDirID = Descripcion="Identificador temporal de la dirección. El definitivo se generará al emitir la póliza.
            End If

        Else
            Dim Persona = registraPersona(Tipo, RFC, HClave, PNom, SNom, App, Apm, Sexo, EdoCivil, TipoDir, Calle, NumExt, NumInt, Col, CP, Pob, Lada, Numero, CelLada, CelNumero, Correo, RazonSocial, FechaConst, NomCom, Giro, Regimen, TipoSociedad)
            PID = Persona(1)
            TranID = Persona(0)
            DirID = Persona(2)
        End If

        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim colElementos2 As XmlNodeList
        Dim Respuesta(11) As String
        Dim ObjEmision = New ABAIssuanceService.ACEmisionClient
        Dim ObjToken = New ABAIssuanceService.Token
        ObjToken.usuario = usuario

        ObjToken.password = password
        Dim StrEmision As XElement
        StrEmision =
                       <EMI>
                           <COTID><%= CotID %></COTID>
                           <VERID><%= VerID %></VERID>
                           <INCISOS>
                               <INCISO>
                                   <COTINCID><%= CotIncID %></COTINCID>
                                   <VERINCID><%= VerIncID %></VERINCID>
                                   <DE>
                                       <ASEGID><%= PID %></ASEGID>
                                       <ASEGDIRID><%= DirID %></ASEGDIRID>
                                       <ASEGTRANID><%= TranID %></ASEGTRANID>
                                       <PROPID><%= PID %></PROPID>
                                       <PROPDIRID><%= DirID %></PROPDIRID>
                                       <PROPTRANID><%= TranID %></PROPTRANID>
                                   </DE>
                                   <SERIE><%= Serie %></SERIE>
                                   <REF><%= Ref %></REF>
                                   <MOTOR><%= Motor %></MOTOR>
                                   <PLACAS><%= Placas %></PLACAS>
                               </INCISO>
                           </INCISOS>
                       </EMI>

        Try
            Try
                Funciones.updateLogWS(StrEmision.ToString, idLogWS, "RequestWS")
            Catch ex As Exception
            End Try
            Dim strRespuesta = ObjEmision.EmitePoliza(ObjToken, StrEmision.ToString)
            Try
                Funciones.updateLogWS(strRespuesta, idLogWS, "ResponseWS")
            Catch ex As Exception
            End Try

            Dim objNodo As XmlNode
            objXmlDocument.LoadXml(strRespuesta)
            colElementos = objXmlDocument.GetElementsByTagName("CVE")
            Dim Clave = colElementos(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("POL")
            ObjData.Emision.Poliza = colElementos2(1).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("END")
            Respuesta(1) = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("PNETA")
            ObjData.Emision.PrimaNeta = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("DER")
            ObjData.Emision.Derechos = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("DES")
            Respuesta(4) = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("IVA")
            ObjData.Emision.Impuesto = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("PTOTAL")
            ObjData.Emision.PrimaTotal = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("REC")
            ObjData.Emision.Recargos = colElementos2(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("BENEFID")
            Respuesta(10) = colElementos2(0).InnerText
            Dim Ruta = imprimeABA(Clave, ObjData.Emision.Poliza)
            ObjData.Emision.Documento = Ruta

            Dim trc = ""
            Dim NsPVal = ""
            If (ObjData.Pago.Carrier = SuperObjeto.Pago.CatCarrier.VISA Or ObjData.Pago.Carrier = SuperObjeto.Pago.CatCarrier.MASTERCARD) And UCase(ObjData.Pago.MedioPago) = "CREDITO" Then
                NsPVal = "v"
                trc = "WSVENTASPYP"
            ElseIf UCase(ObjData.Pago.MedioPago) = "DEBITO" Then ' Visa  medio de pago = debito 
                NsPVal = "d"
                trc = "WSVENTASPROG"
            ElseIf UCase(ObjData.Pago.Carrier) = "AMEX" And UCase(ObjData.Pago.MedioPago) = "CREDITO" Then ' american express(2) (CARRIER)
                NsPVal = "a"
                trc = "WSVENTASPYP"
            End If

            trc = "WSVENTASPYP"

            Dim cis = 1                                                                                ' Número de inciso (si son altas iniciales se envia como 1)

            Dim kli = ObjData.urlRedireccion  ' PRODUCCIÓN URL a donde hará redirect después de terminar el cobro 

            Dim cnzd = 103823                                                                           ' PRODUCCIÓN ' ABA proporciona este valor / ' ID del usuario utilizado, dato constante proporcionado por Aba para cada negocio (“que usuario consume el sistema”) (sera un valor diferente dependiendo del ambiente enque se este trabajando)  

            Dim bab = 1                                                                                ' Moneda de la póliza
            Dim kgb = 2                                                                                ' ABA proporciona este valor / ' Oficina donde se emitió la póliza  

            Dim NeI = 120                                                                              ' ID del negocio de cobranza, dato constante proporcionado por Aba
            Dim aId = 3                                                                                ' RamoId de acuerdo al Ramo de la póliza.
            Dim vcr = 0                                                                                ' Requerir correo electrónico, si se requiere que la interfaz de Cargos en línea, requiera de manera obligatoria el correo electrónico del titular de la tarjeta enviar en “1”, en caso contrario “0” o no enviar.
            Dim vce = ObjData.Cliente.Email 'ClienteObj(20)                                                                   ' "prueba@prueba.com" ' Dirección de correo electrónico, para que aparezca como sugerencia en la interfaz de cargos en línea. Éste parámetro es opcional.
            Dim NsP = NsPVal                                                                           ' Variable para configura los medios de pago a mostrar, se tiene que enviar un string con las siguientes iniciales y el orden que desean que se despliegue en el combo, a = Para AMEX ó v = Para VISA / MasterCard ó d = Para Debito y Clabe (Ejemplos: av = Amex y Visa / Mastercard, va = Visa / MasterCard y Amex
            Dim NmA = PNom + " " + App + " " + Apm                                                     ' Nombre del Tarjetahabiente, asegurado o propietario

            Dim alt64 = "veI=" & Clave & "+za=" & ObjData.Emision.Poliza & "+cis=" & cis & "+bab=" & bab & "+kgb=" & kgb & "+kli=" & kli & "+trc=" & trc & "+cnzd=" & cnzd & "+aId=" & aId & "+vcr=" & vcr & "+NmA=" & NmA

            ' Convirtiendo en Base 64
            Dim alt = Convert.ToBase64String(New System.Text.ASCIIEncoding().GetBytes(alt64))

            'Redirigiento a pagina Produccion
            If ObjData.Emision.Poliza <> "" Then
                ObjData.Emision.Terminal = "http://www5.abaseguros.com/Cobranza.Pagos.UI/Pagos.aspx?alt=" & alt
            End If
            ObjData.Emision.Poliza = Clave + ObjData.Emision.Poliza
            ObjData.Emision.Resultado = "False"
        Catch ex As Exception
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Dim serializer As New JavaScriptSerializer

        Return serializer.Serialize(ObjData)
    End Function

    Private Shared Function RegistraDireccionPersona(ByVal tp As String, ByVal pid As String, ByVal tipoDir As String, ByVal calle As String, ByVal noExt As String, ByVal nInt As String, ByVal col As String, ByVal cp As String, ByVal pob As String) As String()
        Dim Respuesta(3) As String
        Try
            Dim objXmlDocument As New XmlDocument()
            Dim colElementos As XmlNodeList
            Dim colElementos2 As XmlNodeList
            Dim colElementos3 As XmlNodeList
            Dim proxy As New ABARegisterService.PCRegistroClient
            Dim ObjToken As New ABARegisterService.Token
            ObjToken.usuario = usuario
            ObjToken.password = password

            Dim StrEntrada = "<XML><DP>"
            StrEntrada &= "<TP>" & tp & "</TP>"
            StrEntrada &= "<PID>" & pid & "</PID>"
            StrEntrada &= "<DOMICILIO><TIPODIR>1</TIPODIR>"
            StrEntrada &= "<CALLE>" & calle & "</CALLE>"
            StrEntrada &= "<NUMEXT>" & noExt & "</NUMEXT>"
            StrEntrada &= "<NUMINT>" & nInt & "</NUMINT>"
            StrEntrada &= "<COL>" & col & "</COL>"
            StrEntrada &= "<CP>" & cp & "</CP>"
            StrEntrada &= "<POB>" & pob & "</POB>"
            StrEntrada &= "</DOMICILIO></DP></XML>"

            Dim strRespuesta = proxy.RegistraDireccionPersona(ObjToken, StrEntrada)
            objXmlDocument.LoadXml(strRespuesta)
            colElementos = objXmlDocument.GetElementsByTagName("TRANID")
            Respuesta(0) = colElementos(0).InnerText
            colElementos2 = objXmlDocument.GetElementsByTagName("TPID")
            Respuesta(1) = colElementos2(0).InnerText
            colElementos3 = objXmlDocument.GetElementsByTagName("TDIRID")
            Respuesta(2) = colElementos3(0).InnerText
        Catch ex As ServiceModel.FaultException(Of ABARegisterService.Error)
            Respuesta(0) = String.Format("Ocurrio un error en el WCF:\n " +
                                   "Mensaje: {0}\n " + ":: Origen: {1}\n " +
                                   "Stack: {2}", ex.Detail.Mensaje, ex.Detail.Origen, ex.Detail.StackTrace)
        End Try

        Return Respuesta

    End Function

    Private Shared Function ConsultarPersonas(ByVal Tipo As String, ByVal RFC As String, ByVal HClave As String) As String
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList

        Dim Respuesta As String
        Dim ObjPersona = New ABAConsultService.PCConsultasClient
        Dim ObjToken = New ABAConsultService.Token
        ObjToken.usuario = usuario
        ObjToken.password = password
        Dim StrEntrada = "<XML><DP><TP>" & Tipo & "</TP>"
        StrEntrada &= "<RFC>" & RFC & "</RFC>"
        StrEntrada &= "<HCVE>" & HClave & "</HCVE></DP></XML>"
        Dim strRespuesta = ObjPersona.ConsultaPersonas(ObjToken, StrEntrada)
        objXmlDocument.LoadXml(strRespuesta)
        colElementos = objXmlDocument.GetElementsByTagName("PID")
        If colElementos.Count > 0 Then
            Respuesta = colElementos(0).InnerText
        End If
        Return Respuesta
    End Function
    Private Shared Function ConsultarDireccion(ByVal Tipo As String, ByVal PID As String) As String()
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim colElementos2 As XmlNodeList
        Dim colElementos3 As XmlNodeList
        Dim Respuesta(3) As String
        Dim ObjPersona = New ABAConsultService.PCConsultasClient
        Dim ObjToken = New ABAConsultService.Token
        ObjToken.usuario = usuario
        ObjToken.password = password
        Dim StrEntrada = "<XML><DP><TP>" & Tipo & "</TP>"
        StrEntrada &= "<PID>" & PID & "</PID>"
        StrEntrada &= "</DP></XML>"
        Dim strRespuesta = ObjPersona.ConsultaDireccionesPersona(ObjToken, StrEntrada)
        objXmlDocument.LoadXml(strRespuesta)
        colElementos = objXmlDocument.GetElementsByTagName("DIRID")
        Respuesta(0) = colElementos(0).InnerText
        colElementos2 = objXmlDocument.GetElementsByTagName("TRANID")
        Respuesta(1) = colElementos2(0).InnerText
        colElementos3 = objXmlDocument.GetElementsByTagName("CP")
        Respuesta(2) = colElementos3(0).InnerText

        Return Respuesta
    End Function
    Private Shared Function registraPersona(ByVal Tipo As String, ByVal RFC As String, ByVal HClave As String, ByVal PNom As String, ByVal SNom As String, ByVal App As String, ByVal Apm As String, ByVal Sexo As String, ByVal EdoCivil As String,
ByVal TipoDir As String, ByVal Calle As String, ByVal NumExt As String, ByVal NumInt As String, ByVal Col As String, ByVal CP As String, ByVal Pob As String, ByVal Lada As String, ByVal Numero As String,
    ByVal CelLada As String, ByVal CelNumero As String, ByVal Correo As String, ByVal RazonSocial As String, ByVal FechaConst As String,
    ByVal NomCom As String, ByVal Giro As String, ByVal Regimen As String, ByVal TipoSociedad As String) As String()
        ' Tipo: tipo de persona 0 fisica 1 moral
        ' Sexo 0 Femenino, 1 Masculino
        'posibles valores 1: Casado,2: Soltero,3: Viudo ,4:Divorciado
        'TipoSociedad posibles valores: 1: SA,2: CV,3: RL,4: AC,5: SC,6: AF,7: AP,8: AR,9: IP,10: MI"
        'TipoDir posibles valores son 1-Particular,2-Domicilio Extranjero, 3-Fiscal
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim colElementos2 As XmlNodeList
        Dim colElementos3 As XmlNodeList
        Dim objNodo As XmlNode
        Dim objNodo2 As XmlNode
        Dim objNodo3 As XmlNode
        Dim Respuesta(3) As String
        Dim ObjPersona = New ABARegisterService.PCRegistroClient
        Dim ObjToken = New ABARegisterService.Token
        ObjToken.usuario = usuario
        ObjToken.password = password

        Dim StrEntrada = "<XML>"
        StrEntrada &= "<DP><TP>" & Tipo & "</TP>"
        If Tipo = 0 Then
            StrEntrada &= "<FISICA>"
            StrEntrada &= "<RFC>" & RFC & "</RFC>"
            StrEntrada &= "<HCVE>" & HClave & "</HCVE>"
            StrEntrada &= "<PNOM>" & PNom & "</PNOM>"
            StrEntrada &= "<SNOM>" & SNom & "</SNOM>"
            StrEntrada &= "<APP>" & App & "</APP>"
            StrEntrada &= "<APM>" & Apm & "</APM>"
            StrEntrada &= "<SEXO>" & Sexo & "</SEXO>"
            StrEntrada &= "<EDOCIVIL>" & EdoCivil & "</EDOCIVIL>"
            StrEntrada &= "</FISICA>"
        Else
            ' Si es Persona Moral -->
            StrEntrada &= "<MORAL>"
            StrEntrada &= "<RFC>" & RFC & "</RFC>"
            StrEntrada &= "<HCVE>" & HClave & "</HCVE>"
            StrEntrada &= "<RS>" & RazonSocial & "</RS>"
            StrEntrada &= "<FECHACONST>" & FechaConst & "</FECHACONST>"
            StrEntrada &= "<NOMCOM>" & NomCom & "</NOMCOM>"
            StrEntrada &= "<GIRO>" & Giro & "</GIRO>"
            StrEntrada &= "<REGIMEN>" & Regimen & "</REGIMEN>"
            StrEntrada &= "<TS>" & RFC & "</TS>"
            StrEntrada &= "</MORAL>"
        End If
        StrEntrada &= "<DOMICILIO>"
        StrEntrada &= "<TIPODIR>" & TipoDir & "</TIPODIR>"
        StrEntrada &= "<CALLE>" & Calle & "</CALLE>"
        StrEntrada &= "<NUMEXT>" & NumExt & "</NUMEXT>"
        If NumInt <> "" Then
            StrEntrada &= "<NUMINT>" & NumInt & "</NUMINT>"
        End If
        StrEntrada &= "<COL>" & Col & "</COL>"
        StrEntrada &= "<CP>" & CP & "</CP>"
        StrEntrada &= "<POB>" & Pob & "</POB>"
        StrEntrada &= "</DOMICILIO>"

        StrEntrada &= "<TELEFONO>"
        StrEntrada &= "<LADA>" & Lada & "</LADA>"
        StrEntrada &= "<NUMERO>" & Numero & "</NUMERO>"
        StrEntrada &= "</TELEFONO>"

        StrEntrada &= "<CELULAR>"
        StrEntrada &= "<LADA>" & CelLada & "</LADA>"
        StrEntrada &= "<NUMERO>" & CelNumero & "</NUMERO>"
        StrEntrada &= "</CELULAR>"

        StrEntrada &= "<CORREO>" & Correo & "</CORREO>"
        StrEntrada &= "</DP>"
        StrEntrada &= "</XML>"
        Dim strRespuesta = ObjPersona.ConsultaRegistraPersona(ObjToken, StrEntrada)
        objXmlDocument.LoadXml(strRespuesta)
        colElementos = objXmlDocument.GetElementsByTagName("TRANSACCION")

        For Each objNodo In colElementos
            Respuesta(0) = objNodo("TRANID").InnerText()
        Next
        colElementos2 = objXmlDocument.GetElementsByTagName("PERSONA")

        For Each objNodo2 In colElementos2
            Respuesta(1) = objNodo2("TPID").InnerText()
        Next

        colElementos3 = objXmlDocument.GetElementsByTagName("DIRECCION")

        For Each objNodo3 In colElementos3
            Respuesta(2) = objNodo3("TDIRID").InnerText()
        Next
        Return Respuesta
    End Function
    Private Shared Function imprimeABA(ByVal Clave As String, ByVal Poliza As String) As String
        Dim objXmlDocument As New XmlDocument()
        Dim colElementos As XmlNodeList
        Dim objNodo As XmlNode
        Dim Respuesta As String
        Dim ObjImpresion = New ABAPrintService.ACImpresionClient
        Dim ObjToken = New ABAPrintService.Token
        ObjToken.usuario = usuario
        ObjToken.password = password
        Dim StrEntrada = "<IM>"
        StrEntrada &= "<CLAVE>" & Clave & "</CLAVE>"
        StrEntrada &= "<POL>" & Poliza & "</POL>"
        StrEntrada &= "<INCISO>1</INCISO>"
        StrEntrada &= "<MOD>AUTOSAGENTES</MOD>"
        StrEntrada &= "<TPOAPP>14</TPOAPP>"
        StrEntrada &= "</IM>"
        Dim strRespuesta = ObjImpresion.ImpresionSegmentacion(ObjToken, StrEntrada)
        objXmlDocument.LoadXml(strRespuesta)
        colElementos = objXmlDocument.GetElementsByTagName("PATHPDF")
        Respuesta = colElementos(0).InnerText
        Return Respuesta
    End Function
End Class
