﻿Imports Microsoft.VisualBasic

Public Class CatDetalle
    Private _ListadoDetalles As New List(Of ListadoDetalles)
    Private _Marca As String
    Private _Modelo As String
    Private _Descripcion As String
    Private _SubDescripcion As String
    Private Sub Constructor()
        Me._Marca = ""
        Me._Modelo = ""
        Me._Descripcion = ""
        Me._SubDescripcion = ""
    End Sub
    Public Property Marca As String
        Get
            Return Me._Marca
        End Get
        Set(ByVal value As String)
            Me._Marca = value
        End Set
    End Property
    Public Property Modelo As String
        Get
            Return Me._Modelo
        End Get
        Set(ByVal value As String)
            Me._Modelo = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property
    Public Property SubDescripcion As String
        Get
            Return Me._SubDescripcion
        End Get
        Set(ByVal value As String)
            Me._SubDescripcion = value
        End Set
    End Property
    Public Property ListadoDetalles As List(Of ListadoDetalles)
        Get
            Return Me._ListadoDetalles
        End Get
        Set(ByVal value As List(Of ListadoDetalles))
            Me._ListadoDetalles = value
        End Set
    End Property
End Class
Public Class ListadoDetalles
    Private _Detalle As String
    Private Sub Constructor()
        Me._Detalle = ""
    End Sub
    Public Property Detalle As String
        Get
            Return Me._Detalle
        End Get
        Set(ByVal value As String)
            Me._Detalle = value
        End Set
    End Property
End Class
