﻿Imports Microsoft.VisualBasic

Public Class CatSubDescripcion
    Private _ListadoSubDescripciones As New List(Of ListadoSubDescripciones)
    Private _Marca As String
    Private _Modelo As String
    Private _Descripcion As String
    Private Sub Constructor()
        Me._Marca = ""
        Me._Modelo = ""
        Me._Descripcion = ""
    End Sub
    Public Property Marca As String
        Get
            Return Me._Marca
        End Get
        Set(ByVal value As String)
            Me._Marca = value
        End Set
    End Property
    Public Property Modelo As String
        Get
            Return Me._Modelo
        End Get
        Set(ByVal value As String)
            Me._Modelo = value
        End Set
    End Property
    Public Property Descripcion As String
        Get
            Return Me._Descripcion
        End Get
        Set(ByVal value As String)
            Me._Descripcion = value
        End Set
    End Property
    Public Property ListadoSubDescripciones As List(Of ListadoSubDescripciones)
        Get
            Return Me._ListadoSubDescripciones
        End Get
        Set(ByVal value As List(Of ListadoSubDescripciones))
            Me._ListadoSubDescripciones = value
        End Set
    End Property
End Class
Public Class ListadoSubDescripciones
    Private _SubDescripcion As String
    Private Sub Constructor()
        Me._SubDescripcion = ""
    End Sub
    Public Property SubDescripcion As String
        Get
            Return Me._SubDescripcion
        End Get
        Set(ByVal value As String)
            Me._SubDescripcion = value
        End Set
    End Property
End Class
