﻿Imports System.Xml
Imports SuperObjeto
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.IO
Imports System.Xml.Serialization
Imports MySql.Data.MySqlClient

Public Class WsATLAS
    Public Shared Function ATLASCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim respuestaWS As String = ""
        Dim ObjATLAS = New ATLASService.WSSegAtlasImplClient
        Dim obj = New ATLASService.Cotiza_Atlas
        Dim fpago As Long = 0
        Dim plan As Long = 0
        Dim instrumento = 0
        Dim lg As Log = New Log()

        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            fpago = 1
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            fpago = 2
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            fpago = 4
        End If

        If ObjData.Paquete.ToUpper = "AMPLIA" Then
            plan = 2
        ElseIf ObjData.Paquete.ToUpper = "AMPLIAVIP" Then
            plan = 1
        ElseIf ObjData.Paquete.ToUpper = "LIMITADA" Then
            plan = 4
        ElseIf ObjData.Paquete.ToUpper = "RC" Then
            plan = 10
        End If


        obj.DatosEndoso = New ATLASService.DatosEndoso(0) {}
        obj.DatosEndoso(0) = New ATLASService.DatosEndoso()
        obj.DatosEndoso(0).AEmision = 0
        obj.DatosEndoso(0).FechaIni = Date.Now.ToString("yyyy-MM-dd")
        obj.DatosEndoso(0).FinVigenciaCot = Date.Now.AddYears(1).ToString("yyyy-MM-dd")
        obj.DatosEndoso(0).NoPoliza = 0
        obj.DatosEndoso(0).NoReexp = 1

        obj.DatosNuevaCot = New ATLASService.DatosNuevaCot(0) {}
        obj.DatosNuevaCot(0) = New ATLASService.DatosNuevaCot()
        obj.DatosNuevaCot(0).Agente = 7487

        obj.DatosNuevaCot(0).Coberturas = New ATLASService.Coberturas1(6) {}
        obj.DatosNuevaCot(0).Coberturas(0) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(0).Cobertura = 1
        obj.DatosNuevaCot(0).Coberturas(0).Deducible = 5
        obj.DatosNuevaCot(0).Coberturas(0).SumaAsegurada = 1

        obj.DatosNuevaCot(0).Coberturas(1) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(1).Cobertura = 2
        obj.DatosNuevaCot(0).Coberturas(1).Deducible = 10
        obj.DatosNuevaCot(0).Coberturas(1).SumaAsegurada = 1

        obj.DatosNuevaCot(0).Coberturas(2) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(2).Cobertura = 3
        obj.DatosNuevaCot(0).Coberturas(2).Deducible = 0
        obj.DatosNuevaCot(0).Coberturas(2).SumaAsegurada = 1500000

        obj.DatosNuevaCot(0).Coberturas(3) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(3).Cobertura = 4
        obj.DatosNuevaCot(0).Coberturas(3).Deducible = 0
        obj.DatosNuevaCot(0).Coberturas(3).SumaAsegurada = 150000

        obj.DatosNuevaCot(0).Coberturas(4) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(4).Cobertura = 33
        obj.DatosNuevaCot(0).Coberturas(4).Deducible = 0
        obj.DatosNuevaCot(0).Coberturas(4).SumaAsegurada = 0

        obj.DatosNuevaCot(0).Coberturas(5) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(5).Cobertura = 34
        obj.DatosNuevaCot(0).Coberturas(5).Deducible = 0
        obj.DatosNuevaCot(0).Coberturas(5).SumaAsegurada = 1500000


        obj.DatosNuevaCot(0).Coberturas(6) = New ATLASService.Coberturas1()
        obj.DatosNuevaCot(0).Coberturas(6).Cobertura = 96
        obj.DatosNuevaCot(0).Coberturas(6).Deducible = 0
        obj.DatosNuevaCot(0).Coberturas(6).SumaAsegurada = 2000000

        obj.DatosNuevaCot(0).CveCalculo = 1
        obj.DatosNuevaCot(0).CveNegocio = 3
        obj.DatosNuevaCot(0).CveUdi = 0
        obj.DatosNuevaCot(0).DescVehiculo = ObjData.Vehiculo.Descripcion
        obj.DatosNuevaCot(0).CveVehiculo = CInt(ObjData.Vehiculo.Clave)
        obj.DatosNuevaCot(0).Descuento = ObjData.Descuento
        obj.DatosNuevaCot(0).EdoCircula = 9
        obj.DatosNuevaCot(0).FPago = fpago
        obj.DatosNuevaCot(0).Modelo = CInt(ObjData.Vehiculo.Modelo)
        obj.DatosNuevaCot(0).Moneda = 1
        obj.DatosNuevaCot(0).Plan = plan
        obj.DatosNuevaCot(0).PorUdi = 0
        obj.DatosNuevaCot(0).Producto = 1
        obj.DatosNuevaCot(0).Servicio = 1
        obj.DatosNuevaCot(0).Sucursal = "DC2"
        obj.DatosNuevaCot(0).TipCveVehi = 1
        obj.DatosNuevaCot(0).Tipo_Calculo = 1
        obj.DatosNuevaCot(0).Tipo_Uso = 1
        obj.DatosNuevaCot(0).Uso = 1

        obj.DatosSeguridad = New ATLASService.DatosSeguridad(0) {}
        obj.DatosSeguridad(0) = New ATLASService.DatosSeguridad()
        obj.DatosSeguridad(0).CveUsuario = "WSAHORRA"
        obj.DatosSeguridad(0).Password = "WSAHORRA"


        Try
            Dim x2 As New XmlSerializer(obj.GetType)
            Dim xml2 As New StringWriter
            x2.Serialize(xml2, obj)
            Dim xmlRespuesta As String = xml2.ToString
            lg.UpdateLogRequest(xmlRespuesta, idLogWSCot)
        Catch ex As Exception
            Dim errr = ex
        End Try



        Dim serializer As New JavaScriptSerializer
        Dim respuesta As ATLASService.Cotizador
        Try
            respuesta = ObjATLAS.WS_atlas_cotiza(obj)
            If (respuesta.Errores(0).CveError > 0) Then
                respuestaWS = respuesta.Errores(0).Concepto
                Throw New System.Exception("Error al realizar invocacion del metodo WS_atlas_cotiza, respuesta del servicio no satisfactorio")
            End If
        Catch ex As Exception
            ObjData.Emision.Resultado = "false"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Try
            Dim x3 As New XmlSerializer(respuesta.GetType)
            Dim xml3 As New StringWriter
            x3.Serialize(xml3, respuesta)
            Dim errorws4 As String = xml3.ToString
            lg.UpdateLogResponse(errorws4, idLogWSCot)
        Catch ex As Exception

        End Try
        Dim nombreCobertura = ""
        Dim coberturas = New Coberturas
        ObjData.Cotizacion.Resultado = "False"
        For i = 0 To respuesta.Planes.Length - 1
            If respuesta.Planes(i).Plan = plan Then
                ObjData.Cotizacion.Derechos = respuesta.Planes(i).PGastos
                ObjData.Cotizacion.Impuesto = respuesta.Planes(i).PIva
                ObjData.Cotizacion.PrimaNeta = respuesta.Planes(i).PPriBruta
                ObjData.Cotizacion.PrimaTotal = respuesta.Planes(i).PPrimaTot
                ObjData.Cotizacion.Recargos = respuesta.Planes(i).PRecargos
                ObjData.Cotizacion.CotID = respuesta.IdControl
                ObjData.Cotizacion.IDCotizacion = respuesta.NumCotiza
                ObjData.Cotizacion.Resultado = "True"
                For j = 0 To respuesta.Planes(i).Recibos.Length - 1
                    ObjData.Cotizacion.PrimerPago = respuesta.Planes(i).Recibos(0).RPrimaTot
                    If j > 0 Then
                        ObjData.Cotizacion.PagosSubsecuentes = respuesta.Planes(i).Recibos(1).RPrimaTot
                    End If
                Next
                For h = 0 To respuesta.Planes(i).Coberturas.Length - 1
                    If respuesta.Planes(i).Coberturas(h).Cobertura = 1 Then
                        nombreCobertura = "DAÑOS MATERIALES"
                        coberturas.DanosMateriales = "-N" & nombreCobertura & "-S" & respuesta.Planes(i).Coberturas(h).SumaAseg & "-D" & respuesta.Planes(i).Coberturas(h).Deducible
                    ElseIf respuesta.Planes(i).Coberturas(h).Cobertura = 2 Then
                        nombreCobertura = "ROBO TOTAL"
                        coberturas.RoboTotal = "-N" & nombreCobertura & "-S" & respuesta.Planes(i).Coberturas(h).SumaAseg & "-D" & respuesta.Planes(i).Coberturas(h).Deducible
                    ElseIf respuesta.Planes(i).Coberturas(h).Cobertura = 3 Then
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        coberturas.RC = "-N" & nombreCobertura & "-S" & respuesta.Planes(i).Coberturas(h).SumaAseg & "-D" & respuesta.Planes(i).Coberturas(h).Deducible
                    ElseIf respuesta.Planes(i).Coberturas(h).Cobertura = 4 Then
                        nombreCobertura = "GASTOS MÉDICOS OCUPANTES"
                        coberturas.GastosMedicosOcupantes = "-N" & nombreCobertura & "-S" & respuesta.Planes(i).Coberturas(h).SumaAseg & "-D" & respuesta.Planes(i).Coberturas(h).Deducible
                    ElseIf respuesta.Planes(i).Coberturas(h).Cobertura = 34 Then
                        nombreCobertura = "GASTOS LEGALES"
                        coberturas.GastosMedicosOcupantes = "-N" & nombreCobertura & "-S" & respuesta.Planes(i).Coberturas(h).SumaAseg & "-D" & respuesta.Planes(i).Coberturas(h).Deducible
                    ElseIf respuesta.Planes(i).Coberturas(h).Cobertura = 33 Then
                        nombreCobertura = "ASISTENCIA VIAL"
                        coberturas.GastosMedicosOcupantes = "-N" & nombreCobertura & "-S" & respuesta.Planes(i).Coberturas(h).SumaAseg & "-D" & respuesta.Planes(i).Coberturas(h).Deducible
                    End If
                Next
            End If
        Next
        ObjData.Coberturas.Add(coberturas)
        Dim Jsonresponse As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(Jsonresponse, idLogWSCot)

        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function ATLASEmision(ByVal ObjData As Seguro, ByVal idLogWS As String)
        Dim respuestaWS As String = ""
        Dim ObjATLAS = New ATLASService.WSSegAtlasImplClient
        Dim obj = New ATLASService.Emision_Atlas
        Dim idBanco = bancosATLAS(ObjData.Pago.Banco)
        Dim fpago As Long = 0
        Dim plan As Long = 0
        Dim instrumento = 0

        If UCase(ObjData.Pago.MedioPago) = "CREDITO" Then
            instrumento = 50
        ElseIf UCase(ObjData.Pago.MedioPago) = "DEBITO" Then
            instrumento = 3
        End If

        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            fpago = 1
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            fpago = 2
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            fpago = 4
        End If

        If ObjData.Paquete.ToUpper = "AMPLIA" Then
            plan = 2
        ElseIf ObjData.Paquete.ToUpper = "AMPLIAVIP" Then
            plan = 1
        ElseIf ObjData.Paquete.ToUpper = "LIMITADA" Then
            plan = 4
        ElseIf ObjData.Paquete.ToUpper = "RC" Then
            plan = 10
        End If
        obj.AMConductor = ObjData.Cliente.ApellidoMat
        obj.APConductor = ObjData.Cliente.ApellidoPat
        obj.Agrupador = 0
        obj.Alarma = 0

        obj.Contrat = New ATLASService.Contrat(0) {}
        obj.Contrat(0) = New ATLASService.Contrat()
        obj.Contrat(0).AMaterno = ObjData.Cliente.ApellidoMat
        obj.Contrat(0).APaterno = ObjData.Cliente.ApellidoPat
        obj.Contrat(0).BenefPrefer = ""
        obj.Contrat(0).CP = ObjData.Cliente.Direccion.CodPostal
        obj.Contrat(0).Colonia = ObjData.Cliente.Direccion.Colonia
        obj.Contrat(0).Direccion = ObjData.Cliente.Direccion.Calle
        obj.Contrat(0).Email = ObjData.Cliente.Email
        obj.Contrat(0).Estado = 9
        obj.Contrat(0).Nombre = ObjData.Cliente.Nombre
        obj.Contrat(0).RFC = ObjData.Cliente.RFC
        obj.Contrat(0).Telefono = ObjData.Cliente.Telefono
        obj.Contrat(0).TipoPersona = 1
        obj.Contrat(0).cve_giro = 0
        obj.Contrat(0).cve_ocupacion = 0
        obj.Contrat(0).fec_constit = 1
        obj.Contrat(0).folio_mercantil = 1
        obj.Contrat(0).nacionalidad = 1
        obj.Contrat(0).no_exterior = ObjData.Cliente.Direccion.NoExt
        obj.Contrat(0).no_interior = ObjData.Cliente.Direccion.NoInt
        obj.Contrat(0).num_fea = 1
        obj.Contrat(0).pais_nacimiento = 1

        obj.Contrat(0).t_anio_exp = CInt(ObjData.Pago.AnioExp)
        obj.Contrat(0).t_banco = CInt(idBanco)
        obj.Contrat(0).t_codigo = CInt(ObjData.Pago.CodigoSeguridad)
        obj.Contrat(0).t_instrumento = instrumento
        obj.Contrat(0).t_mes_exp = CInt(ObjData.Pago.MesExp)
        obj.Contrat(0).t_nm_e = 0
        obj.Contrat(0).t_nombre = ObjData.Pago.NombreTarjeta
        obj.Contrat(0).t_num_tar = ObjData.Pago.NoTarjeta
        obj.Contrat(0).tipo_exposicion = 1

        obj.DatosSeguridad = New ATLASService.DatosSeguridad1(0) {}
        obj.DatosSeguridad(0) = New ATLASService.DatosSeguridad1()
        obj.DatosSeguridad(0).CveUsuario = "WSAHORRA"
        obj.DatosSeguridad(0).Password = "WSAHORRA"

        obj.DetCoberEA = New ATLASService.DetCoberEA(0) {}
        obj.DetCoberEA(0) = New ATLASService.DetCoberEA()
        obj.DetCoberEA(0).DValor = 0
        obj.DetCoberEA(0).Cobertura = 0
        obj.DetCoberEA(0).Descripcion = ""

        obj.FechaCotiza = Date.Now.ToString("yyyy-MM-dd")
        obj.FechaInicio = Date.Now.ToString("yyyy-MM-dd")
        obj.FechaTermino = Date.Now.AddYears(1).ToString("yyyy-MM-dd")
        obj.IdControl = ObjData.Cotizacion.CotID
        obj.Motor = ObjData.Vehiculo.NoMotor
        obj.NomConductor = ObjData.Cliente.Nombre & " " & ObjData.Cliente.ApellidoPat & " " & ObjData.Cliente.ApellidoMat
        obj.NumContrato = ""
        obj.NumCotiza = ObjData.Cotizacion.IDCotizacion
        obj.NumEco = ""
        obj.Placas = ObjData.Vehiculo.NoPlacas
        obj.Plan = plan
        obj.RFV = 0
        obj.RegresaRuta = 0
        obj.Serie = ObjData.Vehiculo.NoSerie
        obj.Sucursal = "DC2"
        obj.TipoIva = 1
        Dim serializer As New JavaScriptSerializer
        Try
            Dim x As New XmlSerializer(obj.GetType)
            Dim xml As New StringWriter
            x.Serialize(xml, obj)
            Dim errorws2 As String = xml.ToString
            Dim id = Funciones.updateLogWS(errorws2, idLogWS, "RequestWS")

        Catch ex As Exception
        End Try
        Dim respuesta As ATLASService.Emisor_Atlas
        Try
            respuesta = ObjATLAS.WS_atlas_emisor(obj)
            If respuesta.Error_em(0).CveError > 0 Then
                respuestaWS = respuesta.Error_em(0).Concepto
            End If
        Catch ex As Exception
            ObjData.Emision.Resultado = "false"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Try
            Dim x2 As New XmlSerializer(respuesta.GetType)
            Dim xml2 As New StringWriter
            x2.Serialize(xml2, respuesta)
            Dim errorws4 As String = xml2.ToString
            Dim id = Funciones.updateLogWS(errorws4, idLogWS, "ResponseWS")
        Catch ex As Exception

        End Try
        If respuesta.Error_em(0).CveError <> 0 Then
            ObjData.CodigoError = respuesta.Error_em(0).Concepto
            ObjData.Emision.Resultado = "False"
        ElseIf respuesta.Error_em(0).CveError = 0 Then
            Dim Endoso = respuesta.Endoso
            Dim Inciso = respuesta.Inciso
            ObjData.Emision.Poliza = respuesta.Poliza
            ObjData.Emision.PrimaTotal = respuesta.PrimaTotal
            ObjData.Emision.PrimaNeta = respuesta.PrimaBruta
            ObjData.Emision.Impuesto = respuesta.IVA
            ObjData.Emision.Recargos = respuesta.Recargo
            ObjData.Emision.Resultado = "True"
            Dim ObjATLASImpresion = New ATLASImpresion.WS_ImpresionesImplClient
            Dim objImpresion = New ATLASImpresion.Datimpres(0) {}
            objImpresion(0) = New ATLASImpresion.Datimpres()
            objImpresion(0).Anio = 0
            objImpresion(0).Bien = 0
            objImpresion(0).Cola_impres = 0
            objImpresion(0).Dependiente = 1
            objImpresion(0).DetImpres = New ATLASImpresion.DetImpres(0) {}
            objImpresion(0).DetImpres(0) = New ATLASImpresion.DetImpres()
            objImpresion(0).DetImpres(0).Desc = ""
            objImpresion(0).DetImpres(0).ID_Impresion = 1
            objImpresion(0).DetImpres(0).Negocio = 0
            objImpresion(0).EndSerie = Endoso
            objImpresion(0).Id_Solcot = 0
            objImpresion(0).Inciso = Inciso
            objImpresion(0).Linea = 1
            objImpresion(0).Num_rec = 1
            objImpresion(0).Par2_alfa = ""
            objImpresion(0).Par_alfa = ""
            objImpresion(0).Par_num = 0
            objImpresion(0).Password = "WSAHORRA"
            objImpresion(0).PolSolCot = ObjData.Emision.Poliza
            objImpresion(0).Producto = 1
            objImpresion(0).Reexp = 1
            objImpresion(0).Sucursal = "DC2"
            objImpresion(0).Usuario = "WSAHORRA"
            Try
                Dim x2 As New XmlSerializer(objImpresion.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, objImpresion)
                Dim errorws4 As String = xml2.ToString
                FuncionesGeneral.xmlWS(errorws4, ObjData, "Impresion", "ImpresionRespuesta")
            Catch ex As Exception

            End Try
            Dim res = ObjATLASImpresion.WS_Impres_atlas(objImpresion)
            Try
                Dim x2 As New XmlSerializer(res.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, res)
                Dim errorws4 As String = xml2.ToString
                FuncionesGeneral.xmlWS(errorws4, ObjData, "Impresion", "ImpresionEnvio")
            Catch ex As Exception

            End Try
            ObjData.Emision.Documento = "ftp://Documentos:Docu$0213@148.233.128.212/" & res(0).DetResp(0).Ruta_A
        End If


        Return serializer.Serialize(ObjData)
    End Function

    Private Shared Function bancosATLAS(ByVal banco As String) As String
        Dim abr As String
        Dim ConnectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;"
        Dim sql As String = "SELECT idBanco FROM `Ali_R_catBancosATLAS` where Abreviacion = '" + banco + ""
        Using connection As New MySqlConnection(ConnectionString)
            Dim command As New MySqlCommand(sql, connection)
            command.Parameters.Add("@abr", SqlDbType.VarChar)
            command.Parameters("@abr").Value = banco
            Try
                connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    abr = reader("idBanco")
                End While
                connection.Close()
            Catch ex As Exception
            End Try
        End Using
        banco = abr

        Return banco
    End Function
End Class
