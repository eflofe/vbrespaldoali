﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.Web
Imports System.Data

Public Class Busqueda
    <WebMethod>
    Public Shared Function Buscar(ByVal marca As String, ByVal modelo As String, ByVal Descripcion As String, ByVal SubDescripcion As String) As String
        Dim respuesta = ""
        Dim ObjDescripcion As New General
        Dim Aseguradoras(11) As String
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        serializer.MaxJsonLength = Integer.MaxValue
        Dim contieneAseguradora As Integer = 0
        Dim QRYSubmarca As String
        If SubDescripcion <> "select" Then
            QRYSubmarca = " AND SubMarcaEst04 like '%%'"
        End If
        Dim ConnectionString = "Data Source=10.20.18.149;Initial Catalog=ws_seguros_auto;User Id=mmendoza;Password=gL@0jAOS123;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn"
        Dim sql = "SELECT Cia,MarcaEst,IDVehiCia,Modelo,TrimEst FROM `ws_db`.`Ali_R_Catalogos` where MarcaEst like '%%' AND Modelo like '%%' AND SubMarcaEst like '%%' And cia='QUALITAS' and"
        Using Connection As New SqlConnection(ConnectionString)
            Dim command As New SqlCommand(sql, Connection)
            command.Parameters.Add("@marca", SqlDbType.VarChar)
            command.Parameters("@marca").Value = marca
            command.Parameters.Add("@modelo", SqlDbType.VarChar)
            command.Parameters("@modelo").Value = modelo
            command.Parameters.Add("@descripcion", SqlDbType.VarChar)
            command.Parameters("@descripcion").Value = Descripcion
            command.Parameters.Add("@subdescripcion", SqlDbType.VarChar)
            command.Parameters("@subdescripcion").Value = SubDescripcion
            Try
                Dim catalogo As New Catalogo
                Connection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Dim CatDescripciones As New CatDescripciones
                    catalogo.Aseguradora = reader("cia")
                    catalogo.Marca = reader("MarcaEst")
                    catalogo.Modelo = modelo
                    CatDescripciones.clave = reader("IDVehiCia")
                    CatDescripciones.Descripcion = reader("TrimEst")
                    catalogo.CatDescripciones.Add(CatDescripciones)
                End While
                ObjDescripcion.Catalogo.Add(catalogo)
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        sql = "SELECT Cia,MarcaEst,IDVehiCia,Modelo,TrimEst FROM Catalogos where MarcaEst like '%' + @marca + '%' AND Modelo like '%' + @modelo + '%' AND SubMarcaEst like '%' + @descripcion + '%' And cia='BANORTE'" & QRYSubmarca
        Using Connection As New SqlConnection(ConnectionString)
            Dim command As New SqlCommand(sql, Connection)
            command.Parameters.Add("@marca", SqlDbType.VarChar)
            command.Parameters("@marca").Value = marca
            command.Parameters.Add("@modelo", SqlDbType.VarChar)
            command.Parameters("@modelo").Value = modelo
            command.Parameters.Add("@descripcion", SqlDbType.VarChar)
            command.Parameters("@descripcion").Value = Descripcion
            command.Parameters.Add("@subdescripcion", SqlDbType.VarChar)
            command.Parameters("@subdescripcion").Value = SubDescripcion
            Try
                Dim catalogo As New Catalogo
                Connection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Dim CatDescripciones As New CatDescripciones
                    catalogo.Aseguradora = reader("cia")
                    catalogo.Marca = reader("MarcaEst")
                    catalogo.Modelo = modelo
                    CatDescripciones.clave = reader("IDVehiCia")
                    CatDescripciones.Descripcion = reader("TrimEst")
                    catalogo.CatDescripciones.Add(CatDescripciones)
                End While
                Connection.Close()
                ObjDescripcion.Catalogo.Add(catalogo)
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        sql = "SELECT Cia,MarcaEst,IDVehiCia,Modelo,TrimEst FROM Catalogos where MarcaEst like '%' + @marca + '%' AND Modelo like '%' + @modelo + '%' AND SubMarcaEst like '%' + @descripcion + '%'  And cia='AXA'" & QRYSubmarca
        Using Connection As New SqlConnection(ConnectionString)
            Dim command As New SqlCommand(sql, Connection)
            command.Parameters.Add("@marca", SqlDbType.VarChar)
            command.Parameters("@marca").Value = marca
            command.Parameters.Add("@modelo", SqlDbType.VarChar)
            command.Parameters("@modelo").Value = modelo
            command.Parameters.Add("@descripcion", SqlDbType.VarChar)
            command.Parameters("@descripcion").Value = Descripcion
            command.Parameters.Add("@subdescripcion", SqlDbType.VarChar)
            command.Parameters("@subdescripcion").Value = SubDescripcion
            Try
                Dim catalogo As New Catalogo
                Connection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Dim CatDescripciones As New CatDescripciones
                    catalogo.Aseguradora = reader("cia")
                    catalogo.Marca = reader("MarcaEst")
                    catalogo.Modelo = modelo
                    CatDescripciones.clave = reader("IDVehiCia")
                    CatDescripciones.Descripcion = reader("TrimEst")
                    catalogo.CatDescripciones.Add(CatDescripciones)
                End While
                Connection.Close()
                ObjDescripcion.Catalogo.Add(catalogo)
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        sql = "SELECT Cia,MarcaEst,IDVehiCia,Modelo,TrimEst FROM Catalogos where MarcaEst like '%' + @marca + '%' AND Modelo like '%' + @modelo + '%' AND SubMarcaEst like '%' + @descripcion + '%' And cia='GNP'" & QRYSubmarca
        Using Connection As New SqlConnection(ConnectionString)
            Dim command As New SqlCommand(sql, Connection)
            command.Parameters.Add("@marca", SqlDbType.VarChar)
            command.Parameters("@marca").Value = marca
            command.Parameters.Add("@modelo", SqlDbType.VarChar)
            command.Parameters("@modelo").Value = modelo
            command.Parameters.Add("@descripcion", SqlDbType.VarChar)
            command.Parameters("@descripcion").Value = Descripcion
            command.Parameters.Add("@subdescripcion", SqlDbType.VarChar)
            command.Parameters("@subdescripcion").Value = SubDescripcion
            Try
                Dim catalogo As New Catalogo
                Connection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Dim CatDescripciones As New CatDescripciones
                    catalogo.Aseguradora = reader("cia")
                    catalogo.Marca = reader("MarcaEst")
                    catalogo.Modelo = modelo
                    CatDescripciones.clave = reader("IDVehiCia")
                    CatDescripciones.Descripcion = reader("TrimEst")
                    catalogo.CatDescripciones.Add(CatDescripciones)
                End While
                Connection.Close()
                ObjDescripcion.Catalogo.Add(catalogo)
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        sql = "SELECT Cia,MarcaEst,IDVehiCia,Modelo,TrimEst FROM Catalogos where MarcaEst like '%' + @marca + '%' AND Modelo like '%' + @modelo + '%' AND SubMarcaEst like '%' + @descripcion + '%'  And cia='ABA'" & QRYSubmarca
        Using Connection As New SqlConnection(ConnectionString)
            Dim command As New SqlCommand(sql, Connection)
            command.Parameters.Add("@marca", SqlDbType.VarChar)
            command.Parameters("@marca").Value = marca
            command.Parameters.Add("@modelo", SqlDbType.VarChar)
            command.Parameters("@modelo").Value = modelo
            command.Parameters.Add("@descripcion", SqlDbType.VarChar)
            command.Parameters("@descripcion").Value = Descripcion
            command.Parameters.Add("@subdescripcion", SqlDbType.VarChar)
            command.Parameters("@subdescripcion").Value = SubDescripcion
            Try
                Dim catalogo As New Catalogo
                Connection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Dim CatDescripciones As New CatDescripciones
                    catalogo.Aseguradora = reader("cia")
                    catalogo.Marca = reader("MarcaEst")
                    catalogo.Modelo = modelo
                    CatDescripciones.clave = reader("IDVehiCia")
                    CatDescripciones.Descripcion = reader("TrimEst")
                    catalogo.CatDescripciones.Add(CatDescripciones)
                End While
                Connection.Close()
                ObjDescripcion.Catalogo.Add(catalogo)
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        respuesta = ToJSON(ObjDescripcion)
        Return respuesta
    End Function
    Public Shared Function ToJSON(obj As Object) As String
        Dim serializer As New JavaScriptSerializer()
        Return serializer.Serialize(obj)
    End Function
End Class
