﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports SuperObjeto
Imports AXA_Bancs_QA_CS
Imports System.Xml.Serialization
Imports System.IO
Imports MySql.Data.MySqlClient
Imports Newtonsoft.Json


' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
<System.Web.Script.Services.ScriptService()>
<WebService(Namespace:="http://tempuri.org/")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class WebService
    Inherits System.Web.Services.WebService
    Dim lg As Log = New Log()
    Dim quali As CotizadorQualitas = New CotizadorQualitas()
    'LatinoVariables
    Public seguroNombre As String = ""
    Public clave As String = ""
    Public cp As String = ""
    Public descripcion As String = ""
    Public descuento As String = ""
    Public edad As String = ""
    Public fechaNacimiento As String = ""
    Public genero As String = ""
    Public marca As String = ""
    Public modelo As Integer = 0
    Public movimiento As String = ""
    Public paquete As String = ""
    Public servicio As String = ""
    Private LatinoJson As String = ""
    Private JsonGenericoBQ As String = ""
    Private datosHogar As DatosHogar
    Private IdLogWsHogar As String
    Private abaCotizador As AbaHogarCotizador = New AbahogarCotizador
    'Metodo Producción para obtener las MArcas
    <WebMethod>
    Public Function GetMarcasP(ByVal usuario As String, ByVal password As String) As String
        Dim marcasJson As String = lg.GetMarcasMyysql()
        Return marcasJson
    End Function
    'Metodo Producción para obtener las SubMarcas
    <WebMethod>
    Public Function GetSubMarcaP(ByVal usuario As String, ByVal password As String, ByVal marca As String, ByVal modelo As String) As String
        Dim subMarcasJson As String = lg.GetSubMarcaMySql(marca, modelo)
        Return subMarcasJson
    End Function
    'Fin del Metodo Producción para obtener las SubMarcas
    <WebMethod>
    Public Function GetModelosP(ByVal usuario As String, ByVal password As String, ByVal marca As String) As String
        Dim modelosJson As String = lg.GetModelosMysql(marca)
        Return modelosJson
    End Function
    'Fin del Método descripción

    <WebMethod()>
    Public Function GetDescripcionesP(ByVal usuario As String, ByVal password As String, ByVal marca As String, ByVal modelo As String, ByVal subMarca As String) As String
        Dim descripcionesJson As String = lg.GetDescripcionMysql(marca, modelo, subMarca)
        Return descripcionesJson

    End Function
    <WebMethod()>
    Public Function GetSubDescripcionesP(ByVal usuario As String, ByVal password As String, ByVal marca As String, ByVal subMarca As String, ByVal modelo As String, ByVal descripcion As String) As String
        Dim descripcionesJson As String = lg.GetSubDescripcionMysql(marca, subMarca, modelo, descripcion)
        Return descripcionesJson

    End Function
    <WebMethod>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function Buscar(ByVal usuario As String, ByVal password As String, ByVal marca As String, ByVal modelo As String, ByVal Descripcion As String, ByVal SubDescripcion As String, ByVal Detalle As String) As String
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Return servicioCatalogos.Buscar(usuario, password, marca, modelo, Descripcion, SubDescripcion, Detalle)
    End Function

    <WebMethod>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function BuscarMoto(ByVal usuario As String, ByVal password As String, ByVal marca As String, ByVal modelo As String, ByVal Descripcion As String, ByVal SubDescripcion As String, ByVal Detalle As String) As String
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Return servicioCatalogos.BuscarMoto(usuario, password, marca, modelo, Descripcion, SubDescripcion, Detalle)
    End Function

    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function BuscarAseguradora(ByVal usuario As String, ByVal password As String, ByVal marca As String, ByVal modelo As String, ByVal descripcion As String, ByVal subdescripcion As String, ByVal aseguradora As String) As String
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Return servicioCatalogos.BuscarAseguradora(usuario, password, marca, modelo, descripcion, subdescripcion, aseguradora)
    End Function
    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function CotizadorHogar(ByVal usuario As String, ByVal password As String, ByVal data As String, ByVal movimiento As String) As String
        Dim cliente = "AhorraSeguros"
        Dim contrasena = "Ah0rraS3guros2017"
        Dim Respuesta As String = ""
        Dim Deserializer As New JavaScriptSerializer
        Dim serializer As New JavaScriptSerializer
        Dim ObjData As New DatosHogar
        Dim lgh As LogCotizacionHogar = New LogCotizacionHogar
        Dim salida As String = ""

        If (usuario = cliente And contrasena = password) Then
            Try
                ObjData = Deserializer.Deserialize(Of DatosHogar)(data)
            Catch ex As Exception
                Dim errorAutentificacion = "Error de json" + ex.ToString()
            End Try
            If (movimiento = "cotizacion") Then
                IdLogWsHogar = lgh.InsertLog(data)
                salida = abaCotizador.CotizarHogar(ObjData, IdLogWsHogar)
            End If
        End If
        Return salida

    End Function
    Public Function LevenshteinDistance(ByVal s As String, ByVal t As String) As Integer
        Dim n As Integer = s.Length
        Dim m As Integer = t.Length
        Dim d(n + 1, m + 1) As Integer
        If n = 0 Then
            Return m
        End If
        If m = 0 Then
            Return n
        End If
        Dim i As Integer
        Dim j As Integer

        For i = 0 To n
            d(i, 0) = i
        Next

        For j = 0 To m
            d(0, j) = j
        Next

        For i = 1 To n
            For j = 1 To m
                Dim cost As Integer
                If t(j - 1) = s(i - 1) Then
                    cost = 0
                Else
                    cost = 1
                End If
                d(i, j) = Math.Min(Math.Min(d(i - 1, j) + 1, d(i, j - 1) + 1), d(i - 1, j - 1) + cost)
            Next
        Next

        Return d(n, m)
    End Function
    Public Shared Function IndexOfMin(source As IEnumerable(Of Integer)) As Integer
        If source Is Nothing Then
            Throw New ArgumentNullException("source")
        End If

        Dim minValue As Integer = Integer.MaxValue
        Dim minIndex As Integer = -1
        Dim index As Integer = -1

        For Each num As Integer In source
            index += 1

            If num <= minValue Then
                minValue = num
                minIndex = index
            End If
        Next
        Try
            If index = -1 Then
                Throw New InvalidOperationException("Sequence was empty")
            End If
        Catch
        End Try

        Return minIndex
    End Function

    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function CotizacionEmision(ByVal usuario As String, ByVal Password As String, ByVal data As String, ByVal movimiento As String) As String
        Dim cliente = "Ahorra2020"
        Dim contrasena = "Ahorra2020"
        Dim Respuesta As String = ""
        Dim Deserializer As New JavaScriptSerializer
        Dim serializer As New JavaScriptSerializer
        Dim ObjData As New Seguro
        Dim validacion As String = ""
        Dim idLogWS = ""
        Dim idLogWSCot = ""
        If usuario = cliente And Password = contrasena Then
            Deserializer.MaxJsonLength = Integer.MaxValue
            ObjData = Deserializer.Deserialize(Of Seguro)(data)
            If movimiento = "cotizacion" Then
                Dim listseguros As JsonFormato = JsonConvert.DeserializeObject(Of JsonFormato)(data)
                ObjData = Deserializer.Deserialize(Of Seguro)(data)
                seguroNombre = listseguros.Aseguradora
                If seguroNombre = "LATINO" Then
                    paquete = listseguros.Paquete
                    descuento = listseguros.Descuento
                    descripcion = listseguros.Descripcion
                    servicio = listseguros.Servicio
                    marca = listseguros.Marca
                    modelo = listseguros.Modelo
                    clave = listseguros.Clave
                    cp = listseguros.Cp
                    edad = listseguros.Edad.ToString()
                    genero = listseguros.Genero
                    movimiento = listseguros.Movimiento
                    fechaNacimiento = listseguros.FechaNacimiento
                    'Switch Latino
                    Dim jsonLatino As FormatoJsonlLatino = New FormatoJsonlLatino
                    Select Case ObjData.Aseguradora
                        Case "LATINO"
                            LatinoJson = jsonLatino.JsonLatino(seguroNombre, clave, cp, descripcion, descuento, edad,
                                            fechaNacimiento, genero, marca, modelo, paquete, servicio)
                            ObjData = Deserializer.Deserialize(Of Seguro)(LatinoJson)
                    End Select
                End If
                'Fin LatinoFormato
            End If
            If movimiento = "emision" Then
                idLogWS = Funciones.insertLogWS(data, ObjData.Aseguradora, ObjData.Vehiculo.NoSerie)
            ElseIf movimiento = "cotizacion" Then
                idLogWSCot = lg.insertLogWSCot(data, ObjData.Aseguradora, ObjData.Vehiculo.Marca, ObjData.Vehiculo.Modelo, ObjData.Vehiculo.Descripcion, ObjData.Vehiculo.Clave)
            End If
            validacion = ValCotizacion(ObjData)
            Select Case ObjData.Aseguradora
                Case "GNP"
                    If movimiento = "cotizacion" Then
                        If validacion = "True" Then
                            Respuesta = WsGNPNuevo.GNPCotizacion(ObjData, idLogWSCot)
                        Else
                            ObjData.CodigoError = validacion
                            Respuesta = serializer.Serialize(ObjData)
                        End If
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsGNPNuevo.GNPEmision(ObjData, idLogWS)
                        'Respuesta = WsGNPNuevo.GNPEmisionOffline(ObjData, idLogWS)
                    End If
                Case "LATINO"
                    If movimiento = "cotizacion" Then
                        Dim latino As LatinoCot = New LatinoCot
                        Respuesta = latino.CotizacionLatino(data, ObjData, idLogWSCot, movimiento)
                    Else
                        ObjData.CodigoError = validacion
                        Respuesta = serializer.Serialize(ObjData)
                    End If
                Case "GNP2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsGNPNuevo.GNPCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsGNPNuevo.GNPEmisionOffline(ObjData, idLogWS)
                    End If
                Case "GNPMOTOS"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsGNPMotos.GNPCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsGNPMotos.GNPEmision(ObjData, idLogWS)
                    End If
                Case "QUALITAS"
                    If movimiento = "cotizacion" Then
                        If validacion = "True" Then
                            Respuesta = quali.CotizaQualitas(ObjData, idLogWSCot)

                            ' Respuesta = WsQUA.CotizaQualitas(ObjData, idLogWSCot)
                            '  Respuesta = 
                        Else
                            ObjData.CodigoError = validacion
                            Respuesta = serializer.Serialize(ObjData)
                        End If
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsQUA.EmiteQualitas(ObjData, idLogWS)
                    End If
                Case "AXA"
                    If movimiento = "cotizacion" Then
                        If validacion = "True" Then
                            'Respuesta = WsAXA.AXACotizacion(ObjData)
                            Respuesta = WsAXA.AXACotizacion(ObjData, idLogWSCot)
                        Else
                            ObjData.CodigoError = validacion
                            Respuesta = serializer.Serialize(ObjData)
                        End If
                    ElseIf movimiento = "emision" Then
                        'Respuesta = WsAXA.AXAEmision(ObjData)
                        Respuesta = WsAXA.AXAEmision(ObjData, idLogWS)
                    End If
                Case "ABA"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsABA.ABACotizacion(ObjData, idLogWSCot)
                        ObjData.CodigoError = validacion
                        Respuesta = serializer.Serialize(ObjData)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsABA.ABAEmision(ObjData, idLogWS)
                    End If
                Case "BANORTE"
                    If movimiento = "cotizacion" Then
                        If validacion = "True" Then
                            Respuesta = WsBANORTE.BANORTECotizacion(ObjData, idLogWSCot)
                        Else
                            ObjData.CodigoError = validacion
                            Respuesta = serializer.Serialize(ObjData)
                        End If
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsBANORTE.BANORTEemision(ObjData, idLogWS)
                    End If
                Case "BANORTE2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsBANORTEPProd.BANORTECotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then

                    End If
                Case "ANA"
                    If movimiento = "cotizacion" Then
                        If validacion = "True" Then
                            Respuesta = WsANA.ANACotizacion(ObjData, idLogWSCot)
                        Else
                            ObjData.CodigoError = validacion
                            Respuesta = serializer.Serialize(ObjData)
                        End If
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsANA.ANAEmision(ObjData, idLogWS)
                    End If
                'Case "HDI"
                '    If movimiento = "cotizacion" Then
                '        If validacion = "True" Then
                '            Respuesta = WsHDI.HDICotizacion(ObjData, idLogWSCot)
                '        Else
                '            ObjData.CodigoError = validacion
                '            Respuesta = serializer.Serialize(ObjData)
                '        End If
                '    ElseIf movimiento = "emision" Then
                '        Respuesta = WsHDI.HDIEmision(ObjData)
                '    End If
                Case "HDI"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsHDI2.HDICotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsHDI2.HDIEmision(ObjData)
                    End If
                Case "HDI3"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsHDIDesarrollo.HDICotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsHDIDesarrollo.HDIEmision(ObjData, idLogWS)
                    End If
                Case "ATLAS"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsATLASProd.ATLASCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsATLASProd.ATLASEmision(ObjData, idLogWS)
                    End If
                Case "ATLAS2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsATLAS.ATLASCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsATLAS.ATLASEmision(ObjData, idLogWS)
                    End If
                Case "ZURICH"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsZURICH.ZURICHCotizacion(ObjData, idLogWSCot)
                    End If
                Case "ELAGUILA"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsELAGUILA.AguilaCotizacion(ObjData, idLogWSCot)
                    End If
                Case "AIG2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsAIGDesarrollo.AIGCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsAIGDesarrollo.AIGEmision(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0026", "&")
                    End If
                Case "AIG"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsAIGNuevo.AIGCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsAIGNuevo.AIGEmision(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0026", "&")
                    End If
                Case "MAPFRE"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsMAPFRE.CotizacionMAPFRE(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsMAPFRE.EmisionMAPFRE(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0026", "&")
                    End If
                Case "MAPFRE2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsMAPFREDesarrollo.CotizacionMAPFRE(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsMAPFREDesarrollo.EmisionMAPFRE(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0026", "&")
                    End If
                Case "MAPFRE3"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsMAPFRENuevo.CotizacionMAPFRE(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsMAPFRENuevo.EmisionMAPFRE(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0026", "&")
                    End If
                Case "SURA"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsSURAProduccion.CotizacionSURA(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsSURAProduccion.EmisionSURA(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0027", "'")
                    End If
                Case "SURA2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsSURA.CotizacionSURA(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsSURA.EmisionSURA(ObjData, idLogWS)
                        Respuesta = Respuesta.Replace("\u0027", "'")
                    End If
                Case "PRIMEROSEGUROS"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsPS.PSCotizacion(ObjData, idLogWSCot)
                    End If
                Case "GENERALDESEGUROS"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsGSProduccion.GSCotizacion(ObjData, idLogWSCot)
                    End If
                'Case "BXMAS"
                '    If movimiento = "cotizacion" Then
                '        Respuesta = WSBXMASProductivo.BXMASCotizacion(ObjData, idLogWSCot)
                '    ElseIf movimiento = "emision" Then
                '        Respuesta = WSBXMASProductivo.BXMASEmision(ObjData, idLogWS)
                '    End If
                Case "BXMAS"
                    If movimiento = "cotizacion" Then
                        Respuesta = WSBXMASProductivo.BXMASCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WSBXMAS.BXMASEmision(ObjData, idLogWS)
                    End If
                Case "AFIRME"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsAFIRME.AFIRMECotizacion(ObjData, idLogWSCot)
                    Else
                        idLogWSCot = Funciones.insertLogWSCot("RC " + data, ObjData.Aseguradora, ObjData.Vehiculo.Marca, ObjData.Vehiculo.Modelo, ObjData.Vehiculo.Descripcion, ObjData.Vehiculo.Clave)
                        Respuesta = WsAFIRME.AFIRMEEmision(ObjData, idLogWS, idLogWSCot)
                    End If
                Case "INBURSA"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsINBURSA.InbursaCotizacion(ObjData, idLogWSCot)
                    End If
                Case "ABA2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsABA2.ABACotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsABA2.ABAEmision(ObjData, idLogWS)
                    End If
                Case "HDI2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WsHDI2.HDICotizacion(ObjData, idLogWSCot)
                        Respuesta = serializer.Serialize(ObjData)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WsHDI2.HDIEmision(ObjData)
                    End If
                Case "ELPOTOSI"
                    If movimiento = "cotizacion" Then
                        Respuesta = WSPotosi.PotosiCotizacion(ObjData, idLogWSCot)
                        'Respuesta = WsHDI2.HDICotizacion(ObjData, idLogWSCot)
                        'ObjData.CodigoError = validacion
                        'Respuesta = serializer.Serialize(ObjData)
                    ElseIf movimiento = "emision" Then
                        'Respuesta = WsHDI2.HDIEmision(ObjData)
                    End If
                Case "ELPOTOSI2"
                    If movimiento = "cotizacion" Then
                        Respuesta = WSPotosiDesarrollo.PotosiCotizacion(ObjData, idLogWSCot)
                    ElseIf movimiento = "emision" Then
                        Respuesta = WSPotosiDesarrollo.PotosiEmision(ObjData, idLogWS)
                    End If
            End Select

            If movimiento = "emision" Then
                idLogWS = Funciones.updateLogWS(Respuesta, idLogWS, "JSONResponse")
            ElseIf movimiento = "cotizacion" Then
                'idLogWSCot = Funciones.updateLogWSCot(Respuesta, idLogWSCot, "JSONResponse", "FechaFin")
            End If

            Return Respuesta
        Else
            Dim errorAutentificacion = "Error de autenticación"
            Return errorAutentificacion
        End If
    End Function

    '<WebMethod()>
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    '<ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    'Public Sub CotizacionEmisionJSON(ByVal usuario As String, ByVal Password As String, ByVal data As String, ByVal movimiento As String)
    '    Dim cliente = "AhorraSeguros"
    '    Dim contrasena = "Ah0rraS3guros2017"
    '    Dim Respuesta As String = ""
    '    Dim Deserializer As New JavaScriptSerializer
    '    Dim serializer As New JavaScriptSerializer
    '    Dim ObjData As New Seguro
    '    Dim validacion As String = ""
    '    If usuario = cliente And Password = contrasena Then
    '        Deserializer.MaxJsonLength = Integer.MaxValue
    '        ObjData = Deserializer.Deserialize(Of Seguro)(data)
    '        validacion = ValCotizacion(ObjData)
    '        Select Case ObjData.Aseguradora
    '            Case "GNP"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = GNPCotEmi.GNPCotizacion(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = GNPCotEmi.GNPEmision(ObjData)
    '                End If
    '            Case "QUALITAS"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = WsQUA.CotizaQualitas(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = WsQUA.EmiteQualitas(ObjData)
    '                End If
    '            Case "AXA"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = WsAXA.AXACotizacion(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = WsAXA.AXAEmision(ObjData)
    '                End If
    '            Case "ABA"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = WsABA.ABACotizacion(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = WsABA.ABAEmision(ObjData)
    '                End If
    '            Case "BANORTE"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = WsBANORTE.BANORTECotizacion(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = WsBANORTE.BANORTEemision(ObjData)
    '                End If
    '            Case "ANA"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = WsANA.ANACotizacion(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = WsANA.ANAEmision(ObjData)
    '                End If
    '            Case "HDI"
    '                If movimiento = "cotizacion" Then
    '                    If validacion = "True" Then
    '                        Respuesta = WsHDI.HDICotizacion(ObjData)
    '                    Else
    '                        ObjData.CodigoError = validacion
    '                        Respuesta = serializer.Serialize(ObjData)
    '                    End If
    '                ElseIf movimiento = "emision" Then
    '                    Respuesta = WsHDI.HDIEmision(ObjData)
    '                End If
    '                'Case "ZURICH"
    '                '    If movimiento = "cotizacion" Then
    '                '        Respuesta = WsZURICH.ZURICHCotizacion(ObjData)
    '                '    End If
    '        End Select
    '        Context.Response.Write(Respuesta)
    '    Else
    '        Dim errorAutentificacion = "Error de autenticación"
    '        Context.Response.Write(errorAutentificacion)
    '    End If
    'End Sub
    '<WebMethod>
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    'Public Function ToJSON(obj As Object) As String
    '    Dim serializer As New JavaScriptSerializer()
    '    Return serializer.Serialize(obj)
    'End Function


    <WebMethod>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function GNPimpresion(ByVal usuario As String, ByVal Password As String, ByVal idt As String) As String
        Dim cliente = "AhorraSeguros"
        Dim contrasena = "Ah0rraS3guros2017"
        Dim Respuesta As String = ""
        Dim Deserializer As New JavaScriptSerializer
        Dim ObjData As New Seguro
        If usuario = cliente And Password = contrasena Then
            Respuesta = WsGNPNuevo.GNPImpresion(idt)
            Return Respuesta
        Else
            Dim errorAutentificacion = "Error de autentificación"
            Return errorAutentificacion
        End If
    End Function

    <WebMethod>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function AXAimpresion(ByVal usuario As String, ByVal Password As String, ByVal poliza As String) As String
        Dim cliente = "AhorraSeguros"
        Dim contrasena = "Ah0rraS3guros2017"
        Dim Respuesta As String = ""
        Dim Deserializer As New JavaScriptSerializer
        Dim AGENTE = "AGT00607034"
        Dim clienteWS As integratorPortClient = preparaEndPoint()
        If usuario = cliente And Password = contrasena Then
            Dim urlPoliza As getPolicyDocumentRequest = New getPolicyDocumentRequest
            urlPoliza.Item = New downloadDocument
            urlPoliza.Item.UserId = "BancsMCS"
            urlPoliza.Item.Pwd = "b2a4a375f20a323bdd523462988dc070"
            urlPoliza.Item.ProyectoId = "3"
            urlPoliza.Item.ExpId = "3"
            urlPoliza.Item.AgentId = AGENTE
            urlPoliza.Item.ConfiguracionXml = "3"
            urlPoliza.Item.filtros = New abstractDocumentFiltros
            urlPoliza.Item.filtros.filtrop = "numero_poliza=[" & poliza & "]"
            urlPoliza.Item.filtros.filtroh = ""
            urlPoliza.Item.Rama = "1"
            urlPoliza.agentInfo = New agentInfo
            urlPoliza.agentInfo.agentCode = AGENTE
            urlPoliza.agentInfo.participationPercentage = "0"
            urlPoliza.agentInfo.cessionPercentage = "0"
            urlPoliza.agentInfo.cessionPercentageSpecified = True


            Dim x3 As New XmlSerializer(urlPoliza.GetType)
            Dim xml3 As New StringWriter
            x3.Serialize(xml3, urlPoliza)
            Dim errorws4 As String = xml3.ToString

            Dim urlPolizarespuesta As getPolicyDocumentResponse = clienteWS.getPolicyDocument(urlPoliza)
            Respuesta = DirectCast(urlPolizarespuesta.Item, downloadDocumentResponse).DownloadDocumentUrl
            Return Respuesta
        Else
            Dim errorAutentificacion = "Error de autentificación"
            Return errorAutentificacion
        End If
    End Function
    Private Shared Function preparaEndPoint() As integratorPortClient
        'Ambiente Produccion
        'Dim USERNAME As String = "MXS00101483A"
        'Dim PASSWORD As String = "Temporal017*"
        'Dim USERNAME As String = "MXS00101248A"
        'Dim PASSWORD As String = "TriProd17*"
        Dim USERNAME As String = "MXS00101483A"
        Dim PASSWORD As String = "Tri2017*"
        'Ambiente QA - Pruebas
        'Dim USERNAME As String = "MXS00101068A"
        'Dim PASSWORD As String = "Servicio2015*"
        Dim clienteWS As New integratorPortClient("integratorBancsSoap")
        clienteWS.ClientCredentials.UserName.UserName = USERNAME
        clienteWS.ClientCredentials.UserName.Password = PASSWORD
        Return clienteWS
    End Function
    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function GetBancos(ByVal usuario As String, ByVal password As String, ByVal aseguradora As String) As String
        Dim ObjBancos As New CatBancos
        ObjBancos.Aseguradora = UCase(aseguradora)
        If ObjBancos.Aseguradora = "ABA" Or ObjBancos.Aseguradora = "AXA" Or ObjBancos.Aseguradora = "ANA" Or ObjBancos.Aseguradora = "ATLAS" Or ObjBancos.Aseguradora = "SURA" Or ObjBancos.Aseguradora = "AIG" Or ObjBancos.Aseguradora = "AFIRME" Or ObjBancos.Aseguradora = "ELPOTOSI" Then
            aseguradora = "mapfre"
        End If
        Dim serializer As New JavaScriptSerializer()
        Dim cliente = "AhorraSeguros"
        Dim contrasena = "Ah0rraS3guros2017"
        serializer.MaxJsonLength = Integer.MaxValue
        If usuario = cliente And password = contrasena Then
            Dim respuesta As String = "<option value='select'>Marca</option>"
            Dim ConnectionString = "Data Source=10.20.18.149;Initial Catalog=ws_seguros_auto;User Id=mmendoza;Password=gL@0jAOS123;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn"
            Dim sql = "select  * from CatListBancos where " & aseguradora & "=1"
            Using Connection As New SqlConnection(ConnectionString)
                Dim command As New SqlCommand(sql, Connection)
                Try
                    Connection.Open()
                    Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                    While (reader.Read())
                        Dim Bancos As New ListadoBancos
                        Bancos.Abreviacion = reader("abreviacion")
                        Bancos.Nombre = reader("Nombre")
                        ObjBancos.Bancos.Add(Bancos)
                    End While
                    Connection.Close()
                Catch ex As Exception
                    Dim errror = ex
                End Try
            End Using

            Return serializer.Serialize(ObjBancos)
        Else
            Dim errorAutentificacion = "Error de autentificación"
            Return errorAutentificacion
        End If
    End Function

    Function ValCotizacion(ByVal obj As Seguro) As String
        Dim Val As String = "True"
        If obj.Aseguradora = Nothing Then
            Val = "El dato Aseguradora es obligatorio"
        ElseIf obj.Paquete = Nothing Then
            Val = "El dato Paquete es obligatorio"
        ElseIf (obj.PeriodicidadDePago = Nothing And obj.PeriodicidadDePago <> 0) Or (obj.PeriodicidadDePago <> 0 And obj.PeriodicidadDePago <> 4) Then
            Val = "El dato PeriodicidadDePago es obligatorio o el valor es incorrecto"
        ElseIf obj.Cliente.Genero = Nothing Then
            Val = "El dato Genero es obligatorio"
        ElseIf obj.Cliente.Edad = Nothing Then
            Val = "El dato Edad es obligatorio"
        ElseIf obj.Cliente.FechaNacimiento = Nothing Then
            Val = "El dato FechaNacimiento es obligatorio"
        ElseIf obj.Cliente.Direccion.CodPostal = Nothing Then
            Val = "El dato CodPostal es obligatorio"
        ElseIf obj.Vehiculo.Marca = Nothing Then
            Val = "El dato Marca es obligatorio"
        ElseIf obj.Vehiculo.Modelo = Nothing Then
            Val = "El dato Modelo es obligatorio"
        ElseIf obj.Vehiculo.Descripcion = Nothing Then
            Val = "El dato Descripcion es obligatorio"
        ElseIf obj.Vehiculo.Clave = Nothing Then
            Val = "El dato Clave es obligatorio"
        ElseIf obj.Vehiculo.Servicio = Nothing Then
            Val = "El dato Servicio es obligatorio"
        ElseIf obj.Vehiculo.Uso = Nothing Then
            Val = "El dato Uso es obligatorio"
        End If

        If obj.Vehiculo.Marca <> Nothing Then
            Dim ValClave = validarMarca(obj.Vehiculo.Marca, obj.Aseguradora)
            If ValClave <> "True" Then
                Val = "El dato Marca no existe"
            End If
        End If
        If obj.Vehiculo.Clave <> Nothing Then
            Dim ValClave = validarClave(obj.Vehiculo.Clave, obj.Aseguradora)
            If ValClave <> "True" Then
                Val = "El dato Clave es incorrecto"
            End If
        End If
        Return Val
    End Function
    Function validarClave(ByVal clave As String, ByVal Aseguradora As String) As String
        Dim Val As String = "False"
        'Dim ConnectionString = "Data Source=10.20.18.149;Initial Catalog=ws_seguros_auto;User Id=mmendoza;Password=gL@0jAOS123;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn"
        'Dim sql = "select * from CatalogosNuevos where  provider_code='" & Aseguradora & "' And provider_car_id='" & clave & "'"
        'Using Connection As New SqlConnection(ConnectionString)
        '    Dim command As New SqlCommand(sql, Connection)
        '    Try
        '        Connection.Open()
        '        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
        '        While (reader.Read())
        '            Val = "True"
        '        End While
        '        Connection.Close()
        '    Catch ex As Exception
        '        Dim errror = ex
        '    End Try
        'End Using
        Val = "True"

        Return Val
    End Function

    Function validarMarca(ByVal Marca As String, ByVal Aseguradora As String) As String
        Dim Val As String = "False"
        ' Dim ConnectionString = "Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ws_seguros_auto;User Id=servici1;Password=Master@011;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn"
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        Dim sql = "SELECT * FROM `Ali_R_Catalogos` where cia = '" & Aseguradora & "' and MarcaEst = '" & Marca & "' LIMIT 1"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(sql, Connection)
            Try
                Connection.Open()
                Dim reader As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                While (reader.Read())
                    Val = "True"
                End While
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return Val
    End Function
End Class