﻿Imports Microsoft.VisualBasic
Imports SuperObjeto
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports Newtonsoft.Json
Imports MySql.Data.MySqlClient
Imports System.Data

Public Class WsPS
    Public Shared Function PSCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)

        Try
            CallWebService(ObjData, idLogWSCot)
        Catch ex As Exception
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Dim log As Log = New Log()
        Dim serializer As New JavaScriptSerializer
        Dim JsonResponse As String = serializer.Serialize(ObjData)
        log.UpdateLogJsonResponse(JsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Sub CallWebService(ObjData As Seguro, ByVal idLogWSCot As String)
        Dim _url = "http://www.primeroseguros.com/webservices/cotizador?wsdl"

        Dim soapEnvelopeXml As XmlDocument = CreateSoapEnvelope(ObjData)
        Dim webRequest As HttpWebRequest = CreateWebRequest(_url, "")
        InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest)

        ' begin async call to web request.
        Dim asyncResult As IAsyncResult = webRequest.BeginGetResponse(Nothing, Nothing)

        ' suspend this thread until call is complete. You might want to
        ' do something usefull here like update your UI.
        asyncResult.AsyncWaitHandle.WaitOne()

        ' get the response from the completed web request.
        Dim soapResult As String
        Dim nombreCobertura As String = ""
        Dim doc As XmlDocument = New XmlDocument()
        Dim Coberturas As New Coberturas
        Dim lg As Log = New Log()
        Try
            Dim x2 As New XmlSerializer(soapEnvelopeXml.GetType)
            Dim xml2 As New StringWriter
            x2.Serialize(xml2, soapEnvelopeXml)
            Dim xmlRespuesta As String = xml2.ToString
            lg.UpdateLogRequest(xmlRespuesta, idLogWSCot)
            'Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "RequestWS", "FechaInicio")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Using webResponse As WebResponse = webRequest.EndGetResponse(asyncResult)
            Using rd As New StreamReader(webResponse.GetResponseStream())
                soapResult = rd.ReadToEnd()
            End Using
            doc.LoadXml(soapResult)
        End Using
        Try
            lg.UpdateLogResponse(soapResult, idLogWSCot)
            ' Funciones.updateLogWSCot(soapResult, idLogWSCot, "ResponseWS", "FechaFin")
        Catch ex As Exception
            Dim errr = ex
        End Try
        Dim colElementos As XmlNodeList = doc.GetElementsByTagName("return")
        For Each objNodoPaquete In colElementos
            Dim xmlnode3 As XmlNodeList = objNodoPaquete.GetElementsByTagName("coberturas")

            ObjData.Cotizacion.PrimaNeta = objNodoPaquete("prima_neta").InnerText
            ObjData.Cotizacion.Derechos = objNodoPaquete("derechos").InnerText
            ObjData.Cotizacion.Impuesto = objNodoPaquete("iva").InnerText
            ObjData.Cotizacion.PrimaTotal = objNodoPaquete("prima_total").InnerText
            ObjData.Cotizacion.Recargos = objNodoPaquete("recs").InnerText
            ObjData.Cotizacion.PrimerPago = objNodoPaquete("rec1").InnerText
            ObjData.Cotizacion.PagosSubsecuentes = "0"
            If objNodoPaquete("cantRec").InnerText <> "1" Then
                ObjData.Cotizacion.PagosSubsecuentes = objNodoPaquete("recs").InnerText
            End If
            ObjData.Cotizacion.Resultado = "True"
            For Each objnodo4 In xmlnode3
                Dim cobertura As String = objnodo4("id").InnerText
                Dim descripcionCobertura As String = ""
                Dim suma = objnodo4("dsc_sa").InnerText
                Select Case cobertura

                    Case "DM"
                        nombreCobertura = "DAÑOS MATERIALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("dsc_ded").InnerText
                        Coberturas.DanosMateriales = descripcionCobertura
                    Case "RT"
                        nombreCobertura = "ROBO TOTAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("dsc_ded").InnerText
                        Coberturas.RoboTotal = descripcionCobertura
                    Case "RCTBP"
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("dsc_ded").InnerText
                        Coberturas.RC = descripcionCobertura
                    Case "GMO"
                        nombreCobertura = "GASTOS MÉDICOS"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("dsc_ded").InnerText
                        Coberturas.GastosMedicosOcupantes = descripcionCobertura
                    Case "DJUR"
                        nombreCobertura = "GASTOS LEGALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("dsc_ded").InnerText
                        Coberturas.DefensaJuridica = descripcionCobertura
                    Case "ASVI"
                        nombreCobertura = "ASISTENCIA VIAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & suma & "-D" & objnodo4("dsc_ded").InnerText
                        Coberturas.AsitenciaCompleta = descripcionCobertura
                End Select
            Next
        Next
        ObjData.Coberturas.Add(Coberturas)
    End Sub
    Private Shared Function CreateSoapEnvelope(ObjData As Seguro) As XmlDocument
        Dim soapEnvelop As New XmlDocument()
        Dim Finicio = Date.Now.ToString("yyyy-MM-dd")
        Dim FFin = Date.Now.AddYears(1).ToString("yyyy-MM-dd")
        Dim Genero = ""
        If UCase(ObjData.Cliente.Genero) = "MASCULINO" Then
            Genero = "H"
        ElseIf UCase(ObjData.Cliente.Genero) = "FEMENINO" Then
            Genero = "M"
        End If
        Dim FormaPago As String = ""
        If ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.ANUAL Then
            FormaPago = "01"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.MENSUAL Then
            FormaPago = "04"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.SEMESTRAL Then
            FormaPago = "02"
        ElseIf ObjData.PeriodicidadDePago = Seguro.PeriodicidadPago.TRIMESTRAL Then
            FormaPago = "03"
        End If
        If ObjData.Descuento = Nothing Then
            ObjData.Descuento = "0"
        End If
        Dim ocupantes = GetOcupantes(ObjData.Vehiculo.Clave)
        Dim Estado = ConsultaEstados(ObjData.Cliente.Direccion.CodPostal)
        soapEnvelop.LoadXml("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ws='http://ws.businessunits.primero.com/'>" &
   "<soapenv:Header/>" &
   "<soapenv:Body>" &
      "<ws:cotiza>" &
         "<guardaCot>0</guardaCot>" &
         "<emiteCot>0</emiteCot>" &
         "<negocio>2347</negocio>" &
         "<ofi_clave>3554</ofi_clave>" &
         "<clave_proveedor>PRVTRI</clave_proveedor>" &
         "<usuario>PS1893</usuario>" &
         "<password>R2bAsa1Xv6</password>" &
         "<usuario_externo/>" &
         "<estado>" & Estado & "</estado>" &
         "<descuento>" & ObjData.Descuento & "</descuento>" &
         "<forpag>" & FormaPago & "</forpag>" &
         "<clave_auto>" & ObjData.Vehiculo.Clave & "</clave_auto>" &
         "<modelo_auto>" & ObjData.Vehiculo.Modelo & "</modelo_auto>" &
         "<ocupantes>" & ocupantes & "</ocupantes>" &
         "<tipserv>PAR</tipserv>" &
         "<tipuso>01</tipuso>" &
         "<inivig>" & Finicio & "</inivig>" &
         "<finvig>" & FFin & "</finvig>" &
         "<paquete>" & ObjData.Paquete & "</paquete>" &
         "<coberturas>" &
            "<id>DM</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>5</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>RT</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>10</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>RCTBP</id>" &
            "<suma_asegurada>800,000</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>GMO</id>" &
            "<suma_asegurada>200,000</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>DJUR</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>ASVI</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>EXCRCP</id>" &
            "<suma_asegurada>2,300,000</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>GV</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>NPD</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>AS</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
         "<coberturas>" &
            "<id>RCUSA</id>" &
            "<suma_asegurada>0</suma_asegurada>" &
            "<deducible>0</deducible>" &
         "</coberturas>" &
      "</ws:cotiza>" &
   "</soapenv:Body>" &
"</soapenv:Envelope>")
        Return soapEnvelop
    End Function
    Private Shared Function ConsultaEstados(ByVal CPostal As String) As String
        Dim objConnectionCPostal As SqlConnection
        Dim objCommandCPostal As SqlCommand
        Dim strSQLQueryCPostal As String
        Dim Estado As String = ""
        Dim connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
        strSQLQueryCPostal = "Select Estado from `ws_db`.`Ali_R_CatEdosQUA`  where CPostal = '" & CPostal & "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(strSQLQueryCPostal, Connection)
            Try
                Connection.Open()
                Dim rsCPostal As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                rsCPostal.Read()
                Estado = rsCPostal("Estado")
                If Estado = "VERACRUZ DE IGNACIO DE LA LLAVE" Then
                    Estado = "VERACRUZ"
                End If
                Connection.Close()
                Dim connectionString1 = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;"
                Dim strSQLQueryCPostal1 = "Select clave from  `ws_db`.`Ali_R_CatEdosPS` where Nombre = '" & Estado & "'"
                Dim Connection1 As New MySqlConnection(connectionString1)
                Dim command1 As New MySqlCommand(strSQLQueryCPostal1, Connection)
                Connection.Open()
                Dim rsClave As MySqlDataReader = command1.ExecuteReader(CommandBehavior.CloseConnection)
                rsClave.Read()
                Estado = rsClave("clave")
                Connection.Close()

            Catch ex As Exception
                Dim errror = ex
            End Try

        End Using

        '
        Return Estado
    End Function

    Private Shared Function GetOcupantes(ByVal Clave As String)
        Dim servicioCatalogos = New ServicioCatalogos.WebServiceSoapClient
        Dim jsonVehiculo = servicioCatalogos.GetVehiculo("AhorraSeguros", "Ah0rraS3guros2017", "PRIMEROSEGUROS", Clave)
        Dim mapaVehiculo = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(jsonVehiculo)
        Return mapaVehiculo.Item("passengers")
    End Function

    Private Shared Function CreateWebRequest(url As String, action As String) As HttpWebRequest
        Dim webRequest__1 As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
        webRequest__1.Accept = "text/xml"
        webRequest__1.Method = "POST"
        Return webRequest__1
    End Function

    Private Shared Sub InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml As XmlDocument, webRequest As HttpWebRequest)
        Using stream As Stream = webRequest.GetRequestStream()
            soapEnvelopeXml.Save(stream)
        End Using
    End Sub
End Class
