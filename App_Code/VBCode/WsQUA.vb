﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml
Imports Microsoft.VisualBasic
Imports SuperObjeto

Public Class WsQUA
    Public Shared Function CotizaQualitas(ByVal ObjData As Seguro, ByVal idLogWSCot As String) As String
        Dim respuestaWS As String = ""
        Dim obj As New Seguro
        Dim serializer As New JavaScriptSerializer
        Dim var As String = serializer.Serialize(ObjData)
        obj = serializer.Deserialize(Of Seguro)(var)
        Dim Negocio As String = "2886"
        Dim TipoMovimiento As String = "2"
        Dim FecIniVig As String = Date.Now.ToString("yyyy-MM-dd")
        Dim FecFinVig As String = Date.Now.AddYears(1).ToString("yyyy-MM-dd")
        Dim Paquete As String = ""
        Dim Agente As String = "71315"
        Dim FormaPago As String = ""
        Dim Tarifa As String = "LINEA"
        Dim Descuento As String = obj.Descuento
        Dim CveAmis As String = obj.Vehiculo.Clave
        Dim DigitoAmis As String = ""
        Dim lengthCP As Integer = obj.Cliente.Direccion.CodPostal.Length
        Dim lg As Log = New Log()
        For i = lengthCP To 4
            obj.Cliente.Direccion.CodPostal = "0" & obj.Cliente.Direccion.CodPostal
        Next
        If ObjData.PeriodicidadDePago = 0 Then
            FormaPago = "C"
        ElseIf ObjData.PeriodicidadDePago = 2 Then
            FormaPago = "M"
        End If
        If CveAmis.Length = 4 Then
            CveAmis = "0" & CveAmis
            DigitoAmis = DigitoCveAmis(CveAmis).ToString
        ElseIf CveAmis.Length = 3 Then
            CveAmis = "00" & CveAmis
            DigitoAmis = DigitoCveAmis(CveAmis).ToString
        Else
            DigitoAmis = DigitoCveAmis(CveAmis).ToString
        End If

        If obj.Paquete = "AMPLIA" Then
            Paquete = "01"
        ElseIf obj.Paquete = "LIMITADA" Then
            Paquete = "03"
        End If

        Dim uso As String = ""
        If obj.Vehiculo.Uso = "PARTICULAR" Then
            uso = "01"
        End If
        Dim servicio As String = ""
        If obj.Vehiculo.Servicio = "PARTICULAR" Then
            servicio = "01"
        End If

        Dim RC As String = "3000000"
        Dim GM As String = "200000"
        Dim DM As String = "5"
        Dim RT As String = "10"
        Dim AO As String = "50000"
        Dim TotalCoberturas = (ObjData.Coberturas).Count
        If TotalCoberturas > 0 Then
            If ObjData.Coberturas(0).RC <> "-" Then
                RC = obj.Coberturas(0).RC
            End If
            If ObjData.Coberturas(0).GastosMedicosEvento <> "-" Then
                GM = obj.Coberturas(0).GastosMedicosEvento
            End If
            If ObjData.Coberturas(0).DanosMateriales <> "-" Then
                DM = obj.Coberturas(0).DanosMateriales
            End If
            If ObjData.Coberturas(0).RoboTotal <> "-" Then
                RT = obj.Coberturas(0).RoboTotal
            End If
            If ObjData.Coberturas(0).MuerteAccidental <> "-" Then
                AO = obj.Coberturas(0).MuerteAccidental
            End If
        End If

        Dim Municipios As String()
        Try
            Municipios = ConsultaMunicipiosQualitas(obj.Cliente.Direccion.CodPostal, obj.Cliente.Direccion.Colonia)
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [Error al realizar consulta de municipios (CP)], StackTrace: [" + ex.StackTrace.ToString() + "]"
            Return serializer.Serialize(ObjData)
        End Try

        Dim xmlCotizacion = New XElement(<Movimientos></Movimientos>)
        Dim movimiento = New XElement(<Movimiento NoNegocio=<%= Negocio %> NoOTra="" TipoEndoso="" NoEndoso="" NoCotizacion="" NoPoliza="" TipoMovimiento=<%= TipoMovimiento %>></Movimiento>)
        movimiento.Add(<DatosAsegurado NoAsegurado="">
                           <Nombre/>
                           <Direccion/>
                           <Colonia/>
                           <Poblacion/>
                           <Estado><%= Municipios(0) %></Estado>
                           <CodigoPostal><%= obj.Cliente.Direccion.CodPostal %></CodigoPostal>
                           <NoEmpleado/>
                           <Agrupador/>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>3</TipoRegla>
                               <ValorRegla></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>4</TipoRegla>
                               <ValorRegla>1</ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>5</TipoRegla>
                               <ValorRegla></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>6</TipoRegla>
                               <ValorRegla></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                       </DatosAsegurado>)
        Dim datosVehiculo As XElement = <DatosVehiculo NoInciso="1">
                                            <ClaveAmis><%= obj.Vehiculo.Clave %></ClaveAmis>
                                            <Modelo><%= obj.Vehiculo.Modelo %></Modelo>
                                            <DescripcionVehiculo><%= obj.Vehiculo.Descripcion %></DescripcionVehiculo>
                                            <Uso><%= uso %></Uso>
                                            <Servicio><%= servicio %></Servicio>
                                            <Paquete><%= Paquete %></Paquete>
                                            <Motor></Motor>
                                            <Serie></Serie>
                                        </DatosVehiculo>
        If obj.Paquete = "AMPLIA" Then
            datosVehiculo.Add(New XElement(<Coberturas NoCobertura="1">
                                               <SumaAsegurada></SumaAsegurada>
                                               <TipoSuma>0</TipoSuma>
                                               <Deducible><%= DM %></Deducible>
                                               <Prima>0</Prima>
                                           </Coberturas>))
            datosVehiculo.Add(New XElement(<Coberturas NoCobertura="3">
                                               <SumaAsegurada></SumaAsegurada>
                                               <TipoSuma>0</TipoSuma>
                                               <Deducible><%= RT %></Deducible>
                                               <Prima>0</Prima>
                                           </Coberturas>))
        ElseIf obj.Paquete = "LIMITADA" Then
            datosVehiculo.Add(New XElement(<Coberturas NoCobertura="3">
                                               <SumaAsegurada></SumaAsegurada>
                                               <TipoSuma>00</TipoSuma>
                                               <Deducible><%= RT %></Deducible>
                                               <Prima>0</Prima>
                                           </Coberturas>))
        End If
        datosVehiculo.Add(<Coberturas NoCobertura="4">
                              <SumaAsegurada><%= RC %></SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="5">
                              <SumaAsegurada><%= GM %></SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="6">
                              <SumaAsegurada><%= AO %></SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="7">
                              <SumaAsegurada>0</SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="14">
                              <SumaAsegurada>0</SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        movimiento.Add(datosVehiculo)
        movimiento.Add(<DatosGenerales>
                           <FechaEmision><%= Date.Now.ToString("yyyy-MM-dd") %></FechaEmision>
                           <FechaInicio><%= Date.Now.ToString("yyyy-MM-dd") %></FechaInicio>
                           <FechaTermino><%= Date.Now.AddYears(1).ToString("yyyy-MM-dd") %></FechaTermino>
                           <Moneda>0</Moneda>
                           <Agente><%= Agente %></Agente>
                           <FormaPago><%= FormaPago %></FormaPago>
                           <TarifaValores><%= Tarifa %></TarifaValores>
                           <TarifaCuotas><%= Tarifa %></TarifaCuotas>
                           <TarifaDerechos><%= Tarifa %></TarifaDerechos>
                           <Plazo/>
                           <Agencia/>
                           <Contrato/>
                           <PorcentajeDescuento><%= Descuento %></PorcentajeDescuento>
                           <ConsideracionesAdicionalesDG NoConsideracion="01">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla><%= DigitoAmis %></ValorRegla>
                           </ConsideracionesAdicionalesDG>
                           <ConsideracionesAdicionalesDG NoConsideracion="04">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla>0</ValorRegla>
                           </ConsideracionesAdicionalesDG>
                       </DatosGenerales>)
        movimiento.Add(<Primas>
                           <PrimaNeta/>
                           <Derecho>500</Derecho>
                           <Recargo/>
                           <Impuesto/>
                           <PrimaTotal/>
                           <Comision/>
                       </Primas>)
        movimiento.Add(<CodigoError/>)
        xmlCotizacion.Add(movimiento)
        Dim objCotizacion As New QUAService.wsEmisionServiceSoapClient
        Try

            Try
                '// Funciones.updateLogWSCot(xmlCotizacion.ToString, idLogWSCot, "RequestWS", "FechaInicio")
                lg.UpdateLogResponse(xmlCotizacion.ToString, idLogWSCot)

            Catch ex As Exception
                Dim errr = ex
            End Try

            Dim res As String = objCotizacion.obtenerNuevaEmision(xmlCotizacion.ToString)

            Try
                Funciones.updateLogWSCot(res, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try

            Dim xmldoc1 As New XmlDocument
            xmldoc1.LoadXml(res)

            Dim Respuesta(5) As String
            Dim xmlnode As XmlNodeList

            Dim objNodo As XmlNode
            'Primas
            xmlnode = xmldoc1.GetElementsByTagName("Primas")
            For Each objNodo In xmlnode
                ObjData.Cotizacion.PrimaNeta = objNodo("PrimaNeta").InnerText
                ObjData.Cotizacion.Derechos = objNodo("Derecho").InnerText
                ObjData.Cotizacion.Impuesto = objNodo("Impuesto").InnerText
                ObjData.Cotizacion.PrimaTotal = objNodo("PrimaTotal").InnerText
            Next
            'Recibos
            xmlnode = xmldoc1.GetElementsByTagName("Recibos")
            For Each objNodo In xmlnode
                If objNodo.Attributes("NoRecibo").Value = "1" And FormaPago = "C" Then
                    ObjData.Cotizacion.PrimerPago = objNodo("PrimaTotal").InnerText 'Primer PAgo
                    ObjData.Cotizacion.PagosSubsecuentes = Nothing 'Pago Subsecuente
                    Exit For
                ElseIf objNodo.Attributes("NoRecibo").Value = "1" And FormaPago = "M" Then
                    ObjData.Cotizacion.PrimerPago = objNodo("PrimaTotal").InnerText ' Primer pago
                ElseIf objNodo.Attributes("NoRecibo").Value <> "1" And FormaPago = "M" Then
                    ObjData.Cotizacion.PagosSubsecuentes = objNodo("PrimaTotal").InnerText 'Pago subsecuente
                End If
            Next
            xmlnode = xmldoc1.GetElementsByTagName("DatosVehiculo")
            Dim objNodoCobertura As XmlNode
            Dim xmlnode2 As XmlNodeList = xmldoc1.GetElementsByTagName("Coberturas")
            Dim objNodo2 As XmlNode
            Dim nodo As XmlNode
            Dim nombreCobertura As String = ""
            Dim Coberturas As New Coberturas
            For Each objNodoCobertura In xmlnode
                For Each objNodo2 In xmlnode2
                    Dim cobertura As String = objNodo2.Attributes("NoCobertura").Value
                    Dim descripcionCobertura As String = ""


                    Select Case cobertura

                        Case "1"
                            nombreCobertura = "DAÑOS MATERIALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.DanosMateriales = descripcionCobertura
                        Case "3"
                            nombreCobertura = "ROBO TOTAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.RoboTotal = descripcionCobertura
                        Case "4"
                            nombreCobertura = "RESPONSABILIDAD CIVIL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.RCBienes = descripcionCobertura
                        Case "5"
                            nombreCobertura = "GASTOS MÉDICOS"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.GastosMedicosOcupantes = descripcionCobertura
                        Case "6"
                            nombreCobertura = "MUERTE DEL CONDUCTOR POR ACCIDENTE AUTOMOVILISTICO"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.MuerteAccidental = descripcionCobertura
                        Case "7"
                            nombreCobertura = "GASTOS LEGALES"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.DefensaJuridica = descripcionCobertura
                        Case "11"
                            nombreCobertura = "EXTENSIÓN DE RC"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.RCExtension = descripcionCobertura
                        Case "14"
                            nombreCobertura = "ASISTENCIA VIAL"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.AsitenciaCompleta = descripcionCobertura
                        Case "34"
                            nombreCobertura = "RC EN EL EXTRANJERO"
                            descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                            Coberturas.RCExtranjero = descripcionCobertura
                    End Select
                Next
            Next
            ObjData.Coberturas.Add(Coberturas)
            ObjData.Cotizacion.Resultado = "True"
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Return serializer.Serialize(ObjData)
    End Function

    Public Shared Function EmiteQualitas(ByVal ObjData As Seguro, ByVal idLogWS As String) As String
        Dim respuestaWS As String = ""
        Dim serializer As New JavaScriptSerializer
        Dim Negocio As String = "2886"
        Dim TipoMovimiento As String = "3"
        Dim FecIniVig As String = Date.Now.ToString("yyyy-MM-dd")
        Dim FecFinVig As String = Date.Now.AddYears(1).ToString("yyyy-MM-dd")
        Dim Paquete As String = ""
        Dim Agente As String = "71315"
        Dim FormaPago As String = ""
        Dim Tarifa As String = "LINEA"
        Dim Descuento As String = ObjData.Descuento
        Dim CveAmis As String = ObjData.Vehiculo.Clave
        Dim DigitoAmis As String = ""
        Dim Genero As String = ""
        Dim Recargo As String = "14"
        Dim PlazoPago As String = ""
        Dim fechaNac As String = ObjData.Cliente.FechaNacimiento
        Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
        For i = lengthCP To 4
            ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
        Next
        If ObjData.PeriodicidadDePago = 0 Then
            FormaPago = "C"
            PlazoPago = "A"
        ElseIf ObjData.PeriodicidadDePago = 2 Then
            FormaPago = "M"
            PlazoPago = "M"
        End If
        If CveAmis.Length = 4 Then
            CveAmis = "0" & CveAmis
            DigitoAmis = DigitoCveAmis(CveAmis).ToString
        ElseIf CveAmis.Length = 3 Then
            CveAmis = "00" & CveAmis
            DigitoAmis = DigitoCveAmis(CveAmis).ToString
        Else
            DigitoAmis = DigitoCveAmis(CveAmis).ToString
        End If

        If ObjData.Paquete = "AMPLIA" Then
            Paquete = "01"
        ElseIf ObjData.Paquete = "LIMITADA" Then
            Paquete = "03"
        End If

        Dim uso As String = ""
        If ObjData.Vehiculo.Uso = "PARTICULAR" Then
            uso = "01"
        End If
        Dim servicio As String = ""
        If ObjData.Vehiculo.Servicio = "PARTICULAR" Then
            servicio = "01"
        End If
        If ObjData.Cliente.Genero = UCase("MASCULINO") Then
            Genero = "M"
        ElseIf ObjData.Cliente.Genero = UCase("FEMENINO") Then
            Genero = "F"
        End If

        Dim RC As String = "3000000"
        Dim GM As String = "200000"
        Dim DM As String = "5"
        Dim RT As String = "10"
        Dim AO As String = "50000"

        Dim NombreCompleto As String = ObjData.Cliente.Nombre & " " & ObjData.Cliente.ApellidoPat & " " & ObjData.Cliente.ApellidoMat
        Dim Direccion As String = ObjData.Cliente.Direccion.Calle & " " & ObjData.Cliente.Direccion.NoExt & " " & ObjData.Cliente.Direccion.NoInt
        Dim Municipios As String() = ConsultaMunicipiosQualitas(ObjData.Cliente.Direccion.CodPostal, ObjData.Cliente.Direccion.Colonia)

        Dim xmlCotizacion = New XElement(<Movimientos></Movimientos>)
        Dim movimiento = New XElement(<Movimiento NoNegocio=<%= Negocio %> NoOTra="" TipoEndoso="" NoEndoso="" NoCotizacion="" NoPoliza="" TipoMovimiento=<%= TipoMovimiento %>></Movimiento>)
        movimiento.Add(<DatosAsegurado NoAsegurado="">
                           <Nombre><%= NombreCompleto %></Nombre>
                           <Direccion><%= ObjData.Cliente.Direccion.Calle %></Direccion>
                           <Colonia><%= ObjData.Cliente.Direccion.Colonia %></Colonia>
                           <Poblacion><%= Municipios(1) %></Poblacion>
                           <Estado><%= Municipios(0) %></Estado>
                           <CodigoPostal><%= ObjData.Cliente.Direccion.CodPostal %></CodigoPostal>
                           <NoEmpleado/>
                           <Agrupador/>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.Direccion.NoExt %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>2</TipoRegla>
                               <ValorRegla></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>3</TipoRegla>
                               <ValorRegla>MEXICO</ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>4</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.Nombre %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>5</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.ApellidoPat %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>6</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.ApellidoMat %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>7</TipoRegla>
                               <ValorRegla><%= Municipios(2) %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>8</TipoRegla>
                               <ValorRegla><%= Municipios(3) %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>19</TipoRegla>
                               <ValorRegla>1</ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>20</TipoRegla>
                               <ValorRegla><%= fechaNac %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>21</TipoRegla>
                               <ValorRegla>1</ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>28</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.RFC %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>56</TipoRegla>
                               <ValorRegla><%= Genero %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>26</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.Email %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                           <ConsideracionesAdicionalesDA NoConsideracion="40">
                               <TipoRegla>70</TipoRegla>
                               <ValorRegla><%= ObjData.Cliente.Telefono %></ValorRegla>
                           </ConsideracionesAdicionalesDA>
                       </DatosAsegurado>)
        Dim datosVehiculo As XElement = <DatosVehiculo NoInciso="1">
                                            <ClaveAmis><%= ObjData.Vehiculo.Clave %></ClaveAmis>
                                            <Modelo><%= ObjData.Vehiculo.Modelo %></Modelo>
                                            <DescripcionVehiculo><%= ObjData.Vehiculo.Descripcion %></DescripcionVehiculo>
                                            <Uso><%= uso %></Uso>
                                            <Servicio><%= servicio %></Servicio>
                                            <Paquete><%= Paquete %></Paquete>
                                            <Motor><%= ObjData.Vehiculo.NoMotor %></Motor>
                                            <Serie><%= ObjData.Vehiculo.NoSerie %></Serie>
                                        </DatosVehiculo>
        If ObjData.Paquete = "AMPLIA" Then
            datosVehiculo.Add(New XElement(<Coberturas NoCobertura="1">
                                               <SumaAsegurada></SumaAsegurada>
                                               <TipoSuma>0</TipoSuma>
                                               <Deducible><%= DM %></Deducible>
                                               <Prima>0</Prima>
                                           </Coberturas>))
            datosVehiculo.Add(New XElement(<Coberturas NoCobertura="3">
                                               <SumaAsegurada></SumaAsegurada>
                                               <TipoSuma>0</TipoSuma>
                                               <Deducible><%= RT %></Deducible>
                                               <Prima>0</Prima>
                                           </Coberturas>))
        ElseIf ObjData.Paquete = "LIMITADA" Then
            datosVehiculo.Add(New XElement(<Coberturas NoCobertura="3">
                                               <SumaAsegurada></SumaAsegurada>
                                               <TipoSuma>0</TipoSuma>
                                               <Deducible><Deducible><%= RT %></Deducible>
                                               </Deducible>
                                               <Prima>0</Prima>
                                           </Coberturas>))
        End If
        datosVehiculo.Add(<Coberturas NoCobertura="4">
                              <SumaAsegurada><%= RC %></SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="5">
                              <SumaAsegurada><%= GM %></SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="6">
                              <SumaAsegurada><%= AO %></SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="7">
                              <SumaAsegurada>0</SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        datosVehiculo.Add(<Coberturas NoCobertura="14">
                              <SumaAsegurada>0</SumaAsegurada>
                              <TipoSuma>0</TipoSuma>
                              <Deducible>0</Deducible>
                              <Prima>0</Prima>
                          </Coberturas>)
        movimiento.Add(datosVehiculo)
        movimiento.Add(<DatosGenerales>
                           <FechaEmision><%= Date.Now.ToString("yyyy-MM-dd") %></FechaEmision>
                           <FechaInicio><%= Date.Now.ToString("yyyy-MM-dd") %></FechaInicio>
                           <FechaTermino><%= Date.Now.AddYears(1).ToString("yyyy-MM-dd") %></FechaTermino>
                           <Moneda>0</Moneda>
                           <Agente><%= Agente %></Agente>
                           <FormaPago><%= FormaPago %></FormaPago>
                           <TarifaValores><%= Tarifa %></TarifaValores>
                           <TarifaCuotas><%= Tarifa %></TarifaCuotas>
                           <TarifaDerechos><%= Tarifa %></TarifaDerechos>
                           <Plazo/>
                           <Agencia/>
                           <Contrato/>
                           <PorcentajeDescuento><%= Descuento %></PorcentajeDescuento>
                           <ConsideracionesAdicionalesDG NoConsideracion="01">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla><%= DigitoAmis %></ValorRegla>
                           </ConsideracionesAdicionalesDG>
                           <ConsideracionesAdicionalesDG NoConsideracion="04">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla>0</ValorRegla>
                           </ConsideracionesAdicionalesDG>
                           <ConsideracionesAdicionalesDG NoConsideracion="05">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla><%= Recargo %></ValorRegla>
                           </ConsideracionesAdicionalesDG>
                           <ConsideracionesAdicionalesDG NoConsideracion="16">
                               <TipoRegla>1</TipoRegla>
                               <ValorRegla><%= ObjData.Vehiculo.NoPlacas %></ValorRegla>
                           </ConsideracionesAdicionalesDG>
                           <ConsideracionesAdicionalesDG NoConsideracion="31">
                               <TipoRegla></TipoRegla>
                               <ValorRegla></ValorRegla>
                           </ConsideracionesAdicionalesDG>
                           <ConsideracionesAdicionalesDG NoConsideracion="10">
                               <TipoRegla>16</TipoRegla>
                               <ValorRegla><%= ObjData.Vehiculo.NoPlacas %></ValorRegla>
                           </ConsideracionesAdicionalesDG>
                       </DatosGenerales>)
        movimiento.Add(<Primas>
                           <PrimaNeta/>
                           <Derecho>500</Derecho>
                           <Recargo/>
                           <Impuesto/>
                           <PrimaTotal/>
                           <Comision/>
                       </Primas>)
        movimiento.Add(<CodigoError/>)
        xmlCotizacion.Add(movimiento)



        Dim IdBanco As String = getIdBanco(ObjData.Pago.Banco)
        Dim xmlCobranzaTxt As XElement = <oplCollection></oplCollection>
        Dim collectionXML As XElement = <Collection NoNegocio=<%= Negocio %> NoPoliza=""></Collection>
        If ObjData.Pago.MedioPago.ToUpper = "DEBITO" Then
            collectionXML.Add(<collectionData>
                                  <type>D</type>
                                  <userkey>RFC</userkey>
                                  <name><%= ObjData.Pago.NombreTarjeta %></name>
                                  <number><%= ObjData.Pago.NoClabe %></number>
                                  <bankcode><%= IdBanco %></bankcode>
                                  <expmonth/>
                                  <expyear/>
                                  <cvv-csc/>
                              </collectionData>)
        ElseIf ObjData.Pago.MedioPago.ToUpper = "CREDITO" Then 'Credito
            Dim tipoPago = ObjData.Pago.MSI
            If tipoPago = "" Then
                tipoPago = "CL"
            End If
            collectionXML.Add(<collectionData>
                                  <type>tipoPago</type>
                                  <userkey><%= ObjData.Cliente.RFC %></userkey>
                                  <name><%= ObjData.Pago.NombreTarjeta %></name>
                                  <number><%= ObjData.Pago.NoTarjeta %></number>
                                  <bankcode><%= IdBanco %></bankcode>
                                  <expmonth><%= ObjData.Pago.MesExp %></expmonth>
                                  <expyear><%= ObjData.Pago.AnioExp %></expyear>
                                  <cvv-csc><%= ObjData.Pago.CodigoSeguridad %></cvv-csc>
                              </collectionData>)
        End If
        collectionXML.Add(<insuranceData>
                              <akey><%= Agente %></akey>
                              <email><%= ObjData.Cliente.Email %></email>
                              <PlazoPago><%= PlazoPago %></PlazoPago>
                          </insuranceData>)
        collectionXML.Add(<CodigoError/>)
        xmlCobranzaTxt.Add(collectionXML)

        Try
            Dim id = Funciones.updateLogWS(movimiento.ToString + " | " + collectionXML.ToString, idLogWS, "RequestWS")
        Catch ex As Exception
        End Try

        Dim objCotizacion As New QUAService.wsEmisionServiceSoapClient
        Dim res As String = ""
        Try
            res = objCotizacion.obtenerNuevaEmisionDXN(xmlCotizacion.ToString, xmlCobranzaTxt.ToString)
        Catch ex As Exception
            ObjData.Emision.Resultado = "False"
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [Error al realizar invocacion del metodo obtenerNuevaEmisionDXN], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Try
            Funciones.updateLogWS(res, idLogWS, "ResponseWS")
        Catch ex As Exception
        End Try

        Dim Poliza As String
        Dim Recibo As String

        'Respuesta Emision
        Dim xmlResEmision = Mid(res, 1, InStr(res, "~") - 1)
        Dim xmlDocEmision As New XmlDocument
        xmlDocEmision.LoadXml(xmlResEmision)
        Dim errorPol As String = xmlResEmision

        Dim m_xmlResEmi1 As New XmlTextReader(New StringReader(xmlResEmision))
        m_xmlResEmi1.Read()
        While Not m_xmlResEmi1.EOF
            Poliza = m_xmlResEmi1.GetAttribute("NoPoliza")
            If Poliza = Nothing Then
                m_xmlResEmi1.Read()
            Else
                Exit While
            End If
        End While
        m_xmlResEmi1.Close()

        Dim m_xmlResEmi2 As New XmlTextReader(New StringReader(xmlResEmision))
        m_xmlResEmi2.Read()
        While Not m_xmlResEmi2.EOF
            Recibo = m_xmlResEmi2.GetAttribute("NoRecibo")
            If Recibo = Nothing Then
                m_xmlResEmi2.Read()
            Else
                Exit While
            End If
        End While
        m_xmlResEmi2.Close()

        Dim colElementos As XmlNodeList = xmlDocEmision.GetElementsByTagName("Primas")
        Dim objNodo As XmlNode
        For Each objNodo In colElementos
            ObjData.Emision.PrimaNeta = objNodo("PrimaNeta").InnerText
            ObjData.Emision.Derechos = objNodo("Derecho").InnerText
            ObjData.Emision.Impuesto = objNodo("Impuesto").InnerText
            ObjData.Emision.PrimaTotal = objNodo("PrimaTotal").InnerText
            ObjData.Emision.Recargos = objNodo("Recargo").InnerText
        Next

        'Recibos
        colElementos = xmlDocEmision.GetElementsByTagName("Recibos")
        For Each objNodo In colElementos
            If objNodo.Attributes("NoRecibo").Value = "1" And FormaPago = "C" Then
                ObjData.Cotizacion.PrimerPago = objNodo("PrimaTotal").InnerText 'Primer PAgo
                ObjData.Cotizacion.PagosSubsecuentes = Nothing 'Pago Subsecuente
                Exit For
            ElseIf objNodo.Attributes("NoRecibo").Value = "1" And FormaPago = "M" Then
                ObjData.Cotizacion.PrimerPago = objNodo("PrimaTotal").InnerText ' Primer pago
            ElseIf objNodo.Attributes("NoRecibo").Value <> "1" And FormaPago = "M" Then
                ObjData.Cotizacion.PagosSubsecuentes = objNodo("PrimaTotal").InnerText 'Pago subsecuente
            End If
        Next

        'Poliza
        colElementos = xmlDocEmision.GetElementsByTagName("Movimiento")
        For Each objNodo In colElementos
            ObjData.Emision.Poliza = objNodo.Attributes("NoPoliza").Value
        Next

        'Coberturas
        colElementos = xmlDocEmision.GetElementsByTagName("DatosVehiculo")
        Dim objNodoCobertura As XmlNode
        Dim xmlnode2 As XmlNodeList = xmlDocEmision.GetElementsByTagName("Coberturas")
        Dim objNodo2 As XmlNode
        Dim nodo As XmlNode
        Dim nombreCobertura As String = ""
        Dim Coberturas As New Coberturas
        For Each objNodoCobertura In colElementos
            For Each objNodo2 In xmlnode2
                Dim cobertura As String = objNodo2.Attributes("NoCobertura").Value
                Dim descripcionCobertura As String = ""


                Select Case cobertura

                    Case "1"
                        nombreCobertura = "DAÑOS MATERIALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.DanosMateriales = descripcionCobertura
                    Case "3"
                        nombreCobertura = "ROBO TOTAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.RoboTotal = descripcionCobertura
                    Case "4"
                        nombreCobertura = "RESPONSABILIDAD CIVIL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.RCBienes = descripcionCobertura
                    Case "5"
                        nombreCobertura = "GASTOS MÉDICOS"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.GastosMedicosOcupantes = descripcionCobertura
                    Case "6"
                        nombreCobertura = "MUERTE DEL CONDUCTOR POR ACCIDENTE AUTOMOVILISTICO"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.MuerteAccidental = descripcionCobertura
                    Case "7"
                        nombreCobertura = "GASTOS LEGALES"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.DefensaJuridica = descripcionCobertura
                    Case "11"
                        nombreCobertura = "EXTENSIÓN DE RC"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.RCExtension = descripcionCobertura
                    Case "14"
                        nombreCobertura = "ASISTENCIA VIAL"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.AsitenciaCompleta = descripcionCobertura
                    Case "34"
                        nombreCobertura = "RC EN EL EXTRANJERO"
                        descripcionCobertura &= "-N" & nombreCobertura & "-S" & objNodo2("SumaAsegurada").InnerText & "-D" & objNodo2("Deducible").InnerText
                        Coberturas.RCExtranjero = descripcionCobertura
                End Select
            Next
        Next
        ObjData.Coberturas.Add(Coberturas)

        Try
            If errorPol.Contains("<CodigoError>") Then
                respuestaWS = Mid(errorPol, InStr(errorPol, "<CodigoError>"), InStr(errorPol, "</CodigoError>") - InStr(errorPol, "<CodigoError>"))
                ObjData.Emision.Resultado = "False"
                Throw New System.Exception("Error al procesar emision, respuesta del sericio web no satisfactorio")
            Else
                ObjData.CodigoError = Nothing
            End If
            Dim xmlResCobro = Mid(res, InStr(res, "~") + 1)
            Dim xmlDocCobro As New XmlDocument

            Dim errorCob As String = xmlResCobro
            If errorCob.Contains("<Error>") Then
                respuestaWS = Mid(errorCob, InStr(errorCob, "<Error>"), InStr(errorCob, "</Error>") - InStr(errorCob, "<Error>"))
                ObjData.Emision.Resultado = "False"
                Throw New System.Exception("Error al procesar emision, respuesta del sericio web no satisfactorio")
            Else
                ObjData.Emision.Resultado = "True"
            End If

            Dim SubRamo As String = Mid(Poliza, 1, 2)
            Dim objImpresion As New QUA_Impr_ProdPrueb.entradaSoapClient
            Poliza = Mid(Poliza, 3, 10)

            Dim pdf As String = objImpresion.RecuperaImpresionM15(Poliza, "", "", "", 1, 0, 0, 0, SubRamo, "polizaf1_logoQ_pdf", "recibo_logoQ_pdf", "polizaf2_logoQ_pdf", "000000", Negocio, Agente, "Hola", "102030")

            ObjData.Emision.Poliza = Poliza
            ObjData.Emision.Documento = pdf
        Catch ex As Exception
            ObjData.Emision.Poliza = Poliza
            ObjData.Emision.Documento = Nothing
            ObjData.CodigoError = "WSRequest: [" + respuestaWS + "]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try
        Return serializer.Serialize(ObjData)
    End Function
    Private Shared Function DigitoCveAmis(ByVal Amis As String) As String
        Dim Suma As String = ""
        Dim Non1 As Integer = Mid(Amis, 1, 1)
        Dim Non2 As Integer = Mid(Amis, 3, 1)
        Dim Non3 As Integer = Mid(Amis, 5, 1)
        Dim Par1 As Integer = Mid(Amis, 2, 1)
        Dim Par2 As Integer = Mid(Amis, 4, 1)
        Dim Digito As Integer = 0
        Suma = ((Non1 + Non2 + Non3) * 3) + (Par1 + Par2)
        While (Suma Mod 10) <> 0
            Digito = Digito + 1
            Suma = Suma + 1
        End While
        Return Digito
    End Function
    Private Shared Function ConsultaMunicipiosQualitas(ByVal CPostal As String, ByVal Colonia As String) As String()
        Dim objConnectionCPostal As SqlConnection
        Dim objCommandCPostal As SqlCommand
        Dim strSQLQueryCPostal As String
        Dim Lista(3) As String
        Dim BusquedaColonia As String = ""
        If Colonia <> "" And Colonia <> Nothing Then
            BusquedaColonia = " And Colonia='" & Colonia & "'"
        End If
        objConnectionCPostal = New SqlConnection("Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ws_seguros_auto;User Id=servici1;Password=Master@011;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn")
        strSQLQueryCPostal = "Select * from CatEdosQUA where CPostal = '" & CPostal & "'" & BusquedaColonia
        objCommandCPostal = New SqlCommand(strSQLQueryCPostal, objConnectionCPostal)
        objConnectionCPostal.Open()

        Dim rsCPostal As SqlDataReader = objCommandCPostal.ExecuteReader
        rsCPostal.Read()
        Lista(0) = rsCPostal("IDEdo_Qua")
        Lista(1) = rsCPostal("Municipio")
        Lista(2) = rsCPostal("IDMun_Qua")
        Lista(3) = rsCPostal("IDColonia_Qua")
        Return Lista
    End Function

    Private Shared Function getIdBanco(ByVal Banco As String) As String
        Dim objConnectionCPostal As SqlConnection
        Dim objCommandCPostal As SqlCommand
        Dim strSQLQueryCPostal As String
        Dim Lista(3) As String
        Dim IdBanco As String = ""

        objConnectionCPostal = New SqlConnection("Data Source=10.20.18.149;Initial Catalog=ws_seguros_auto;User Id=mmendoza;Password=gL@0jAOS123;Connect Timeout=80;Max Pool Size=100000;Network Library=dbmssocn")
        strSQLQueryCPostal = "Select * from CatBancos where Abreviacion ='" & Banco & "'"
        objCommandCPostal = New SqlCommand(strSQLQueryCPostal, objConnectionCPostal)
        objConnectionCPostal.Open()

        Dim rsCPostal As SqlDataReader = objCommandCPostal.ExecuteReader
        rsCPostal.Read()
        IdBanco = rsCPostal("IdBanco")
        Return IdBanco
    End Function
End Class
