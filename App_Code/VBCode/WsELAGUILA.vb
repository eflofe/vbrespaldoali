﻿Imports System.Data
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Xml.Serialization
Imports Microsoft.VisualBasic
Imports SuperObjeto
Public Class WsELAGUILA


    Public Shared Function AguilaCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Dim cotizar = New ELAGUILAService.CalculoAguilaClient
        Dim catalogo = New ELAGUILACatalogueService.CatCotizadorClient
        Dim respuestaCatalogo = catalogo.SubRamo("AgtTrigarante2017#%")
        Dim coberturas = New Coberturas
        Dim genero = ""
        Dim lg As Log = New Log
        If ObjData.Cliente.Genero.ToUpper() = "MASCULINO" Then
            genero = "H"
        ElseIf ObjData.Cliente.Genero.ToUpper() = "FEMENINO" Then
            genero = "M"
        End If

        Try
            Dim request As String = "<tem:CotizaAguila><strPasswordAPP>AgtTrigarante2017#%<strPasswordAPP>" &
                                    " <tem:intModAuto> " & ObjData.Vehiculo.Modelo & "</tem:intModAuto> " &
                                    " <tem:intModelo>" & ObjData.Vehiculo.Clave & "</tem:intModelo>" &
                                    " <tem:strSubRamo>R</tem:strSubRamo> " &
                                    " <tem:intCP>" & ObjData.Cliente.Direccion.CodPostal & "</tem:intCP>" &
                                    " <tem:strFechaNaciCondu>" & ObjData.Cliente.FechaNacimiento & "</tem:strFechaNaciCondu>" &
                                    " <tem:strSexoCondu>" & genero & "</tem:strSexoCondu>" &
                                    " <tem:strEdoCivil>C</tem:strEdoCivil>" &
                                    " </tem:CotizaAguila>"
            'Funciones.updateLogWSCot(request, idLogWSCot, "RequestWS", "FechaInicio")
            lg.UpdateLogRequest(request, idLogWSCot)
        Catch ex As Exception
            Dim errr = ex
        End Try

        Dim respuesta
        Try
            respuesta = cotizar.CotizaAguila("AgtTrigarante2017#%", CInt(ObjData.Vehiculo.Modelo), CInt(ObjData.Vehiculo.Clave), "R", CInt(ObjData.Cliente.Direccion.CodPostal), ObjData.Cliente.FechaNacimiento, genero, "C")
        Catch ex As Exception
            ObjData.CodigoError = "WSRequest: [N/A]" + ", Message: [" + ex.Message + "], StackTrace: [" + ex.StackTrace.ToString() + "]"
        End Try

        Try
            Dim x3 As New XmlSerializer(respuesta.GetType)
            Dim xml3 As New StringWriter
            x3.Serialize(xml3, respuesta)
            Dim errorws4 As String = xml3.ToString
            ' Funciones.updateLogWSCot(errorws4, idLogWSCot, "ResponseWS", "FechaFin")
            lg.UpdateLogResponse(errorws4, idLogWSCot)
        Catch ex As Exception

        End Try

        ObjData.Cotizacion.Resultado = "False"
        For Each row As DataRow In respuesta.Tables(0).Rows
            Dim paqueteDataSet = row("NombreCobertura")
            Dim sumaAsegRC = ""
            Dim sumaAsegERC = ""
            Dim sumaAsegAV = ""
            Dim sumaAsegAL = ""
            If paqueteDataSet.ToUpper().Contains(ObjData.Paquete) Then
                ObjData.Cotizacion.PrimaTotal = row("TotalContado")
                ObjData.Cotizacion.Impuesto = (CInt(row("TotalContado")) * 0.16).ToString
                ObjData.Cotizacion.PrimaNeta = (CInt(ObjData.Cotizacion.PrimaTotal) - CInt(ObjData.Cotizacion.Impuesto)).ToString
                If row("ExtensionRC") = "S" Then
                    sumaAsegERC = "AMPARADA"
                End If
                If row("AsisVje") <> "N" Or row("AsisVje") <> "No" Then
                    sumaAsegAV = "AMPARADA"
                End If
                If row("AseLegal") = "S" Then
                    sumaAsegAL = "AMPARADA"
                End If
                coberturas.DanosMateriales = "-NDAÑOS MATERIALES-S0-D" & row("DM")
                coberturas.RoboTotal = "-NROBO TOTAL-S0-D" & row("RT")
                coberturas.RC = "-NRESPONSABILIDAD CIVIL-S" & row("RC") & "-D0"
                coberturas.GastosMedicosEvento = "-NGASTOS MÉDICOS-S" & row("GM") & "-D0"
                If sumaAsegERC <> "" Then
                    coberturas.RCExtension = "-NEXTENSION RC-S" & sumaAsegERC & "-D0"
                End If
                If sumaAsegAL <> "" Then
                    coberturas.DefensaJuridica = "-NDEF. JUD. Y ASIS. LEGAL-S" & sumaAsegAL & "-D0"
                End If
                If sumaAsegAV <> "" Then
                    coberturas.AsitenciaCompleta = "-NASISTENCIA VIAL-S" & sumaAsegAV & "-D0"
                End If
            End If
            ObjData.Cotizacion.Resultado = "True"
        Next
        coberturas.RCFamiliar = "-"
        ObjData.Coberturas.Add(coberturas)
        Dim serializer As New JavaScriptSerializer
        Dim JsonResponse As String = serializer.Serialize(ObjData)
        lg.UpdateLogJsonResponse(JsonResponse, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function
End Class