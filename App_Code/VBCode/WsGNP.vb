﻿
Imports System.Xml
Imports SuperObjeto
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.IO
Imports System.Xml.Serialization
Imports MySql.Data.MySqlClient

Partial Class GNPCotEmi
    Inherits System.Web.UI.Page
    Public Shared Function GNPCotizacion(ByVal ObjData As Seguro, ByVal idLogWSCot As String)
        Try
            Dim ObjGNP = New GNPService.CotizadorGNPSoapClient
            Dim usuario As String = "TRIGARANTEWS"
            Dim password As String = "Trig_WS"
            Dim codPromocion As String = "TRIGARANTE_WSC"
            Dim tienda = "0"
            Dim opcionContratar As String = "TRIGARANTE HC1"
            Dim MetodoPago As String = "I"
            Dim InicioVigencia As String = Now.ToString("dd") & "/" & Now.ToString("MM") & "/" & Now.Year.ToString
            Dim FinVigencia As String = Now.ToString("dd") & "/" & Now.ToString("MM") & "/" & Now.AddYears(1).Year.ToString
            Dim movimiento As String = "C"
            Dim descuento As String = ObjData.Descuento
            Dim cesionComicion As String = "0"
            Dim recibos As String = "IN"
            Dim codIntermediario As String = "0089487001"
            Dim ValVehi As String = "0"
            Dim FormIndemnizacion As String = "Valor Convenido"
            Dim dataXML As New XmlDocument
            Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
            For i = lengthCP To 4
                ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
            Next
            Dim estado = ConsultaPoblacionGNP(ObjData.Cliente.Direccion.CodPostal)
            Dim EdoCirculacion As String = GetCodEdo(estado)
            Dim uso As String = ""
            If ObjData.Vehiculo.Uso = "PARTICULAR" Then
                uso = "01"
            End If

            Dim RC As String = "3000000"
            Dim EXRC As String = "3000000"
            Dim PL As String = "3000000"
            Dim GMO As String = "200000"
            Dim DMPP As String = "5"
            Dim DMPT As String = "5"
            Dim RT As String = "10"
            Dim CRISTALES As String = ""
            Dim TotalCoberturas = (ObjData.Coberturas).Count

            If TotalCoberturas > 0 Then
                RC = ObjData.Coberturas(0).RC
                EXRC = ObjData.Coberturas(0).RC
                PL = ObjData.Coberturas(0).DefensaJuridica
                GMO = ObjData.Coberturas(0).GastosMedicosEvento
                DMPP = ObjData.Coberturas(0).DanosMaterialesPP
                RT = ObjData.Coberturas(0).RoboTotal
                DMPT = ObjData.Coberturas(0).DanosMateriales

                If RC = "3000000" And PL = "3000000" And GMO = "200000" And DMPP = "3" And DMPT = "3" And RT = "5" Then
                    codPromocion = "TRIG_OPC2"
                    opcionContratar = "TRIG AMP DED 3 Y 5 GM 200"
                ElseIf RC = "3000000" And PL = "3000000" And GMO = "200000" And DMPP = "3" And DMPT = "0" And RT = "0" Then
                    codPromocion = "TRIG_OPC3"
                    opcionContratar = "TRIG AMP DED 0 Y 3 GM 200"
                ElseIf RC = "3000000" And PL = "3000000" And GMO = "200000" And DMPP = "3" And DMPT = "3" And RT = "5" Then
                    codPromocion = "TRIG_OPC4"
                    opcionContratar = "TRIG AMP DED 3 Y 5 GM 300"
                ElseIf RC = "4000000" And PL = "4000000" And GMO = "500000" And DMPP = "3" And DMPT = "3" And RT = "5" Then
                    codPromocion = "TRIG_OPC5"
                    opcionContratar = "TRIG AMP DED 3 Y 5 GM 500"
                ElseIf RC = "5000000" And PL = "5000000" And GMO = "500000" And DMPP = "3" And DMPT = "0" And RT = "0" Then
                    codPromocion = "TRIG_OPC6"
                    opcionContratar = "TRIG AMP DED 0 Y 3 GM 500"
                End If
            End If

            If ObjData.Vehiculo.Clave.Contains("GM") Then
                ObjData.Vehiculo.Marca = "GENERAL MOTORS"
            ElseIf ObjData.Vehiculo.Clave.Contains("CH") Then
                ObjData.Vehiculo.Marca = "CHRYSLER"
            ElseIf ObjData.Vehiculo.Clave.Contains("FR") Then
                ObjData.Vehiculo.Marca = "FORD"
            ElseIf ObjData.Vehiculo.Clave.Contains("NI") Then
                ObjData.Vehiculo.Marca = "NISSAN"
            End If
            Dim XmlCotizacion As XElement = <Solicitud></Solicitud>
            Dim fechaNac As String = ObjData.Cliente.FechaNacimiento
            XmlCotizacion.Add(<General>
                                  <Usuario><%= usuario %></Usuario>
                                  <Password><%= password %></Password>
                                  <CodigoPromocion><%= codPromocion %></CodigoPromocion>
                                  <Tienda><%= tienda %></Tienda>
                                  <OpcionesAContratar><%= opcionContratar %></OpcionesAContratar>
                                  <Paquete><%= ObjData.Paquete %></Paquete><!--“RC”, ”Amplia”, ”AmpliaGL”, ”Limitada”-->
                                  <FormaPago><%= ObjData.PeriodicidadDePago %></FormaPago>
                                  <MetodoPago><%= MetodoPago %></MetodoPago><!--“I” para especificar pago por intermediario o “C” de contado, si es intermediario no se piden datos bancarios. “r” para tarjeta de crédito y “d” si es tarjeta de débito.-->
                                  <InicioVig><%= InicioVigencia %></InicioVig>
                                  <FinVig><%= FinVigencia %></FinVig>
                                  <Accion>C</Accion><!--"E" - Emite, "C" - Cotiza ' Opcional si es cotizacion-->
                                  <Cotizacion/>
                                  <descuentoGC></descuentoGC>
                                  <cesioncomision><%= cesionComicion %></cesioncomision><!--Porcentaje de cesion de comision-->
                                  <primerrecibo><%= recibos %></primerrecibo>
                                  <recibosubsecuente><%= recibos %></recibosubsecuente>
                                  <CodigoIntermediario><%= codIntermediario %></CodigoIntermediario>
                                  <CodigoCliente/>
                                  <TipoPago/>
                                  <ValVehi/>
                                  <PrimaTotal><%= ValVehi %></PrimaTotal>
                              </General>)
            XmlCotizacion.Add(<Autos>
                                  <Auto>
                                      <EstadoCirculacion><%= EdoCirculacion %></EstadoCirculacion>
                                      <Uso><%= uso %></Uso>
                                      <ClaveMarca><%= ObjData.Vehiculo.Clave %></ClaveMarca>
                                      <Marca><%= ObjData.Vehiculo.Marca %></Marca>
                                      <Modelo><%= ObjData.Vehiculo.Modelo %></Modelo>
                                      <Descripcion></Descripcion>
                                      <Motor></Motor>
                                      <Serie></Serie>
                                      <Placas></Placas>
                                      <FormaIndemnizacion><%= FormIndemnizacion %></FormaIndemnizacion><!--Valor comercial: "" , "Valor Factura" (agregar nodo ValVehi con valor del vehiculo) , "Valor Convenido" , "Valor Convenido + 10%"-->
                                      <ValorConvenido></ValorConvenido><!--Valor comercial: "" , Valor convenido: enviar valor -->
                                  </Auto>
                              </Autos>)
            XmlCotizacion.Add(<Personas>
                                  <Contratante>
                                      <CodigoCliente/>
                                      <Tipo>C</Tipo><!--Contratante: C-->
                                      <Paterno></Paterno>
                                      <Materno></Materno>
                                      <Nombre></Nombre>
                                      <EdoCivil/>
                                      <RFC></RFC>
                                      <Telefono></Telefono>
                                      <Lada></Lada>
                                      <TelefonoOficina></TelefonoOficina>
                                      <LadaOficina></LadaOficina>
                                      <TelefonoMovil></TelefonoMovil>
                                      <LadaMovil></LadaMovil>
                                      <Extension/>
                                      <CorreoElectronico></CorreoElectronico>
                                      <Ocupacion></Ocupacion>
                                      <FecNac><%= fechaNac %></FecNac><!--dd/mm/aaaa-->
                                      <Nacionalidad></Nacionalidad>
                                      <CP><%= ObjData.Cliente.Direccion.CodPostal %></CP>
                                      <Estado></Estado>
                                      <Poblacion></Poblacion>
                                      <Calle></Calle>
                                      <Numero>0</Numero>
                                      <Interior/>
                                      <Colonia></Colonia>
                                      <Beneficiario/>
                                      <Sexo><%= ObjData.Cliente.Genero %></Sexo>
                                      <RazonSocial/>
                                      <Giro/>
                                      <fechaConstitucion/>
                                      <ContraIgualConductor>S</ContraIgualConductor>
                                      <Edad><%= ObjData.Cliente.Edad %></Edad>
                                  </Contratante>
                                  <Asegurado>
                                      <CodigoCliente/>
                                      <Tipo></Tipo>
                                      <Paterno></Paterno>
                                      <Materno></Materno>
                                      <Nombre></Nombre>
                                      <RFC></RFC>
                                      <Telefono></Telefono>
                                      <FecNac></FecNac>
                                      <Sexo></Sexo>
                                  </Asegurado>
                              </Personas>)
            If UCase(ObjData.Paquete) = "AMPLIA" Then
                XmlCotizacion.Add(<Coberturas>
                                      <Cobertura>
                                          <clave>0000001273</clave>
                                          <sumaasegurada><%= RC %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa><%= FormatCurrency(RC, 0) %></impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000001285</clave>
                                          <sumaasegurada><%= PL %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000906</clave>
                                          <sumaasegurada><%= GMO %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa><%= FormatCurrency(GMO, 0) %></impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000916</clave>
                                          <sumaasegurada/>
                                          <deducible><%= RT / 100 %></deducible>
                                          <impresionsa/>
                                          <impresiondeducible><%= RT & "%" %></impresiondeducible>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000001289</clave>
                                          <sumaasegurada/>
                                          <deducible><%= DMPP / 100 %></deducible>
                                          <impresionsa/>
                                          <impresiondeducible><%= DMPP & "%" %></impresiondeducible>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000001288</clave>
                                          <sumaasegurada/>
                                          <deducible><%= DMPT / 100 %></deducible>
                                          <impresionsa/>
                                          <impresiondeducible><%= DMPT & "%" %></impresiondeducible>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000001268</clave>
                                          <sumaasegurada>Amparada</sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000904</clave>
                                          <sumaasegurada><%= RC %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000891</clave>
                                          <sumaasegurada>Amparada</sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                  </Coberturas>)
            ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
                XmlCotizacion.Add(<Coberturas>
                                      <Cobertura>
                                          <clave>0000001273</clave>
                                          <sumaasegurada><%= RC %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa><%= FormatCurrency(RC, 0) %></impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000001285</clave>
                                          <sumaasegurada><%= PL %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000906</clave>
                                          <sumaasegurada><%= GMO %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa><%= FormatCurrency(GMO, 0) %></impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000916</clave>
                                          <sumaasegurada/>
                                          <deducible><%= RT / 100 %></deducible>
                                          <impresionsa/>
                                          <impresiondeducible><%= RT & "%" %></impresiondeducible>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000001268</clave>
                                          <sumaasegurada>Amparada</sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000904</clave>
                                          <sumaasegurada><%= RC %></sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                      <Cobertura>
                                          <clave>0000000891</clave>
                                          <sumaasegurada>Amparada</sumaasegurada>
                                          <deducible/>
                                          <impresionsa>Amparada</impresionsa>
                                          <impresiondeducible/>
                                      </Cobertura>
                                  </Coberturas>)
            End If
            Try
                ' lg.UpdateLogRequest(XmlCotizacion, idLogWSCot)
                ' Funciones.updateLogWSCot(XmlCotizacion.ToString, idLogWSCot, "RequestWS", "FechaInicio")
            Catch ex As Exception
                Dim errr = ex
            End Try
            Dim Respuesta = ObjGNP.Cotizar(XmlCotizacion.ToString)
            Try
                Dim x2 As New XmlSerializer(Respuesta.GetType)
                Dim xml2 As New StringWriter
                x2.Serialize(xml2, Respuesta)
                Dim xmlRespuesta As String = xml2.ToString
                ' lg.UpdateLogResponse(xmlRespuesta, idLogWSCot)
                'Funciones.updateLogWSCot(xmlRespuesta, idLogWSCot, "ResponseWS", "FechaFin")
            Catch ex As Exception
                Dim errr = ex
            End Try
            dataXML.LoadXml(Respuesta.OuterXml)
            For Each Respuesta In dataXML.GetElementsByTagName("Totales")
                ObjData.Cotizacion.PrimaTotal = Respuesta("TotalPagar").InnerText
                ObjData.Cotizacion.PrimaNeta = Respuesta("PrimaNeta").InnerText
                ObjData.Cotizacion.Recargos = Respuesta("RecargoPagoFracc").InnerText
                ObjData.Cotizacion.Derechos = Respuesta("DerechoPoliza").InnerText
                ObjData.Cotizacion.Impuesto = Respuesta("IVA").InnerText
                ObjData.Cotizacion.PrimerPago = Respuesta("PrimerPago").InnerText
                ObjData.Cotizacion.PagosSubsecuentes = Respuesta("PagosSubsecuentes").InnerText
            Next
            Dim coberNum = 0
            Dim coberturas As New Coberturas
            For Each Respuesta In dataXML.GetElementsByTagName("Cobertura")
                Dim descripcionCobertura As String = ""
                If Respuesta("Nombre").InnerText = "Extensión Cobertura Resp. Civil" Or Respuesta("Nombre").InnerText = "Protección Legal" Then
                    If Respuesta("Deducible").InnerText <> "" Then
                        descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-SAmparada" & "-D" & Respuesta("Deducible").InnerText
                    Else
                        descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-SAmparada" & "-D" & "No aplica"
                    End If
                Else
                    If Respuesta("Deducible").InnerText <> "" Then
                        descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-SV. Convenido" & Respuesta("Suma_Asegurada").InnerText & "-D" & Respuesta("Deducible").InnerText
                    Else
                        descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-S" & Respuesta("Suma_Asegurada").InnerText & "-D" & "No aplica"
                    End If
                End If
                coberNum += 1

                Select Case (Respuesta("Nombre").InnerText)
                    Case "Daños Materiales Pérdida Total"
                        coberturas.DanosMateriales = descripcionCobertura.Replace("Daños Materiales Pérdida Total", "DAÑOS MATERIALES").ToUpper
                    Case "Robo Total"
                        coberturas.RoboTotal = descripcionCobertura.ToUpper
                    Case "Club GNP Autos"
                        coberturas.AsitenciaCompleta = descripcionCobertura.Replace("No aplica", "").ToUpper
                    Case "Protección Legal"
                        coberturas.DefensaJuridica = descripcionCobertura.Replace("No aplica", "").ToUpper
                    Case "Gastos Médicos Ocupantes"
                        coberturas.GastosMedicosOcupantes = descripcionCobertura.Replace("No aplica", "").ToUpper
                    Case "Responsabilidad Civil por Daños a Terceros"
                        coberturas.RCBienes = descripcionCobertura.Replace("No aplica", "").ToUpper
                    Case "Extensión Cobertura Resp. Civil"
                        coberturas.RCExtension = descripcionCobertura.Replace("No aplica", "").ToUpper
                    Case "Cristales"
                        coberturas.Cristales = descripcionCobertura.Replace("No aplica", "").ToUpper
                End Select

            Next
            ObjData.CodigoError = Respuesta.InnerText
            ObjData.Coberturas.Add(coberturas)
            ObjData.Cotizacion.Resultado = "True"
        Catch ex As Exception
            ObjData.CodigoError = ex.Message.ToString
            ObjData.Cotizacion.Resultado = "False"
        End Try
        Dim serializer As New JavaScriptSerializer
        Dim JsonRespose As String = serializer.Serialize(ObjData)
        Dim log As Log = New Log
        log.UpdateLogJsonResponse(JsonRespose, idLogWSCot)
        Return serializer.Serialize(ObjData)
    End Function


    Public Shared Function GNPEmision(ByVal ObjData As Seguro)
        Dim ObjGNP = New GNPService.CotizadorGNPSoapClient
        Dim usuario As String = "TRIGARANTEWS"
        Dim password As String = "Trig_WS"
        Dim codPromocion As String = "TRIGARANTE_WSC"
        Dim tienda = "0"
        Dim opcionContratar As String = "TRIGARANTE HC1"
        Dim MetodoPago As String = "I"
        Dim InicioVigencia As String = Now.ToString("dd") & "/" & Now.ToString("MM") & "/" & Now.Year.ToString
        Dim FinVigencia As String = Now.ToString("dd") & "/" & Now.ToString("MM") & "/" & Now.AddYears(1).Year.ToString
        Dim movimiento As String = "C"
        Dim descuento As String = ObjData.Descuento
        Dim cesionComicion As String = "0"
        Dim recibos As String = "IN"
        Dim codIntermediario As String = "0089487001"
        Dim ValVehi As String = "0"
        Dim FormIndemnizacion As String = "Valor Convenido"
        Dim dataXML As New XmlDocument
        Dim lengthCP As Integer = ObjData.Cliente.Direccion.CodPostal.Length
        For i = lengthCP To 4
            ObjData.Cliente.Direccion.CodPostal = "0" & ObjData.Cliente.Direccion.CodPostal
        Next
        Dim poblacion = ConsultaPoblacionGNP(ObjData.Cliente.Direccion.CodPostal)
        Dim estadoCircula As String = GetCodEdo(poblacion)
        Dim uso As String = ""
        Dim Telefono As String = ""
        Dim Lada As String = ""
        Dim ocupacion = "OTRO"
        Telefono = Mid(ObjData.Cliente.Telefono, 3, 8)
        Lada = Mid(ObjData.Cliente.Telefono, 1, 2)
        If ObjData.Vehiculo.Uso = "PARTICULAR" Then
            uso = "01"
        End If

        Dim RC As String = "3000000"
        Dim EXRC As String = "3000000"
        Dim PL As String = "3000000"
        Dim GMO As String = "200000"
        Dim DMPP As String = "5"
        Dim DMPT As String = "5"
        Dim RT As String = "10"
        Dim key = "5uPx0PzUxp5vAsf1SkV9xg=="
        Dim TotalCoberturas = (ObjData.Coberturas).Count

        If TotalCoberturas > 0 Then
            RC = ObjData.Coberturas(0).RC
            EXRC = ObjData.Coberturas(0).RC
            PL = ObjData.Coberturas(0).DefensaJuridica
            GMO = ObjData.Coberturas(0).GastosMedicosEvento
            DMPP = ObjData.Coberturas(0).DanosMaterialesPP
            RT = ObjData.Coberturas(0).RoboTotal
            DMPT = ObjData.Coberturas(0).DanosMateriales

            If RC = "3000000" And PL = "3000000" And GMO = "200000" And DMPP = "3" And DMPT = "3" And RT = "5" Then
                codPromocion = "TRIG_OPC2"
                opcionContratar = "TRIG AMP DED 3 Y 5 GM 200"
                key = "RXUX2D/wVWvYMoBNFNJhJQ=="
            ElseIf RC = "3000000" And PL = "3000000" And GMO = "200000" And DMPP = "3" And DMPT = "0" And RT = "0" Then
                codPromocion = "TRIG_OPC3"
                opcionContratar = "TRIG AMP DED 0 Y 3 GM 200"
                key = "RXUX2D/wVWsCrnYlOmJRg=="
            ElseIf RC = "3000000" And PL = "3000000" And GMO = "200000" And DMPP = "3" And DMPT = "3" And RT = "5" Then
                codPromocion = "TRIG_OPC4"
                opcionContratar = "TRIG AMP DED 3 Y 5 GM 300"
                key = "RXUX2D/wVWtpBsWUwsg/LA=="
            ElseIf RC = "4000000" And PL = "4000000" And GMO = "500000" And DMPP = "3" And DMPT = "3" And RT = "5" Then
                codPromocion = "TRIG_OPC5"
                opcionContratar = "TRIG AMP DED 3 Y 5 GM 500"
                key = "RXUX2D/wVWtvElc/8DZzCg=="
            ElseIf RC = "5000000" And PL = "5000000" And GMO = "500000" And DMPP = "3" And DMPT = "0" And RT = "0" Then
                codPromocion = "TRIG_OPC6"
                opcionContratar = "TRIG AMP DED 0 Y 3 GM 500"
                key = "RXUX2D/wVWtbF7dEZdSlzg=="
            End If
        End If

        If ObjData.Vehiculo.Clave.Contains("GM") Then
            ObjData.Vehiculo.Marca = "GENERAL MOTORS"
        ElseIf ObjData.Vehiculo.Clave.Contains("CH") Then
            ObjData.Vehiculo.Marca = "CHRYSLER"
        ElseIf ObjData.Vehiculo.Clave.Contains("FR") Then
            ObjData.Vehiculo.Marca = "FORD"
        ElseIf ObjData.Vehiculo.Clave.Contains("NI") Then
            ObjData.Vehiculo.Marca = "NISSAN"
        End If

        Dim XmlCotizacion As XElement = <Solicitud></Solicitud>
        Dim fechaNac As String = ObjData.Cliente.FechaNacimiento
        XmlCotizacion.Add(<General>
                              <Usuario><%= usuario %></Usuario>
                              <Password><%= password %></Password>
                              <CodigoPromocion><%= codPromocion %></CodigoPromocion>
                              <Tienda><%= tienda %></Tienda>
                              <OpcionesAContratar><%= opcionContratar %></OpcionesAContratar>
                              <Paquete><%= ObjData.Paquete %></Paquete><!--“RC”, ”Amplia”, ”AmpliaGL”, ”Limitada”-->
                              <FormaPago><%= ObjData.PeriodicidadDePago %></FormaPago>
                              <MetodoPago><%= MetodoPago %></MetodoPago><!--“I” para especificar pago por intermediario o “C” de contado, si es intermediario no se piden datos bancarios. “r” para tarjeta de crédito y “d” si es tarjeta de débito.-->
                              <InicioVig><%= InicioVigencia %></InicioVig>
                              <FinVig><%= FinVigencia %></FinVig>
                              <Accion>C</Accion><!--"E" - Emite, "C" - Cotiza ' Opcional si es cotizacion-->
                              <Cotizacion/>
                              <descuentoGC></descuentoGC>
                              <cesioncomision><%= cesionComicion %></cesioncomision><!--Porcentaje de cesion de comision-->
                              <primerrecibo><%= recibos %></primerrecibo>
                              <recibosubsecuente><%= recibos %></recibosubsecuente>
                              <CodigoIntermediario><%= codIntermediario %></CodigoIntermediario>
                              <CodigoCliente/>
                              <TipoPago/>
                              <ValVehi/>
                              <PrimaTotal><%= ValVehi %></PrimaTotal>
                          </General>)
        XmlCotizacion.Add(<Autos>
                              <Auto>
                                  <EstadoCirculacion><%= estadoCircula %></EstadoCirculacion>
                                  <Uso><%= uso %></Uso>
                                  <ClaveMarca><%= ObjData.Vehiculo.Clave %></ClaveMarca>
                                  <Marca><%= ObjData.Vehiculo.Marca %></Marca>
                                  <Modelo><%= ObjData.Vehiculo.Modelo %></Modelo>
                                  <Descripcion><%= ObjData.Vehiculo.Descripcion %></Descripcion>
                                  <Motor><%= ObjData.Vehiculo.NoMotor %></Motor>
                                  <Serie><%= ObjData.Vehiculo.NoSerie %></Serie>
                                  <Placas><%= ObjData.Vehiculo.NoPlacas %></Placas>
                                  <FormaIndemnizacion><%= FormIndemnizacion %></FormaIndemnizacion><!--Valor comercial: "" , "Valor Factura" (agregar nodo ValVehi con valor del vehiculo) , "Valor Convenido" , "Valor Convenido + 10%"-->
                                  <ValorConvenido></ValorConvenido><!--Valor comercial: "" , Valor convenido: enviar valor -->
                              </Auto>
                          </Autos>)
        XmlCotizacion.Add(<Personas>
                              <Contratante>
                                  <CodigoCliente/>
                                  <Tipo>C</Tipo><!--Contratante: C-->
                                  <Paterno><%= ObjData.Cliente.ApellidoPat %></Paterno>
                                  <Materno><%= ObjData.Cliente.ApellidoMat %></Materno>
                                  <Nombre><%= ObjData.Cliente.Nombre %></Nombre>
                                  <EdoCivil/>
                                  <RFC><%= ObjData.Cliente.RFC %></RFC>
                                  <Telefono><%= Telefono %></Telefono>
                                  <Lada><%= Lada %></Lada>
                                  <TelefonoOficina></TelefonoOficina>
                                  <LadaOficina></LadaOficina>
                                  <TelefonoMovil></TelefonoMovil>
                                  <LadaMovil></LadaMovil>
                                  <Extension/>
                                  <CorreoElectronico><%= ObjData.Cliente.Email %></CorreoElectronico>
                                  <Ocupacion><%= ocupacion %></Ocupacion>
                                  <FecNac><%= fechaNac %></FecNac><!--dd/mm/aaaa-->
                                  <Nacionalidad></Nacionalidad>
                                  <CP><%= ObjData.Cliente.Direccion.CodPostal %></CP>
                                  <Estado><%= estadoCircula %></Estado>
                                  <Poblacion><%= poblacion %></Poblacion>
                                  <Calle><%= ObjData.Cliente.Direccion.Calle %></Calle>
                                  <Numero><%= ObjData.Cliente.Direccion.NoExt %></Numero>
                                  <Interior><%= ObjData.Cliente.Direccion.NoInt %></Interior>
                                  <Colonia><%= ObjData.Cliente.Direccion.Colonia %></Colonia>
                                  <Beneficiario/>
                                  <Sexo><%= ObjData.Cliente.Genero %></Sexo>
                                  <Edad><%= ObjData.Cliente.Edad %></Edad>
                                  <RazonSocial/>
                                  <Giro/>
                                  <fechaConstitucion/>
                                  <ContraIgualConductor>S</ContraIgualConductor>
                              </Contratante>
                              <Asegurado>
                                  <CodigoCliente/>
                                  <Tipo></Tipo>
                                  <Paterno></Paterno>
                                  <Materno></Materno>
                                  <Nombre></Nombre>
                                  <RFC></RFC>
                                  <Telefono></Telefono>
                                  <FecNac></FecNac>
                                  <Sexo></Sexo>
                              </Asegurado>
                          </Personas>)
        If UCase(ObjData.Paquete) = "AMPLIA" Then
            XmlCotizacion.Add(<Coberturas>
                                  <Cobertura>
                                      <clave>0000001273</clave>
                                      <sumaasegurada><%= RC %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa><%= FormatCurrency(RC, 0) %></impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000001285</clave>
                                      <sumaasegurada><%= PL %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000906</clave>
                                      <sumaasegurada><%= GMO %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa><%= FormatCurrency(GMO, 0) %></impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000916</clave>
                                      <sumaasegurada/>
                                      <deducible><%= RT / 100 %></deducible>
                                      <impresionsa/>
                                      <impresiondeducible><%= RT & "%" %></impresiondeducible>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000001289</clave>
                                      <sumaasegurada/>
                                      <deducible><%= DMPP / 100 %></deducible>
                                      <impresionsa/>
                                      <impresiondeducible><%= DMPP & "%" %></impresiondeducible>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000001288</clave>
                                      <sumaasegurada/>
                                      <deducible><%= DMPT / 100 %></deducible>
                                      <impresionsa/>
                                      <impresiondeducible><%= DMPT & "%" %></impresiondeducible>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000001268</clave>
                                      <sumaasegurada>Amparada</sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000904</clave>
                                      <sumaasegurada><%= RC %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000891</clave>
                                      <sumaasegurada>Amparada</sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                              </Coberturas>)
        ElseIf UCase(ObjData.Paquete) = "LIMITADA" Then
            XmlCotizacion.Add(<Coberturas>
                                  <Cobertura>
                                      <clave>0000001273</clave>
                                      <sumaasegurada><%= RC %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa><%= FormatCurrency(RC, 0) %></impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000001285</clave>
                                      <sumaasegurada><%= PL %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000906</clave>
                                      <sumaasegurada><%= GMO %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa><%= FormatCurrency(GMO, 0) %></impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000916</clave>
                                      <sumaasegurada/>
                                      <deducible><%= RT / 100 %></deducible>
                                      <impresionsa/>
                                      <impresiondeducible><%= RT & "%" %></impresiondeducible>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000001268</clave>
                                      <sumaasegurada>Amparada</sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000904</clave>
                                      <sumaasegurada><%= RC %></sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                                  <Cobertura>
                                      <clave>0000000891</clave>
                                      <sumaasegurada>Amparada</sumaasegurada>
                                      <deducible/>
                                      <impresionsa>Amparada</impresionsa>
                                      <impresiondeducible/>
                                  </Cobertura>
                              </Coberturas>)
        End If
        Dim Respuesta = ObjGNP.Cotizar(XmlCotizacion.ToString)
        dataXML.LoadXml(Respuesta.OuterXml)
        For Each Respuesta In dataXML.GetElementsByTagName("Totales")
            ObjData.Emision.PrimaTotal = Respuesta("TotalPagar").InnerText
            ObjData.Emision.PrimaNeta = Respuesta("PrimaNeta").InnerText
            ObjData.Emision.Recargos = Respuesta("RecargoPagoFracc").InnerText
            ObjData.Emision.Derechos = Respuesta("DerechoPoliza").InnerText
            ObjData.Emision.Impuesto = Respuesta("IVA").InnerText
            ObjData.Emision.PrimerPago = Respuesta("PrimerPago").InnerText
            ObjData.Emision.PagosSubsecuentes = Respuesta("PagosSubsecuentes").InnerText
        Next
        Dim coberNum = 0
        Dim coberturas As New Coberturas
        For Each Respuesta In dataXML.GetElementsByTagName("Cobertura")
            Dim descripcionCobertura As String = ""
            If Respuesta("Nombre").InnerText = "Extensión Cobertura Resp. Civil" Or Respuesta("Nombre").InnerText = "Protección Legal" Then
                If Respuesta("Deducible").InnerText <> "" Then
                    descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-SAmparada" & "-D" & Respuesta("Deducible").InnerText
                Else
                    descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-SAmparada" & "-D" & "No aplica"
                End If
            Else
                If Respuesta("Deducible").InnerText <> "" Then
                    descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-SV. Convenido" & Respuesta("Suma_Asegurada").InnerText & "-D" & Respuesta("Deducible").InnerText
                Else
                    descripcionCobertura &= "-N" & Respuesta("Nombre").InnerText & "-S" & Respuesta("Suma_Asegurada").InnerText & "-D" & "No aplica"
                End If
            End If
            coberNum += 1

            Select Case (Respuesta("Nombre").InnerText)
                Case "Daños Materiales Pérdida Total"
                    coberturas.DanosMateriales = descripcionCobertura.Replace("Daños Materiales Pérdida Total", "DAÑOS MATERIALES").ToUpper
                Case "Robo Total"
                    coberturas.RoboTotal = descripcionCobertura.ToUpper
                Case "Club GNP Autos"
                    coberturas.AsitenciaCompleta = descripcionCobertura.Replace("No aplica", "")
                Case "Protección Legal"
                    coberturas.DefensaJuridica = descripcionCobertura.Replace("No aplica", "").ToUpper
                Case "Gastos Médicos Ocupantes"
                    coberturas.GastosMedicosOcupantes = descripcionCobertura.Replace("No aplica", "").ToUpper
                Case "Responsabilidad Civil por Daños a Terceros"
                    coberturas.RCBienes = descripcionCobertura.Replace("No aplica", "").ToUpper
                Case "Extensión Cobertura Resp. Civil"
                    coberturas.RCExtension = descripcionCobertura.Replace("No aplica", "").ToUpper
                Case "Cristales"
                    coberturas.Cristales = descripcionCobertura.Replace("No aplica", "").ToUpper
            End Select

        Next

        ObjData.Coberturas.Add(coberturas)

        Dim ColElementos As XmlNodeList = dataXML.GetElementsByTagName("General")
        For Each Respuesta In ColElementos
            ObjData.Emision.IDCotizacion = Respuesta("Cotizacion").InnerText
        Next
        Dim tempGnp As String
        ObjData.Emision.Resultado = "True"
        If ObjData.Emision.PrimerPago <> Nothing Or ObjData.Emision.PrimerPago <> "" Then
            tempGnp = Replace(ObjData.Emision.PrimerPago, "$", "")
            tempGnp = Replace(tempGnp, ",", "")
            ObjData.Emision.Resultado = "True"
        Else
            ObjData.Emision.Resultado = "False"
        End If
        ObjData.Emision.Terminal = "http://gnpventamasiva.com.mx/portal/pagotv.aspx?TipOP=P&IdT=" & ObjData.Emision.IDCotizacion & " &Mac=" & tempGnp & "&idLlave=" & key & "&URLResp=" & ObjData.urlRedireccion
        ObjData.Emision.Terminal = Replace(ObjData.Emision.Terminal, " ", "")
        ObjData.CodigoError = Respuesta.InnerText


        Dim serializer As New JavaScriptSerializer

        Return Replace(serializer.Serialize(ObjData), "\u0026", "&")
    End Function

    Private Shared Function ConsultaPoblacionGNP(ByVal CPostal As String) As String
        Try
            Dim objCotizaGNP As New GNPService.CotizadorGNPSoapClient
            Dim Municipio As XmlNode = objCotizaGNP.ObtenerCatalogo("<SolicitudCatalogo><Catalogo><Usuario>TRIGARANTEWS</Usuario><Password>Trig_WS</Password><Tipo>codigopostal</Tipo><Parametro1>" & CPostal & "</Parametro1><Parametro2>estado</Parametro2></Catalogo></SolicitudCatalogo>")
            Dim Municipios As String = Municipio.OuterXml
            Dim objXmlDocumentMun As New XmlDocument
            Dim colElmentosMun As XmlNodeList
            objXmlDocumentMun.LoadXml(Municipios)
            Dim Poblacion As String = ""
            colElmentosMun = objXmlDocumentMun.GetElementsByTagName("Elemento")
            For Each Municipio In colElmentosMun
                Poblacion = Municipio("Valor").InnerText
            Next
            Return Poblacion
        Catch ex As Exception

        End Try

    End Function
    Public Shared Function GetCodEdo(ByVal Estado As String) As String

        Dim CodEdo As String = ""
        Dim connectionString = "datasource=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=ws_db;"
        Dim sql = "Select IdEdo FROM `Ali_R_CatEdosGNP` where Edo='" + Estado + "'"
        Using Connection As New MySqlConnection(connectionString)
            Dim command As New MySqlCommand(sql, Connection)

            Try
                Connection.Open()
                Dim rs As MySqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection)
                CodEdo = rs("IdEdo")
                Connection.Close()
            Catch ex As Exception
                Dim errror = ex
            End Try
        End Using
        Return CodEdo

    End Function
    Public Shared Function ImprimeGNP(ByVal poliza As String) As String
        Dim objCotizaGNP As New GNP_impresion.CotizadorGNPSoapClient
        Dim XmlImprime As String
        Dim Usuario As String = "TRIGARANTEWS"
        Dim Password As String = "Trig_WS"
        XmlImprime = "<Impresion>"
        XmlImprime &= "<Catalogo>"
        XmlImprime &= "<Usuario>" & Usuario & "</Usuario>"
        XmlImprime &= "<Password>" & Password & "</Password>"
        XmlImprime &= "<Tipo>URL</Tipo>"
        XmlImprime &= "<Cotizacion>" & poliza & "</Cotizacion>"
        XmlImprime &= "</Catalogo>"
        XmlImprime &= "</Impresion>"
        Dim Respuesta As XmlNode = objCotizaGNP.Imprimir(XmlImprime)
        Dim Respuesta2 As String = Respuesta.OuterXml
        Dim objXmlDocument As New XmlDocument
        Dim URL As String = Respuesta.InnerText
        Return URL
    End Function

End Class
