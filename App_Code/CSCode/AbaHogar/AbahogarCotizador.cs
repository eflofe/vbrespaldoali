﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

/// <summary>
/// Descripción breve de AbahogarCotizador
/// </summary>
public class AbahogarCotizador
{
     private string usr = "WSTRIGARANTE";
        private string pass = "VIRTUAL1$";
        private int agente = 94625;
        private int negocio = 94625;
        string FechaInicio = DateTime.Now.ToString("yyyy-MM-dd");
        string FechaTermino = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");
        private int tipoMuro = 7135;
        private int tipoTecho = 7136;
        private int tipoHabitacional = 1;
       TimeSpan parar; 
       TimeSpan inicioTotal = new TimeSpan(DateTime.Now.Ticks);
        
        JsonFinal jf = new JsonFinal();

        AbaCotizacionWs.CotizacionClient AbaClient = new AbaCotizacionWs.CotizacionClient();
        AbaCotizacionWs.Token token = new AbaCotizacionWs.Token();
        AbaCotizacionWs.InputGetCotizacion inputCot = new AbaCotizacionWs.InputGetCotizacion();
        AbaCotizacionWs.RequestGetCotizacion requestCotizacion = new AbaCotizacionWs.RequestGetCotizacion();
    
        List<AbaCotizacionWs.MedidaSeguridad> listaMedidasS = new List<AbaCotizacionWs.MedidaSeguridad>();
        List<AbaCotizacionWs.Cobertura> listCoberturas = new List<AbaCotizacionWs.Cobertura>();
        LogCotizacionHogar lg = new LogCotizacionHogar();
        

        public string CotizarHogar(DatosHogar datosHogar, string LogCot)
        {
            try
            {
                AbaCotizacionWs.DatosGenerales dg = new AbaCotizacionWs.DatosGenerales();
                AbaCotizacionWs.Caracteristicas ca = new AbaCotizacionWs.Caracteristicas();
                AbaCotizacionWs.Ubicacion ubi = new AbaCotizacionWs.Ubicacion();
                AbaCotizacionWs.Paquete paq = new AbaCotizacionWs.Paquete();
                AbaCotizacionWs.DatosExtras dte = new AbaCotizacionWs.DatosExtras();
            //inicio request tiempo 
            TimeSpan pararRequest;
            TimeSpan inicioRequets = new TimeSpan(DateTime.Now.Ticks);


                //----------------------Datos Generales------------
               
                dg.NegocioId = 6481;
                dg.AgenteId = 94625;
                dg.ConductoId = 0;
                dg.IsSegmentacion = Convert.ToBoolean(0);
                dg.MonedaId = 1;
                dg.FechaInicioPoliza = FechaInicio;
                dg.FechaFinPoliza = FechaTermino;
                inputCot.DatosGenerales = dg;
                //----------------------Datos Generales------------

                //----------------------Caracteristicas------------
                ca.TipoMuroId = 7135;
                ca.TipoTechoId = 7136;
                ca.TipoHabitacionalId = 1;
                ca.TipoHabitacionalId = 1;
                ca.CantNiveles = 1;
                ca.CantSotanos = 0;
                ca.NoPiso = 1;
                ca.NoPisoAsegurado = 0;
                ca.RiesgoDesc = "0";
               
                var medidaS1 = new AbaCotizacionWs.MedidaSeguridad
                {
                    Id = 7
                };
                var medidaS2 = new AbaCotizacionWs.MedidaSeguridad
                {
                    Id = 8
                };
                var medidaS3 = new AbaCotizacionWs.MedidaSeguridad
                {
                    Id = 10
                };
                listaMedidasS.Add(medidaS1);
                listaMedidasS.Add(medidaS2);
                listaMedidasS.Add(medidaS3);
                ca.MedidasSeguridad = listaMedidasS.ToArray();
                inputCot.Caracteristicas = ca;
                //----------------------Caracteristicas------------

                //----------------------Ubicacion------------------------
                ubi.PaisId = 1;
                ubi.EstadoId = datosHogar.Estado;
                ubi.MunicipioId = datosHogar.Municipio;
                ubi.PoblacionId = datosHogar.Poblacion;
                ubi.ColoniaId = datosHogar.Colonia;
                ubi.Calle = "calle";
                ubi.NoExt = "7";
                ubi.NoInt = "0";
                ubi.IsBeachFront = Convert.ToBoolean(0);
                inputCot.Ubicacion = ubi;
                //----------------------Ubicacion------------------------

                //---------------------Paquete--------------
                paq.Id = 559;
                paq.FormaPagoId = 12;
                paq.TipoCalculoId = 1;
                paq.TipoDeducibleId = 1;

                var cobertura1 = new AbaCotizacionWs.Cobertura
                {
                    Id = 51,
                    Desc = "Incendio Edificio",
                    SAMonto = 1000000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor=0,
                    TipoDedMinId=11,
                    DedMaxValor=0,
                    TipoDedMaxId=11,
                    DsctoPctValor=0,
                    RecargoPctValor=0,
                    BonificaPctValor=0

                };

                var cobertura2 = new AbaCotizacionWs.Cobertura
                {
                    Id = 123,
                    Desc = "Resto de los Riesgos (Edificio)",
                    SAMonto = 1000000,
                    SALabel = "0",
                    DeducibleValor = 10,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 5,
                    DedMinValor = 5000,
                    TipoDedMinId = 10,
                    DedMaxValor = 750,
                    TipoDedMaxId = 35,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };
                
                var cobertura3 = new AbaCotizacionWs.Cobertura
                {
                    Id = 52,
                    Desc = "Incendio Contenidos",
                    SAMonto = 500000,
                    SALabel = "0",
                    DeducibleValor=0,
                    DeducibleLabel = "0",
                    TipoDeducibleId=11,
                    DedMinValor=0,
                    TipoDedMinId=11,
                    DedMaxValor=0,
                    TipoDedMaxId=11,
                    DsctoPctValor=0,
                    RecargoPctValor=0,
                    BonificaPctValor=0
                };

                var cobertura4 = new AbaCotizacionWs.Cobertura
                {
                    Id = 124,
                    Desc = "Resto de los Riesgos (Contenido)",
                    SAMonto = 500000,
                    SALabel = "0",
                    DeducibleValor = 10,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 5,
                    DedMinValor = 5000,
                    TipoDedMinId = 10,
                    DedMaxValor = 750,
                    TipoDedMaxId = 35,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura5 = new AbaCotizacionWs.Cobertura
                {
                    Id = 74,
                    Desc = "Terremoto y/o Erupción Volcánica (Edificio)",
                    SAMonto = 1000000,
                    SALabel = "0",
                    DeducibleValor = 2,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 3,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura6 = new AbaCotizacionWs.Cobertura
                {
                    Id = 75,
                    Desc = "Fenómenos Hidrometeorológicos (Edificio)",
                    SAMonto = 1000000,
                    SALabel = "0",
                    DeducibleValor = 1,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 3,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura7 = new AbaCotizacionWs.Cobertura
                {
                    Id = 121,
                    Desc = "Bienes a la Intemperie (Edificio)",
                    SAMonto = 300000,
                    SALabel = "0",
                    DeducibleValor = 5,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 3,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura8 = new AbaCotizacionWs.Cobertura
                {
                    Id = 76,
                    Desc = "Terremoto y/o Erupción Volcánica (Contenido)",
                    SAMonto = 500000,
                    SALabel = "0",
                    DeducibleValor = 2,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 3,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura9 = new AbaCotizacionWs.Cobertura
                {
                    Id = 77,
                    Desc = "Fenómenos Hidrometeorológicos (Contenido)",
                    SAMonto = 500000,
                    SALabel = "0",
                    DeducibleValor = 1,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 3,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura10 = new AbaCotizacionWs.Cobertura
                {
                    Id = 122,
                    Desc = "Bienes a la Intemperie (Contenido)",
                    SAMonto = 150000,
                    SALabel = "0",
                    DeducibleValor = 5,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 3,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura11 = new AbaCotizacionWs.Cobertura
                {
                    Id = 651,
                    Desc = "Mascotas",
                    SAMonto = 10000,
                    SALabel = "0",
                    DeducibleValor = 1000,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 19,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura12 = new AbaCotizacionWs.Cobertura
                {
                    Id = 735,
                    Desc = "Bienes Nuevos",
                    SAMonto = 10000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura13 = new AbaCotizacionWs.Cobertura
                {
                    Id = 56,
                    Desc = "Robo con Violencia y/o Asalto",
                    SAMonto = 38000,
                    SALabel = "0",
                    DeducibleValor = 10,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 5,
                    DedMinValor = 60,
                    TipoDedMinId = 35,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura14 = new AbaCotizacionWs.Cobertura
                {
                    Id = 60,
                    Desc = "Objetos Personales",
                    SAMonto = 5000,
                    SALabel = "0",
                    DeducibleValor = 10,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 5,
                    DedMinValor = 60,
                    TipoDedMinId = 35,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura15 = new AbaCotizacionWs.Cobertura
                {
                    Id = 61,
                    Desc = "Responsabilidad Civil Familiar",
                    SAMonto = 1000000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura16 = new AbaCotizacionWs.Cobertura
                {
                    Id = 108,
                    Desc = "Asistencias de Emergencia en el hogar",
                    SAMonto = 2000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura17 = new AbaCotizacionWs.Cobertura
                {
                    Id = 727,
                    Desc = "Asistencia Tecnológica",
                    SAMonto = 500,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura18 = new AbaCotizacionWs.Cobertura
                {
                    Id = 729,
                    Desc = "Asistencia Técnico Especialista",
                    SAMonto = 1000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura19 = new AbaCotizacionWs.Cobertura
                {
                    Id = 54,
                    Desc = "Remoción de Escombros",
                    SAMonto = 50000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 17,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                var cobertura20 = new AbaCotizacionWs.Cobertura
                {
                    Id = 70,
                    Desc = "Pérdida de Rentas",
                    SAMonto = 60000,
                    SALabel = "0",
                    DeducibleValor = 0,
                    DeducibleLabel = "0",
                    TipoDeducibleId = 11,
                    DedMinValor = 0,
                    TipoDedMinId = 11,
                    DedMaxValor = 0,
                    TipoDedMaxId = 11,
                    DsctoPctValor = 0,
                    RecargoPctValor = 0,
                    BonificaPctValor = 0
                };

                listCoberturas.Add(cobertura1);
                listCoberturas.Add(cobertura2);
                listCoberturas.Add(cobertura3);
                listCoberturas.Add(cobertura4);
                listCoberturas.Add(cobertura5);
                listCoberturas.Add(cobertura6);
                listCoberturas.Add(cobertura7);
                listCoberturas.Add(cobertura8);
                listCoberturas.Add(cobertura9);
                listCoberturas.Add(cobertura10);
                listCoberturas.Add(cobertura11);
                listCoberturas.Add(cobertura12);
                listCoberturas.Add(cobertura13);
                listCoberturas.Add(cobertura14);
                listCoberturas.Add(cobertura15);
                listCoberturas.Add(cobertura16);
                listCoberturas.Add(cobertura17);
                listCoberturas.Add(cobertura18);
                listCoberturas.Add(cobertura19);
                listCoberturas.Add(cobertura20);

                paq.Coberturas = listCoberturas.ToArray();
                paq.DsctoPctMonto = 0;
                paq.RecargoPctMonto = 0;
                paq.BonificaPctMonto = 0;
                inputCot.Paquete = paq;
                //---------------------Paquete--------------

                //---------------------DAtos extras--------------
                dte.TextoLibre = "";
                dte.Email = datosHogar.Correo;
                dte.NombreAsegurado = datosHogar.nombre + " " + datosHogar.apaterno + " " + datosHogar.apaterno;
                inputCot.DatosExtras = dte;
                //---------------------DAtos extras--------------
                
                //-----------------------------------------------------------------
                requestCotizacion.InputGetCotizacion = inputCot;//Va al final
                token.Usuario = this.usr;
                token.Password = this.pass;

                //-------------------------------------------------------------
                requestCotizacion.Token = token;

                //----------------Creacion del XML request-------------------------------
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(requestCotizacion.GetType());
                MemoryStream ms = new MemoryStream();
                XmlWriter xmlWriter = XmlWriter.Create(ms);
                serializer.Serialize(xmlWriter, requestCotizacion);
                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                StreamReader reader = new StreamReader(ms);
                string res = reader.ReadToEnd();
                //Tiempo de request
                pararRequest  = new TimeSpan(DateTime.Now.Ticks);
                string segundosRequests = pararRequest.Subtract(inicioRequets ).TotalSeconds.ToString();
                lg.UpdateLogRequestWithTime(res,LogCot ,segundosRequests);
                //lg.UpdateRequest(res, LogCot);
                //----------------Creacion del XML request-------------------------------

                AbaCotizacionWs.OutputGetCotizacion p = AbaClient.GetCotizacion(token, requestCotizacion.InputGetCotizacion);

                //----------------Creacion del XML response-------------------------------
                System.Xml.Serialization.XmlSerializer serializer2 = new System.Xml.Serialization.XmlSerializer(p.GetType());
                MemoryStream ms2 = new MemoryStream();
                XmlWriter xmlWriter2 = XmlWriter.Create(ms2);
                serializer2.Serialize(xmlWriter2,p);
                ms2.Flush();
                ms2.Seek(0, SeekOrigin.Begin);
                StreamReader reader2 = new StreamReader(ms2);
                string res2 = reader2.ReadToEnd();

            //----------------Creacion del XML response-------------------------------
            TimeSpan pararResponse;
            TimeSpan inicioResponse = new TimeSpan(DateTime.Now.Ticks);
                string jsonR = JsonConvert.SerializeObject(p);
                string respuesta = GenerarJson(jsonR);
            pararResponse = new TimeSpan(DateTime.Now.Ticks);
            string segundosResponse = pararResponse.Subtract(inicioResponse).TotalSeconds.ToString();
            //Tiempo Total Requets/Response
            parar = new TimeSpan(DateTime.Now.Ticks);
            string segundosTotal = parar.Subtract(inicioTotal).TotalSeconds.ToString();
            lg.UpdateLogResponseWithTimeP(res2,respuesta ,LogCot,segundosResponse ,segundosTotal,segundosTotal );
            //lg.UpdateResponse(res2,LogCot,respuesta);
                return respuesta;
               }
            catch (Exception ex)
            {
                return "Ya" + ex;
            }
        }

        public string GenerarJson(string JsonR)
        {
            this.jf = JsonConvert.DeserializeObject<JsonFinal>(JsonR);
            string df = JsonConvert.SerializeObject(jf);
            return df;
        }
    }

    public partial class JsonFinal
    {
      
        public long CotizacionId { get; set; }
        public long VersionId { get; set; }
        public Paquete Paquete { get; set; }
        public Clausula[] Clausulas { get; set; }
    }

    public partial class Clausula
    {
        public string Desc { get; set; }
    }

    public partial class Paquete
    {
        public long FormaPagoId { get; set; }
        public long TipoDeducibleId { get; set; }
        public Cobertura[] Coberturas { get; set; }
        public string PrimaTotalLabel { get; set; }
    }

    public partial class Cobertura
    {
        public long Id { get; set; }
        public string Desc { get; set; }
        public long SaMonto { get; set; }
        public string SaLabel { get; set; }
        public long DeducibleValor { get; set; }
        public string DeducibleLabel { get; set; }
        public long TipoDeducibleId { get; set; }
        public long DedMinValor { get; set; }
        public long TipoDedMinId { get; set; }
        public long DedMaxValor { get; set; }
        public long TipoDedMaxId { get; set; }
        public long DsctoPctValor { get; set; }
        public long RecargoPctValor { get; set; }
        public long BonificaPctValor { get; set; }
    }
