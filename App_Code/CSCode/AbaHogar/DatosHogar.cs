﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DatosHogar
/// </summary>
public class DatosHogar
{
    public string tipoVivienda { get; set; }
    public string PrOAr { get; set; }
    public string noPisos { get; set; }
    public string nombre { get; set; }
    public string apaterno { get; set; }
    public string amaterno { get; set; }
    public string telefono { get; set; }
    public string Correo { get; set; }
    public int Estado { get; set; }
    public int Municipio { get; set; }
    public int Poblacion { get; set; }
    public int Colonia { get; set; }
    public string CP { get; set; }
    public string codigoError { get; set; }
}