﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de LogCotizacionHogar
/// </summary>
public class LogCotizacionHogar
{
    MySqlConnection con = new MySqlConnection();
    static string segundos = "";
    string connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";

    public string InsertLog(string JsonRequest)
    {
        TimeSpan parar;
        TimeSpan iniciar = new TimeSpan(DateTime.Now.Ticks);

        try
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            con.ConnectionString = connectionString;
            con.Open();
            var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string query = "INSERT INTO LogCotHogarWSP (JSONRequest,InsertJsonTime,RequestWS,RequestTime,ResponseWS,ResponseWSTIME,ResponseFullTime,JSONResponse,Aseguradora,FechaInicio,FechaFin)" +
                "VALUES('" + JsonRequest + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AbaTEST', '" + fechaInicio + "',NULL)";
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
            parar = new TimeSpan(DateTime.Now.Ticks);
            segundos = parar.Subtract(iniciar).TotalSeconds .ToString ();
            var Id = GetLastId();
            return Id;
        }
        catch (Exception e)
        {
            string error = e.Message;
            return error;
        }
    }


    /// <summary>
    /// Medidor de tiempo
    /// </summary>
    public void UpdateLogRequestWithTime(string XmlRequest, string id, string timeRequest)
    {
        try
        {

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            con.ConnectionString = connectionString ;
            con.Open();
            var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string query = "Update LogCotHogarWSP set RequestWS ='" + XmlRequest + "', InsertJSONTime = '" + segundos  + "',RequestTIME = '" + timeRequest + "'  WHERE idLogCot =" + id;
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
        }
    }



    public void UpdateRequest(string xml, string id)
    {
        try
        {

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            con.ConnectionString = connectionString;
            con.Open();
            var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string query = "Update LogCotHogar set RequestWS = '" + xml + "' where IdLogCot = " + id;
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
        }
    }

    public void UpdateResponse(string xml, string id, string JsonResponse)
    {
        try
        {

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            con.ConnectionString = connectionString;
            con.Open();
            var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string query = "Update LogCotHogar set ResponseWS = '" + xml + "', JsonResponse = '" + JsonResponse + "', FechaFin='" + fechaFin + "' where IdLogCot = " + id;
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
        }
    }

    /// <summary>
    /// Método para guardar en la base el response de la cotización
    /// </summary>
    /// <param name="XmlResponse"></param>
    /// <param name="id"></param>
    /// <param name="responseTime"></param>
    /// <param name="responseFullTime"></param>
    /// <returns></returns>
    public bool UpdateLogResponseWithTimeP(string XmlResponse, string jsonResponse, string id,string respuestaTime ,string responseTime, string responseFullTime)
    {
        try
        {

            MySqlDataAdapter adapter = new MySqlDataAdapter();
            con.ConnectionString = connectionString;
            con.Open();
            var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string query = "Update LogCotHogarWSP set ResponseWS ='" + XmlResponse + "',JsonResponse = '"+jsonResponse+"',ResponseWSTIME = '" + respuestaTime  + "',ResponseFullTime = '" + responseFullTime + "',FechaFin = '"+fechaFin+"' ,Time = '"+responseFullTime+"'  WHERE idLogCot =" + id;
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
        }
    }

    public string GetLastId()
    {
        try
        {
            con.ConnectionString = connectionString;
            con.Open();
            string query = "select MAX(IdLogCot) from LogCotHogarWSP";
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            string Id = command.ExecuteScalar().ToString();
            con.Close();
            return Id;
        }
        catch (Exception e)
        {
            string error = e.Message;
            return error;
        }
    }
}