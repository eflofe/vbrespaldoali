﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Log
/// </summary>
public class Log
{
    static List<latinoSubDesc> listaSubLatino = new List<latinoSubDesc>();
    string descSubLatino;
    static string seconds = "";
    private string descripcion = "";
    private string subMarcaLatino = "";
    private string claveDescripcion = "";
    private string iD = "";
    private string estado = "";
    private string codigo_postalEstadoMexico = "";
    private string codigo_postalEstadoMexicoDosNumeros = "";
    private string codigo_postalExterno = "";
    private string codigo_postalEcatepec = "";
    private string codigo_postalChiapas = "";
    private string codigo_postalCiudadMexico = "";
    private string codigo_postalJalisco = "";
    private string codigo_postalMichoacan = "";
    private string codigo_postalNuevoLeon = "";
    private string codigo_postalSinaloa = "";
    private string codigo_postalSonora = "";
    private string codigo_postalTamaulipas = "";
    private string codigo_postalIxtapaluca = "";
    private string codigo_postaBajaCalifornia = "";
    private string codigo_postaQuintanaRoo = "";
    private string codigo_postaHidalgo = "";
    private string codigo_postalVeracruz = "";
    private string codigo_postalGuerrero = "";


    string connectionString = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";

    /// <summary>
    /// GetMarcas Latino
    /// </summary>
    /// <param name="aseguradora"></param>
    /// <returns></returns>
    public string GetMarcasMyysql()
    {

        try
        {
            string connectionMysql = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";
            MySqlConnection connection = new MySqlConnection(connectionMysql);
            connection.Open();
            string query = "SELECT CONCAT('m',ROW_NUMBER()Over(ORDER BY text)) as id,(text)FROM( SELECT DISTINCT CLAVE_MARCA as text FROM LATINO_DESCRIPCIONES) ma";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            string json = JsonConvert.SerializeObject(dt);
            connection.Close();
            return json;

        }
        catch (Exception e)
        {
            return "error: " + e.Message;
        }
    }

    /// <summary>
    /// SubMarcas latino
    /// </summary>
    /// <param name="aseguradora"></param>
    /// <param name="marca"></param>
    /// <returns></returns>
    public string GetSubMarcaMySql(string marca, string modelo)
    {
        try
        {
            string connectionMysql = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";
            MySqlConnection connection = new MySqlConnection(connectionMysql);
            connection.Open();
            string query = "SELECT CONCAT('s',ROW_NUMBER()Over(ORDER BY text)) as id,(text)FROM( SELECT DISTINCT CLAVE_SUBMARCA as text from LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '"+marca+"' AND `AÑO` = "+modelo+") sub";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            connection.Close();
            string json = JsonConvert.SerializeObject(dt);
            return json;

        }
        catch (Exception e)
        {
            return "error: " + e.Message;
        }
    }

    /// <summary>
    /// Modelos latino
    /// </summary>
    /// <param name="aseguradora"></param>
    /// <param name="marca"></param>
    /// <returns></returns>
    public string GetModelosMysql(string marca)
    {
        try
        {
            string connectionMysql = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";
            MySqlConnection connection = new MySqlConnection(connectionMysql);
            connection.Open();
            string query = "SELECT CONCAT('o',ROW_NUMBER()Over(ORDER BY text)) as id,(text)FROM( SELECT DISTINCT `AÑO` as text  from LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '" + marca + "') mo";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            connection.Close();
            string json = JsonConvert.SerializeObject(dt);
            return json;

        }
        catch (Exception e)
        {
            return "error: " + e.Message;
        }
    }

    /// <summary>
    /// Método que obtiene las Descripciones de las aseguradoras
    /// </summary>
    /// <param name="marca"></param>
    /// <param name="subMarca"></param>
    /// <param name="modelo"></param>
    /// <returns></returns>
    public string GetDescripcionMysql(string marca,  string modelo, string subMarca)
    {
        try
        {
            string connectionMysql = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";
            MySqlConnection connection = new MySqlConnection(connectionMysql);
            connection.Open();
            string query = "SELECT CONCAT('d',ROW_NUMBER()Over(ORDER BY text)) as id,(text) FROM( SELECT DISTINCT DESCHOM as text from HomologacionALI.Consolidado c INNER JOIN HomologacionALI.Aseguradoras ase on ase.idAseguradora = c.idAseguradora INNER JOIN HomologacionALI.P_Marcas pm on pm.idmarca = pm.idmarca INNER JOIN HomologacionALI.P_SubMarcas ps on ps.idsubMarca = c.idSubMarca WHERE c.idMarca = (SELECT idMarca FROM HomologacionALI.P_Marcas WHERE Marca = '"+marca+"') AND c.idModelo = (SELECT idModelo FROM HomologacionALI.ModeloPrueba WHERE MODELO = "+modelo+") AND c.idSubMarca = (SELECT idSubMarca FROM HomologacionALI.P_SubMarcas psm INNER JOIN HomologacionALI.P_Marcas pr ON pr.IdMarca = psm.IdMarca WHERE SubMarca = '"+subMarca +"' AND psm.idMarca = (SELECT idMarca FROM HomologacionALI.P_Marcas WHERE MARCA = '"+marca+"' )) AND c.idAseguradora = 20) de";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            connection.Close();
            string json = JsonConvert.SerializeObject(dt);
            return json;

        }
        catch (Exception e)
        {
            return "error" + e.Message;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="aseguradora"></param>
    /// <param name="marca"></param>
    /// <param name="subMarca"></param>
    /// <param name="modelo"></param>
    /// <returns></returns>
    public string GetSubDescripcionMysql( string marca, string subMarca, string modelo, string descripcion)
    {
        try
        {
            string connectionMysql = "datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI;";
            MySqlConnection connection = new MySqlConnection(connectionMysql);
            connection.Open();
            string query = "SELECT DESCRIPCION  FROM LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '"+marca+"' AND  `AÑO` = "+modelo+" AND CLAVE_SUBMARCA = '"+subMarca+"' AND DESCRIPCION LIKE '%"+descripcion+"%'";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            int countRow = dt.Rows.Count;
            for (int i = 0; i <= countRow - 1; i++)
            {
                latinoSubDesc sas = new latinoSubDesc();

                string a = dt.Rows[i][0].ToString();
                string[] separatingString;
                separatingString = a.Split('-');


                string g = separatingString[1].ToString();
                string c = separatingString[0].ToString();

                sas.id = c;
                sas.text = g;

                listaSubLatino.Add(sas);
            }

            descSubLatino = JsonConvert.SerializeObject(listaSubLatino);
            listaSubLatino.Clear();
            return descSubLatino;

        }
        catch (Exception e)
        {
            return "error" + e.Message;
        }


    }


    //insercion al Log
    public string insertLogWSCot(string JSONrequest, string Aseguradora, string Marca, string Modelo, string Descripcion, string clave)
    {

        TimeSpan stop;
        TimeSpan start = new TimeSpan(DateTime.Now.Ticks);

        try
        {
            var fechaInicio = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string query = "Insert into LogCotizacionWSP (JSONRequest,InsertJSONTime,RequestWS,RequestTIME,ResponseWS,ResponseWSTIME,ResponseFullTime,JSONResponse,Aseguradora,Marca,Modelo,Descripcion,clave,FechaInicio,FechaFin,time)" +
    "VALUES ('" + JSONrequest + "',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'" + Aseguradora + "','" + Marca + "','" + Modelo + "','" + Descripcion + "','" + clave + "'," +
    "'" + fechaInicio + "',NULL,NULL)";

            stop = new TimeSpan(DateTime.Now.Ticks);
            seconds = stop.Subtract(start).TotalSeconds.ToString();
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(query, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            var IdReturned = GetLastId();
            return IdReturned;
        }
        catch (Exception ex)
        {
            throw new ArgumentException("error al insertar el log " + ex.Message);
        }
    }



    //Obtener ultimo ID
    public string GetLastId()
    {
        try
        {
            MySqlDataReader MyReader2;
            MySqlConnection connection = new MySqlConnection(connectionString);
            string query = "select MAX(idLogWSCot)  from LogCotizacionWSP";
            MySqlCommand myCommand = new MySqlCommand(query, connection);
            connection.Open();
            MyReader2 = myCommand.ExecuteReader();
            while (MyReader2.Read())
            {
                iD = MyReader2.GetValue(0).ToString();
            }
            MyReader2.Close();
            myCommand.Dispose();
            connection.Close();
            return iD;
        }
        catch (Exception e)
        {
            return "Error" + e;
        }
    }
    public bool UpdateLogResponse(string XmlResponse, string id)
    {
        try
        {
            string queryUpdate = "Update ReplicaDeAli_LogWSCotizacion set ResponseWS ='" + XmlResponse + "' WHERE idLogWSCot =" + id;
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
        }
    }

    public bool UpdateLogResponseWithTime(string XmlResponse, string id, string responseTime)
    {
        try
        {
            string queryUpdate = "Update LogCotizacionWSP set ResponseWS ='" + XmlResponse + "',ResponseTime = '" + responseTime + "'  WHERE idLogWSCot =" + id;
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
        }
    }

    public bool UpdateLogResponseWithTimeP(string XmlResponse, string id, string responseTime,string responseFullTime)
    {
        try
        {
            string queryUpdate = "Update LogCotizacionWSP set ResponseWS ='" + XmlResponse + "',ResponseWSTIME = '" + responseTime + "',ResponseFullTime = '"+responseFullTime +"'   WHERE idLogWSCot =" + id;
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML response" + e.Message);
        }
     }


    public bool UpdateLogRequest(string XmlRequest, string id)
    {
        try
        {
            string queryUpdate = "Update LogCotizacionWSP set RequestWS ='" + XmlRequest + "', InsertJSONTime = '" + seconds   + "' WHERE idLogWSCot =" + id;
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
        }
    }

    /// <summary>
    /// Medidor de tiempo
    /// </summary>
    public bool UpdateLogRequestWithTime(string XmlRequest, string id,string timeRequest)
    {
        try
        {
            string queryUpdate = "Update LogCotizacionWSP set RequestWS ='" + XmlRequest + "', InsertJSONTime = '" + seconds + "',RequestTIME = '"+timeRequest+"'  WHERE idLogWSCot =" + id;
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade XML request" + e.Message);
        }
    }


    public bool UpdateLogJsonResponse(string JsonResponse, string id)
    {
        try
        {
            var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string queryUpdate = "Update ReplicaDeAli_LogWSCotizacion set JSONResponse ='" + JsonResponse + "', FechaFin='" + fechaFin + "' WHERE idLogWSCot =" + GetLastId();
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade JSON request" + e.Message);
        }
    }

    public bool UpdateLogJsonResponseP(string JsonResponse, string id,string time)
    {
        try
        {
            var fechaFin = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            string queryUpdate = "Update LogCotizacionWSP set JSONResponse ='" + JsonResponse + "', FechaFin='" + fechaFin + "', time = "+time+" WHERE idLogWSCot =" + GetLastId();
            MySqlConnection miConexion = new MySqlConnection(connectionString);
            MySqlCommand miComando = new MySqlCommand(queryUpdate, miConexion);
            MySqlDataReader leerDatos;
            miConexion.Open();
            leerDatos = miComando.ExecuteReader();
            miConexion.Close();
            return true;
        }
        catch (Exception e)
        {
            throw new ArgumentException("error al hacer el uptade JSON request" + e.Message);
        }
    }
    //-.Query Qualitas 
    public string getEstado(string Cp)
    {

        string conn = (@"datasource=webservices.cputezxs2i4c.us-east-2.rds.amazonaws.com;port=3306;username=servici1;password=UUpzNvi9huoAF4SX;database=HomologacionALI ");
        try
        {
            MySqlDataReader MyReader2;
            string queryEstado = "Select Distinct IDEdo_Qua from Ali_R_CatEdosQUA where CPostal ='" + Cp + "'";
            MySqlConnection conexion = new MySqlConnection(conn);
            MySqlCommand myCommand = new MySqlCommand(queryEstado, conexion);
            conexion.Open();
            MyReader2 = myCommand.ExecuteReader();
            while (MyReader2.Read())
            {
                estado = MyReader2.GetValue(0).ToString();
            }
            MyReader2.Close();
            myCommand.Dispose();
            conexion.Close();
            return estado;
        }

        catch (Exception ex)
        {
            return "Error: " + ex;
        }
    }
    //.-Fin Query Qualitas

    //.- Query latino
    /// <summary>
    /// Metodo que regresa submarca latino
    /// </summary>
    /// <param name="clave_Marca"></param>
    /// <param name="descripcion"></param>
    /// <param name="año"></param>
    /// <returns></returns>
    public string GetSubMarcaLatino(string clave_Marca, string año, string descripcion)
    {
        try
        {
            MySqlDataReader MyReader2;
            MySqlConnection connection = new MySqlConnection(connectionString);
            string query = "select  DISTINCT(clave_submarca) from Latino_Catalogos_LATINO_DESCRIPCIONES where CLAVE_MARCA ='" + clave_Marca + "' and AÑO ='" + año + "' and DESCRIPCION LIKE '%" + descripcion + "%'";
            MySqlCommand myCommand = new MySqlCommand(query, connection);
            connection.Open();
            myCommand.CommandTimeout = 0;
            MyReader2 = myCommand.ExecuteReader();
            while (MyReader2.Read())
            {
                subMarcaLatino = MyReader2.GetValue(0).ToString();
            }
            MyReader2.Close();
            myCommand.Dispose();
            connection.Close();
            string json = subMarcaLatino;
            return json;

        }
        catch (Exception e)
        {
            return "error: " + e;
        }
    }


    /// <summary>
    /// Método que recupera la clave descripcion de la latino
    /// </summary>
    /// <param name="aseguradora"></param>
    /// <param name="marca"></param>
    /// <param name="modelo"></param>
    /// <param name="subMarca"></param>
    /// <returns>Clave descrpción</returns>
    public string GetClaveDescripcionLatino(string clave_Marca, string clave_SubMarca, string descripcion, string año)
    {
        try
        {
            MySqlDataReader MyReader2;
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string query = "SELECT  clave_descripcion FROM Latino_Catalogos_LATINO_DESCRIPCIONES where CLAVE_MARCA = '" + clave_Marca + "' and CLAVE_SUBMARCA = '" + clave_SubMarca + "' and DESCRIPCION like '%" + descripcion + "%' and AÑO = '" + año + "'";
            MySqlCommand myCommand = new MySqlCommand(query, connection);
            myCommand.CommandTimeout = 0;
            MyReader2 = myCommand.ExecuteReader();
            while (MyReader2.Read())
            {
                claveDescripcion = MyReader2.GetValue(0).ToString();
            }
            MyReader2.Close();
            myCommand.Dispose();
            connection.Close();
            string json = claveDescripcion;
            return json;


        }
        catch (Exception e)
        {
            return "error: " + e;
        }
    }

    /// <summary>
    /// Retorna la descripcion de el auto seleccionado
    /// </summary>
    /// <param name="marca"></param>
    /// <param name="subMarca"></param>
    /// <param name="año"></param>
    /// <returns></returns>
    public string GetDescripcion(string marca, string subMarca, string año, string claveD)
    {
        try
        {
            MySqlDataReader MyReader2;
            MySqlConnection connection = new MySqlConnection(connectionString);
            string query = "select DESCRIPCION from Latino_Catalogos_LATINO_DESCRIPCIONES where CLAVE_MARCA = '" + marca + "' and CLAVE_SUBMARCA =  '" + subMarca + "'and AÑO = '" + año + "' and DESCRIPCION = '" + claveD + "';";
            MySqlCommand myCommand = new MySqlCommand(query, connection);
            connection.Open();
            myCommand.CommandTimeout = 0;
            MyReader2 = myCommand.ExecuteReader();
            while (MyReader2.Read())
            {
                descripcion = MyReader2.GetValue(0).ToString();
            }
            MyReader2.Close();
            myCommand.Dispose();
            connection.Close();
            string json = descripcion;
            return json;

        }
        catch (Exception e)
        {
            return "error: " + e;
        }
    }
    public string GetEstadoLatinoMysql(string Cp)
    {
        //SqlConnection conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Replica_Ali_R;user id = servici1; password = Master@011");
        try
        {
            int comparadorEcatepec = 0;
            int comparadorIxtapaluca = 0;
            int comparadorCiudadMexico = 0;
            codigo_postalEstadoMexico = Cp.Substring(0, 3);
            codigo_postalEstadoMexicoDosNumeros = Cp.Substring(0, 2);
            codigo_postalExterno = Cp.Substring(0, 2);
            codigo_postalEcatepec = Cp.Substring(2, 1);
            codigo_postalChiapas = Cp.Substring(0, 2);
            codigo_postalCiudadMexico = Cp.Substring(0, 2);
            codigo_postalJalisco = Cp.Substring(0, 2);
            codigo_postalMichoacan = Cp.Substring(0, 2);
            codigo_postalNuevoLeon = Cp.Substring(0, 2);
            codigo_postalSinaloa = Cp.Substring(0, 2);
            codigo_postalTamaulipas = Cp.Substring(0, 2);
            codigo_postalIxtapaluca = Cp.Substring(2, 1);
            codigo_postaHidalgo = Cp.Substring(0, 2);
            codigo_postaBajaCalifornia = Cp.Substring(0, 3);
            codigo_postaQuintanaRoo = Cp.Substring(0, 3);
            codigo_postalSonora = Cp.Substring(0, 2);
            codigo_postalGuerrero = Cp.Substring(0, 2);
            codigo_postalVeracruz = Cp.Substring(0, 3);

            comparadorEcatepec = int.Parse(codigo_postalEcatepec);
            comparadorCiudadMexico = int.Parse(codigo_postalCiudadMexico);
            comparadorIxtapaluca = int.Parse(codigo_postalIxtapaluca);

            if (codigo_postalEstadoMexico == "557" || codigo_postalEstadoMexico == "550" || codigo_postalEstadoMexico == "568" || codigo_postalEstadoMexico == "565" || codigo_postalEstadoMexico == "527" || codigo_postalEstadoMexico == "548")
            {
                Cp = codigo_postalEstadoMexico;
            }
            else if (codigo_postalChiapas == "29" || codigo_postalChiapas == "30")
            {
                Cp = "30";
            }
            else if (codigo_postalEstadoMexicoDosNumeros == "55")
            {
                Cp = "55";
            }
            else if (comparadorCiudadMexico <= 16)
            {
                Cp = "01";
            }
            else if (codigo_postalJalisco == "47" || codigo_postalJalisco == "45")
            {
                Cp = "49";
            }
            else if (codigo_postalMichoacan == "59" || codigo_postalMichoacan == "61")
            {
                Cp = "58";
            }
            else if (codigo_postalNuevoLeon == "64" || codigo_postalNuevoLeon == "65")
            {
                Cp = "67";
            }
            else if (codigo_postalSinaloa == "80")
            {
                Cp = "81";
            }
            else if (codigo_postalSonora == "84" || codigo_postalSonora == "85")
            {
                Cp = "83";
            }
            else if (codigo_postalTamaulipas == "88" || codigo_postalTamaulipas == "89")
            {
                Cp = "87";
            }
            else if (codigo_postaQuintanaRoo == "775")
            {
                Cp = "77";
            }
            else if (comparadorIxtapaluca == 5)
            {
                Cp = "565";
            }
            else if (Cp == "56509")
            {
                Cp = "56509";
            }
            else if (codigo_postaBajaCalifornia == "227")
            {
                Cp = "21";
            }
            else if (codigo_postaHidalgo == "42" || codigo_postaHidalgo == "43")
            {
                Cp = "42";
            }
            else if (codigo_postalVeracruz == "917")
            {
                Cp = "91";
            }
            else if (codigo_postalGuerrero == "40" || codigo_postalGuerrero == "41")
            {
                Cp = "39";
            }
            else if (comparadorEcatepec == 7)
            {
                Cp = "550";
            }
            else
            {
                Cp = codigo_postalExterno;
            }
            MySqlDataReader MyReader2;
            MySqlConnection connection = new MySqlConnection(connectionString);
            string queryEstado = "SELECT DISTINCT(clave_estado) FROM Latino_Catalogos_LATINO_ESTADOS where Cp_general ='" + Cp + "'";
            MySqlCommand myCommand = new MySqlCommand(queryEstado, connection);
            connection.Open();
            myCommand.CommandTimeout = 0;
            MyReader2 = myCommand.ExecuteReader();
            while (MyReader2.Read())
            {
                descripcion = MyReader2.GetValue(0).ToString();
            }
            MyReader2.Close();
            myCommand.Dispose();
            connection.Close();
            string json = descripcion;
            return json;


        }

        catch (Exception ex)
        {
            return "Error: " + ex;
        }
    }

    /// <summary>
    /// Retorna la descripción del coche
    /// </summary>
    /// <param name="marca"></param>
    /// <param name="subMarca"></param>
    /// <param name="año"></param>
    /// <param name="descripcion"></param>
    /// <returns></returns>
    public string GetClabeDescripcionMysqlLatino(string marca, string subMarca, string año, string descripcion)
    {
        try
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string query = "SELECT CLAVE_DESCRIPCION FROM  Latino_Catalogos_LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '" + marca + "' and CLAVE_SUBMARCA = '" + subMarca + "' and AÑO = '" + año + "' and DESCRIPCION = '" + descripcion + "' ";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            string json = JsonConvert.SerializeObject(dt);
            connection.Close();
            return json;
        }
        catch (Exception e)
        {
            return "error: " + e;
        }
    }

    /// <summary>
    /// Regresa las marcas en la latino
    /// </summary>
    /// <returns></returns>
    public string GetMarcasLatiMysql()
    {

        try
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string query = "SELECT DISTINCT(CLAVE_MARCA) FROM Latino_Catalogos_LATINO_DESCRIPCIONES order by CLAVE_MARCA";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            string json = JsonConvert.SerializeObject(dt);
            connection.Close();
            return json;
        }
        catch (Exception e)
        {
            return "error: " + e;
        }
    }

    /// <summary>
    /// Metodo que obtien la submarca latino
    /// </summary>
    /// <param name="Marca"></param>
    /// <returns></returns>
    public string GetSubMarcasLatinoMysql(string Marca)
    {
        try
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string query = "SELECT DISTINCT(CLAVE_SUBMARCA) FROM Latino_Catalogos_LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '" + Marca + "' order by CLAVE_SUBMARCA";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            string json = JsonConvert.SerializeObject(dt);
            connection.Close();
            return json;
        }
        catch (Exception e)
        {
            return "error: " + e;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Marca"></param>
    /// <param name="SubMarca"></param>
    /// <returns></returns>
    public string GetModelosLatiMysql(string Marca, string SubMarca)
    {
        try
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string query = "SELECT DISTINCT(AÑO) FROM Latino_Catalogos_LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '" + Marca + "' and CLAVE_SUBMARCA = '" + SubMarca + "' order by AÑO";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            string json = JsonConvert.SerializeObject(dt);
            connection.Close();
            return json;
        }
        catch (Exception e)
        {
            return "ERROR" + e;
        }

    }

    /// <summary>
    /// Trae descripciones de  modelo seleccionado
    /// </summary>
    /// <param name="Marca"></param>
    /// <param name="SubMarca"></param>
    /// <returns></returns>
    public string GetDescripcionesMysqlatino(string Marca, string SubMarca, string modelo)
    {
        try
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            string query = "SELECT DESCRIPCION FROM Latino_Catalogos_LATINO_DESCRIPCIONES WHERE CLAVE_MARCA = '" + Marca + "' and CLAVE_SUBMARCA = '" + SubMarca + "' and AÑO = '" + modelo + "' ";
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);
            DataTable dt = new DataTable();
            adapter.SelectCommand.CommandTimeout = 0;
            adapter.Fill(dt);
            string json = JsonConvert.SerializeObject(dt);
            connection.Close();
            return json;
        }
        catch (Exception e)
        {
            return "ERROR" + e;
        }

    }

    public partial class latinoSubDesc
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("text")]
        public string text { get; set; }


    }

    //.-Fin Query Qualitas

}