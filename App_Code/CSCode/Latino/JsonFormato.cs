﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de JsonFormato
/// </summary>
public class JsonFormato
{
    [JsonProperty("aseguradora")]
    public string Aseguradora { get; set; }

    [JsonProperty("clave")]
    public string Clave { get; set; }

    [JsonProperty("cp")]
    public string Cp { get; set; }

    [JsonProperty("descripcion")]
    public string Descripcion { get; set; }

    [JsonProperty("descuento")]
    public string Descuento { get; set; }

    [JsonProperty("edad")]

    public string Edad { get; set; }

    [JsonProperty("fechaNacimiento")]
    public string FechaNacimiento { get; set; }

    [JsonProperty("genero")]
    public string Genero { get; set; }

    [JsonProperty("marca")]
    public string Marca { get; set; }

    [JsonProperty("modelo")]

    public int Modelo { get; set; }

    [JsonProperty("movimiento")]
    public string Movimiento { get; set; }

    [JsonProperty("paquete")]
    public string Paquete { get; set; }

    [JsonProperty("servicio")]
    public string Servicio { get; set; }

}
