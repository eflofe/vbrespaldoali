﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using SuperObjeto;

/// <summary>
/// Descripción breve de LatinoCot
/// </summary>
public class LatinoCot
{

    private long usr = 10041;
    private string pass = "CD56JH34ED23MRVC92";
    private string usrlog = "9257";
    private string usrpss = "L4t1nO9257";
    //----------
    private string claveVigencia = "0";
    private string claveDescuento = "0.00";
    private string claveAgente = "9257";
    private string claveFormaPago = "0";
    //---------
    private string claveProducto = "1";
    private string tarifa = "1704";
    private string clavePerfil = "22";
    private string modelo = "2019";
    private string tipoVehiculo = "AU";
    private string numreroSerieAuto = "";
    private string numPlacas = "";
    private string numMotor = "";
    private string numeControlVehicular = "";
    private string nombreConductor = "";
    private string beneficiarioPreferente = "";
    //--------  
    private string valorConvenido = "0";
    private string fechaFactura = "";
    private string valorFactura = "0";
    //----Guardar
    private string vigenciaInicial = DateTime.Now.ToString("dd/MM/yyyy");
    private string vigenciaFinal = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy");
    //Emitir
    private string claveCliente = "";
    private string nombre = "";
    private string apellidoPaterno = "";
    private string apellidoMaterno = "";
    private string nombreCompleto = "";
    private string RFC = "";
    private string colonia = "";
    private string calle = "";
    private string noInterior = "";
    private string NoExterior = "";
    private string claveColonia = "";
    private string JsonClienteDatos = "";
    private string cadenaClave = "";
    private string urlPoliza = "";
    private Log lg = new Log();
    string respuestaWS = "";
    XmlFormat xml = new XmlFormat();
    DateTime inicio = new DateTime();
    DateTime parar = new DateTime();
    TimeSpan lapsoTiempo = new TimeSpan();
    //Total cotización
    TimeSpan stop;
    TimeSpan start = new TimeSpan(DateTime.Now.Ticks);
    string seconds = "";
    /// <summary>
    /// Metodo que recibe datos de cotización  y los tranforma 
    /// </summary>
    /// <param name="seguro"></param>
    /// <param name="logWSCot"></param>
    /// <returns>XML</returns>
    public string CotizacionLatino(string data ,Seguro seguro, string logWSCot, string movimiento)
    {
        string Paquete = "";
        string servicio = "";
        string cp = seguro.Cliente.Direccion.CodPostal;
        try
        {
            LatinoWS.CotizadorLatinoClient cotizador = new LatinoWS.CotizadorLatinoClient();
            LatinoWS.DatosCotizar datos = new LatinoWS.DatosCotizar();
            LatinoWS.Credencial cred = new LatinoWS.Credencial();
            LatinoWS.Auto auto = new LatinoWS.Auto();
            LatinoWS.CoberturaAmparada cA = new LatinoWS.CoberturaAmparada();
            List<LatinoWS.CoberturaAmparada> listaCoberturas = new List<LatinoWS.CoberturaAmparada>();
            LatinoWS.CaracteristicasCotizacion cACotizacion = new LatinoWS.CaracteristicasCotizacion();
            Log lG = new Log();
            //inicio calculo de tíempo
            inicio = DateTime.Now;
            //Descuentos
            //Cobtador total 
            DateTime inicioTotal = new DateTime();
            DateTime pararTotal = new DateTime();
            TimeSpan lapsoTotal = new TimeSpan();
            inicioTotal = DateTime.Now;
            //contador de tiempo request
            DateTime inicioRequets = new DateTime();
            DateTime pararRequest = new DateTime();
            TimeSpan lapsoTiempoRequest = new TimeSpan();
            inicioRequets = DateTime.Now;
            if (seguro.Descuento == "0")
            {
                claveDescuento = "0.00";
            }
            else if (seguro.Descuento == "5")
            {
                claveDescuento = "5.00";
            }
            else if (seguro.Descuento == "10")
            {
                claveDescuento = "10.00";
            }
            else if (seguro.Descuento == "15")
            {
                claveDescuento = "15.00";
            }
            else if (seguro.Descuento == "20")
            {
                claveDescuento = "20.00";
            }
            else if (seguro.Descuento == "25")
            {
                claveDescuento = "25.00";
            }
            else if (seguro.Descuento == "30")
            {
                claveDescuento = "30.00";
            }
            else if (seguro.Descuento == "35")
            {
                claveDescuento = "35.00";
            }
            else if (seguro.Descuento == "40")
            {
                claveDescuento = "40.00";
            }
            else if (seguro.Descuento == "45")
            {
                claveDescuento = "45.00";
            }
            //paquetes
            if (seguro.Paquete == "AMPLIA")
            {
                Paquete = "0";
            }
            else if (seguro.Paquete == "LIMITADA")
            {
                Paquete = "1";
            }
            //-------------------Servicio
            if (seguro.Vehiculo.Servicio == "PARTICULAR")
            {
                servicio = "1";
            }
            //-----------------------
            cred.IdApp = this.usr;
            cred.PassApp = this.pass;
            cred.ClaveUsuario = this.usrlog;
            cred.Password = this.usrpss;
            //------
            cACotizacion.ClaveEstado = lg.GetEstadoLatinoMysql(cp).ToString();
            cACotizacion.ClavePaquete = Paquete;
            cACotizacion.ClaveVigencia = claveVigencia;
            cACotizacion.ClaveServicio = servicio;
            cACotizacion.ClaveDescuento = claveDescuento;
            cACotizacion.ClaveAgente = claveAgente;
            cACotizacion.ClaveFormaPago = claveFormaPago;
            //------------------
            Vehiculo vH = new Vehiculo();
            vH.Marca = seguro.Vehiculo.Marca;
            vH.Modelo = seguro.Vehiculo.Modelo;
            vH.CodMarca  = seguro.Vehiculo.CodMarca;
            vH.Descripcion = seguro.Vehiculo.Descripcion;
            vH.Clave = seguro.Vehiculo.Clave;
            //------------
            auto.ClaveMarca = vH.Marca;
            auto.ClaveModelo = vH.Modelo.ToString();
            auto.ClavePerfil = clavePerfil;
            auto.ClaveSubMarca = vH.CodMarca;
            auto.ClaveVehiculo = vH.Clave;
            auto.ClaveProducto = claveProducto;
            auto.Tarifa = tarifa;
            auto.TipoVehiculo = tipoVehiculo;
            auto.NumeroSerieAuto = numreroSerieAuto;
            auto.NumeroPlacas = numPlacas;
            auto.NumeroMotor = numMotor;
            auto.NumeroControlVehicular = numeControlVehicular;
            auto.NombreConductor = nombreConductor;
            auto.SerieValida = false;
            //------------------------------
            var coberturaDM = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "DM",
                Descripcion = "DANOS MATERIALES",
                SumaAsegurada = 0,
                DescripcionSumaAsegurada = "VALOR COMERCIAL",
                PorcentajeDeducible = 5,
                DescripcionDeducible = "5%",
                PrimaNeta = "null"
            };
            var coberturaCR = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "CR",
                Descripcion = "ROTURA DE CRISTALES",
                SumaAsegurada = 0,
                DescripcionSumaAsegurada = "VALOR COMERCIAL",
                PorcentajeDeducible = 20,
                DescripcionDeducible = "20%",
                PrimaNeta = "null"
            };
            var cobertura1 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "RT",
                Descripcion = "ROBO TOTAL",
                SumaAsegurada = 0,
                DescripcionSumaAsegurada = "VALOR COMERCIAL",
                PorcentajeDeducible = 10,
                DescripcionDeducible = "10%",
                PrimaNeta = "null"
            };

            var cobertura2 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "RC",
                Descripcion = "RESPONSBILIDAD CIVIL L.U.C.*",
                SumaAsegurada = 1500000,
                DescripcionSumaAsegurada = "1,500,000.00",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };

            var cobertura3 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "RCE",
                Descripcion = "EXTENSION DE RESPONSABILIDAD CIVIL",
                SumaAsegurada = 0,
                DescripcionSumaAsegurada = "AMPARADA",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };
            var cobertura4 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "RCC",
                Descripcion = "EXCESO DE RC A PERSONAS ART.502",
                SumaAsegurada = 2500000,
                DescripcionSumaAsegurada = "2,500,000.00",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };
            var cobertura5 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "GM",
                Descripcion = "GASTOS MEDICOS OCUPANTES L.U.C.*",
                SumaAsegurada = 125000,
                DescripcionSumaAsegurada = "125,000.0",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };
            var cobertura6 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "DJ",
                Descripcion = "ASESORIA LEGAL, FIANZAS Y/O CAUCIONES",
                SumaAsegurada = 0,
                DescripcionSumaAsegurada = "AMPARADA",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };
            var cobertura7 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "AI",
                Descripcion = "ASISTENCIA VIAL",
                SumaAsegurada = 0,
                DescripcionSumaAsegurada = "AMPARADA",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };
            var cobertura8 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "MA",
                Descripcion = "ACCIDENTES AL CONDUCTOR",
                SumaAsegurada = 100000,
                DescripcionSumaAsegurada = "100,000.00",
                PorcentajeDeducible = 0,
                DescripcionDeducible = "0",
                PrimaNeta = "null"
            };
            var cobertura9 = new LatinoWS.CoberturaAmparada
            {
                Amparada = true,
                Clave = "AS",
                Descripcion = "AUTO SUSTITUTO",
                SumaAsegurada = 350,
                DescripcionSumaAsegurada = "DAÑOS Y ROBO TOTAL",
                PorcentajeDeducible = 500,
                DescripcionDeducible = "500 PESOS",
                PrimaNeta = "null"
            };
            //---------------------------sobrecarga datos
            listaCoberturas.Add(coberturaCR);
            listaCoberturas.Add(coberturaDM);
            listaCoberturas.Add(cobertura1);
            listaCoberturas.Add(cobertura2);
            listaCoberturas.Add(cobertura3);
            listaCoberturas.Add(cobertura4);
            listaCoberturas.Add(cobertura5);
            listaCoberturas.Add(cobertura6);
            listaCoberturas.Add(cobertura7);
            listaCoberturas.Add(cobertura8);
            listaCoberturas.Add(cobertura9);
            datos.credenciales = cred;
            datos.caracteristicasCotizacion = cACotizacion;
            datos.datosAuto = auto;
            datos.coberturasAmparadas = listaCoberturas.ToArray();
            datos.ValorConvenido = 0;
            datos.ValorFactura = 0;
            datos.FechaFactura = "";
            JsonRespuesta jd = new JsonRespuesta();
            ////Validar si es emisión 
            if (movimiento == "emision" || movimiento == "Emision")
            {
                JsonRespuesta.DatosJson datosEmision = JsonConvert.DeserializeObject<JsonRespuesta.DatosJson>(data);
                //Datos de domicilio 
                //vehiculo
                numPlacas = datosEmision.vehiculo.NoPlacas;
                numMotor = datosEmision.vehiculo.NoMotor;
                numeControlVehicular = datosEmision.vehiculo.NumeroControlVehicular;
                numreroSerieAuto = datosEmision.vehiculo.NoSerie;
                //cliente
                nombre = seguro.Cliente.Nombre;
                apellidoPaterno = seguro.Cliente.ApellidoPat;
                apellidoMaterno = seguro.Cliente.ApellidoMat;
                RFC = seguro.Cliente.RFC;
                //Domicilio
                colonia = seguro.Cliente.Direccion.Colonia;
                calle = seguro.Cliente.Direccion.Calle;
                noInterior = seguro.Cliente.Direccion.NoInt;
                NoExterior = seguro.Cliente.Direccion.NoExt;

                string responseEmision = GuardarLatino(data, seguro, datosEmision.emision.IDCotizacion, datosEmision.cotizacion.NumeroReferencia, datosEmision.cliente.direccion.CodPostal, datosEmision.cliente.FechaNacimiento, logWSCot);
                return responseEmision;
            }
            //---RequestWs 
            System.Xml.Serialization.XmlSerializer serializer = new XmlSerializer(datos.GetType());
            MemoryStream ms = new MemoryStream();
            XmlWriter xmlWriter = XmlWriter.Create(ms);
            serializer.Serialize(xmlWriter, datos);
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);
            StreamReader read1 = new StreamReader(ms);
            string res = read1.ReadToEnd();
            pararRequest = DateTime.Now;
            lapsoTiempoRequest = pararRequest.Subtract(inicioRequets);
            string totalRequest = lapsoTiempoRequest.TotalSeconds.ToString();
            var datosCarga = cotizador.Cotizar(datos);
            lg.UpdateLogRequestWithTime(res, logWSCot , totalRequest);
            //Calculo Response
            parar = DateTime.Now;
            lapsoTiempo = parar.Subtract(inicio);
            string Segundos = lapsoTiempo.TotalSeconds.ToString();
            //Xml response
            var respuestaResponse = datosCarga;
            System.Xml.Serialization.XmlSerializer serializerResponse = new XmlSerializer(datosCarga.GetType());
            MemoryStream msr = new MemoryStream();
            XmlWriter xmlWriterr = XmlWriter.Create(msr);
            serializerResponse.Serialize(xmlWriterr, datosCarga);
            msr.Flush();
            msr.Seek(0, SeekOrigin.Begin);
            StreamReader read2 = new StreamReader(msr);
            string res2 = read2.ReadToEnd();
            //Calculo Response
            parar = DateTime.Now;
            lapsoTiempo = parar.Subtract(inicio);
            string totalSegundos = lapsoTiempo.TotalSeconds.ToString();
            //Calculo total Response
            pararTotal = DateTime.Now;
            lapsoTotal = pararTotal.Subtract(inicioTotal);
            string totalRequestResponse = lapsoTotal.TotalSeconds.ToString();
            lg.UpdateLogResponseWithTimeP(res2,logWSCot ,totalSegundos ,totalRequestResponse );
            //JSON Response
            string response = JsonConvert.SerializeObject(datosCarga);
            string responseFinal = jd.JsonLatinoRespuesta(response, seguro, datosCarga.NumeroReferencia);
            stop = new TimeSpan(DateTime.Now.Ticks);
            seconds   = stop.Subtract(start).TotalSeconds.ToString();
            lg.UpdateLogJsonResponseP(responseFinal , logWSCot, seconds );
            return responseFinal;
        }
        catch (Exception exc)
        {
            throw new ArgumentException("Ha habido un error en la cotizacion" + exc.Message);
        }
    }

    /// <summary>
    /// Método que realiza el proceso de generar la caratula de la cotización
    /// </summary>
    /// <param name="seguro"></param>
    /// <param name="logWSCot"></param>
    /// <param name="numReferencia"></param>
    /// <returns>JSNO guardar</returns>
    public string GuardarLatino(string data, Seguro seguro, string idCotizacion, string numReferencia, string codigoPostal, string fechaNacimento, string logWSCot)
    {

        LatinoWS.DatosGuardar datosG = new LatinoWS.DatosGuardar();
        LatinoWS.Credencial cred = new LatinoWS.Credencial();
        LatinoWS.CotizadorLatinoClient c = new LatinoWS.CotizadorLatinoClient();
        LatinoWS.Cliente cliente = new LatinoWS.Cliente();
        LatinoWS.DatosCliente datosCliente = new LatinoWS.DatosCliente();
        LatinoWS.DatosCP DCP = new LatinoWS.DatosCP();
        LatinoWS.Domicilio domicilio = new LatinoWS.Domicilio();


        try
        {

            //-----------------------
            cred.IdApp = this.usr;
            cred.PassApp = this.pass;
            cred.ClaveUsuario = this.usrlog;
            cred.Password = this.usrpss;
            //cliente
            nombreCompleto = nombre + " " + apellidoPaterno + " " + apellidoMaterno;

            cliente.Nombre = nombre;
            cliente.ApellidoPaterno = apellidoPaterno;
            cliente.ApellidoMaterno = apellidoMaterno;
            cliente.FechaNacimiento = fechaNacimento;
            cliente.RFC = RFC;
            cliente.TipoPersona = "1";
            cliente.ClaveCliente = "";


            //DatosCP para colonias
            DCP.CodigoPostal = codigoPostal;
            DCP.Credenciales = cred;
            string totalColonias = JsonConvert.SerializeObject((c.ObtenerColonias(DCP)));
            List<ColoniasClave> listaDeColonias = JsonConvert.DeserializeObject<List<ColoniasClave>>(totalColonias);
            int countColonias = listaDeColonias.Count;
            foreach (ColoniasClave col in listaDeColonias)
            {
                if (col.Descripcion == colonia)
                {
                    claveColonia = col.Clave;
                }
            }
            //Llenado datos domicilio cliente
            List<LatinoWS.Domicilio> d = new List<LatinoWS.Domicilio>();
            string n = claveColonia;
            var dom = new LatinoWS.Domicilio
            {
                Activo = true,
                Calle = calle,
                ClaveColonia = n,
                CodigoPostal = codigoPostal,
                NumeroExterior = NoExterior,
                NumeroInterior = noInterior
            };
            d.Add(dom);
            cliente.domicilios = d.ToArray();
            //--------------Agregar un cliente o buscarlo
            //--datoscliente
            datosCliente.Cliente = cliente;
            datosCliente.Credenciales = cred;

            string busquedaCliente = JsonConvert.SerializeObject(c.ObtenerClientes(datosCliente));

            var datosCargaJsonClienteEncontrado = busquedaCliente;
            var clientesEnArray = new JavaScriptSerializer().Deserialize<ClienteJson2>(datosCargaJsonClienteEncontrado);
            //En caso de no encontrar al cliente
            if (clientesEnArray.clientes == null)
            {
                JsonClienteDatos = JsonConvert.SerializeObject(c.AgregarCliente(datosCliente));
                string cadenaRecortada = JsonClienteDatos.Substring(73, 7);
                claveCliente = cadenaRecortada;

            }
            else
            {
                int countClientes = clientesEnArray.clientes.Count();
                for (int i = 0; i <= countClientes; i++)
                {
                    if (clientesEnArray.clientes[0].Rfc == RFC)
                    {
                        claveCliente = clientesEnArray.clientes[0].ClaveCliente.ToString();
                    }
                    else
                    {
                        //Si el cliente es nuevo y no cuenta con RFC 
                        JsonClienteDatos = JsonConvert.SerializeObject(c.AgregarCliente(datosCliente));
                        string cadenaRecortada = JsonClienteDatos.Substring(73, 7);
                        claveCliente = cadenaRecortada;
                    }

                }
            }


            //--------------
            datosG.Credenciales = cred;
            datosG.IDCotizacionWCF = idCotizacion;
            datosG.NumeroReferencia = numReferencia;
            datosG.NombreAsegurado = nombreCompleto;
            datosG.VigenciaInicial = vigenciaInicial;
            datosG.VigenciaFinal = vigenciaFinal;

            //

            var respuestaGuardar = c.Guardar(datosG);

            string response = JsonConvert.SerializeObject(respuestaGuardar);

            string emisionJson = EmitirLatino(data, seguro, idCotizacion, vigenciaInicial, vigenciaFinal, respuestaGuardar.NumeroCotizacion, logWSCot);
            return emisionJson;


        }
        catch (Exception exc)
        {
            throw new ArgumentException("Ha habido un error en la cotizacion" + exc.Message);
        }

    }


    /// <summary>
    /// Método que realiza la emisión de la póliza
    /// </summary>
    /// <param name="idCotizacion"></param>
    /// <param name="numeroPoliza"></param>
    /// <param name="vigenciaInicial"></param>
    /// <param name="vigenciaFinal"></param>
    /// <param name="numeroCotizacion"></param>
    /// <returns>Poliza</returns>
    public string EmitirLatino(string data, Seguro seguro, string idCotizacion, string vigenciaInicial, string vigenciaFinal, string numeroCotizacion, string logWSCot)
    {
        LatinoWS.DatosEmitir datosE = new LatinoWS.DatosEmitir();
        LatinoWS.Credencial cred = new LatinoWS.Credencial();
        LatinoWS.Auto auto = new LatinoWS.Auto();
        LatinoWS.Cliente datosCl = new LatinoWS.Cliente();
        //LogCotizacion log = new LogCotizacion();
        //-----
        cred.IdApp = this.usr;
        cred.PassApp = this.pass;
        cred.ClaveUsuario = this.usrlog;
        cred.Password = this.usrpss;
        //------
        auto.NumeroSerieAuto = seguro.Vehiculo.NoSerie;
        auto.NumeroPlacas = seguro.Vehiculo.NoPlacas;
        auto.NumeroMotor = seguro.Vehiculo.NoMotor;
        auto.NumeroControlVehicular = seguro.Vehiculo.Clave;
        auto.NombreConductor = nombreCompleto;
        auto.SerieValida = false;
        auto.BeneficiarioPreferente = nombreCompleto;
        //------
        datosCl.ClaveCliente = claveCliente;
        datosCl.Nombre = nombre;
        datosCl.ApellidoPaterno = apellidoPaterno;
        datosCl.ApellidoMaterno = apellidoMaterno;
        //-------
        datosE.Credenciales = cred;
        datosE.datosAuto = auto;
        datosE.cliente = datosCl;
        datosE.NumeroCotizacion = numeroCotizacion;
        datosE.VigenciaInicial = vigenciaInicial;
        datosE.VigenciaFinal = vigenciaFinal;
        datosE.IDCotizacionWCF = idCotizacion;

        //--------
        LatinoWS.CotizadorLatinoClient c = new LatinoWS.CotizadorLatinoClient();
        var JresponseWs = c.Emitir(datosE);
        //Xml response
        System.Xml.Serialization.XmlSerializer serializerResponse = new XmlSerializer(JresponseWs.GetType());
        MemoryStream msr = new MemoryStream();
        XmlWriter xmlWriterr = XmlWriter.Create(msr);
        serializerResponse.Serialize(xmlWriterr, JresponseWs);
        msr.Flush();
        msr.Seek(0, SeekOrigin.Begin);
        StreamReader read2 = new StreamReader(msr);
        string res2 = read2.ReadToEnd();
        // log.UpdateLogResponseEmision(res2, logWSCot);

        string jsonLinksEmision = JsonConvert.SerializeObject(JresponseWs);
        string[] separatingString;
        separatingString = jsonLinksEmision.Split('|');
        int countUrlPoliza = separatingString.Count();
        for (int i = 0; i <= countUrlPoliza; i++)
        {
            if (i == 6)
            {
                urlPoliza = separatingString[i].ToString();
            }

        }
        JsonRespuesta.DatosJson datosEmision = JsonConvert.DeserializeObject<JsonRespuesta.DatosJson>(data);
        datosEmision.emision.Poliza = urlPoliza;
        datosEmision.emision.Documento = urlPoliza;
       // JsonRespuestaEmision emision = new JsonRespuestaEmision();
        //string JsonEmision = emision.JsonLatinoRespuesta(data, datosEmision.emision.Poliza, datosEmision.emision.Documento);
        //log.UpdateLogJsonResponseEmision(JsonEmision, logWSCot);
        return "dasd";

    }


}