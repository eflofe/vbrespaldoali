﻿using Newtonsoft.Json;
using SuperObjeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de JsonRespuesta
/// </summary>
public class JsonRespuesta
{
    DatosJson datosJ = new DatosJson();
    Seguro seguro = new Seguro();
    //coberturas
    string descripcion = "";
    string sumaAsegurada = "";
    string descripcionDeducible = "";
    //Error
    public bool error;

    /// <summary>
    /// Método para regresar un formato de json valido
    /// </summary>
    /// <param name="response"></param>
    /// <param name="seguro"></param>
    /// <returns></returns>
    public string JsonLatinoRespuesta(string response, Seguro seguro, string numeroReferencia)
    {

        JsonF listCotizacion = JsonConvert.DeserializeObject<JsonF>(response);

        //aseguradora
        datosJ.Aseguradora = seguro.Aseguradora;
        //cliente
        Cliente cl = new Cliente();
        cl.TipoDepersona = "";
        cl.Nombre = "";
        cl.ApellidoPat = "";
        cl.ApellidoMat = "";
        cl.RFC = "";
        cl.FechaNacimiento = seguro.Cliente.FechaNacimiento;
        cl.Ocupacion = "";
        cl.Curp = "";

        //Direccion con cliente
        Direccion dR = new Direccion();
        dR.Calle = "";
        dR.NoExt = "";
        dR.NoInt = "";
        dR.Colonia = "";
        dR.CodPostal = seguro.Cliente.Direccion.CodPostal;
        dR.Poblacion = "";
        dR.Ciudad = "";
        dR.Pais = "";
        //cliente llenado
        cl.direccion = dR;
        cl.Edad = seguro.Cliente.Edad.ToString();
        cl.Genero = seguro.Cliente.Genero;
        cl.Telefono = seguro.Cliente.Telefono.ToString();
        cl.Email = "";
        //Vehiculo
        Vehiculo vH = new Vehiculo();
        vH.Uso = seguro.Vehiculo.Uso;
        vH.Marca = seguro.Vehiculo.Marca;
        vH.Modelo = seguro.Vehiculo.Modelo.ToString();
        vH.NoMotor = "";
        vH.NoSerie = "";
        vH.NoPlacas = "";
        vH.NumeroControlVehicular = "";
        vH.Descripcion = seguro.Vehiculo.Descripcion;
        vH.CodMarca = "";
        vH.CodDescripcion = "";
        vH.CodUso = "";
        vH.Clave = seguro.Vehiculo.Clave;
        vH.Servicio = seguro.Vehiculo.Servicio;
        //Coberturas
        CoberturasFinal cFinal = new CoberturasFinal();
        for (int i = 0; i <= 9; i++)

        {
            switch (listCotizacion.coberturas[i].Clave)
            {
                case "RT":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible;
                    cFinal.RoboTotal = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "RC":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible;
                    cFinal.RC = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "RCE":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible;
                    cFinal.RCExtension = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "RCC":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.RCC = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "GM":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.GastosMedicosOcupantes = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "AI":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.AsitenciaCompleta = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "DJ":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.DefensaJuridica = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "MA":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.MuerteAccidental = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "CR":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.Cristales = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;
                case "DM":
                    descripcion = listCotizacion.coberturas[i].Descripcion;
                    sumaAsegurada = listCotizacion.coberturas[i].SumaAsegurada.ToString();
                    descripcionDeducible = listCotizacion.coberturas[i].DescripcionDeducible.ToString();
                    cFinal.DanosMateriales = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + descripcionDeducible;
                    break;

            }
        }
        cFinal.DanosMaterialesPP = "";
        cFinal.RCPersonas = "";
        cFinal.RCBienes = "";
        cFinal.RCFamiliar = "";
        cFinal.RCExtranjero = "";
        cFinal.RCPExtra = "";
        cFinal.GastosMedicosEvento = "";

        //cotizacion
        Cotizacion cT = new Cotizacion();
        cT.PrimaTotal = listCotizacion.TotalPrimerRecibo.ToString();
        cT.PrimaNeta = listCotizacion.PrimaNeta.ToString();
        cT.Derechos = listCotizacion.GastosExpedicion.ToString();
        cT.Impuesto = listCotizacion.Iva.ToString();
        cT.Recargos = "";
        cT.PrimerPago = listCotizacion.TotalPrimerRecibo.ToString();
        cT.PagosSubsecuentes = "";
        cT.IDCotizacion = listCotizacion.IdCotizacionWcf.ToString();
        cT.CotID = "";
        cT.VerID = "";
        cT.CotIncID = "";
        cT.VerIncID = "";
        cT.NumeroReferencia = listCotizacion.NumeroReferencia.ToString();
        if (seguro.CodigoError == "")
        {
            cT.Resultado = true;
        }
        else
        {
            cT.Resultado = false;
        }
        //Emision
        Emision eM = new Emision();
        eM.PrimaTotal = "";
        eM.PrimaNeta = "";
        eM.Derechos = "";
        eM.Impuesto = "";
        eM.Recargos = "";
        eM.PrimerPago = "";
        eM.PagosSubsecuentes = "";
        eM.IDCotizacion = listCotizacion.IdCotizacionWcf.ToString();
        eM.Terminal = "";
        eM.Documento = "";
        eM.Poliza = "";
        eM.Resultado = "";
        //Pago
        Pago pG = new Pago();
        pG.medioPago = "";
        pG.nombreTarjeta = "";
        pG.banco = "";
        pG.noTarjeta = "";
        pG.mesExp = "";
        pG.anioExp = "";
        pG.codigoSeguridad = "";
        pG.carrier = "";
        pG.noClabe = "";
        //Rellenar datos
        datosJ.cliente = cl;
        datosJ.vehiculo = vH;
        datosJ.Coberturas = cFinal;
        datosJ.Paquete = seguro.Paquete;
        datosJ.Descuento = seguro.Descuento.ToString();
        datosJ.PeriodicidadDePago = 0;
        datosJ.cotizacion = cT;
        datosJ.emision = eM;
        datosJ.pago = pG;
        datosJ.CodigoError = seguro.CodigoError;
        datosJ.urlRedireccion = seguro.urlRedireccion;

        string responde = JsonConvert.SerializeObject(datosJ);
        return responde;
    }



    public partial class DatosJson
    {
        [JsonProperty("aseguradora")]
        public string Aseguradora { get; set; }
        public Cliente cliente { get; set; }
        public Vehiculo vehiculo { get; set; }
        public CoberturasFinal Coberturas { get; set; }

        [JsonProperty("paquete")]
        public string Paquete { get; set; }

        [JsonProperty("descuento")]
        public string Descuento { get; set; }

        [JsonProperty("PeriodicidadDePago")]
        public long PeriodicidadDePago { get; set; }
        public Cotizacion cotizacion { get; set; }
        public Emision emision { get; set; }
        public Pago pago { get; set; }
        public string CodigoError { get; set; }
        public string urlRedireccion { get; set; }

    }
    public partial class Cliente
    {
        public string TipoDepersona { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPat { get; set; }
        public string ApellidoMat { get; set; }
        public string RFC { get; set; }
        public string FechaNacimiento { get; set; }
        public string Ocupacion { get; set; }
        public string Curp { get; set; }
        public Direccion direccion { get; set; }
        public string Edad { get; set; }
        public string Genero { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }

    }
    public partial class Direccion
    {
        public string Calle { get; set; }
        public string NoExt { get; set; }
        public string NoInt { get; set; }
        public string Colonia { get; set; }
        public string CodPostal { get; set; }
        public string Poblacion { get; set; }
        public string Ciudad { get; set; }
        public string Pais { get; set; }
    }

    public partial class Vehiculo
    {
        public string Descripcion { get; set; }
        public string Uso { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string NoPlacas { get; set; }
        public string NumeroControlVehicular { get; set; }
        public string NoMotor { get; set; }
        public string NoSerie { get; set; }
        public string CodMarca { get; set; }
        public string CodDescripcion { get; set; }
        public string CodUso { get; set; }
        public string Clave { get; set; }
        public string Servicio { get; set; }
    }

    public partial class CoberturasFinal
    {
        public string DanosMateriales { get; set; }
        public string DanosMaterialesPP { get; set; }
        public string RoboTotal { get; set; }
        public string RCBienes { get; set; }
        public string RCPersonas { get; set; }
        public string RCC { get; set; }
        public string RC { get; set; }
        public string RCFamiliar { get; set; }
        public string RCExtension { get; set; }
        public string RCExtranjero { get; set; }
        public string RCPExtra { get; set; }
        public string AsitenciaCompleta { get; set; }
        public string DefensaJuridica { get; set; }
        public string GastosMedicosOcupantes { get; set; }
        public string MuerteAccidental { get; set; }
        public string GastosMedicosEvento { get; set; }
        public string Cristales { get; set; }
    }

    public partial class Cotizacion
    {
        public string PrimaTotal { get; set; }
        public string PrimaNeta { get; set; }
        public string Derechos { get; set; }
        public string Impuesto { get; set; }
        public string Recargos { get; set; }
        public string PrimerPago { get; set; }
        public string PagosSubsecuentes { get; set; }
        public string IDCotizacion { get; set; }
        public string CotID { get; set; }
        public string VerID { get; set; }
        public string CotIncID { get; set; }
        public string VerIncID { get; set; }
        public bool Resultado { get; set; }
        public string NumeroReferencia { get; set; }
    }

    public partial class Emision
    {
        public string PrimaTotal { get; set; }
        public string PrimaNeta { get; set; }
        public string Derechos { get; set; }
        public string Impuesto { get; set; }
        public string Recargos { get; set; }
        public string PrimerPago { get; set; }
        public string PagosSubsecuentes { get; set; }
        public string IDCotizacion { get; set; }
        public string Terminal { get; set; }
        public string Documento { get; set; }
        public string Poliza { get; set; }
        public string Resultado { get; set; }
    }

    public partial class Pago
    {
        public string medioPago { get; set; }
        public string nombreTarjeta { get; set; }
        public string banco { get; set; }
        public string noTarjeta { get; set; }
        public string mesExp { get; set; }
        public string anioExp { get; set; }
        public string codigoSeguridad { get; set; }
        public string noClabe { get; set; }
        public string carrier { get; set; }
    }



    //DAtos json obtenidos
    public partial class JsonF
    {
        public long IdCotizacionWcf { get; set; }

        [JsonProperty("NumeroReferencia")]
        public Guid NumeroReferencia { get; set; }

        [JsonProperty("PrimaNeta")]
        public double PrimaNeta { get; set; }

        [JsonProperty("RecargoPagoFraccionado")]
        public long RecargoPagoFraccionado { get; set; }

        [JsonProperty("GastosExpedicion")]
        public long GastosExpedicion { get; set; }

        [JsonProperty("SubTotal")]
        public double SubTotal { get; set; }

        [JsonProperty("Iva")]
        public double Iva { get; set; }

        [JsonProperty("TotalPrimerRecibo")]
        public double TotalPrimerRecibo { get; set; }

        [JsonProperty("TotalReciboSubsecuente")]
        public long TotalReciboSubsecuente { get; set; }

        [JsonProperty("TotalPrimaAnual")]
        public double TotalPrimaAnual { get; set; }

        [JsonProperty("TotalPrimaSemestral")]
        public long TotalPrimaSemestral { get; set; }

        [JsonProperty("TotalPrimaTrimestral")]
        public long TotalPrimaTrimestral { get; set; }

        [JsonProperty("TotalPrimaMensual")]
        public long TotalPrimaMensual { get; set; }

        [JsonProperty("Coberturas")]
        public Coberturas[] coberturas { get; set; }

        [JsonProperty("mensajes")]
        public string[] Mensajes { get; set; }
    }

    public partial class Coberturas
    {
        [JsonProperty("Amparada")]
        public bool Amparada { get; set; }

        [JsonProperty("Clave")]
        public string Clave { get; set; }

        [JsonProperty("Descripcion")]
        public string Descripcion { get; set; }

        [JsonProperty("SumaAsegurada")]
        public long SumaAsegurada { get; set; }

        [JsonProperty("DescripcionSumaAsegurada")]
        public string DescripcionSumaAsegurada { get; set; }

        [JsonProperty("PorcentajeDeducible")]
        public long PorcentajeDeducible { get; set; }

        [JsonProperty("DescripcionDeducible")]
        public string DescripcionDeducible { get; set; }

        [JsonProperty("PrimaNeta")]
        public string PrimaNeta { get; set; }
    }

}