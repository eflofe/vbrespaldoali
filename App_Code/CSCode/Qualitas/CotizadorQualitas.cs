﻿using SuperObjeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

/// <summary>
/// Descripción breve de CotizadorQualitas
/// </summary>
public class CotizadorQualitas
{
    DateTime inicio = new DateTime();
    DateTime parar = new DateTime();
    TimeSpan lapsoTiempo = new TimeSpan();   
    XmlFormat xml = new XmlFormat();
        Log lg = new Log();
        string respuestaWS = "";
        string Negocio = "2886";
        string TipoMovimiento = "2";
        string FecIniVig = DateTime.Now.ToString("yyyy-MM-dd");
        string FecFinVig = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");
        string Paquete = "";
        string Agente = "71315";
        string FormaPago = "";
        string Tarifa = "LINEA";
        string DigitoAmis;
        string uso = "";
        string servicio = "";

        public string CotizaQualitas(Seguro seguro, string log)
        {
 
        inicio = DateTime.Now;
            string CveAmis = seguro.Vehiculo.Clave.ToString();

            if (CveAmis.Length == 4)
            {
                CveAmis = "0" + CveAmis;
                DigitoAmis = CveAmis;
            }
            else if (CveAmis.Length == 3)
            {
                CveAmis = "00" + CveAmis;
                DigitoAmis = CveAmis;
            }
            else
            {
                DigitoAmis = CveAmis;
            }
            if (seguro.Paquete == "AMPLIA")
            {
                Paquete = "01";
            }
            else if (seguro.Paquete == "LIMITADA")
            {
                Paquete = "03";
            }
            if (seguro.Vehiculo.Uso == "PARTICULAR")
            {
                uso = "01";
            }
            if (seguro.Vehiculo.Servicio == "PARTICULAR")
            {
                servicio = "01";
            }
            string validador = DigitoValidador(DigitoAmis);
            string idEdo = lg.getEstado(seguro.Cliente.Direccion.CodPostal);
        //contador de tiempo request
        DateTime inicioRequets = new DateTime();
        DateTime pararRequest = new DateTime();
        TimeSpan lapsoTiempoRequest = new TimeSpan();
        inicioRequets = DateTime.Now;
        var xmlCotizacion = xml.GenerateXml(seguro, uso, Paquete, servicio, seguro.Vehiculo.Clave.ToString(), validador, idEdo);
        pararRequest = DateTime.Now;
        lapsoTiempoRequest = pararRequest.Subtract(inicioRequets);
        string totalSegundosRequets = lapsoTiempoRequest.TotalSeconds.ToString();
        lg.UpdateLogRequestWithTime (xmlCotizacion, log,totalSegundosRequets);
        //termina request  
        //Inicia response
        DateTime inicioResponse = new DateTime();
        DateTime pararResponse = new DateTime();
        TimeSpan lapsoTiempoResponse = new TimeSpan();
        inicioResponse  = DateTime.Now;

        WsCotizaQ.wsEmisionServiceSoapClient q = new WsCotizaQ.wsEmisionServiceSoapClient();
        respuestaWS = q.obtenerNuevaEmision(xmlCotizacion);
        pararResponse  = DateTime.Now;
        lapsoTiempoResponse = pararResponse.Subtract(inicioResponse);
        string totalResponse = lapsoTiempoResponse.TotalSeconds.ToString();
        // lg.UpdateLogResponse(respuestaWS, log);
        parar = DateTime.Now;
        lapsoTiempo = parar.Subtract(inicio);
        string totalSegundos = lapsoTiempo.TotalSeconds.ToString();
        lg.UpdateLogResponseWithTimeP (respuestaWS  ,log,totalResponse,totalSegundos);


            XmlDocument doc = new XmlDocument();
            doc.LoadXml(respuestaWS);
            string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
            JsonRespuestaQualitas JsonQ = new JsonRespuestaQualitas();
            string jsonResponse = JsonQ.JsonQualitasRespuesta(json, seguro);
        parar  = DateTime.Now;
        lapsoTiempo = parar.Subtract(inicio);
        string segundosTotales = lapsoTiempo.TotalSeconds.ToString();
        // lg.UpdateLogJsonResponse(jsonResponse, log);
        lg.UpdateLogJsonResponseP(jsonResponse, log, segundosTotales);
        return jsonResponse;
        }

        public string DigitoValidador(string clave)
        {
            string ami = clave;
            string impar1 = ami.Substring(0, 1);
            string impar2 = ami.Substring(2, 1);
            string impar3 = ami.Substring(4, 1);
            string par1 = ami.Substring(1, 1);
            string par2 = ami.Substring(3, 1);
            int sumaTotal = ((Convert.ToInt32(impar1) + Convert.ToInt32(impar2) + Convert.ToInt32(impar3)) * 3) + (Convert.ToInt32(par1) + Convert.ToInt32(par2));
            int r = 10 - (sumaTotal % 10);
            return r.ToString();
        }
    }
