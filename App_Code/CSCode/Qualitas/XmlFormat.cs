﻿using SuperObjeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

/// <summary>
/// Descripción breve de XmlFormat
/// </summary>
public class XmlFormat
{
    
        string NoNegocio = "2886";
        string NoOtra = "";
        string TipoEndoso = "";
        string NoEndoso = "";
        string NoCotizacion = "";
        string NoPoliza = "";
        string TipoMovimiento = "2";
        string Fecha = DateTime.Now.ToString("yyyy-MM-dd");
        string FechaTermino = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd");

        public string GenerateXml(Seguro seguro, string uso, string paquete, string servicio, string digitoAmis, string validador, string idEdo)
        {
            string xml = "";

            XmlDocument doc = new XmlDocument();

            XmlNode rootNode = doc.CreateElement("Movimientos");
            doc.AppendChild(rootNode);

            //Nodo Movimiento           
            //Los atributos del Nodo van al revez por el orden, se comienza con el ultimo y se termina con el primero.
            XmlNode movimientoNode = doc.CreateElement("Movimiento");

            XmlAttribute NoNegocioAt = doc.CreateAttribute("NoNegocio"); //Atributo de Nodo Movimiento
            NoNegocioAt.Value = NoNegocio;//Asignacion de valor al atributo NoNegocio
            movimientoNode.Attributes.Append(NoNegocioAt);//Agregado del atributo al nodo

            XmlAttribute NoOtraAt = doc.CreateAttribute("NoOTra");//Atributo NoOtra al Nodo Movimiento
            NoOtraAt.Value = NoOtra;//Asignacion de valor al Atributo
            movimientoNode.Attributes.Append(NoOtraAt);//Agregado del atributo al nodo


            XmlAttribute TipoEndosoAT = doc.CreateAttribute("TipoEndoso"); //Atributo
            TipoEndosoAT.Value = TipoEndoso;//Asignacion de valor al Atributo
            movimientoNode.Attributes.Append(TipoEndosoAT);//Agregado del atributo al nodo

            XmlAttribute NoEndosoAt = doc.CreateAttribute("NoEndoso");//Atributo
            NoEndosoAt.Value = NoEndoso;//Asignacion de valor al Atributo
            movimientoNode.Attributes.Append(NoEndosoAt);//Agregado del atributo al nodo

            XmlAttribute NoCotizacionAt = doc.CreateAttribute("NoCotizacion");//Atributo
            NoCotizacionAt.Value = NoCotizacion;//Asignacion de valor al Atributo
            movimientoNode.Attributes.Append(NoCotizacionAt);//Agregado del atributo al nodo

            XmlAttribute NoPolizaAT = doc.CreateAttribute("NoPoliza");//Atributo
            NoPolizaAT.Value = NoPoliza;//Asignacion de valor al Atributo
            movimientoNode.Attributes.Append(NoPolizaAT);//Agregado del atributo al nodo

            XmlAttribute TipoMovimientoAt = doc.CreateAttribute("TipoMovimiento");
            TipoMovimientoAt.Value = TipoMovimiento;//Asignacion de valor al Atributo
            movimientoNode.Attributes.Append(TipoMovimientoAt);//Agregado del atributo al nodo

            //Fin del Nodo Movimiento

            //Nodo DatosAsegurado
            //Inicio del grupo del Nodo DatosAsegurado
            XmlNode DatosAseguradoNode = doc.CreateElement("DatosAsegurado");
            XmlAttribute NoAseguradoAt = doc.CreateAttribute("NoAsegurado");
            NoAseguradoAt.Value = "";
            DatosAseguradoNode.Attributes.Append(NoAseguradoAt);
            //Fin del Nodo DatosAsegurado

            XmlNode NombreNode = doc.CreateElement("Nombre");//Nodo vacio Nombre
            XmlNode DireccionNode = doc.CreateElement("Direccion");//Nodo vacio Direccion
            XmlNode ColoniaNode = doc.CreateElement("Colonia");//Nodo vacio Colonia
            XmlNode PoblacionNode = doc.CreateElement("Poblacion");//Nodo vacio Poblacion
            XmlNode EstadoNode = doc.CreateElement("Estado");//Nodo vacio Estado *************Pendiente por llenar sus atributos *****************
            EstadoNode.InnerText = idEdo;
            XmlNode CpNode = doc.CreateElement("CodigoPostal");//Nodo CP 
            CpNode.InnerText = seguro.Cliente.Direccion.CodPostal;
            XmlNode NoEmpleadoNode = doc.CreateElement("NoEmpleado");//Nodo vacio NoEmpleado
            XmlNode AgrupadorNode = doc.CreateElement("Agrupador");//Nodo vacio Agrupador

            //Nodo ConsideracionesAdicionalesDA 1
            XmlNode ConsideracionesAdicionalesDANode = doc.CreateElement("ConsideracionesAdicionalesDA");
            XmlAttribute NoConsideracionAt = doc.CreateAttribute("NoConsideracion");
            NoConsideracionAt.Value = "40";
            ConsideracionesAdicionalesDANode.Attributes.Append(NoConsideracionAt);
            XmlNode TipoReglaNode = doc.CreateElement("TipoRegla");//Nodo TipoRegla
            TipoReglaNode.InnerText = "3";
            XmlNode ValorReglaNode = doc.CreateElement("ValorRegla");
            ValorReglaNode.InnerText = "";
            //Fin del Nodo ConsideracionesAdicionalesDA 1

            //Nodo ConsideracionesAdicionalesDA 2
            XmlNode ConsideracionesAdicionalesDANode2 = doc.CreateElement("ConsideracionesAdicionalesDA");
            XmlAttribute NoConsideracionAt2 = doc.CreateAttribute("NoConsideracion");
            NoConsideracionAt2.Value = "40";
            ConsideracionesAdicionalesDANode2.Attributes.Append(NoConsideracionAt2);
            XmlNode TipoReglaNode2 = doc.CreateElement("TipoRegla");//Nodo TipoRegla
            TipoReglaNode2.InnerText = "4";
            XmlNode ValorReglaNode2 = doc.CreateElement("ValorRegla");
            ValorReglaNode2.InnerText = "1";
            //Fin del Nodo ConsideracionesAdicionalesDA 2

            //Nodo ConsideracionesAdicionalesDA 3
            XmlNode ConsideracionesAdicionalesDANode3 = doc.CreateElement("ConsideracionesAdicionalesDA");
            XmlAttribute NoConsideracionAt3 = doc.CreateAttribute("NoConsideracion");
            NoConsideracionAt3.Value = "40";
            ConsideracionesAdicionalesDANode3.Attributes.Append(NoConsideracionAt3);
            XmlNode TipoReglaNode3 = doc.CreateElement("TipoRegla");//Nodo TipoRegla
            TipoReglaNode3.InnerText = "5";
            XmlNode ValorReglaNode3 = doc.CreateElement("ValorRegla");
            ValorReglaNode3.InnerText = "";
            //Fin del Nodo ConsideracionesAdicionalesDA 3

            //Nodo ConsideracionesAdicionalesDA 4
            XmlNode ConsideracionesAdicionalesDANode4 = doc.CreateElement("ConsideracionesAdicionalesDA");
            XmlAttribute NoConsideracionAt4 = doc.CreateAttribute("NoConsideracion");
            NoConsideracionAt4.Value = "40";
            ConsideracionesAdicionalesDANode4.Attributes.Append(NoConsideracionAt4);
            XmlNode TipoReglaNode4 = doc.CreateElement("TipoRegla");//Nodo TipoRegla
            TipoReglaNode4.InnerText = "6";
            XmlNode ValorReglaNode4 = doc.CreateElement("ValorRegla");
            ValorReglaNode4.InnerText = "";
            //Fin del Nodo ConsideracionesAdicionalesDA 4
            //Fin del Grupo del Nodo DatosAsegurado

            //inicio del grupo del Nodo DatosVehiculo
            //Nodo DatosVehiculo
            XmlNode DatosVehiculoNode = doc.CreateElement("DatosVehiculo");
            XmlAttribute NoIncisoAt = doc.CreateAttribute("NoInciso");
            NoIncisoAt.Value = "1";
            DatosVehiculoNode.Attributes.Append(NoIncisoAt);
            XmlNode ClaveAmisNode = doc.CreateElement("ClaveAmis");
            ClaveAmisNode.InnerText = digitoAmis;
            XmlNode ModeloNode = doc.CreateElement("Modelo");
            ModeloNode.InnerText = seguro.Vehiculo.Modelo.ToString();
            XmlNode DescripcionVehiculoNode = doc.CreateElement("DescripcionVehiculo");
            DescripcionVehiculoNode.InnerText = seguro.Vehiculo.Descripcion;
            XmlNode UsoNode = doc.CreateElement("Uso");
            UsoNode.InnerText = uso;
            XmlNode ServicioNode = doc.CreateElement("Servicio");
            ServicioNode.InnerText = servicio;
            XmlNode PaqueteNode = doc.CreateElement("Paquete");
            PaqueteNode.InnerText = paquete;
            XmlNode MotorNode = doc.CreateElement("Motor");//Nodo vacio
            MotorNode.InnerText = "";
            XmlNode SerieNode = doc.CreateElement("Serie");//Nodo vacio
            SerieNode.InnerText = "";

            //Grupo de nodos Cobertura
            XmlNode CoberturasNode = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt.Value = "1";
            CoberturasNode.Attributes.Append(NoCoberturaAt);
            XmlNode SumaAseguradaNode = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode.InnerText = "";
            XmlNode TipoSumaNode = doc.CreateElement("TipoSuma");
            TipoSumaNode.InnerText = "0";
            XmlNode DeducibleNode = doc.CreateElement("Deducible");
            DeducibleNode.InnerText = "5";
            XmlNode PrimaNode = doc.CreateElement("Prima");
            PrimaNode.InnerText = "0";
            //Grupo de nodos Cobertura

            //Grupo de nodos Cobertura2
            XmlNode CoberturasNode2 = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt2 = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt2.Value = "3";
            CoberturasNode2.Attributes.Append(NoCoberturaAt2);
            XmlNode SumaAseguradaNode2 = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode2.InnerText = "";
            XmlNode TipoSumaNode2 = doc.CreateElement("TipoSuma");
            TipoSumaNode2.InnerText = "0";
            XmlNode DeducibleNode2 = doc.CreateElement("Deducible");
            DeducibleNode2.InnerText = "10";
            XmlNode PrimaNode2 = doc.CreateElement("Prima");
            PrimaNode2.InnerText = "0";
            //Grupo de nodos Cobertura2

            //Grupo de nodos Cobertura3
            XmlNode CoberturasNode3 = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt3 = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt3.Value = "4";
            CoberturasNode3.Attributes.Append(NoCoberturaAt3);
            XmlNode SumaAseguradaNode3 = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode3.InnerText = "3000000";
            XmlNode TipoSumaNode3 = doc.CreateElement("TipoSuma");
            TipoSumaNode3.InnerText = "0";
            XmlNode DeducibleNode3 = doc.CreateElement("Deducible");
            DeducibleNode3.InnerText = "0";
            XmlNode PrimaNode3 = doc.CreateElement("Prima");
            PrimaNode3.InnerText = "0";
            //Grupo de nodos Cobertura3

            //Grupo de nodos Cobertura4
            XmlNode CoberturasNode4 = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt4 = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt4.Value = "5";
            CoberturasNode4.Attributes.Append(NoCoberturaAt4);
            XmlNode SumaAseguradaNode4 = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode4.InnerText = "200000";
            XmlNode TipoSumaNode4 = doc.CreateElement("TipoSuma");
            TipoSumaNode4.InnerText = "0";
            XmlNode DeducibleNode4 = doc.CreateElement("Deducible");
            DeducibleNode4.InnerText = "0";
            XmlNode PrimaNode4 = doc.CreateElement("Prima");
            PrimaNode4.InnerText = "0";
            //Grupo de nodos Cobertura4

            //Grupo de nodos Cobertura5
            XmlNode CoberturasNode5 = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt5 = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt5.Value = "6";
            CoberturasNode5.Attributes.Append(NoCoberturaAt5);
            XmlNode SumaAseguradaNode5 = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode5.InnerText = "50000";
            XmlNode TipoSumaNode5 = doc.CreateElement("TipoSuma");
            TipoSumaNode5.InnerText = "0";
            XmlNode DeducibleNode5 = doc.CreateElement("Deducible");
            DeducibleNode5.InnerText = "0";
            XmlNode PrimaNode5 = doc.CreateElement("Prima");
            PrimaNode5.InnerText = "0";
            //Grupo de nodos Cobertura5

            //Grupo de nodos Cobertura6
            XmlNode CoberturasNode6 = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt6 = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt6.Value = "7";
            CoberturasNode6.Attributes.Append(NoCoberturaAt6);
            XmlNode SumaAseguradaNode6 = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode6.InnerText = "0";
            XmlNode TipoSumaNode6 = doc.CreateElement("TipoSuma");
            TipoSumaNode6.InnerText = "0";
            XmlNode DeducibleNode6 = doc.CreateElement("Deducible");
            DeducibleNode6.InnerText = "0";
            XmlNode PrimaNode6 = doc.CreateElement("Prima");
            PrimaNode6.InnerText = "0";
            //Grupo de nodos Cobertura6

            //Grupo de nodos Cobertura7
            XmlNode CoberturasNode7 = doc.CreateElement("Coberturas");
            XmlAttribute NoCoberturaAt7 = doc.CreateAttribute("NoCobertura");
            NoCoberturaAt7.Value = "14";
            CoberturasNode7.Attributes.Append(NoCoberturaAt7);
            XmlNode SumaAseguradaNode7 = doc.CreateElement("SumaAsegurada");
            SumaAseguradaNode7.InnerText = "0";
            XmlNode TipoSumaNode7 = doc.CreateElement("TipoSuma");
            TipoSumaNode7.InnerText = "0";
            XmlNode DeducibleNode7 = doc.CreateElement("Deducible");
            DeducibleNode7.InnerText = "0";
            XmlNode PrimaNode7 = doc.CreateElement("Prima");
            PrimaNode7.InnerText = "0";
            //Grupo de nodos Cobertura7

            //Nodo DAtosGenerales
            XmlNode DatosGeneralesNode = doc.CreateElement("DatosGenerales");
            XmlNode FechaEmisionNode = doc.CreateElement("FechaEmision");
            FechaEmisionNode.InnerText = Fecha;
            XmlNode FechaInicioNode = doc.CreateElement("FechaInicio");
            FechaInicioNode.InnerText = Fecha;
            XmlNode FechaTerminoNode = doc.CreateElement("FechaTermino");
            FechaTerminoNode.InnerText = FechaTermino;
            XmlNode MonedaNode = doc.CreateElement("Moneda");
            MonedaNode.InnerText = "0";
            XmlNode AgenteNode = doc.CreateElement("Agente");
            AgenteNode.InnerText = "71315";
            XmlNode FormaPagoNode = doc.CreateElement("FormaPago");
            FormaPagoNode.InnerText = "C";
            XmlNode TarifaValoresNode = doc.CreateElement("TarifaValores");
            TarifaValoresNode.InnerText = "LINEA";
            XmlNode TarifaCuotasNode = doc.CreateElement("TarifaCuotas");
            TarifaCuotasNode.InnerText = "LINEA";
            XmlNode TarifaDerechosNode = doc.CreateElement("TarifaDerechos");
            TarifaDerechosNode.InnerText = "LINEA";
            XmlNode PlazoNode = doc.CreateElement("Plazo");
            XmlNode AgenciaNode = doc.CreateElement("Agencia");
            XmlNode ContratoNode = doc.CreateElement("Contrato");
            XmlNode PorcentajeDescuentoNode = doc.CreateElement("PorcentajeDescuento");
            PorcentajeDescuentoNode.InnerText = "20";//Varia dependiendo la aseguradora

            XmlNode ConsideracionesAdicionalesDGNode = doc.CreateElement("ConsideracionesAdicionalesDG");
            XmlAttribute NoConsideracionAtDG = doc.CreateAttribute("NoConsideracion");
            NoConsideracionAtDG.Value = "01";
            ConsideracionesAdicionalesDGNode.Attributes.Append(NoConsideracionAtDG);
            XmlNode tipoReglaNodeDG = doc.CreateElement("TipoRegla");
            tipoReglaNodeDG.InnerText = "1";
            XmlNode ValorReglaNodeDG = doc.CreateElement("ValorRegla");
            ValorReglaNodeDG.InnerText = validador;

            XmlNode ConsideracionesAdicionalesDGNode2 = doc.CreateElement("ConsideracionesAdicionalesDG");
            XmlAttribute NoConsideracionAtDG2 = doc.CreateAttribute("NoConsideracion");
            NoConsideracionAtDG2.Value = "04";
            ConsideracionesAdicionalesDGNode2.Attributes.Append(NoConsideracionAtDG2);
            XmlNode tipoReglaNodeDG2 = doc.CreateElement("TipoRegla");
            tipoReglaNodeDG2.InnerText = "1";
            XmlNode ValorReglaNodeDG2 = doc.CreateElement("ValorRegla");
            ValorReglaNodeDG2.InnerText = "0";
            //Fin Nodo DAtosGenerales

            //Nodo Primas
            XmlNode primasNodeM = doc.CreateElement("Primas");
            XmlNode PrimaNeta = doc.CreateElement("PrimaNeta");
            XmlNode Derecho = doc.CreateElement("Derecho");
            Derecho.InnerText = "500";
            XmlNode Recargo = doc.CreateElement("Recargo");
            XmlNode Impuesto = doc.CreateElement("Impuesto");
            XmlNode PrimaTotal = doc.CreateElement("PrimaTotal");
            XmlNode Comision = doc.CreateElement("Comision");
            //Nodo Primas

            //Nodo CodigoError
            XmlNode CodigoError = doc.CreateElement("CodigoError");
            //Nodo CodigoError

            //Estructura Primas
            primasNodeM.AppendChild(PrimaNeta);
            primasNodeM.AppendChild(Derecho);
            primasNodeM.AppendChild(Recargo);
            primasNodeM.AppendChild(Impuesto);
            primasNodeM.AppendChild(PrimaTotal);
            primasNodeM.AppendChild(Comision);
            //Estructura Primas

            //Estructura DAtos Generales
            DatosGeneralesNode.AppendChild(FechaEmisionNode);
            DatosGeneralesNode.AppendChild(FechaInicioNode);
            DatosGeneralesNode.AppendChild(FechaTerminoNode);
            DatosGeneralesNode.AppendChild(MonedaNode);
            DatosGeneralesNode.AppendChild(AgenteNode);
            DatosGeneralesNode.AppendChild(FormaPagoNode);
            DatosGeneralesNode.AppendChild(TarifaValoresNode);
            DatosGeneralesNode.AppendChild(TarifaCuotasNode);
            DatosGeneralesNode.AppendChild(TarifaDerechosNode);
            DatosGeneralesNode.AppendChild(PlazoNode);
            DatosGeneralesNode.AppendChild(AgenciaNode);
            DatosGeneralesNode.AppendChild(ContratoNode);
            DatosGeneralesNode.AppendChild(PorcentajeDescuentoNode);
            ConsideracionesAdicionalesDGNode.AppendChild(tipoReglaNodeDG);
            ConsideracionesAdicionalesDGNode.AppendChild(ValorReglaNodeDG);
            DatosGeneralesNode.AppendChild(ConsideracionesAdicionalesDGNode);
            ConsideracionesAdicionalesDGNode2.AppendChild(tipoReglaNodeDG2);
            ConsideracionesAdicionalesDGNode2.AppendChild(ValorReglaNodeDG2);
            DatosGeneralesNode.AppendChild(ConsideracionesAdicionalesDGNode2);
            //Estructura Datos Generales

            //Estructura DatosVehiculo
            DatosVehiculoNode.AppendChild(ClaveAmisNode);
            DatosVehiculoNode.AppendChild(ModeloNode);
            DatosVehiculoNode.AppendChild(DescripcionVehiculoNode);
            DatosVehiculoNode.AppendChild(UsoNode);
            DatosVehiculoNode.AppendChild(ServicioNode);
            DatosVehiculoNode.AppendChild(PaqueteNode);
            DatosVehiculoNode.AppendChild(MotorNode);
            DatosVehiculoNode.AppendChild(SerieNode);
            DatosVehiculoNode.AppendChild(CoberturasNode);
            DatosVehiculoNode.AppendChild(CoberturasNode2);
            DatosVehiculoNode.AppendChild(CoberturasNode3);
            DatosVehiculoNode.AppendChild(CoberturasNode4);
            DatosVehiculoNode.AppendChild(CoberturasNode5);
            DatosVehiculoNode.AppendChild(CoberturasNode6);
            DatosVehiculoNode.AppendChild(CoberturasNode7);


            CoberturasNode.AppendChild(SumaAseguradaNode);
            CoberturasNode.AppendChild(TipoSumaNode);
            CoberturasNode.AppendChild(DeducibleNode);
            CoberturasNode.AppendChild(PrimaNode);

            CoberturasNode2.AppendChild(SumaAseguradaNode2);
            CoberturasNode2.AppendChild(TipoSumaNode2);
            CoberturasNode2.AppendChild(DeducibleNode2);
            CoberturasNode2.AppendChild(PrimaNode2);

            CoberturasNode3.AppendChild(SumaAseguradaNode3);
            CoberturasNode3.AppendChild(TipoSumaNode3);
            CoberturasNode3.AppendChild(DeducibleNode3);
            CoberturasNode3.AppendChild(PrimaNode3);

            CoberturasNode4.AppendChild(SumaAseguradaNode4);
            CoberturasNode4.AppendChild(TipoSumaNode4);
            CoberturasNode4.AppendChild(DeducibleNode4);
            CoberturasNode4.AppendChild(PrimaNode4);

            CoberturasNode5.AppendChild(SumaAseguradaNode5);
            CoberturasNode5.AppendChild(TipoSumaNode5);
            CoberturasNode5.AppendChild(DeducibleNode5);
            CoberturasNode5.AppendChild(PrimaNode5);

            CoberturasNode6.AppendChild(SumaAseguradaNode6);
            CoberturasNode6.AppendChild(TipoSumaNode6);
            CoberturasNode6.AppendChild(DeducibleNode6);
            CoberturasNode6.AppendChild(PrimaNode6);

            CoberturasNode7.AppendChild(SumaAseguradaNode7);
            CoberturasNode7.AppendChild(TipoSumaNode7);
            CoberturasNode7.AppendChild(DeducibleNode7);
            CoberturasNode7.AppendChild(PrimaNode7);
            //Estructura DatosVehiculo

            //Estructura ConsideracionesAdiconalesDA
            ConsideracionesAdicionalesDANode.AppendChild(TipoReglaNode);
            ConsideracionesAdicionalesDANode.AppendChild(ValorReglaNode);
            ConsideracionesAdicionalesDANode2.AppendChild(TipoReglaNode2);
            ConsideracionesAdicionalesDANode2.AppendChild(ValorReglaNode2);
            ConsideracionesAdicionalesDANode3.AppendChild(TipoReglaNode3);
            ConsideracionesAdicionalesDANode3.AppendChild(ValorReglaNode3);
            ConsideracionesAdicionalesDANode4.AppendChild(TipoReglaNode4);
            ConsideracionesAdicionalesDANode4.AppendChild(ValorReglaNode4);
            //Estructura ConsideracionesAdiconalesDA

            //Estructura datosAsegurado
            DatosAseguradoNode.AppendChild(NombreNode);
            DatosAseguradoNode.AppendChild(DireccionNode);
            DatosAseguradoNode.AppendChild(ColoniaNode);
            DatosAseguradoNode.AppendChild(PoblacionNode);
            DatosAseguradoNode.AppendChild(EstadoNode);
            DatosAseguradoNode.AppendChild(CpNode);
            DatosAseguradoNode.AppendChild(NoEmpleadoNode);
            DatosAseguradoNode.AppendChild(AgrupadorNode);
            DatosAseguradoNode.AppendChild(ConsideracionesAdicionalesDANode);
            DatosAseguradoNode.AppendChild(ConsideracionesAdicionalesDANode2);
            DatosAseguradoNode.AppendChild(ConsideracionesAdicionalesDANode3);
            DatosAseguradoNode.AppendChild(ConsideracionesAdicionalesDANode4);
            //Estructura datosAsegurado

            //estructura Movimiento node
            movimientoNode.AppendChild(DatosAseguradoNode);//Agregado del Nodo al cuerpo Movimiento
            movimientoNode.AppendChild(DatosVehiculoNode);
            movimientoNode.AppendChild(DatosGeneralesNode);
            movimientoNode.AppendChild(primasNodeM);
            movimientoNode.AppendChild(CodigoError);
            //estructura Movimiento node
            rootNode.AppendChild(movimientoNode);//Agregado del Nodo al cuerpo del XML

            //doc.Save("C:\\Users\\Eduardo Medina\\Desktop\\Xml2_0.xml");
            //xml = doc.DocumentElement.InnerXml;
            xml = doc.OuterXml;
            return xml;
        }
}
