﻿using Newtonsoft.Json;
using SuperObjeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de JsonRespuestaQualitas
/// </summary>
public class JsonRespuestaQualitas
{
    
        DatosJson datosJ = new DatosJson();

        //coberturas
        string descripcion = "";
        string sumaAsegurada = "";
        string deducible = "";
        public string JsonQualitasRespuesta(string response, Seguro seguro)
        {
            JsonF listCotizacion = JsonConvert.DeserializeObject<JsonF>(response);

            //aseguradora
            datosJ.Aseguradora = seguro.Aseguradora;
            //cliente
            Cliente cl = new Cliente();
            cl.TipoDepersona = "";
            cl.Nombre = "";
            cl.ApellidoPat = "";
            cl.ApellidoMat = "";
            cl.RFC = "";
            cl.FechaNacimiento = seguro.Cliente.FechaNacimiento;
            cl.Ocupacion = "";
            cl.Curp = "";
            //Direccion con cliente
            Direccion dR = new Direccion();
            dR.Calle = "";
            dR.NoExt = "";
            dR.NoInt = "";
            dR.Colonia = "";
            dR.CodPostal = seguro.Cliente.Direccion.CodPostal;
            dR.Poblacion = "";
            dR.Ciudad = "";
            dR.Ciudad = "";
            //cliente llenado
            cl.direccion = dR;
            cl.Edad = seguro.Cliente.Edad.ToString();
            cl.Genero = seguro.Cliente.Genero;
            cl.Telefono = seguro.Cliente.Telefono.ToString();
            cl.Email = "";
            //Vehiculo
            Vehiculo vH = new Vehiculo();
            vH.Uso = seguro.Vehiculo.Uso;
            vH.Marca = seguro.Vehiculo.Marca;
            vH.Modelo = seguro.Vehiculo.Modelo.ToString();
            vH.NoMotor = "";
            vH.NoSerie = "";
            vH.NoPlacas = "";
            vH.Descripcion = seguro.Vehiculo.Descripcion;
            vH.CodMarca = "";
            vH.CodDescripcion = "";
            vH.CodUso = "";
            vH.Clave = seguro.Vehiculo.Clave;
            vH.Servicio = seguro.Vehiculo.Servicio;
            //Coberturas
            CoberturasFinal cFinal = new CoberturasFinal();
            for (int i = 0; i <= 7; i++)

            {
                var x = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].NoCobertura;
                switch (listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].NoCobertura)
                {
                    case 1:
                        descripcion = "Daños Materiales";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].Deducible;
                        cFinal.DanosMateriales = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;
                    case 3:
                        descripcion = "Robo total";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        cFinal.RoboTotal = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;
                    case 4:
                        descripcion = "Responsabilidad Civil";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        cFinal.RC = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;
                    case 5:
                        descripcion = "Gastos Médicos";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        cFinal.GastosMedicosOcupantes = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;
                    case 6:
                        descripcion = "Accidentes Ocupantes ";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        cFinal.AO = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;
                    case 7:
                        descripcion = "Gastos Legales";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        cFinal.GL = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;
                    case 14:
                        descripcion = "Asistencia Vial";
                        sumaAsegurada = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        deducible = listCotizacion.Movimientos.movimiento.DatosVehiculo.Coberturas[i].SumaAsegurada.ToString();
                        cFinal.AsitenciaCompleta = "-N" + descripcion + "-S" + sumaAsegurada + "-D" + deducible;
                        break;

                }
            }
            cFinal.DanosMaterialesPP = "";
            cFinal.RCPersonas = "";
            cFinal.RCBienes = "";
            cFinal.RCFamiliar = "";
            cFinal.RCExtranjero = "";
            cFinal.RCPExtra = "";
            cFinal.GastosMedicosEvento = "";
            cFinal.Cristales = "";
            //cotizacion
            Cotizacion cT = new Cotizacion();
            cT.PrimaTotal = listCotizacion.Movimientos.movimiento.Primas.PrimaTotal.ToString();
            cT.PrimaNeta = listCotizacion.Movimientos.movimiento.Primas.PrimaNeta.ToString();
            cT.Derechos = listCotizacion.Movimientos.movimiento.Primas.Derecho.ToString();
            cT.Impuesto = listCotizacion.Movimientos.movimiento.Primas.Impuesto.ToString();
            cT.Recargos = "";
            cT.PrimerPago = listCotizacion.Movimientos.movimiento.Primas.PrimaTotal.ToString();
            cT.PagosSubsecuentes = "";
            cT.IDCotizacion = "";
            cT.CotID = "";
            cT.VerID = "";
            cT.CotIncID = "";
            cT.VerIncID = "";
            if (seguro.CodigoError == "")
            {
                cT.Resultado = true;
            }
            else
            {
                cT.Resultado = false;
            }
            //Emision
            Emision eM = new Emision();
            eM.PrimaTotal = "";
            eM.PrimaNeta = "";
            eM.Derechos = "";
            eM.Impuesto = "";
            eM.Recargos = "";
            eM.PrimerPago = "";
            eM.PagosSubsecuentes = "";
            eM.IDCotizacion = listCotizacion.Movimientos.movimiento.NoCotizacion;
            eM.Terminal = "";
            eM.Documento = "";
            eM.Poliza = "";
            eM.Resultado = "";
            //Pago
            Pago pG = new Pago();
            pG.medioPago = "";
            pG.nombreTarjeta = "";
            pG.banco = "";
            pG.noTarjeta = "";
            pG.mesExp = "";
            pG.anioExp = "";
            pG.codigoSeguridad = "";
            pG.carrier = "";

            datosJ.cliente = cl;
            datosJ.vehiculo = vH;
            datosJ.Coberturas = cFinal;
            datosJ.cotizacion = cT; datosJ.Paquete = seguro.Paquete;
            datosJ.Descuento = seguro.Descuento.ToString();
            datosJ.PeriodicidadDePago = 0;
            datosJ.emision = eM;
            datosJ.pago = pG;
            datosJ.CodigoError = seguro.CodigoError;
            datosJ.urlRedireccion = seguro.urlRedireccion;

            string responde = JsonConvert.SerializeObject(datosJ);
            return responde;
        }
            public partial class DatosJson
    {
        [JsonProperty("aseguradora")]
        public string Aseguradora { get; set; }
        public Cliente cliente { get; set; }
        public Vehiculo vehiculo { get; set; }
        public CoberturasFinal Coberturas { get; set; }

        [JsonProperty("paquete")]
        public string Paquete { get; set; }

        [JsonProperty("descuento")]
        public string Descuento { get; set; }

        [JsonProperty("PeriodicidadDePago")]
        public long PeriodicidadDePago { get; set; }
        public Cotizacion cotizacion { get; set; }
        public Emision emision { get; set; }
        public Pago pago { get; set; }
        public string CodigoError { get; set; }
        public string urlRedireccion { get; set; }

    }
    public partial class Cliente
    {
        public string TipoDepersona { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPat { get; set; }
        public string ApellidoMat { get; set; }
        public string RFC { get; set; }
        public string FechaNacimiento { get; set; }
        public string Ocupacion { get; set; }
        public string Curp { get; set; }
        public Direccion direccion { get; set; }
        public string Edad { get; set; }
        public string Genero { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }

    }
    public partial class Direccion
    {
        public string Calle { get; set; }
        public string NoExt { get; set; }
        public string NoInt { get; set; }
        public string Colonia { get; set; }
        public string CodPostal { get; set; }
        public string Poblacion { get; set; }
        public string Ciudad { get; set; }
        public string Pais { get; set; }
    }

    public partial class Vehiculo
    {
        public string Descripcion { get; set; }
        public string Uso { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string NoPlacas { get; set; }
        public string NoMotor { get; set; }
        public string NoSerie { get; set; }
        public string CodMarca { get; set; }
        public string CodDescripcion { get; set; }
        public string CodUso { get; set; }
        public string Clave { get; set; }
        public string Servicio { get; set; }
        public string SubMarca { get; set; }
    }

    public partial class CoberturasFinal
    {
        public string SPT { get; set; }
        public string DanosMateriales { get; set; }
        public string AO { get; set; }
        public string GL { get; set; }
        public string DanosMaterialesPP { get; set; }
        public string RoboTotal { get; set; }
        public string RCBienes { get; set; }
        public string DJ { get; set; }
        public string RCPersonas { get; set; }
        public string RCC { get; set; }
        public string RC { get; set; }
        public string RCFamiliar { get; set; }
        public string RCExtension { get; set; }
        public string RCExtranjero { get; set; }
        public string RCPExtra { get; set; }
        public string AsitenciaCompleta { get; set; }
        public string DefensaJuridica { get; set; }
        public string GastosMedicosOcupantes { get; set; }
        public string MuerteAccidental { get; set; }
        public string GastosMedicosEvento { get; set; }
        public string Cristales { get; set; }
    }

    public partial class Cotizacion
    {
        public string PrimaTotal { get; set; }
        public string PrimaNeta { get; set; }
        public string Derechos { get; set; }
        public string Impuesto { get; set; }
        public string Recargos { get; set; }
        public string PrimerPago { get; set; }
        public string PagosSubsecuentes { get; set; }
        public string IDCotizacion { get; set; }
        public string CotID { get; set; }
        public string VerID { get; set; }
        public string CotIncID { get; set; }
        public string VerIncID { get; set; }
        public bool Resultado { get; set; }
    }

    public partial class Emision
    {
        public string PrimaTotal { get; set; }
        public string PrimaNeta { get; set; }
        public string Derechos { get; set; }
        public string Impuesto { get; set; }
        public string Recargos { get; set; }
        public string PrimerPago { get; set; }
        public string PagosSubsecuentes { get; set; }
        public string IDCotizacion { get; set; }
        public string Terminal { get; set; }
        public string Documento { get; set; }
        public string Poliza { get; set; }
        public string Resultado { get; set; }
    }

    public partial class Pago
    {
        public string medioPago { get; set; }
        public string nombreTarjeta { get; set; }
        public string banco { get; set; }
        public string noTarjeta { get; set; }
        public string mesExp { get; set; }
        public string anioExp { get; set; }
        public string codigoSeguridad { get; set; }
        public string noClabe { get; set; }
        public string carrier { get; set; }
    }
    //-----------Response
    public partial class JsonF
    {

        [JsonProperty("Movimientos")]
        public Movimientos Movimientos { get; set; }
    }

    public partial class Movimientos
    {
        public Movimiento movimiento;
    }

    public partial class Movimiento
    {
        [JsonProperty("@TipoMovimiento")]
        public long TipoMovimiento { get; set; }

        [JsonProperty("@NoNegocio")]
        public long NoNegocio { get; set; }

        [JsonProperty("@NoPoliza")]
        public string NoPoliza { get; set; }

        [JsonProperty("@NoCotizacion")]
        public string NoCotizacion { get; set; }

        [JsonProperty("@NoEndoso")]
        public string NoEndoso { get; set; }

        [JsonProperty("@TipoEndoso")]
        public long TipoEndoso { get; set; }

        [JsonProperty("@NoOTra")]
        public string NoOTra { get; set; }

        [JsonProperty("DatosAsegurado")]
        public DatosAsegurado DatosAsegurado { get; set; }

        [JsonProperty("DatosVehiculo")]
        public DatosVehiculo DatosVehiculo { get; set; }

        [JsonProperty("DatosGenerales")]
        public DatosGenerales DatosGenerales { get; set; }

        [JsonProperty("Primas")]
        public Primas Primas { get; set; }

        [JsonProperty("Recibos")]
        public Recibos Recibos { get; set; }

        [JsonProperty("CodigoError")]
        public object CodigoError { get; set; }
    }

    public partial class DatosAsegurado
    {
        [JsonProperty("@NoAsegurado")]
        public string NoAsegurado { get; set; }

        [JsonProperty("Nombre")]
        public object Nombre { get; set; }

        [JsonProperty("Direccion")]
        public object Direccion { get; set; }

        [JsonProperty("Colonia")]
        public object Colonia { get; set; }

        [JsonProperty("Poblacion")]
        public object Poblacion { get; set; }

        [JsonProperty("Estado")]
        public long Estado { get; set; }

        [JsonProperty("CodigoPostal")]
        public long CodigoPostal { get; set; }

        [JsonProperty("NoEmpleado")]
        public object NoEmpleado { get; set; }

        [JsonProperty("Agrupador")]
        public object Agrupador { get; set; }

        [JsonProperty("ConsideracionesAdicionalesDA")]
        public ConsideracionesAdicionalesD[] ConsideracionesAdicionalesDa { get; set; }
    }

    public partial class ConsideracionesAdicionalesD
    {
        [JsonProperty("@NoConsideracion")]
        public long NoConsideracion { get; set; }

        [JsonProperty("TipoRegla")]
        public long? TipoRegla { get; set; }

        [JsonProperty("ValorRegla")]
        public long? ValorRegla { get; set; }
    }

    public partial class DatosGenerales
    {
        [JsonProperty("FechaEmision")]
        public DateTimeOffset FechaEmision { get; set; }

        [JsonProperty("FechaInicio")]
        public DateTimeOffset FechaInicio { get; set; }

        [JsonProperty("FechaTermino")]
        public DateTimeOffset FechaTermino { get; set; }

        [JsonProperty("Moneda")]
        public long Moneda { get; set; }

        [JsonProperty("Agente")]
        public long Agente { get; set; }

        [JsonProperty("FormaPago")]
        public string FormaPago { get; set; }

        [JsonProperty("TarifaValores")]
        public long TarifaValores { get; set; }

        [JsonProperty("TarifaCuotas")]
        public long TarifaCuotas { get; set; }

        [JsonProperty("TarifaDerechos")]
        public long TarifaDerechos { get; set; }

        [JsonProperty("Plazo")]
        public object Plazo { get; set; }

        [JsonProperty("Agencia")]
        public object Agencia { get; set; }

        [JsonProperty("Contrato")]
        public object Contrato { get; set; }

        [JsonProperty("PorcentajeDescuento")]
        public long PorcentajeDescuento { get; set; }

        [JsonProperty("ConsideracionesAdicionalesDG")]
        public ConsideracionesAdicionalesD[] ConsideracionesAdicionalesDg { get; set; }
    }

    public partial class DatosVehiculo
    {
        [JsonProperty("@NoInciso")]
        public string NoInciso { get; set; }

        [JsonProperty("ClaveAmis")]
        public string ClaveAmis { get; set; }

        [JsonProperty("Modelo")]
        public long Modelo { get; set; }

        [JsonProperty("DescripcionVehiculo")]
        public string DescripcionVehiculo { get; set; }

        [JsonProperty("Uso")]
        public long Uso { get; set; }

        [JsonProperty("Servicio")]
        public long Servicio { get; set; }

        [JsonProperty("Paquete")]
        public long Paquete { get; set; }

        [JsonProperty("Motor")]
        public object Motor { get; set; }

        [JsonProperty("Serie")]
        public object Serie { get; set; }

        [JsonProperty("Coberturas")]
        public Cobertura[] Coberturas { get; set; }

        [JsonProperty("ConsideracionesAdicionalesDV")]
        public ConsideracionesAdicionalesD ConsideracionesAdicionalesDv { get; set; }
    }

    public partial class Cobertura
    {
        [JsonProperty("@NoCobertura")]
        public long NoCobertura { get; set; }

        [JsonProperty("SumaAsegurada")]
        public long SumaAsegurada { get; set; }

        [JsonProperty("TipoSuma")]
        public long TipoSuma { get; set; }

        [JsonProperty("Deducible")]
        public string Deducible { get; set; }

        [JsonProperty("Prima")]
        public string Prima { get; set; }
    }

    public partial class Primas
    {
        [JsonProperty("PrimaNeta")]
        public string PrimaNeta { get; set; }

        [JsonProperty("Derecho")]
        public long Derecho { get; set; }

        [JsonProperty("Recargo")]
        public string Recargo { get; set; }

        [JsonProperty("Impuesto")]
        public string Impuesto { get; set; }

        [JsonProperty("PrimaTotal")]
        public string PrimaTotal { get; set; }

        [JsonProperty("Comision")]
        public long Comision { get; set; }
    }

    public partial class Recibos
    {
        [JsonProperty("@NoRecibo")]
        public long NoRecibo { get; set; }

        [JsonProperty("FechaInicio")]
        public DateTimeOffset FechaInicio { get; set; }

        [JsonProperty("FechaTermino")]
        public DateTimeOffset FechaTermino { get; set; }

        [JsonProperty("PrimaNeta")]
        public string PrimaNeta { get; set; }

        [JsonProperty("Derecho")]
        public long Derecho { get; set; }

        [JsonProperty("Recargo")]
        public string Recargo { get; set; }

        [JsonProperty("Impuesto")]
        public string Impuesto { get; set; }

        [JsonProperty("PrimaTotal")]
        public string PrimaTotal { get; set; }

        [JsonProperty("Comision")]
        public string Comision { get; set; }
    }

    public partial class Xml
    {
        [JsonProperty("@version")]
        public string Version { get; set; }

        [JsonProperty("@encoding")]
        public string Encoding { get; set; }
    }

}
    