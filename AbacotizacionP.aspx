﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AbacotizacionP.aspx.cs" Inherits="AbacotizacionP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
          <div>
            <p hidden="hidden">No le muevas prro >:v</p>
            <asp:DropDownList ID="TipoVivienda" runat="server">
                <asp:ListItem Text="Tipo Vivienda"></asp:ListItem>
                <asp:ListItem Text="CASA"></asp:ListItem>
                <asp:ListItem Text="DEPARTAMENTO"></asp:ListItem>
            </asp:DropDownList>
             <asp:DropDownList ID="PoA" runat="server">
                <asp:ListItem Text="¿Eres propietario o arrendador?"></asp:ListItem>
                <asp:ListItem Text="PROPIETARIO"></asp:ListItem>
                <asp:ListItem Text="ARRENDADOR"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="NoPisos" runat="server">
                <asp:ListItem Text="No. de pisos"></asp:ListItem>
            </asp:DropDownList>
            <br /> <br />
            <input type="text" runat="server" id="txtNombre" placeholder="Nombre(s)"/> 
            <input type="text" runat="server" id="txtApaterno" placeholder="Apellido Paterno"/>
            <input type="text" runat="server" id="txtAmaterno" placeholder="Apellido Materno"/>
            <br /> <br />
            <input type="number" runat="server" id="txtTelefono" placeholder="telefono"/> 
            <input type="email" runat="server" id="txtCorreo" placeholder="correo electronico"/>
             <asp:DropDownList ID="Estados" runat="server" OnSelectedIndexChanged="Estados_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="Seleccione un Estado" Value="0"></asp:ListItem>
            </asp:DropDownList>
            <br /> <br />
             <asp:DropDownList ID="Municipio" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Municipio_SelectedIndexChanged">
                <asp:ListItem Text="Seleccione un Municipio"></asp:ListItem>
            </asp:DropDownList>
             <asp:DropDownList ID="Poblaciones" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Poblaciones_SelectedIndexChanged">
                <asp:ListItem Text="Seleccione una Poblacion "></asp:ListItem>
            </asp:DropDownList>
             <asp:DropDownList ID="ColoniasD" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ColoniasD_SelectedIndexChanged">
                <asp:ListItem Text="Seleccione una Colonia "></asp:ListItem>
            </asp:DropDownList>
            <br /> <br />
            <asp:Button runat="server" Text="Cotizar" id="btnCotizar" OnClick="btnCotizar_Click"/>
            <br /><br />
            <asp:Label runat="server" ID="lblJson"></asp:Label>
        </div>
    </form>
</body>
</html>
