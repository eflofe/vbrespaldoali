﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AbacotizacionP : System.Web.UI.Page
{
    DatosJson dj = new DatosJson();
    static List<long> ids = new List<long>();
    static List<long> idsMuni = new List<long>();
    static List<long> idsCol = new List<long>();
    static long idM;
    static long idP;
    static long idC;
    static long cp;
    ConsultaDireccion consultaDireccion = new ConsultaDireccion();

    protected void Page_Load(object sender, EventArgs e)
    {
        FillPisos();
        FillEstados();
    }
    private void FillPisos()
    {
        for (int i = 1; i <= 40; i++)
        {
            NoPisos.Items.Add(i.ToString());
        }
    }

    private void FillEstados()
    {
        List<string> listaEstados = new List<string>();
        listaEstados.Add("AGUASCALIENTES");
        listaEstados.Add("BAJA CALIFORNIA");
        listaEstados.Add("BAJA CALIFORNIA SUR");
        listaEstados.Add("CAMPECHE");
        listaEstados.Add("CHIHUAHUA");
        listaEstados.Add("CHIAPAS");
        listaEstados.Add("COAHUILA");
        listaEstados.Add("COLIMA");
        listaEstados.Add("CIUDAD DE MEXICO");
        listaEstados.Add("DURANGO");
        listaEstados.Add("GUERRERO");
        listaEstados.Add("GUANAJUATO");
        listaEstados.Add("HIDALGO");
        listaEstados.Add("JALISCO");
        listaEstados.Add("MEXICO");
        listaEstados.Add("MICHOACAN");
        listaEstados.Add("MORELOS");
        listaEstados.Add("NAYARIT");
        listaEstados.Add("NUEVO LEON");
        listaEstados.Add("OAXACA");
        listaEstados.Add("PUEBLA");
        listaEstados.Add("QUERETARO");
        listaEstados.Add("QUINTANA ROO");
        listaEstados.Add("SINALOA");
        listaEstados.Add("SAN LUIS POTOSI");
        listaEstados.Add("SONORA");
        listaEstados.Add("TABASCO");
        listaEstados.Add("TAMAULIPAS");
        listaEstados.Add("TLAXCALA");
        listaEstados.Add("VERACRUZ");
        listaEstados.Add("YUCATAN");
        listaEstados.Add("ZACATECAS");

        foreach (var item in listaEstados)
        {
            Estados.Items.Add(item);
        }

    }

    public void fillMuni(int idEdo)
    {

        string jsonMunicipios = consultaDireccion.getMunicipio(Estados.SelectedIndex);
        List<Municipios> municipios = JsonConvert.DeserializeObject<List<Municipios>>(jsonMunicipios);
        ids.Clear();
        foreach (var item in municipios)
        {
            Municipio.Items.Add(item.Desc);

            ids.Add(item.Id);
        }

    }

    public void FillPoblaciones()
    {
        long p = Municipio.SelectedIndex;
        idM = ids.ElementAt(Convert.ToInt32(p) - 1);
        Response.Write(idM);
        string JsonPoblaciones = consultaDireccion.getPoblaciones(Convert.ToInt32(idM));
        List<Poblaciones> poblaciones = JsonConvert.DeserializeObject<List<Poblaciones>>(JsonPoblaciones);
        foreach (var item in poblaciones)
        {
            Poblaciones.Items.Add(item.Desc);
            idsMuni.Add(item.Id);
        }
    }

    public void FillColonias()
    {
        long pobSel = Poblaciones.SelectedIndex;
        idP = idsMuni.ElementAt(Convert.ToInt32(pobSel) - 1);
        Response.Write(idP);
        string JsonColonias = consultaDireccion.getColonias(Estados.SelectedIndex, Convert.ToInt32(idM), Convert.ToInt32(idP));
        List<Colonias> colonias = JsonConvert.DeserializeObject<List<Colonias>>(JsonColonias);
        foreach (var item in colonias)
        {
            ColoniasD.Items.Add(item.ColoniaDsc);
            idsCol.Add(item.ColoniaId);
        }
    }

    protected void Estados_SelectedIndexChanged(object sender, EventArgs e)
    {
        Municipio.Items.Clear();
        Municipio.Items.Add("Seleccione un Municipio");
        Poblaciones.Items.Clear();
        Poblaciones.Items.Add("Seleccione una Poblacion");
        ColoniasD.Items.Clear();
        ColoniasD.Items.Add("Seleccione una Poblacion");
        fillMuni(Estados.SelectedIndex);
    }

    protected void Municipio_SelectedIndexChanged(object sender, EventArgs e)
    {
        Poblaciones.Items.Clear();
        Poblaciones.Items.Add("Seleccione una Poblacion");
        FillPoblaciones();
    }

    protected void Poblaciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        ColoniasD.Items.Clear();
        ColoniasD.Items.Add("Seleccione una Colonia");
        FillColonias();
    }

    protected void btnCotizar_Click(object sender, EventArgs e)
    {
        dj.tipoVivienda = TipoVivienda.SelectedValue.ToString();
        dj.PrOAr = PoA.SelectedValue.ToString();
        dj.noPisos = NoPisos.SelectedValue.ToString();
        dj.nombre = txtNombre.Value;
        dj.apaterno = txtApaterno.Value;
        dj.amaterno = txtAmaterno.Value;
        dj.telefono = txtTelefono.Value;
        dj.Correo = txtCorreo.Value;
        dj.Estado = Estados.SelectedIndex;
        dj.Municipio = Convert.ToInt32(idM);
        dj.Poblacion = Convert.ToInt32(idP);
        dj.Colonia = Convert.ToInt32(idC);
        dj.CP = consultaDireccion.GetCP(Estados.SelectedIndex, Convert.ToInt32(idM), Convert.ToInt32(idP));
        string json = JsonConvert.SerializeObject(dj);
        lblJson.Text = json;
    }

    protected void ColoniasD_SelectedIndexChanged(object sender, EventArgs e)
    {
        long colSel = ColoniasD.SelectedIndex;
        idC = idsCol.ElementAt(Convert.ToInt32(colSel) - 1);
    }
}

public class DatosJson
{
    public string tipoVivienda { get; set; }
    public string PrOAr { get; set; }
    public string noPisos { get; set; }
    public string nombre { get; set; }
    public string apaterno { get; set; }
    public string amaterno { get; set; }
    public string telefono { get; set; }
    public string Correo { get; set; }
    public int Estado { get; set; }
    public int Municipio { get; set; }
    public int Poblacion { get; set; }
    public int Colonia { get; set; }
    public string CP { get; set; }
    public string codigoError { get; set; }
}
public class Municipios
{
    public long Id { get; set; }
    public string Desc { get; set; }
}
public class Poblaciones
{
    public long Id { get; set; }
    public string Desc { get; set; }
}
public class Colonias
{
    public long ColoniaId { get; set; }
    public string ColoniaDsc { get; set; }
    public double IvaPct { get; set; }
}
