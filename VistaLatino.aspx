﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VistaLatino.aspx.cs" Inherits="VistaLatino" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <style>
        html, body{
  height: 100%;
  
}

.contenedor{
  height: 49%;
  
  margin: 1% 1%;
}
    </style>
    
    <form id="form1" runat="server">
          <div class="contenedor">
            <asp:DropDownList runat="server" ID="DropMarca" OnSelectedIndexChanged="DropMarca_SelectedIndexChanged" AutoPostBack="true" >
                <asp:ListItem Value="0">Seleccione una Marca</asp:ListItem>
                <asp:ListItem Value="1">'NINGUNO'</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList runat ="server" ID="DropSubMarca" OnSelectedIndexChanged="DropSubMarca_SelectedIndexChanged" AutoPostBack  ="true">
               <asp:ListItem Value="0">Selecicone una SubMarca</asp:ListItem>
               <asp:ListItem value="1">'NINGUNO'</asp:ListItem>
            </asp:DropDownList>
              <asp:DropDownList runat="server" ID="DropModelo" AutoPostBack="true" OnSelectedIndexChanged="DropModelo_SelectedIndexChanged" >
                <asp:ListItem Value="0">Seleccione un Modelo</asp:ListItem>
                <asp:ListItem Value="1">'NINGUNO'</asp:ListItem>
            </asp:DropDownList>
              <asp:DropDownList runat="server" ID="DropDescripcion" AutoPostBack="true" OnSelectedIndexChanged="DropDescripcion_SelectedIndexChanged" >
                <asp:ListItem Value="0">Seleccione una descripcion</asp:ListItem>
                <asp:ListItem Value="1">'NINGUNO'</asp:ListItem>
            </asp:DropDownList>

             <asp:DropDownList runat="server" ID="DropDownDescuento" OnSelectedIndexChanged="DropEdad_SelectedIndexChanged" AutoPostBack="true" >
                <asp:ListItem Value="">Selecciona un descuento</asp:ListItem>
                 <asp:ListItem Value="0">Sin descuento</asp:ListItem>
                 <asp:ListItem Value="5">5%</asp:ListItem>
                 <asp:ListItem Value="10">10%</asp:ListItem>
                 <asp:ListItem Value="15">15%</asp:ListItem>
                 <asp:ListItem Value="20">20%</asp:ListItem>
                 <asp:ListItem Value="25">25%</asp:ListItem>
                 <asp:ListItem Value="30">30%</asp:ListItem>
                 <asp:ListItem Value="35">35%</asp:ListItem>
                 <asp:ListItem Value="40">40%</asp:ListItem>
                 <asp:ListItem Value="45">45%</asp:ListItem>
            </asp:DropDownList>

            <asp:DropDownList runat="server" ID="DropEdad" OnSelectedIndexChanged="DropEdad_SelectedIndexChanged" AutoPostBack="true" >
                <asp:ListItem Value="0">Selecciona tu edad</asp:ListItem>
            </asp:DropDownList>

            <asp:DropDownList runat="server" ID="DropGenero" OnSelectedIndexChanged="DropGenero_SelectedIndexChanged" AutoPostBack="true" >
                <asp:ListItem Value="Ninguno">Selecciona tu genero</asp:ListItem>
                <asp:ListItem Value="Masculino">Masculino</asp:ListItem>
                <asp:ListItem Value="Femenino">Femenino</asp:ListItem>
            </asp:DropDownList>
            
           
            <asp:Label runat="server" ID="LblCp" Visible="true">Código postal</asp:Label>
            <asp:TextBox runat="server" ID="TxtCp"></asp:TextBox>

            <asp:Label runat="server" ID="lblNombre" Visible="true">Nombre</asp:Label>
            <asp:TextBox runat="server" ID="TxtNombre"></asp:TextBox>
            
            <asp:Label runat="server" ID="lblEmail" Visible="true">EMAIL</asp:Label>
            <asp:TextBox runat="server" ID="TxtEmail"></asp:TextBox>
            
            <asp:Label runat="server" ID="LblTelefono" Visible="true" >TELEFONO</asp:Label>
            <asp:TextBox runat="server" ID="TxtTelefono"></asp:TextBox>
            
            
            <asp:Label runat="server" ID="lblMarcaSelected" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblSubMarca" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblModelo" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblaño" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="Lbldescripcion" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblIndex" Visible="false"></asp:Label>

            <asp:Button runat="server" text="Crear solicitud" OnClick="BtnCrearJson" ID="BtnCotizar"  />
            <br />
            <br />
            <asp:Label runat="server" ID="lblJsno" Visible="true"></asp:Label>
        </div>
    </form>
</body>
</html>
